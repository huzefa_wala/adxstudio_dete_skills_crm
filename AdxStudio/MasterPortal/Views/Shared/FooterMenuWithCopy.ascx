﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<footer class="well">
	<div class="container">
		<% var footerLinks = Html.WebLinkSet("Footer"); %>
		<% if (footerLinks != null) { %>
			<div class="row">
				<div class="span4">
					<%: Html.HtmlAttribute(footerLinks.Copy) %>
				</div>
				<div class="span8 <%: footerLinks.Editable ? "xrm-entity xrm-editable-adx_weblinkset" : string.Empty %>" data-weblinks-maxdepth="2">
					<% if (footerLinks.WebLinks.Any()) { %>
						<% var rowIndex = 0; %>
						<% while (true) { %>
							<%
								var rowLinks = footerLinks.WebLinks.Skip(rowIndex * 4).Take(4).ToArray();
								if (!rowLinks.Any()) break;
								rowIndex++;
							%>
							<ul class="row">
								<% foreach (var link in rowLinks) { %>
									<li class="span2">
										<h4><%: link.Url == null ? Html.AttributeLiteral(link.Name) : Html.WebLink(link) %></h4>
										<ul class="nav">
											<% if (link.DisplayPageChildLinks) { %>
												<% foreach (var childNode in Html.SiteMapChildNodes(link.Url)) { %>
													<li>
														<a href="<%: childNode.Url %>"><%: childNode.Title %></a>
													</li>
												<% } %>
											<% } else { %>
												<% foreach (var childLink in link.WebLinks) { %>
													<%: Html.WebLinkListItem(childLink, maximumWebLinkChildDepth: 0) %>
												<% } %>
											<% } %>
										</ul>
									</li>
								<% } %>
							</ul>
						<% } %>
					<% } else { %>
						<ul class="row"></ul>
					<% } %>
					<% if (footerLinks.Editable) { %>
						<%: Html.WebLinkSetEditingMetadata(footerLinks) %>
					<% } %>
				</div>
			</div>
		<% } %>
		<%: Html.HtmlSnippet("Footer") %>
	</div>
</footer>