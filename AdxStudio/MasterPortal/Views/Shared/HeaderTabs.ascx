﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="Adxstudio.Xrm.Collections.Generic" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Microsoft.Xrm.Portal.IdentityModel.Configuration" %>

<div class="navbar navbar-static-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<div class="pull-left hidden-desktop">
				<%: Html.HtmlSnippet("Mobile Header") %>
			</div>
			<div class="nav-collapse collapse">
				<div class="pull-left">
					<%: Html.HtmlSnippet("Navbar Left") %>
				</div>
				<%: Html.WebLinksNavBar("Primary Navigation", "hidden-desktop weblinks pull-left", currentSiteMapNodeCssClass: "active", ancestorSiteMapNodeCssClass: "active") %>
				<ul class="nav pull-left">
					<% if (Request.IsAuthenticated) { %>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-user"></i> <%: Html.AttributeLiteral(Html.PortalUser(), "fullname") %> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<% if (Html.BooleanSetting("Header/ShowAllProfileNavigationLinks").GetValueOrDefault(true)) { %>
									<% var profileNavigation = Html.WebLinkSet("Profile Navigation"); %>
									<% if (profileNavigation != null) { %>
										<% foreach (var webLink in profileNavigation.WebLinks) { %>
											<%: Html.WebLinkListItem(webLink, false, false) %>
										<% } %>
									<% } %>
								<% } else { %>
									<li><a href="<%: Html.SiteMarkerUrl("Profile") %>"><%: Html.SnippetLiteral("Profile Link Text", "Profile") %></a></li>
								<% } %>
								<li class="divider"></li>
								<li><a href="<%: Url.SignOutUrl(Request.RawUrl) %>"><%: Html.SnippetLiteral("links/logout", "Sign Out") %></a></li>
							</ul>
						</li>
					<% } else { %>
						<li>
							<% Html.RenderPartial("SignInLink"); %>
						</li>
					<% } %>
				</ul>
				<ul class="nav pull-right">
					<% var relatedWebsites = Html.RelatedWebsites(linkTitleSiteSettingName:"Site Name"); %>
					<% if (relatedWebsites.Any) { %>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-globe"></i> <%: relatedWebsites.Current.Title %> <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<% foreach (var relatedWebsiteLink in relatedWebsites.Links) { %>
									<li><a href="<%: relatedWebsiteLink.Url %>"><%: relatedWebsiteLink.Title %></a></li>
								<% } %>
							</ul>
						</li>
					<% } %>
					<li class="shopping-cart-status" data-href="<%: Url.Action("Status", "ShoppingCart", new {area = "Commerce", __portalScopeId__ = Html.Website().EntityReference.Id}) %>">
						<a href="<%: Html.SiteMarkerUrl("Shopping Cart") %>">
							<i class="icon-shopping-cart"></i>
							<%: Html.SnippetLiteral("Shopping Cart Status Link Text", "Cart") %>
							<span class="count">(<span class="value"></span>)</span>
						</a>
					</li>
				</ul>
				<form class="navbar-search form-search pull-right" method="GET" action="<%: Html.SiteMarkerUrl("Search") %>">
					<% var searchFilterOptions = Html.SearchFilterOptions().ToArray(); %>
					<div class="input-append <%= searchFilterOptions.Any() ? "input-prepend" : string.Empty %>">
						<% if (searchFilterOptions.Any()) { %>
							<div class="btn-group btn-select" data-target="#filter" data-focus="#q">
								<a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
									<span class="selected"><%: Html.SnippetLiteral("Default Search Filter Text", "All") %></span>
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a data-value=""><%: Html.SnippetLiteral("Default Search Filter Text", "All") %></a>
									</li>
									<% foreach (var option in searchFilterOptions) { %>
										<li>
											<a data-value="<%: option.Value %>"><%: option.Key %></a>
										</li>
									<% } %>
								</ul>
							</div>
							<select id="filter" name="filter" class="input-medium btn-select">
								<% if (string.IsNullOrEmpty(Request["filter"])) { %>
									<option value="" selected="selected"><%: Html.SnippetLiteral("Default Search Filter Text", "All") %></option>
								<% } else {%>
									<option value=""><%: Html.SnippetLiteral("Default Search Filter Text", "All") %></option>
								<% } %>
								<% foreach (var option in searchFilterOptions) { %>
									<% if (Request["filter"] == option.Value) { %>
										<option value="<%: option.Value %>" selected="selected"><%: option.Key %></option>
									<% } else {%>
										<option value="<%: option.Value %>"><%: option.Key %></option>
									<% } %>
								<% } %>
							</select>
						<% } %>
						<input type="text" id="q" name="q" class="search-query" placeholder="Search" value="<%: Request["q"] %>">
						<button type="submit" class="btn"><i class="icon-search"></i></button>
					</div>
				</form>
			</div><!--/.nav-collapse -->
		</div>
	</div>
</div>
<div class="masthead tabs well visible-desktop">
	<div class="container">
		<%: Html.HtmlSnippet("Header") %>
		<%: Html.WebLinksDropdowns("Primary Navigation", "weblinks", "nav nav-tabs", currentSiteMapNodeCssClass: "active", ancestorSiteMapNodeCssClass: "active") %>
	</div>
</div>
