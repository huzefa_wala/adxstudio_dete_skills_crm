﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<% var signInUrl = Url.SignInUrl((string) ViewBag.ReturnUrl); %>
<% if (!string.IsNullOrWhiteSpace(signInUrl)) { %>
	<% var isPrimary = ((bool?) ViewBag.IsPrimary).GetValueOrDefault(); %>
	<a class="<%: isPrimary ? "btn btn-primary" : string.Empty %>" href="<%: signInUrl %>">
		<i class="icon-lock <%: isPrimary ? "icon-white" : string.Empty %>"></i>
		<%: Html.SnippetLiteral("links/login", "Sign In") %>
	</a>
<% } %>