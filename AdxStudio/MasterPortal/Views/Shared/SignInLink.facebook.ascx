﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<% var signInUrl = Url.FacebookSignInUrl(); %>
<% if (!string.IsNullOrWhiteSpace(signInUrl)) { %>
	<a class="facebook-signin" href="<%: signInUrl %>">
		<i class="icon-lock"></i>
		<%: Html.SnippetLiteral("links/login", "Sign In") %>
	</a>
<% } %>