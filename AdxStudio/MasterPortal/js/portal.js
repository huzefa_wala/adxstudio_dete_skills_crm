﻿var portal = portal || {};

portal.convertAbbrDateTimesToTimeAgo = function($) {
  $("abbr.timeago").each(function() {
    var dateTime = Date.parse($(this).text());
    if (dateTime) {
      $(this).attr("title", dateTime.toString("yyyy-MM-ddTHH:mm:ss"));
      $(this).text(dateTime.toString("MMMM dd, yyyy h:mm tt"));
    }
  });

  $("abbr.timeago").timeago();
  
  $("abbr.posttime").each(function () {
    var dateTime = Date.parse($(this).text());
    if (dateTime) {
      $(this).attr("title", dateTime.toString("MMMM dd, yyyy h:mm tt"));
      $(this).text(dateTime.toString("MMMM dd, yyyy h:mm tt"));
    }
  });
};

portal.initializeHtmlEditors = function () {
  tinymce.init({
    selector: '.html-editors textarea',
    content_css: '/css/bootstrap.min.css,/css/tinymce.css',
    plugins: [
      "advlist autolink lists link image nonbreaking charmap print preview hr anchor",
      "searchreplace visualblocks visualchars code fullscreen",
      "insertdatetime media table contextmenu paste directionality"
    ],
    toolbar: "styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    toolbar_items_size: 'small',
    browser_spellcheck: true,
    convert_urls: false,
    height: 240,
    resize: 'both',
    style_formats: [
      {title: 'Headers', items: [
        {title: 'Header 1', format: 'h1'},
        {title: 'Header 2', format: 'h2'},
        {title: 'Header 3', format: 'h3'},
        {title: 'Header 4', format: 'h4'},
        {title: 'Header 5', format: 'h5'},
        {title: 'Header 6', format: 'h6'}
      ]},
      {title: 'Inline', items: [
        {title: 'Bold', icon: 'bold', format: 'bold'},
        {title: 'Italic', icon: 'italic', format: 'italic'},
        {title: 'Underline', icon: 'underline', format: 'underline'},
        {title: 'Strikethrough', icon: 'strikethrough', format: 'strikethrough'},
        {title: 'Superscript', icon: 'superscript', format: 'superscript'},
        {title: 'Subscript', icon: 'subscript', format: 'subscript'},
        {title: 'Code', icon: 'code', format: 'code'}
      ]},
      {title: 'Blocks', items: [
        {title: 'Paragraph', format: 'p'},
        {title: 'Blockquote', format: 'blockquote'},
        {title: 'Div', format: 'div'},
        {title: 'Pre', format: 'pre' },
        {title: 'Code Block', format: 'codeblock' }
      ]},
      {title: 'Alignment', items: [
        {title: 'Left', icon: 'alignleft', format: 'alignleft'},
        {title: 'Center', icon: 'aligncenter', format: 'aligncenter'},
        {title: 'Right', icon: 'alignright', format: 'alignright'},
        {title: 'Justify', icon: 'alignjustify', format: 'alignjustify'}
      ]}
    ],
    formats: {
      codeblock: {block: 'pre', classes: 'prettyprint linenums'}
    },
    setup: function(editor) {
      editor.on('change', function() {
        editor.save();
      });
    }
  });
};

(function ($, XRM) {
  portal.initializeHtmlEditors();
  
  $(function () {
    portal.convertAbbrDateTimesToTimeAgo($);
    
    var facebookSignin = $(".facebook-signin");
    facebookSignin.on("click", function (e) {
      e.preventDefault();
      window.open(facebookSignin.attr("href"), "facebook_auth", "menubar=1,resizable=1,scrollbars=yes,width=800,height=600");
    });

    // Map dropdowns with .btn-select class to backing field.
    $('.btn-select.btn-group').each(function() {
      var select = $(this),
        target   = $(select.data('target')),
        selected = $('option:selected', target),
        focus    = $(select.data('focus')),
        label    = $('.btn .selected', select);
      
      if (selected.length > 0) {
        label.text(selected.text());
        $('.dropdown-menu li > a[data-value="' + selected.val() + '"]', select).parent('li').addClass('active');
      }
      
      $('.dropdown-menu li > a', select).click(function () {
        var option = $(this),
            value  = option.data('value');

        $('.dropdown-menu li', select).removeClass("active");
        option.parent('li').addClass("active");
        target.val(value);
        label.text(option.text());
        focus.focus();
      });
    });
    
    // Convert GMT timestamps to client time.
    $('abbr.timestamp').each(function() {
      var element = $(this);
      var text = element.text();
      var dateTime = Date.parse(text);
      if (dateTime) {
        element.attr("title", text);
        var format = element.attr('data-format') || "MMMM d, yyyy h:mm tt";
        element.text(dateTime.toString(format));
      }
    });
    
    // Convert GMT date ranges to client time.
    $('.vevent').each(function() {
      var start = $('.dtstart', this);
      var end = $('.dtend', this);
      var startText = start.text();
      var endText = end.text();
      var startDate = Date.parse(startText);
      var endDate = Date.parse(endText);
      
      if (startDate) {
        start.attr('title', startText);
        start.text(startDate.toString(start.attr('data-format') || 'MMMM dd, yyyy h:mm tt'));
      }
      
      if (endDate) {
        end.attr('title', endText);
        var sameDay = startDate
          && startDate.getYear() == endDate.getYear()
          && startDate.getMonth() == endDate.getMonth()
          && startDate.getDate() == endDate.getDate();
        end.text(endDate.toString(sameDay ? 'h:mm tt' : end.attr('data-format') || 'MMMM dd, yyyy h:mm tt'));
      }
    });

    // Initialize Bootstrap Carousel for any elements with the .carousel class.
    $('.carousel').carousel();

    // Workaround for jQuery UI and Bootstrap tooltip name conflict
    if ($.ui && $.ui.tooltip) {
      $.widget.bridge('uitooltip', $.ui.tooltip);
    }

    $('.has-tooltip').tooltip();

    prettyPrint();
    
    // Initialize any shopping cart status displays.
    $('.shopping-cart-status').each(function() {
      var element = $(this),
          service = element.attr('data-href'),
          count = element.find('.count'),
          countValue = count.find('.value');

      if (!service) {
        return;
      }

      $.getJSON(service, function (data) {
        if (data != null && data.Count && data.Count > 0) {
          countValue.text(data.Count);
          count.show();
          element.show();
        }
      });
    });
  });

  if (typeof XRM != 'undefined' && XRM) {
    XRM.zindex = 2000;

    var tinymceConfigurations = [XRM.tinymceSettings, XRM.tinymceCompactSettings];

    for (var i = 0; i < tinymceConfigurations.length; i++) {
      var configuration = tinymceConfigurations[i];

      if (!configuration) continue;

      // Load all page stylesheets into TinyMCE, for as close to WYSIWYG as possible.
      var stylesheets = $('head > link[rel="stylesheet"]').map(function (_, e) {
        var href = $(e).attr('href');
        return href.match(/,/) ? null : href;
      }).get();

      stylesheets.push('/css/tinymce.css');
      
      configuration.content_css = stylesheets.join(',');
    }
  }
})(window.jQuery, window.XRM);
