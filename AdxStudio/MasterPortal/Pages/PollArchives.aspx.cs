using System;
using System.Linq;
using System.Web.UI.WebControls;
using Xrm;

namespace Site.Pages
{
	public partial class PollArchives : PortalPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
			{
				return;
			}

			var polls = ServiceContext.Adx_pollSet
				.Where(p => p.adx_websiteid.Id == Website.Id && p.Adx_ExpirationDate <= DateTime.UtcNow)
				.ToList();

			PollsArchiveListView.DataSource = polls;
			PollsArchiveListView.DataBind();
		}

		protected void PollsArchiveListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var listItem = e.Item as ListViewDataItem;

			if (listItem == null || listItem.DataItem == null)
			{
				return;
			}

			var poll = (Adx_poll)listItem.DataItem;

			var listView = (ListView)e.Item.FindControl("PollResponsesListView");
			var totalLabel = (Label)e.Item.FindControl("Total");

			var pollResponses = ServiceContext.Adx_polloptionSet.Where(p => p.adx_pollid.Id == poll.Id).ToList();

			var totalVotes = pollResponses.Sum(p => p.Adx_votes).GetValueOrDefault(0);

			totalLabel.Text = totalVotes.ToString();

			if (totalVotes <= 0)
			{
				return;
			}

			var results = from response in pollResponses
				select new
				{
					Response = response.Adx_Answer,
					Count = response.Adx_votes,
					Percentage = Convert.ToInt32((response.Adx_votes.GetValueOrDefault(0)) / ((float)totalVotes) * (100))
				};

			listView.DataSource = results;

			listView.DataBind();
		}
	}
}