﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Site.Pages
{
	public partial class FullPage : PortalPage
	{
		private readonly Lazy<IEnumerable<SiteMapNode>> _childNodes = new Lazy<IEnumerable<SiteMapNode>>(GetChildNodes, LazyThreadSafetyMode.None);

		protected void Page_Load(object sender, EventArgs e)
		{
			ChildView.DataSource = GetChildNodes();
			ChildView.DataBind();
		}

		private static IEnumerable<SiteMapNode> GetChildNodes()
		{
			var currentNode = System.Web.SiteMap.CurrentNode;

			if (currentNode == null)
			{
				return new SiteMapNode[] { };
			}

			return currentNode.ChildNodes.Cast<SiteMapNode>().ToList();
		}

		protected string GetSummaryPropertyName(string entityLogicalName)
		{
			if (string.IsNullOrEmpty(entityLogicalName)) return null;

			switch (entityLogicalName)
			{
				case "adx_shortcut":
					return "Adx_description";
				case "adx_webfile":
					return "Adx_Summary";
				case "adx_webpage":
					return "Adx_Summary";
				default:
					return null;
			}
		}
	}
}