﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeBehind="WebForm.aspx.cs" Inherits="Site.Pages.WebForm" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/css/webforms.css") %>" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" ViewStateMode="Enabled" runat="server">
	<form id="content_form" runat="server">
		<asp:ScriptManager runat="server"/>
		<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
		<div class="page-heading">
			<% Html.RenderPartial("Breadcrumbs"); %>
			<div class="page-header">
				<h1>
					<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
				</h1>
			</div>
		</div>
		<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
		<script type="text/javascript">
			function webFormClientValidate() {
				// Custom client side validation. Method is called by the next/submit button's onclick event.
				// Must return true or false. Returning false will prevent the form from submitting.
				return true;
			}
		</script>
		<adx:WebForm ID="WebFormControl" runat="server" FormCssClass="crmEntityFormView" OnItemSaving="OnItemSaving" OnItemSaved="OnItemSaved" OnMovePrevious="OnMovePrevious" PreviousButtonCssClass="btn" NextButtonCssClass="btn btn-primary" SubmitButtonCssClass="btn btn-primary" ClientIDMode="Static" />
	</form>
</asp:Content>
