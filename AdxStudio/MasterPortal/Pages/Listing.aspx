﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="Listing.aspx.cs" Inherits="Site.Pages.Listing" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>
<%@ OutputCache CacheProfile="User" %>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<asp:ListView ID="ChildView" runat="server">
		<LayoutTemplate>
			<div class="listing">
				<div class="page-header">
					<h4>
						<i class="icon-folder-open"></i>
						<crm:Snippet SnippetName="Page Children Heading" DefaultText="In This Section" EditType="text" runat="server"/>
					</h4>
				</div>
				<ul>
					<li id="itemPlaceholder" runat="server"/>
				</ul>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<li runat="server">
				<h4>
					<a href="<%# Eval("Url") %>"><%# Eval("Title") %></a>
				</h4>
				<crm:CrmEntityDataSource ID="ChildEntity" DataItem='<%# Eval("Entity") %>' runat="server"/>
				<crm:Property DataSourceID="ChildEntity" PropertyName='<%# GetSummaryPropertyName(Eval("Entity.LogicalName", "{0}")) %>' EditType="html" runat="server"/>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarAbove" runat="server">
	<div class="page-children">
		<% if (Shortcuts.Any()) {%>
			<h4>
				<i class="icon-share"></i>
				<crm:Snippet SnippetName="Page Related Heading" DefaultText="Related Topics" EditType="text" runat="server"/>
			</h4>
			<ul class="nav nav-tabs nav-stacked">
				<% foreach (var node in Shortcuts) { %>
					<li>
						<a href="<%= node.Url %>">
							<i class="icon-share-alt"></i>
							<%= node.Title %>
						</a>
					</li>
				<% } %>
			</ul>
		<% } %>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarBottom" runat="server">
	<div class="section">
		<adx:MultiRatingControl ID="MultiRatingControl" RatingType="rating" runat="server" />
	</div>
</asp:Content>

