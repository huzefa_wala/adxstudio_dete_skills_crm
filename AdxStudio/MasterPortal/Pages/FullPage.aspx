﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="FullPage.aspx.cs" Inherits="Site.Pages.FullPage" ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register src="~/Controls/Comments.ascx" tagname="Comments" tagprefix="adx" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="page-metadata clearfix">
		<div class="page-last-updated">
			<crm:Snippet SnippetName="Page Modified On Prefix" DefaultText="This page was last updated" EditType="text" runat="server"/>
			<abbr class="timeago">
				<%: string.Format("{0:r}", Html.AttributeLiteral("modifiedon")) %>
			</abbr>.
			<% var displayDate = Html.AttributeLiteral("adx_displaydate"); %>
			<% if (displayDate != null) { %>
				<crm:Snippet SnippetName="Page Published On Prefix" DefaultText="It was published" EditType="text" runat="server"/>
				<abbr class="timeago">
					<%: string.Format("{0:r}", displayDate) %>
				</abbr>.
			<% } %>
		</div>
		
		<asp:ListView ID="ChildView" runat="server">
			<LayoutTemplate>
				<div class="listing">
					<h4>
						<i class="icon-folder-open"></i>
						<crm:Snippet SnippetName="Page Children Heading" DefaultText="In This Section" EditType="text" runat="server"/>
					</h4>
					<ul>
						<li id="itemPlaceholder" runat="server"/>
					</ul>
				</div>
			</LayoutTemplate>
			<ItemTemplate>
				<li runat="server">
					<h4>
						<a href="<%# Eval("Url") %>"><%# Eval("Title") %></a>
					</h4>
					<crm:CrmEntityDataSource ID="ChildEntity" DataItem='<%# Eval("Entity") %>' runat="server"/>
					<crm:Property DataSourceID="ChildEntity" PropertyName='<%# GetSummaryPropertyName(Eval("Entity.LogicalName", "{0}")) %>' EditType="html" runat="server"/>
				</li>
			</ItemTemplate>
		</asp:ListView>
		
		<div class="section">
			<adx:MultiRatingControl ID="MultiRatingControl" RatingType="rating" runat="server" />
		</div>
		<crm:Snippet SnippetName="Social Share Widget Code Page Bottom" EditType="text" DefaultText="" runat="server"/>
		
	</div>
	<adx:Comments RatingType="vote" EnableRatings="false" runat="server" />
</asp:Content>


