<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="True" CodeBehind="AuthenticatedPage.aspx.cs" Inherits="Site.Pages.AuthenticatedPage" %>
<%@ OutputCache CacheProfile="User" %>

<asp:Content ContentPlaceHolderID="SidebarTop" runat="server">
	<asp:SiteMapDataSource ID="ChildDataSource" StartFromCurrentNode="true" ShowStartingNode="False" StartingNodeOffset="0" runat="server"/>
	<asp:ListView DataSourceID="ChildDataSource" runat="server">
		<LayoutTemplate>
			<ul class="nav nav-list">
				<li class="nav-header">
					<crm:Snippet SnippetName="Child Navigation Heading" DefaultText="Inside" EditType="text" runat="server"/>
				</li>
				<li id="itemPlaceholder" runat="server"/>
			</ul>
		</LayoutTemplate>
		<ItemTemplate>
			<li runat="server">
				<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server"/>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>