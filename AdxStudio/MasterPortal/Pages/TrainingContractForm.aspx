﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="TrainingContractForm.aspx.cs" Inherits="Site.Pages.TrainingContractForm" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">

    <asp:TextBox class="form-control" ID="txt_contractId" ViewStateMode="Enabled" runat="server" Visible="false"></asp:TextBox>

    <asp:Table ID="mainTable" CssClass="mainTable" runat="server" HorizontalAlign="Center" ViewStateMode="Enabled">
        <asp:TableRow ID="TableRow29" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table5" runat="server">
                        <asp:TableRow ID="TableRow30" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Data Source</asp:Label>               
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow31" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Data_Source" ViewStateMode="Enabled" runat="server" Enabled="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table6" runat="server">
                        <asp:TableRow ID="TableRow32" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Intray/Contract No</asp:Label>                 
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow33" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_intray" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>


                    </asp:Table>
                </div>
            </asp:TableCell>
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table7" runat="server">
                        <asp:TableRow ID="TableRow34" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Apprentice</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow35" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Apprentice" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow37" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table8" runat="server">
                        <asp:TableRow ID="TableRow38" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">School</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow39" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_School" data-custom="myOwnValidator" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table9" runat="server">
                        <asp:TableRow ID="TableRow40" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Commencement</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow41" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_commencement" required="true" data-custom="myOwnValidator" onchange="javascript:onCommencementDateChange(this);" runat="server" />
                                <div class="help-block with-errors"></div>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table10" runat="server">
                        <asp:TableRow ID="TableRow42" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Recommencement?</asp:Label>         
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow43" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:RadioButtonList class="form-control" ID="check_Recommencement" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                    <asp:ListItem Text="Yes" Value="1" />
                                    <asp:ListItem Text="No" Value="0" />
                                </asp:RadioButtonList>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow44" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table11" runat="server">
                        <asp:TableRow ID="TableRow45" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Nominal Completion</asp:Label>  
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow46" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Nominal_Completion" runat="server"></asp:TextBox>
                                <div class="help-block with-errors"></div>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table12" runat="server">
                        <asp:TableRow ID="TableRow47" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Contract Substatus</asp:Label>         
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow48" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Contract_Substatus" runat="server" Enabled="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table13" runat="server">
                    <asp:TableRow ID="TableRow49" runat="server" ForeColor="Black">
                        <asp:TableCell>
                                <asp:Label runat="server" class="control-label">District</asp:Label>     
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow57" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:TextBox class="form-control" ID="txt_District" runat="server" Enabled="false"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow1" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table14" runat="server">
                        <asp:TableRow ID="TableRow2" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Termination</asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow3" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Termination" runat="server"></asp:TextBox>
                                <div class="help-block with-errors"></div>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table15" runat="server">
                        <asp:TableRow ID="TableRow4" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Probation Completion</asp:Label>               
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow5" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Probation_Completion" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table16" runat="server">
                        <asp:TableRow ID="TableRow6" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Previous Employee</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow7" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:RadioButtonList class="form-control" ID="check_Previous_Employee" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                    <asp:ListItem Text="Yes" Value="1" />
                                    <asp:ListItem Text="No" Value="0" />
                                </asp:RadioButtonList>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow8" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table17" runat="server">
                        <asp:TableRow ID="TableRow16" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Credit (Months)</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow58" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Credit_Months" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table18" runat="server">
                        <asp:TableRow ID="TableRow59" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Full Time Employment Start Date</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow60" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Full_Time_Employment_Start_Date" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table19" runat="server">
                        <asp:TableRow ID="TableRow61" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Full Time Employment Months</asp:Label>      
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow62" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Full_Time_Employment_Months" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow63" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table20" runat="server">
                        <asp:TableRow ID="TableRow64" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Part Time Employment Start Date</asp:Label>  
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow65" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Part_Time_Employment_Start_Date" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table21" runat="server">
                        <asp:TableRow ID="TableRow66" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Part Time Employment Months</asp:Label>         
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow67" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Part_Time_Employment_Months" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table22" runat="server">
                    <asp:TableRow ID="TableRow68" runat="server" ForeColor="Black">
                        <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Casual Time Employment Start Date</asp:Label>    
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow69" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:TextBox class="form-control" ID="txt_Casual_Time_Employment_Start_Date" runat="server" Enabled="false"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow17" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow ID="TableRow18" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Casual Time Employment Months</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow21" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Casual_Time_Employment_Months" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>


                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table23" runat="server">
                        <asp:TableRow ID="TableRow22" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Qualification</asp:Label>      
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow50" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Qualification" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table24" runat="server">
                    <asp:TableRow ID="TableRow51" runat="server" ForeColor="Black">
                        <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Occupational Model</asp:Label>    
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow52" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:TextBox class="form-control" ID="txt_Occupational_Model" runat="server" Enabled="false"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow53" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table25" runat="server">
                        <asp:TableRow ID="TableRow54" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Current SRTO</asp:Label> 
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow55" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Current_SRTO" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table26" runat="server">
                        <asp:TableRow ID="TableRow70" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Contract Mode</asp:Label>         
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow71" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Current_Mode" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table27" runat="server">
                    <asp:TableRow ID="TableRow72" runat="server" ForeColor="Black">
                        <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Contract Type</asp:Label>   
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow73" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:TextBox class="form-control" ID="txt_contractType" runat="server" Enabled="false"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow9" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table2" runat="server">
                        <asp:TableRow ID="TableRow10" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Receipt No</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow11" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Receipt_No" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table28" runat="server">
                        <asp:TableRow ID="TableRow12" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Contract Status</asp:Label>        
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow56" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_contractStatus" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>


                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table29" runat="server">
                    <asp:TableRow ID="TableRow74" runat="server" ForeColor="Black">
                        <asp:TableCell>
                                <asp:Label runat="server" class="control-label">AAC</asp:Label>   
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow75" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:TextBox class="form-control" ID="txt_AAC" runat="server" Enabled="false"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow76" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table30" runat="server">
                        <asp:TableRow ID="TableRow77" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">State Funding?</asp:Label> 
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow78" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:RadioButtonList class="form-control" ID="check_State_Funding" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                    <asp:ListItem Text="Yes" Value="1" />
                                    <asp:ListItem Text="No" Value="0" />
                                </asp:RadioButtonList>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table31" runat="server">
                        <asp:TableRow ID="TableRow79" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Existing Worker</asp:Label>         
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow80" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:RadioButtonList class="form-control" ID="check_Existing_Worker" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                    <asp:ListItem Text="Yes" Value="1" />
                                    <asp:ListItem Text="No" Value="0" />
                                </asp:RadioButtonList>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table32" runat="server">
                    <asp:TableRow ID="TableRow81" runat="server" ForeColor="Black">
                        <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Full Time Employment End Date</asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow82" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:TextBox class="form-control" ID="txt_Full_Time_Employment_End_Date" runat="server" Enabled="false"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow13" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table33" runat="server">
                        <asp:TableRow ID="TableRow14" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Probation Period</asp:Label> 
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow15" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Probation_Period" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table34" runat="server">
                        <asp:TableRow ID="TableRow19" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Part Time Employment End Date</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow20" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Part_Time_Employment_End_Date" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table35" runat="server">
                    <asp:TableRow ID="TableRow83" runat="server" ForeColor="Black">
                        <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Part Time Employment Hours</asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow84" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:TextBox class="form-control" ID="txt_Part_Time_Employment_Hours" runat="server" Enabled="false"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow85" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table36" runat="server">
                        <asp:TableRow ID="TableRow86" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Casual Time Employment End Date</asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow87" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Casual_Time_Employment_End_Date" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table37" runat="server">
                        <asp:TableRow ID="TableRow88" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Casual Time Employment Hours</asp:Label> 
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow89" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_Casual_Time_Employment_Hours" runat="server" Enabled="false"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table38" runat="server">
                    <asp:TableRow ID="TableRow90" runat="server" ForeColor="Black">
                        <asp:TableCell>
                                <asp:Label runat="server" class="control-label"></asp:Label> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow91" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:Button CssClass="btn btn-primary" ID="btn_update" runat="server" Text="Update" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Scripts" runat="server">
    <script src="../js/moment.js"></script>
    <script src="../js/jquery-1.9.0.js"></script>
    <script src="../js/validator.js"></script>

    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <link href="../css/pikaday.css" rel="stylesheet" />
    <link href="../css/site.css" rel="stylesheet" />
    <link href="../css/theme.css" rel="stylesheet" />
    <script src="../js/pikaday.js"></script>

    <script>
        function onCommencementDateChange(ctrlDate) {
            if (ctrlDate.value.trim() == "") {
                alert("Commencement date is mandatory");
                return false;
            }

            // regular expression to match required date format
            re = /^\d{4}\/\d{1,2}\/\d{1,2}$/;

            if (ctrlDate.value != '' && !ctrlDate.value.match(re)) {
                alert("Invalid date format: " + ctrlDate.value + ". Date should be in YYYY/MM/DD format");
                ctrlDate.value = "";
                ctrlDate.focus();
                return false;
            }

            // Parse the date parts to integers
            var parts = ctrlDate.value.split("/");
            var day = parseInt(parts[2], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[0], 10);
            var isValidDate = true;

            // Check the ranges of month and year
            if (year < 1000 || year > 3000 || month == 0 || month > 12)
            {
                isValidDate = false;
            }               
            else
            {
                var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                // Adjust for leap years
                if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                    monthLength[1] = 29;

                // Check the range of the day
                isValidDate = day > 0 && day <= monthLength[month - 1];
            }

            if (!isValidDate) {
                alert("Date is not valid : " + ctrlDate.value);
                ctrlDate.value = "";
                ctrlDate.focus();
                return false;
            }
        }

        $(document).ready(function () {
            $().validator({
                custom: {
                    myOwnValidator: function (elem) {
                        // dummy example: value should be current timestamp
                        alert(1);
                        return false;
                        //return $(elem).val() == new Date().getTime()
                    }
                }
            })
        });
    </script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('MainContent_MainContent_txt_commencement'),
                firstday: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2025-12-31'),
                defaultDate: new Date(),
                yearRange: [2000, 2025],
                numberOfMonths: 1,
                theme: 'dark-theme',
                format: 'YYYY/MM/DD'
            });
    </script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('MainContent_MainContent_txt_Termination'),
                firstday: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2025-12-31'),
                defaultDate: new Date(),
                yearRange: [2000, 2025],
                numberOfMonths: 1,
                theme: 'dark-theme',
                format: 'YYYY/MM/DD'
            });
    </script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('MainContent_MainContent_txt_Nominal_Completion'),
                firstday: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2025-12-31'),
                defaultDate: new Date(),
                yearRange: [2000, 2025],
                numberOfMonths: 1,
                theme: 'dark-theme',
                format: 'YYYY/MM/DD'
            });
    </script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('MainContent_MainContent_txt_Probation_Completion'),
                firstday: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2025-12-31'),
                defaultDate: new Date(),
                yearRange: [2000, 2025],
                numberOfMonths: 1,
                theme: 'dark-theme',
                format: 'YYYY/MM/DD'
            });
    </script>
</asp:Content>
