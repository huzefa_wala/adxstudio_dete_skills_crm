﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="Directory.aspx.cs" Inherits="Site.Pages.Directory" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="page-heading">
		<% Html.RenderPartial("Breadcrumbs"); %>
		<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
		<div class="page-header">
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
		</div>
	</div>
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
	<div class="directory row">
		<div class="span4">
			<asp:SiteMapDataSource ID="ChildDataSource" StartFromCurrentNode="true" ShowStartingNode="False" StartingNodeOffset="0" runat="server"/>
			<asp:ListView DataSourceID="ChildDataSource" runat="server">
				<LayoutTemplate>
					<div class="subnav">
						<ul class="nav nav-tabs nav-stacked">
							<li id="itemPlaceholder" runat="server"/>
						</ul>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<li runat="server">
						<a href="<%# Eval("Entity.Id", "#{0}") %>"><i class="icon-chevron-right"></i> <%# Eval("Title") %></a>
					</li>
				</ItemTemplate>
			</asp:ListView>
		</div>
		<div class="span8">
			<asp:ListView DataSourceID="ChildDataSource" runat="server">
				<LayoutTemplate>
					<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
				</LayoutTemplate>
				<ItemTemplate>
					<section id="<%# Eval("Entity.Id", "{0}") %>">
						<div class="page-header">
							<h2>
								<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server"/>
							</h2>
						</div>
						<asp:Panel Visible='<%# IsWebPage(Eval("Entity")) %>' runat="server">
							<crm:CrmEntityDataSource ID="SectionEntity" DataItem='<%# Eval("Entity") %>' runat="server"/>
							<crm:Property DataSourceID="SectionEntity" PropertyName='<%# IsWebPage(Eval("Entity")) ? "Adx_Summary" : null %>' EditType="html" runat="server" />
						</asp:Panel>
						<asp:SiteMapDataSource ID="SubChildDataSource" StartingNodeUrl='<%# Eval("Url") %>' ShowStartingNode="False" StartingNodeOffset="0" runat="server"/>
						<asp:ListView DataSourceID="SubChildDataSource" runat="server">
							<LayoutTemplate>
								<ul class="nav">
									<li id="itemPlaceholder" runat="server"/>
								</ul>
							</LayoutTemplate>
							<ItemTemplate>
								<li runat="server">
									<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server"/>
								</li>
							</ItemTemplate>
						</asp:ListView>
					</section>
				</ItemTemplate>
			</asp:ListView>
		</div>
	</div>
</asp:Content>
