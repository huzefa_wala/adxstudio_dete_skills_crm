﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Account;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.IdentityModel;
using Adxstudio.Xrm.Web;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.IdentityModel.Claims;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Messages;
using Microsoft.Xrm.Portal.IdentityModel.Configuration;
using Xrm;

namespace Site.Pages
{
	public partial class ProfilePage : PortalPage
	{
		private const string _userFetchXmlFormat = @"
			<fetch mapping=""logical"">
				<entity name=""contact"">
					<all-attributes />
					<filter type=""and"">
						<condition attribute=""contactid"" operator=""eq"" value=""{0}""/>
					</filter>
				</entity>
			</fetch>";

		public bool ShowMarketingOptionsPanel
		{
			get
			{
				var showMarketingSetting = ServiceContext.GetSiteSettingValueByName(Website, "Profile/ShowMarketingOptionsPanel") ?? "false";

				return showMarketingSetting.ToLower() == "true";
			}
		}

		public bool ForceRegistration
		{
			get
			{
				var siteSetting = ServiceContext.GetSiteSettingValueByName(Website, "Profile/ForceSignUp") ?? "false";

				return siteSetting.ToLower() == "true";
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			if (ForceRegistration && !ServiceContext.ValidateProfileSuccessfullySaved(Contact))
			{
				MissingFieldsMessage.Visible = true;
			}

			if (ShowMarketingOptionsPanel)
			{
				MarketingOptionsPanel.Visible = true;
			}

			ProfileDataSource.FetchXml = _userFetchXmlFormat.FormatWith(Contact.Id);

			ProfileAlertInstructions.Visible = Contact.adx_profilealert.GetValueOrDefault();

			if (IsPostBack)
			{
				return;
			}

			var contact = XrmContext.ContactSet.FirstOrDefault(c => c.ContactId == Contact.Id);

			if (contact == null)
			{
				throw new ApplicationException(string.Format("Could not retrieve contact record with contactid equal to '{0}'", Contact.Id));
			}

			if (ShowMarketingOptionsPanel)
			{
				marketEmail.Checked = !contact.DoNotEMail.GetValueOrDefault(false);
				marketFax.Checked = !contact.DoNotFax.GetValueOrDefault(false);
				marketPhone.Checked = !contact.DoNotPhone.GetValueOrDefault(false);
				marketMail.Checked = !contact.DoNotPostalMail.GetValueOrDefault(false);
			}

			PopulateMarketingLists();
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			MissingFieldsMessage.Visible = false;

			var contact = XrmContext.MergeClone(Contact);

			ManageLists(XrmContext, contact);

			ProfileFormView.UpdateItem();

			var returnUrlKey = FederationCrmConfigurationManager.GetUserRegistrationSettings().ReturnUrlKey ?? "returnurl";
			var returnUrl = Request[returnUrlKey];

			if (!string.IsNullOrWhiteSpace(returnUrl))
			{
				Context.RedirectAndEndResponse(returnUrl);
			}
		}

		protected void OnItemUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e)
		{
			e.Values["adx_profilemodifiedon"] = DateTime.UtcNow;
			e.Values["adx_profilealert"] = ProfileAlertInstructions.Visible = false;

			if (ShowMarketingOptionsPanel)
			{
				e.Values["donotemail"] = !marketEmail.Checked;
				e.Values["donotbulkemail"] = !marketEmail.Checked;
				e.Values["donotfax"] = !marketFax.Checked;
				e.Values["donotphone"] = !marketPhone.Checked;
				e.Values["donotpostalmail"] = !marketMail.Checked;
			}
		}

		protected void OnItemUpdated(object sender, EventArgs args)
		{
			ConfirmationMessage.Visible = true;
		}

		public bool IsListChecked(object listoption)
		{
			var list = (List)listoption;

			if (Request.IsAuthenticated)
			{
				var contact = XrmContext.ContactSet.FirstOrDefault(c => c.ContactId == Contact.Id);

				return contact != null
					&& contact.listcontact_association
					.Any(l => l.ListId == list.ListId);
			}

			return false;
		}

		public void ManageLists(XrmServiceContext context, Contact contact)
		{
			foreach (var item in MarketingListsListView.Items)
			{
				if (item == null)
				{
					continue;
				}

				var listViewItem = item;

				var hiddenListId = (HiddenField)listViewItem.FindControl("ListID");

				if (hiddenListId == null)
				{
					continue;
				}

				var listId = new Guid(hiddenListId.Value);

				var ml = context.ListSet.First(m => m.ListId == listId);

				var listCheckBox = (CheckBox)item.FindControl("ListCheckbox");

				if (listCheckBox == null)
				{
					continue;
				}

				var contactLists = contact.listcontact_association.ToList();

				var inList = contactLists.Any(list => list.ListId == ml.ListId);

				if (listCheckBox.Checked && !inList)
				{
					context.AddMemberList(ml.ListId.GetValueOrDefault(), contact.ContactId.GetValueOrDefault());
				}
				else if (!listCheckBox.Checked && inList)
				{
					context.RemoveMemberList(ml.ListId.GetValueOrDefault(), contact.ContactId.GetValueOrDefault());
				}
			}
		}

		protected void PopulateMarketingLists()
		{
			if (Website == null)
			{
				MarketingLists.Visible = false;
				return;
			}

			var website = XrmContext.Adx_websiteSet.FirstOrDefault(w => w.Adx_websiteId == Website.Id);

			if (website == null)
			{
				MarketingLists.Visible = false;
				return;
			}

			// Note: Marketing Lists with 'Dynamic' Type (i.e. value of 1 or true) do not support manually adding members

			if (!website.adx_website_list.Any(l => l.Type.GetValueOrDefault() == false))
			{
				MarketingLists.Visible = false;
				return;
			}

			MarketingListsListView.DataSource = website.adx_website_list.Where(l => l.Type.GetValueOrDefault() == false);

			MarketingListsListView.DataBind();
		}
	}
}