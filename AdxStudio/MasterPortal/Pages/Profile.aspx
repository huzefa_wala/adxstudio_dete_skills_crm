﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Profile.master" AutoEventWireup="True" CodeBehind="Profile.aspx.cs" Inherits="Site.Pages.ProfilePage" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/css/webforms.css") %>">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<asp:Panel ID="ConfirmationMessage" runat="server" CssClass="alert alert-success alert-block" Visible="False">
		<a class="close" data-dismiss="alert" href="#">×</a>
		<crm:Snippet runat="server" SnippetName="Profile Update Success Text" DefaultText="Your profile has been updated successfully." Editable="true" EditType="html" />
	</asp:Panel>
		
	<asp:Panel ID="MissingFieldsMessage" runat="server" CssClass="alert alert-error alert-block" Visible="False">
		<a class="close" data-dismiss="alert" href="#">×</a>
		<crm:Snippet runat="server" SnippetName="Force Sign-Up Profile Explanation" DefaultText="You must complete your profile before using the features of this website." Editable="true" EditType="html" />
	</asp:Panel>

	<asp:Panel ID="ProfileAlertInstructions" runat="server" CssClass="alert alert-block" Visible="False">
		<a class="close" data-dismiss="alert" href="#">×</a>
		<asp:Label runat="server" Text='<%$ Context: Property=User, Attribute=adx_profilealertinstructions %>' />
	</asp:Panel>

	<fieldset>
		<legend>
			<crm:Snippet SnippetName="Profile Form Legend" DefaultText="Your Information" EditType="text" runat="server"/>
		</legend>

		<adx:CrmDataSource ID="ProfileDataSource" runat="server"/>
		<adx:CrmEntityFormView ID="ProfileFormView" runat="server"
			DataSourceID="ProfileDataSource"
			CssClass="crmEntityFormView"
			EntityName="contact"
			FormName="Profile Web Form"
			OnItemUpdated="OnItemUpdated"
			OnItemUpdating="OnItemUpdating"
			ValidationGroup="Profile"
			ValidationSummaryCssClass="alert alert-error alert-block"
			RecommendedFieldsRequired="True"
			ShowUnsupportedFields="False"
			ToolTipEnabled="False"
			Mode="Edit">
			<UpdateItemTemplate>
			</UpdateItemTemplate>
		</adx:CrmEntityFormView>
	</fieldset>
		
	<asp:Panel ID="MarketingOptionsPanel" Visible="false" runat="server">
		<fieldset>
			<legend>
				<crm:Snippet runat="server" SnippetName="Profile/MarketingPref" DefaultText="How may we contact you? <small>Select all that apply.</small>" Editable="True" EditType="text" />
			</legend>
			<div class="form-horizontal">
				<div class="control-group">
					<label class="checkbox">
						<crm:Snippet runat="server" SnippetName="Profile/MarketEmail" DefaultText="Email" Editable="True" EditType="text" />
						<asp:CheckBox runat="server" ID="marketEmail" />
					</label>
					<label class="checkbox">
						<crm:Snippet runat="server" SnippetName="Profile/MarketFax" DefaultText="Fax" Editable="True" EditType="text" />
						<asp:CheckBox runat="server" ID="marketFax" />
					</label>
					<label class="checkbox">
						<crm:Snippet runat="server" SnippetName="Profile/MarketPhone" DefaultText="Phone" Editable="True" EditType="text" />
						<asp:CheckBox runat="server" ID="marketPhone" />
					</label>
					<label class="checkbox">
						<crm:Snippet runat="server" SnippetName="Profile/MarketMail" DefaultText="Mail" Editable="True" EditType="text" />
						<asp:CheckBox runat="server" ID="marketMail" />
					</label>
				</div>
			</div>
		</fieldset>
	</asp:Panel>

	<asp:Panel ID="MarketingLists" runat="server">
		<fieldset>
			<legend>
				<crm:Snippet runat="server" SnippetName="Profile Marketing Lists Title Text" DefaultText="Subscribe to the following email lists" Editable="true" EditType="text" />
			</legend>
			<div class="form-horizontal">
				<div class="control-group">
					<asp:Listview ID="MarketingListsListView" runat="server">
						<LayoutTemplate>
							<ul class="unstyled">
								<asp:PlaceHolder ID="ItemPlaceholder" runat="server" />
							</ul>
						</LayoutTemplate>
						<ItemTemplate>
							<li>
								<label class="checkbox">
									<%# Eval("ListName") %> &ndash; <%# Eval("Purpose") %>
									<asp:CheckBox ID="ListCheckbox" runat="server" Checked='<%# IsListChecked(Container.DataItem) %>'/>
									<asp:HiddenField ID="ListID" Value='<%# Eval("ListID") %>' runat="server" />
								</label>
							</li>
						</ItemTemplate>
					</asp:Listview>
				</div>
			</div>
		</fieldset>
	</asp:Panel>

	<div class="form-actions">
		<asp:Button ID="SubmitButton" Text='<%$ Snippet: Profile Submit Button Text, Update %>' CssClass="btn btn-primary" OnClick="SubmitButton_Click" ValidationGroup="Profile" runat="server" />
	</div>
</asp:Content>