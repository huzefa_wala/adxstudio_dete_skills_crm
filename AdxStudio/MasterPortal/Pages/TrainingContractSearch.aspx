﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="TrainingContractSearch.aspx.cs" Inherits="Site.Pages.TrainingContractSearch"%>

<asp:Content ID="Content3" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
    <%--<div>--%>
    <asp:Table ID="mainTable" CssClass="mainTable" runat="server" HorizontalAlign="Center">
        <asp:TableRow ID="TableRow1" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow ID="TableRow2" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Training Contract Number</asp:Label>                  
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow3" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_contractNumber" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table3" runat="server">
                        <asp:TableRow ID="TableRow8" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Training Contract Status</asp:Label>                  
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow9" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:DropDownList class="form-control" ID="list_contractStatus" ViewStateMode="Enabled" AutoPostBack="true" OnSelectedIndexChanged="list_contractStatus_SelectedIndexChanged" runat="server" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table5" runat="server">
                        <asp:TableRow ID="TableRow10" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Training Contract Sub Status</asp:Label>                 
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow11" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:DropDownList class="form-control" ID="list_contractSubStatus" ViewStateMode="Enabled" runat="server" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow7" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table2" runat="server">
                        <asp:TableRow ID="TableRow5" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Commencement Date</asp:Label>               
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow6" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_commencementDate" runat="server"></asp:TextBox>    
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table6" runat="server">
                        <asp:TableRow ID="TableRow12" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Employer - Legal & Trading</asp:Label>               
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow13" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_employer" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table7" runat="server">
                        <asp:TableRow ID="TableRow14" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">AAC</asp:Label>             
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow15" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_aac" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow4" runat="server" ForeColor="Black">
            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table8" runat="server">
                        <asp:TableRow ID="TableRow16" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Training Contract Type</asp:Label>         
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow17" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:DropDownList class="form-control" ID="list_contractType" ViewStateMode="Enabled" runat="server" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <div class="form-group">
                    <asp:Table ID="Table9" runat="server">
                        <asp:TableRow ID="TableRow18" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Label runat="server" class="control-label">Apprentice name</asp:Label>         
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow19" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:TextBox class="form-control" ID="txt_apprenticeFullName" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Table ID="Table10" runat="server">
                        <asp:TableRow ID="TableRow20" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow21" runat="server" ForeColor="Black">
                            <asp:TableCell>
                                <asp:Button CssClass="btn btn-primary" ID="btn_search" runat="server" Text="Search" OnClick="search_onClick" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <div class="row">
        <div class="col-lg-12">

            <asp:Label ID="Message"
                ForeColor="Red"
                runat="server" />

            <asp:GridView ID="dataGrid" class="dataTable" runat="server" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="7" OnPageIndexChanging="grdData_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField='guid' HeaderText="ID" Visible="false" />
                    <asp:HyperLinkField DataTextField='contractNumber' NavigateUrl="~/TrainingContractForm.aspx"
                        DataNavigateUrlFormatString="TrainingContractForm.aspx?ID={0}" DataNavigateUrlFields="guid"
                        HeaderText="Contract Number" ItemStyle-CssClass="ContractNumberContent" />
                    <asp:BoundField DataField='contractType' HeaderText="Contract Type" SortExpression="name" />
                    <asp:BoundField DataField='contractStatus' HeaderText="Contract Status" />
                    <asp:BoundField DataField='employer' HeaderText="Employer Workplace" />
                    <asp:BoundField DataField='district' HeaderText="District" />
                    <asp:BoundField DataField='aac' HeaderText="AAC" />
                    <asp:BoundField DataField='apprentice' HeaderText="Apprentice" />
                    <asp:BoundField DataField='commencementDate' HeaderText="Commencement Date" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <%--</div>--%>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Scripts" runat="server">
    <script src="../js/moment.js"></script>
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <link href="../css/pikaday.css" rel="stylesheet" />
    <link href="../css/site.css" rel="stylesheet" />
    <link href="../css/theme.css" rel="stylesheet" />
    <script src="../js/pikaday.js"></script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('MainContent_MainContent_txt_commencementDate'),
                firstday: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2025-12-31'),
                defaultDate: new Date(),
                yearRange: [2000, 2025],
                numberOfMonths: 1,
                theme: 'dark-theme',
                format: 'YYYY/MM/DD'
            });
    </script>
</asp:Content>
