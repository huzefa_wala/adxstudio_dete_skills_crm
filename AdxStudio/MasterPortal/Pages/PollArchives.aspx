<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" Inherits="Site.Pages.PollArchives" Codebehind="PollArchives.aspx.cs" %>
<%@ OutputCache CacheProfile="User" %>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<asp:ListView ID="PollsArchiveListView" runat="server" OnItemDataBound="PollsArchiveListView_ItemDataBound">
		<LayoutTemplate>
			<div class="poll-archives">
				<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<div class="well poll">
				<div class="poll-question">
					<%# Eval("Adx_question") %>
				</div>
				<asp:ListView ID="PollResponsesListView" runat="server">
					<LayoutTemplate>
						<div class="poll-results">
							<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
						</div>
					</LayoutTemplate>
					<ItemTemplate>
						<div class="poll-result">
							<asp:Label CssClass="poll-option" runat="server">
								<%# Eval("Response") %> (<%# Eval("Count") ?? 0 %>)
							</asp:Label>
							<div class="progress">
								<div class="bar" style="<%# Eval("Percentage", "width: {0}%") %>"></div>
							</div>
						</div>
					</ItemTemplate>
					<EmptyDataTemplate>
						<crm:Snippet runat="server" SnippetName="polls/archives/emptyresponsemessage" DefaultText="There are no responses to this poll." EditType="html"/>
					</EmptyDataTemplate>
				</asp:ListView>
				<div>
					<crm:Snippet runat="server" CssClass="TotalLabel" SnippetName="polls/archives/totalslabel" DefaultText="Total Votes:"/>
					<asp:Label ID="Total" runat="server"/>
				</div>
			</div>
		</ItemTemplate>
		<EmptyDataTemplate>
			<crm:Snippet runat="server" SnippetName="polls/archives/emptyarchivesmessage" DefaultText="There are currently no archived polls." EditType="html"/>
		</EmptyDataTemplate>
	</asp:ListView>
</asp:Content>