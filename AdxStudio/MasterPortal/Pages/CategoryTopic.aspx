﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeBehind="CategoryTopic.aspx.cs" Inherits="Site.Pages.CategoryTopic" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="page-heading">
		<% Html.RenderPartial("Breadcrumbs"); %>
		<asp:SiteMapDataSource ID="CategoryData" StartFromCurrentNode="True" ShowStartingNode="True" StartingNodeOffset="-1" runat="server"/>
		<asp:SiteMapDataSource ID="TopicData" StartFromCurrentNode="True" ShowStartingNode="False" StartingNodeOffset="-1" runat="server"/>
		<div class="page-header">
			<div class="visible-desktop pull-right">
				<asp:ListView DataSourceID="TopicData" runat="server">
					<LayoutTemplate>
						<ul class="nav nav-pills">
							<asp:ListView DataSourceID="CategoryData" runat="server">
								<LayoutTemplate>
									<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
								</LayoutTemplate>
								<ItemTemplate>
									<li>
										<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server" />
									</li>
								</ItemTemplate>
							</asp:ListView>
							<li id="itemPlaceholder" runat="server"/>
						</ul>
					</LayoutTemplate>
					<ItemTemplate>
						<li class="<%# IsCurrentNode(Container.DataItem) ? "active" : string.Empty %>">
							<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server"/>
						</li>
					</ItemTemplate>
				</asp:ListView>
			</div>
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
			<asp:ListView DataSourceID="TopicData" runat="server">
				<LayoutTemplate>
					<ul class="nav nav-pills nav-stacked hidden-desktop">
						<asp:ListView DataSourceID="CategoryData" runat="server">
							<LayoutTemplate>
								<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
							</LayoutTemplate>
							<ItemTemplate>
								<li>
									<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server" />
								</li>
							</ItemTemplate>
						</asp:ListView>
						<li id="itemPlaceholder" runat="server"/>
					</ul>
				</LayoutTemplate>
				<ItemTemplate>
					<li class="<%# IsCurrentNode(Container.DataItem) ? "active" : string.Empty %>">
						<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server"/>
					</li>
				</ItemTemplate>
			</asp:ListView>
		</div>
	</div>
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
</asp:Content>
