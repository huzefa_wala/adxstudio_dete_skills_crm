﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="Site.Pages.Category" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="page-heading">
		<% Html.RenderPartial("Breadcrumbs"); %>
		<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
		<div class="page-header">
			<div class="pull-right visible-desktop">
				<asp:SiteMapDataSource ID="TopicData" StartFromCurrentNode="True" ShowStartingNode="False" StartingNodeOffset="0" runat="server"/>
				<asp:ListView DataSourceID="TopicData" runat="server">
					<LayoutTemplate>
						<ul class="nav nav-pills">
							<li class="active">
								<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: Current, Return=Url %>' Text='<%$ CrmSiteMap: Current, Eval=Title %>' runat="server"/>
							</li>
							<li id="itemPlaceholder" runat="server"/>
						</ul>
					</LayoutTemplate>
					<ItemTemplate>
						<li>
							<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server"/>
						</li>
					</ItemTemplate>
				</asp:ListView>
			</div>
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
			<asp:ListView DataSourceID="TopicData" runat="server">
				<LayoutTemplate>
					<ul class="nav nav-pills nav-stacked hidden-desktop">
						<li class="active">
							<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: Current, Return=Url %>' Text='<%$ CrmSiteMap: Current, Eval=Title %>' runat="server"/>
						</li>
						<li id="itemPlaceholder" runat="server"/>
					</ul>
				</LayoutTemplate>
				<ItemTemplate>
					<li>
						<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Title") %>' runat="server"/>
					</li>
				</ItemTemplate>
			</asp:ListView>
		</div>
	</div>
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
</asp:Content>
