﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Xrm;
using Microsoft.Xrm.Sdk;

namespace Site.Pages
{
    public partial class TrainingContractForm : System.Web.UI.Page
    {
        XrmServiceContext context;

        public TrainingContractForm()
        {
            context = new XrmServiceContext("Xrm");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btn_update.Click += new EventHandler(update_onClick);

            if (!IsPostBack)
            {
                TP_contract trainingContract;
                Guid contractId = new Guid(Request["ID"]);


                if (contractId != null)
                {
                    trainingContract = context.TP_contractSet.FirstOrDefault(contract => contract.TP_contractId == contractId);

                    txt_contractId.Text = trainingContract.TP_contractId.ToString();

                    txt_Data_Source.Text = trainingContract.tp_Contract_Data_Source == null ? "" : trainingContract.tp_Contract_Data_Source.ToEntity<TP_type_data_source>().TP_name;
                    txt_Data_Source.ToolTip = txt_Data_Source.Text;

                    txt_intray.Text = trainingContract.tp_delta_contractid == null ? "" : trainingContract.tp_delta_contractid;
                    txt_intray.ToolTip = txt_intray.Text;

                    txt_Apprentice.Text = trainingContract.tp_Apprentice == null ? "" : trainingContract.tp_Apprentice;
                    txt_Apprentice.ToolTip = txt_Apprentice.Text;

                    txt_School.Text = trainingContract.tp_school_accountid == null ? "" : trainingContract.tp_school_accountid.ToString();
                    txt_School.ToolTip = txt_School.Text;

                    txt_commencement.Text = trainingContract.tp_commencement_date == null ? "" : trainingContract.tp_commencement_date.Value.ToString("yyyy/MM/dd");
                    txt_commencement.ToolTip = txt_commencement.Text;

                    check_Recommencement.SelectedValue = trainingContract.tp_recommencement == null ? "" : trainingContract.tp_recommencement.Value ? "1" : "0";
                    check_Recommencement.ToolTip = check_Recommencement.Text;

                    check_Previous_Employee.SelectedValue = trainingContract.tp_previous_employee == null ? "" : trainingContract.tp_previous_employee.Value ? "1" : "0";
                    check_Previous_Employee.ToolTip = check_Previous_Employee.Text;

                    txt_Nominal_Completion.Text = trainingContract.tp_nominal_completion_date == null ? "" : trainingContract.tp_nominal_completion_date.Value.ToString("yyyy/MM/dd");
                    txt_Nominal_Completion.ToolTip = txt_Nominal_Completion.Text;

                    txt_Contract_Substatus.Text = trainingContract.tp_Contract_Status_Subtype == null ? "" : trainingContract.tp_Contract_Status_Subtype.ToEntity<TP_type_contract_status_subtype>().TP_name;
                    txt_Contract_Substatus.ToolTip = txt_Contract_Substatus.Text;

                    txt_District.Text = trainingContract.tp_District == null ? "" : trainingContract.tp_District;
                    txt_District.ToolTip = txt_District.Text;

                    txt_Termination.Text = trainingContract.tp_termination_date == null ? "" : trainingContract.tp_termination_date.Value.ToString("yyyy/MM/dd");
                    txt_Termination.ToolTip = txt_Termination.Text;

                    txt_Probation_Completion.Text = trainingContract.tp_probation_completion == null ? "" : trainingContract.tp_probation_completion.Value.ToString("yyyy/MM/dd");
                    txt_Probation_Completion.ToolTip = txt_Probation_Completion.Text;

                    txt_Probation_Period.Text = trainingContract.TP_probation_period == null ? "" : trainingContract.TP_probation_period.ToString();
                    txt_Probation_Period.ToolTip = txt_Probation_Period.Text;

                    txt_Credit_Months.Text = trainingContract.tp_credit == null ? "" : trainingContract.tp_credit.Value.ToString();
                    txt_Credit_Months.ToolTip = txt_Credit_Months.Text;

                    txt_Full_Time_Employment_Start_Date.Text = trainingContract.tp_previous_fulltime_emp_start_date == null ? "" : trainingContract.tp_previous_fulltime_emp_start_date.Value.ToString("yyyy/MM/dd");
                    txt_Full_Time_Employment_Start_Date.ToolTip = txt_Full_Time_Employment_Start_Date.Text;

                    txt_Full_Time_Employment_Months.Text = trainingContract.tp_previous_fulltime_emp_months == null ? "" : trainingContract.tp_previous_fulltime_emp_months.Value.ToString();
                    txt_Full_Time_Employment_Months.ToolTip = txt_Full_Time_Employment_Months.Text;

                    txt_Part_Time_Employment_Start_Date.Text = trainingContract.tp_previous_parttime_emp_start_date == null ? "" : trainingContract.tp_previous_parttime_emp_start_date.Value.ToString("yyyy/MM/dd");
                    txt_Part_Time_Employment_Start_Date.ToolTip = txt_Part_Time_Employment_Start_Date.Text;

                    txt_Part_Time_Employment_Months.Text = trainingContract.tp_previous_parttime_emp_months == null ? "" : trainingContract.tp_previous_parttime_emp_months.Value.ToString();
                    txt_Part_Time_Employment_Months.ToolTip = txt_Part_Time_Employment_Months.Text;

                    txt_Casual_Time_Employment_Start_Date.Text = trainingContract.tp_previous_casual_emp_start_date == null ? "" : trainingContract.tp_previous_casual_emp_start_date.Value.ToString("yyyy/MM/dd");
                    txt_Casual_Time_Employment_Start_Date.ToolTip = txt_Casual_Time_Employment_Start_Date.Text;

                    txt_Casual_Time_Employment_Months.Text = trainingContract.tp_previous_casual_emp_months == null ? "" : trainingContract.tp_previous_casual_emp_months.Value.ToString();
                    txt_Casual_Time_Employment_Months.ToolTip = txt_Casual_Time_Employment_Months.Text;


                    txt_Qualification.Text = trainingContract.tp_Contract_Qualification == null ? "" : trainingContract.tp_Contract_Qualification.ToEntity<TP_type_qualification>().TP_name;
                    txt_Qualification.ToolTip = txt_Qualification.Text;

                    txt_Occupational_Model.Text = trainingContract.tp_Contract_Occupation == null ? "" : trainingContract.tp_Contract_Occupation.ToEntity<TP_type_occupation>().TP_name;
                    txt_Occupational_Model.ToolTip = txt_Occupational_Model.Text;

                    txt_Current_SRTO.Text = trainingContract.tp_Account_Contract_SRTO == null ? "" : trainingContract.tp_Account_Contract_SRTO.ToEntity<Account>().Name;
                    txt_Current_SRTO.ToolTip = txt_Current_SRTO.Text;

                    txt_Current_Mode.Text = trainingContract.tp_Contract_Mode == null ? "" : trainingContract.tp_Contract_Mode.ToEntity<TP_type_contract_mode>().TP_name;
                    txt_Current_Mode.ToolTip = txt_Current_Mode.Text;

                    txt_contractType.Text = trainingContract.tp_Contract_Type_Contract == null ? "" : trainingContract.tp_Contract_Type_Contract.ToEntity<TP_type_contract>().TP_name;
                    txt_contractType.ToolTip = txt_contractType.Text;

                    txt_Receipt_No.Text = trainingContract.tp_receipt_no == null ? "" : trainingContract.tp_receipt_no;
                    txt_Receipt_No.ToolTip = txt_Receipt_No.Text;

                    txt_contractStatus.Text = trainingContract.tp_Contract_Status == null ? "" : trainingContract.tp_Contract_Status.ToEntity<TP_type_contract_status>().TP_name;
                    txt_contractStatus.ToolTip = txt_contractStatus.Text;

                    txt_AAC.Text = trainingContract.tp_Account_Contract_AAC == null ? "" : trainingContract.tp_Account_Contract_AAC.ToEntity<Account>().Name;
                    txt_AAC.ToolTip = txt_AAC.Text;

                    check_State_Funding.SelectedValue = trainingContract.tp_state_funding == null ? "" : trainingContract.tp_state_funding.Value ? "1" : "0";
                    check_State_Funding.ToolTip = check_State_Funding.Text;

                    check_Existing_Worker.SelectedValue = trainingContract.tp_existing_worker == null ? "" : trainingContract.tp_existing_worker.Value ? "1" : "0";
                    check_Existing_Worker.ToolTip = check_Existing_Worker.Text;

                    txt_Full_Time_Employment_End_Date.Text = trainingContract.tp_previous_fulltime_emp_end_date == null ? "" : trainingContract.tp_previous_fulltime_emp_end_date.Value.ToString("yyyy/MM/dd");
                    txt_Full_Time_Employment_End_Date.ToolTip = txt_Full_Time_Employment_End_Date.Text;

                    txt_Part_Time_Employment_End_Date.Text = trainingContract.tp_previous_parttime_emp_end_date == null ? "" : trainingContract.tp_previous_parttime_emp_end_date.Value.ToString("yyyy/MM/dd");
                    txt_Part_Time_Employment_End_Date.ToolTip = txt_Part_Time_Employment_End_Date.Text;

                    txt_Part_Time_Employment_Hours.Text = trainingContract.tp_previous_parttime_emp_hours == null ? "" : trainingContract.tp_previous_parttime_emp_hours.Value.ToString();
                    txt_Part_Time_Employment_Hours.ToolTip = txt_Part_Time_Employment_Hours.Text;

                    txt_Casual_Time_Employment_End_Date.Text = trainingContract.tp_previous_casual_emp_end_date == null ? "" : trainingContract.tp_previous_casual_emp_end_date.Value.ToString("yyyy/MM/dd");
                    txt_Casual_Time_Employment_End_Date.ToolTip = txt_Casual_Time_Employment_End_Date.Text;

                    txt_Casual_Time_Employment_Hours.Text = trainingContract.tp_previous_casual_emp_hours == null ? "" : trainingContract.tp_previous_casual_emp_hours.Value.ToString();
                    txt_Casual_Time_Employment_Hours.ToolTip = txt_Casual_Time_Employment_Hours.Text;
                }
            }
        }

        protected void update_onClick(object sender, EventArgs e)
        {
            Guid contractId = new Guid(txt_contractId.Text);
            if (contractId != null)
            {
                TP_contract trainingContract = context.TP_contractSet.FirstOrDefault(contract => contract.TP_contractId == contractId);

                // Remove / add attribute
                trainingContract.Attributes.Remove("tp_commencement_date");
                trainingContract.Attributes.Add("tp_commencement_date", DateTime.ParseExact(txt_commencement.Text, "yyyy/MM/dd", null));

                trainingContract.Attributes.Remove("tp_nominal_completion_date");
                if (txt_Nominal_Completion.Text == "")
                {
                    trainingContract.Attributes.Add("tp_nominal_completion_date", null);
                }
                else
                {
                    trainingContract.Attributes.Add("tp_nominal_completion_date", DateTime.ParseExact(txt_Nominal_Completion.Text, "yyyy/MM/dd", null));
                }                

                trainingContract.Attributes.Remove("tp_termination_date");
                if (txt_Termination.Text == "")
                {
                    trainingContract.Attributes.Add("tp_termination_date", null);
                }
                else
                {
                    trainingContract.Attributes.Add("tp_termination_date", DateTime.ParseExact(txt_Termination.Text, "yyyy/MM/dd", null));
                }

                context.UpdateObject(trainingContract);
                context.SaveChanges();
            }
        }
    }
}