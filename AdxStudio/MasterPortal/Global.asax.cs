﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Profile;
using System.Web.Routing;
using System.Web.WebPages;
using Adxstudio.Xrm.Web;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.IdentityModel.Claims;

namespace Site
{
	public class Global : HttpApplication
	{
		void Application_Start(object sender, EventArgs e)
		{
			this.ConfigurePortal();

			//Uncomment if deploying to Azure and using Cloud Drive
			//Note: add the following using statements
			//using Microsoft.WindowsAzure.ServiceRuntime; 
			//using Microsoft.WindowsAzure.StorageClient;
			//if (RoleEnvironment.IsAvailable)
			//{
			//	var driveCache = RoleEnvironment.GetLocalResource("CloudDriveCache");
			//	CloudDrive.InitializeCache(driveCache.RootPath, driveCache.MaximumSizeInMegabytes);
			//}

			var areaRegistrationState = new PortalAreaRegistrationState();
			Application[typeof(IPortalAreaRegistrationState).FullName] = areaRegistrationState;

			AreaRegistration.RegisterAllAreas(areaRegistrationState);
			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);
		}

		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
		}

		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
		}

		protected void Application_PostAuthenticateRequest()
		{
			if (Request.IsAuthenticated && User.Identity.AuthenticationType == "Forms")
			{
				var identity = Context.User.Identity as ClaimsIdentity;

				if (identity != null)
				{
					identity.Claims.Add(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", User.Identity.AuthenticationType));
					identity.Claims.Add(new Claim(ClaimTypes.NameIdentifier, identity.Name));
					identity.NameClaimType = ClaimTypes.NameIdentifier;
				}
			}
		}

		public override string GetVaryByCustomString(HttpContext context, string arg)
		{
			switch (arg)
			{
				case "user":
					var availableDisplayModeIds = DisplayModeProvider.Instance
						.GetAvailableDisplayModesForContext(context.Request.RequestContext.HttpContext, null)
						.Select(mode => mode.DisplayModeId);

					var identity = context.User != null ? context.User.Identity.Name : string.Empty;

					return string.Format("IsAuthenticated={0},Identity={1},DisplayModes={2}",
						context.Request.IsAuthenticated,
						identity,
						string.Join(",", availableDisplayModeIds));
			}

			return base.GetVaryByCustomString(context, arg);
		}

		public void Profile_MigrateAnonymous(object sender, ProfileMigrateEventArgs e)
		{
			var portalAreaRegistrationState = Application[typeof (IPortalAreaRegistrationState).FullName] as IPortalAreaRegistrationState;

			if (portalAreaRegistrationState != null)
			{
				portalAreaRegistrationState.OnProfile_MigrateAnonymous(sender, e);
			}
		}

		void Application_EndRequest(object sender, EventArgs e)
		{
			this.RedirectOnStatusCode();
		}
	}
}
