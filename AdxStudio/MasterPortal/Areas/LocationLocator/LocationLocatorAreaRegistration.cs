﻿using System.Web.Mvc;

namespace Site.Areas.LocationLocator
{
	public class LocationLocatorAreaRegistration: AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "LocationLocator";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute("LocationLocatorMapSearch", "LocationLocator/Search/{longitude}/{latitude}/{distance}", new { controller = "Map", action = "Search", longitude = UrlParameter.Optional, latitude = UrlParameter.Optional, distance = UrlParameter.Optional });
		}
	}
}