﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="LocationLocatorMap.aspx.cs" Inherits="Site.Areas.LocationLocator.Pages.LocationLocatorMap" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>

<asp:Content runat="server" ContentPlaceHolderID="Head">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/LocationLocator/css/locator.css") %>" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<div class="page-heading">
		<% Html.RenderPartial("Breadcrumbs"); %>
		<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
		<div class="page-header">
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
		</div>
	</div>
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
	<div class="modal hide" style="width:440px" id="dialog-modal" title="Basic modal dialog" tabindex="-1" role="dialog" aria-labelledby="resolve-case-modal-label" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="directions-modal-label">
				Get Directions
			</h3>
		</div>
		<div class="modal-body direction-popup-control">
			<div class="control-group">
				<div class="form-inline">
					<strong><crm:Snippet runat="server" SnippetName="Location Map Get Directions From Text" DefaultText="From:" Editable="true" EditType="text" /></strong><br/>
					<input class="form-inline" id="directionFrom" type="text"/>
				</div>
				<a id="TaskHost_DrivingDirectionsWaypointReverse"
					href="#" 
					title="Reverse" 
					class="btn pull-right"
					onclick="ADX.locatorMap.reverseDirections();">
						<i class="icon-retweet">
						</i>
				</a>
			</div>
			<div class="control-group">
				<div class="form-inline">
					<strong><crm:Snippet runat="server" SnippetName="Location Map Get Directions To Text" DefaultText="To:" Editable="true" EditType="text" /></strong><br/>
					<input class="form-inline" id="directionTo" type="text"/>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<div class="controls">
				<input class="hidden" id="latitudeTo" type="hidden" />
				<input class="hidden" id="longitudeTo" type="hidden" />
				<input type="button" class="btn btn-primary form-inline directionsButton" 
					value="<%$ Snippet: Get Directions Button Text, Get Directions %>" 
					onclick="ADX.locatorMap.createDirections();" runat="server"/>
			</div>
		</div>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Path="~/xrm-adx/js/jquery-ui-1.9.2.min.js" />
			<asp:ScriptReference Path="~/Areas/LocationLocator/js/json2.min.js" />
			<asp:ScriptReference Path="~/Areas/LocationLocator/js/date.format.min.js" />
			<asp:ScriptReference Path="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0" />
			<asp:ScriptReference Path="~/Areas/LocationLocator/js/settings.js.aspx" />
			<asp:ScriptReference Path="~/Areas/LocationLocator/js/locator.map.js?v=1" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div id="searchOptions">
		<div class="form-inline">
			<div class="options form-inline">
				<strong><crm:Snippet runat="server" SnippetName="Location Map Search Text" DefaultText="Search for store close to:" Editable="true" EditType="text" /></strong><br/>
				<input class="form-inline" id="locationSearch" type="text"/>
			</div>
			<div class="options form-inline">
				<strong><crm:Snippet runat="server" SnippetName="Distance within to search Text" DefaultText="Within (km):" Editable="true" EditType="text" /></strong><br/>
				<asp:DropDownList class="form-inline input-mini span1 withinTxtDropDown" ID="withinTxtDropDown" type="text" runat="server">
				</asp:DropDownList>
				<input type="button" 
					class="btn form-inline" 
					value="<%$ Snippet: Location Search Reset Button Text, Reset %>" 
					onclick="ADX.locatorMap.reset();" runat="server" /> 
				<input type="button" class="btn btn-primary form-inline" 
					value="<%$ Snippet: Location Search Button Text, Search %>" 
					onclick="	ADX.locatorMap.locationSearch();" runat="server"/>
			</div>

		</div>
	</div>
	<div class="row">
		<div class="span4 pull-right locator-results" id="location-list">
			<table class="table table-hover unstyled">
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="span8" >
			<div id="mapContainer">
				<div id="locatorMap"></div>
			</div>
		</div>
	</div>
	<div id='directionsItinerary'> </div> 
	<div class="clearfix"></div>
	<script type="text/javascript">
		ADX.locatorMap.initialize('locatorMap', "<%: Url.Action("Search", "Map", new{ area = "LocationLocator" }) %>", true);
	</script>
</asp:Content>

