﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Site.Pages;
using Adxstudio.Xrm.Cms;

namespace Site.Areas.LocationLocator.Pages
{
	public partial class LocationLocatorMap : PortalPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string input = ServiceContext.GetSiteSettingValueByName(Website, "LocationLocator/DistanceDropDownValues") ?? "5, 25, 100"; 

			var stringArray = Regex.Split(input, @"\D+");

			foreach (var s in stringArray)
			{
				withinTxtDropDown.Items.Add(new ListItem(s, s));
			}
		}
	}
}