﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Adxstudio.Xrm.Mapping;
using Xrm;

namespace Site.Areas.LocationLocator.Controllers
{
	public class MapController : Controller
	{
		public class LocationMapNode
		{
			public LocationMapNode(string accountName, string location,double latitude, double longitude, string pushpinImageUrl)
			{
				AccountName = accountName;

				Location = location;

				Latitude = (decimal) latitude;

				Longitude = (decimal) longitude;

				PushpinImageUrl = pushpinImageUrl;
			}

			public string AccountName { get; set; }
			public string Title { get; set; }
			public string Location { get; set; }
			public string Status { get; set; }
			public int StatusId { get; set; }
			public string Priority { get; set; }
			public int PriorityId { get; set; }
			public DateTime? IncidentDate { get; set; }
			public DateTime? ScheduledDate { get; set; }
			public DateTime? ClosedDate { get; set; }
			public decimal Latitude { get; set; }
			public decimal Longitude { get; set; }
			public string PushpinImageUrl { get; set; }
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Search(double longitude, double latitude, double distance)
		{

			var originLatitude = latitude;
			var originLongitude = longitude;

			var radius = (int)distance;

			var context = new XrmServiceContext();

			var accounts = context.CreateQuery("adx_store")
				.Where(a => a.GetAttributeValue<double?>("adx_latitude") != null && a.GetAttributeValue<double?>("adx_longitude") != null)
				.FilterByBoxDistance(originLatitude, originLongitude, "adx_longitude", "adx_latitude", "double", radius, GeoHelpers.Units.Kilometers);

			var accountLocations = accounts.DistanceQuery(originLatitude, originLongitude, "adx_longitude", "adx_latitude", radius, GeoHelpers.Units.Kilometers).ToList();

			var serviceRequestMapNodes = new List<LocationMapNode>();

			if (accountLocations.Any())
			{
				serviceRequestMapNodes = accountLocations.Select(s =>
					new LocationMapNode(s.GetAttributeValue<string>("adx_name"), 
						string.Format("{0}, {1}, {2}", s.GetAttributeValue<string>("adx_address"),s.GetAttributeValue<string>("adx_city"), s.GetAttributeValue<string>("adx_country")),
						s.GetAttributeValue<Double?>("adx_latitude") ?? 0,
						s.GetAttributeValue<Double?>("adx_longitude") ?? 0,
						LocationLocatorHelpers.GetPushpinImageUrl(s))).ToList();
			}

			var json = Json(serviceRequestMapNodes);

			return json;
		}

	}

}
