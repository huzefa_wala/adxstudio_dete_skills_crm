﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Client;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Sdk.Client;
using Xrm;

namespace Site.Areas.LocationLocator
{
	public class LocationLocatorHelpers
	{
		public static string GetPushpinImageUrl(Entity entity)
		{
			var portalContext = PortalCrmConfigurationManager.CreatePortalContext();

			var context = portalContext.ServiceContext;

			var url = context.GetSiteSettingValueByName(portalContext.Website, "LocationLocator/defaultPushpinImageURL");

			if (string.IsNullOrWhiteSpace(url))
			{
				return string.Empty;
			}

			if (url.StartsWith("http", true, CultureInfo.InvariantCulture))
			{
				return url;
			}

			return WebsitePathUtility.ToAbsolute(portalContext.Website, url);
		}
	}
}