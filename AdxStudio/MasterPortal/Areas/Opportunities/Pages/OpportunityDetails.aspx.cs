﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Web.UI.WebControls;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Core;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Site.Pages;
using Xrm;

namespace Site.Areas.Opportunities.Pages
{
	public partial class OpportunityDetails : PortalPage
	{
		private Opportunity _opportunity;

		public Opportunity OpenOpportunity
		{
			get
			{
				if (_opportunity != null)
				{
					return _opportunity;
				}

				Guid opportunityId;

				if (!Guid.TryParse(Request["OpportunityID"], out opportunityId))
				{
					return null;
				}

				_opportunity = XrmContext.OpportunitySet.FirstOrDefault(o => o.Id == opportunityId);

				return _opportunity;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			var primaryContact = GetPrimaryContactAndSetCompanyName();

			if (primaryContact == null || OpenOpportunity.MSA_AllocatedtoPartner != (int)Adxstudio.Xrm.Partner.Enums.AllocatedToPartner.Yes)
			{
				//Push a content-snippet error message saying that the opportunity is corrupt.
				ErrorMessage.Visible = true;
				CrmEntityFormViewsPanel.Visible = false;
				OpportunityStatusPanel.Visible = false;
				return;
			}

			var contactFormViewDataSource = CreateDataSource("ContactWebFormDataSource", "contact", "contactid", primaryContact.Id);

			ContactFormView.DataSourceID = contactFormViewDataSource.ID;

			var opportunityDataSource = CreateDataSource("OpportunityDataSource", "opportunity", "opportunityid", OpenOpportunity.Id);

			OpportunityFormView.DataSourceID = opportunityDataSource.ID;

			//GetContactList();

			//GetLeadHistory();

			PipelinePhaseText.Text = OpenOpportunity.StepName;

			if (!IsPostBack)
			{
				GetContactList();

				GetLeadHistory();

				BindPipelinePhaseDetails();
			}

			BindProductsLeadNotesContactsAndAssignedTo();

			if (!(OpenOpportunity.adx_PartnerCreated ?? false))
			{
				CancelOpportunity.Visible = false;
				CancelDetails.Visible = false;
				//CancelButton.Visible = false;
				//AddContactCheckBox.Visible = false;
				//AddContactList.Visible = false;
			}
			else
			{
				ReturnToNetwork.Visible = false;
				ReasonForReturn.Visible = false;
				//ReasonForReturnSource.Visible = false;
			}

			AddContactButton.QueryStringCollection = CreateCustomerContactQueryString();

		}

		protected void CancelButton_Click(object sender, EventArgs e)
		{
			Response.Redirect(Request.RawUrl);
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}
			var accessPermissions = XrmContext.GetOpportunityAccessByContact(Contact).Cast<adx_opportunitypermissions>();

			bool canSave = false;

			foreach (var adxOpportunitypermissionse in accessPermissions)
			{
				if (adxOpportunitypermissionse.adx_Write ?? false)
				{
					canSave = true;
				}
			}
			if (canSave == true)
			{
				ContactFormView.UpdateItem();

				OpportunityFormView.UpdateItem();
			}

		}

		protected void ContactUpdating(object senders, CrmEntityFormViewUpdatingEventArgs e)
		{

		}

		protected void OpportunityUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e)
		{
			if (UpdatePipelinePhase.Checked)
			{
				e.Values["stepname"] = PipelinePhase.SelectedItem.Text;
				e.Values["salesstagecode"] = int.Parse(PipelinePhase.SelectedValue);

				//var processCode = PipelinePhase.SelectedValue;
			}
			else if (ReturnToNetwork.Checked)
			{
				e.Values["closeprobability"] = 0;
				e.Values["adx_reasonforreturn"] = ReasonForReturn.SelectedIndex + 100000000;
			}

			e.Values["description"] = OpportunityNotes.Text;

			Guid id;

			if ((AssignToList != null && !String.IsNullOrEmpty(AssignToList.SelectedValue)) && Guid.TryParse(AssignToList.SelectedItem.Value, out id))
			{
				e.Values["msa_partneroppid"] = id;
			}

		}

		//TODO: REFACTOR
		protected void OpportunityUpdated(object sender, CrmEntityFormViewUpdatedEventArgs e)
		{
			var context = new XrmServiceContext();

			var opportunity = context.OpportunitySet.First(o => o.Id == e.Entity.Id);

			var partner = context.AccountSet.First(p => p.Id == opportunity.msa_partnerid.Id);

			//var partnerDetails = partner.adx_account_partnerdetails.First();

			if (partner.adx_NumberofOpportunitieswithFeedback == null)
			{
				partner.adx_NumberofOpportunitieswithFeedback = 0;
			}

			if ((partner.adx_NumberofOpportunitiesAccepted ?? 0) == 0)
			{
				partner.adx_NumberofOpportunitiesAccepted = 1;
			}

			adx_opportunitynote oppnote = new adx_opportunitynote();

			adx_opportunitynote oppnote2 = new adx_opportunitynote();

			if (UpdatePipelinePhase.Checked)
			{
				context.SetOpportunityStatusAndSave(opportunity, "Open", 0);
				opportunity.StatusCode = (int)Adxstudio.Xrm.Partner.Enums.OpportunityStatusReason.InProgress;
				if (opportunity.adx_FeedbackYet != true)
				{
					if (!(opportunity.adx_PartnerCreated ?? false))
					{
						partner.adx_NumberofOpportunitieswithFeedback = partner.adx_NumberofOpportunitieswithFeedback + 1;
						partner.adx_FeedbackRate = (double)partner.adx_NumberofOpportunitieswithFeedback / (double)(partner.adx_NumberofOpportunitiesAccepted ?? 1.0);
						opportunity.adx_FeedbackYet = true;
					}
				}

				oppnote.adx_name = PipelinePhase.SelectedItem.Text;
				oppnote.adx_Date = DateTime.UtcNow;
				oppnote.adx_Description = PipelineUpdateDetails.Text;
				//set the date, and description once you can get the damn codegen working!

				//add the Opportun ityNote entity

			}
			else if (WinOpportunity.Checked)
			{
				context.SetOpportunityStatusAndSave(opportunity, "Won", 0);
				opportunity.StatusCode = (int)Adxstudio.Xrm.Partner.Enums.OpportunityStatusReason.Purchased;
				if (opportunity.adx_FeedbackYet != true)
				{
					if (!(opportunity.adx_PartnerCreated ?? false))
					{
						partner.adx_NumberofOpportunitieswithFeedback = partner.adx_NumberofOpportunitieswithFeedback + 1;
						partner.adx_FeedbackRate = (double)partner.adx_NumberofOpportunitieswithFeedback / (double)(partner.adx_NumberofOpportunitiesAccepted ?? 1.0);
						opportunity.adx_FeedbackYet = true;
					}
				}

				opportunity.adx_WonDate = DateTime.UtcNow;
				var wonSetting = XrmContext.Adx_sitesettingSet.Where(ss => ss.Adx_name == "Won Opportunity Note").FirstOrDefault();
				var wonNote = "Won";
				wonNote = (wonSetting != null) ? wonSetting.Adx_Value : wonNote;


				oppnote.adx_name = wonNote;
				oppnote.adx_Date = DateTime.UtcNow;
				oppnote.adx_Description = WonDetails.Text;

				//add the OpportunityNote entity
			}
			else if (CancelOpportunity.Checked)
			{
				context.SetOpportunityStatusAndSave(opportunity, "Lost", 0);
				opportunity.StatusCode = (int)Adxstudio.Xrm.Partner.Enums.OpportunityStatusReason.Canceled;
				if (opportunity.adx_FeedbackYet != true)
				{
					if (!(opportunity.adx_PartnerCreated ?? false))
					{
						partner.adx_NumberofOpportunitieswithFeedback = partner.adx_NumberofOpportunitieswithFeedback + 1;
						partner.adx_FeedbackRate = (double)partner.adx_NumberofOpportunitieswithFeedback / (double)(partner.adx_NumberofOpportunitiesAccepted ?? 1.0);
						opportunity.adx_FeedbackYet = true;
					}
				}

				var cancelSetting = XrmContext.Adx_sitesettingSet.Where(ss => ss.Adx_name == "Cancel Opportunity Note").FirstOrDefault();
				var cancelNote = "Canceled";
				cancelNote = (cancelSetting != null) ? cancelSetting.Adx_Value : cancelNote;

				oppnote.adx_name = cancelNote;
				oppnote.adx_Date = DateTime.UtcNow;
				oppnote.adx_Description = CancelDetails.Text;

				//add the OpportunityNote entity
			}
			else if (AddContactCheckBox.Checked)
			{
				Guid selectedGuid = new Guid(AddContactList.SelectedItem.Value);

				var contact = context.ContactSet.Where(c => c.Id == selectedGuid).FirstOrDefault();

				var contactCrossover =
					opportunity.adx_opportunity_contact.Where(c => c.ContactId == contact.ContactId).FirstOrDefault();

				if (contactCrossover == null)
				{
					context.AddLink(opportunity, new Relationship("adx_opportunity_contact"), contact);

					oppnote2.adx_name = "Contact Added: " + contact.FullName;
					oppnote2.adx_Date = DateTime.UtcNow;
					oppnote2.adx_Description = "Contact Added: " + contact.FullName;

					context.UpdateObject(contact);
				}

				//var opportunity = OpenOpportunity;
			}
			else if (ReturnToNetwork.Checked)
			{
				context.SetOpportunityStatusAndSave(opportunity, "Lost", 0);
				opportunity.StatusCode = (int)Adxstudio.Xrm.Partner.Enums.OpportunityStatusReason.Returned;
				if (partner.adx_NumberofReturnedOpportunities == null)
				{
					partner.adx_NumberofReturnedOpportunities = 0;
				}
				if (!(opportunity.adx_PartnerCreated ?? false))
				{
					partner.adx_NumberofReturnedOpportunities = partner.adx_NumberofReturnedOpportunities + 1;
					partner.adx_ReturnRate = (double)partner.adx_NumberofReturnedOpportunities / (double)(partner.adx_NumberofOpportunitiesAccepted ?? 1.0);
				}

				var returnSetting = XrmContext.Adx_sitesettingSet.Where(ss => ss.Adx_name == "Return Opportunity Note").FirstOrDefault();
				var returnNote = "Returned to Network";
				returnNote = (returnSetting != null) ? returnSetting.Adx_Value : returnNote;


				oppnote.adx_name = returnNote;
				oppnote.adx_Date = DateTime.UtcNow;
				oppnote.adx_Description = ReasonForReturn.SelectedItem.Text;

				//add the OpportunityNote entity
			}

			var calculatePartnerDetailsAction = new adx_calculatepartnercapacityworkflowaction() { adx_AccountId = partner.ToEntityReference() };

			if (!String.IsNullOrEmpty(oppnote.adx_name))
			{
				oppnote.adx_OpportunityId = opportunity.ToEntityReference();
				oppnote.adx_AssignedTo = (opportunity.msa_contact_opportunity != null) ? opportunity.msa_contact_opportunity.FullName : "";
				context.AddObject(oppnote);
			}
			if (!String.IsNullOrEmpty(oppnote2.adx_name))
			{
				oppnote2.adx_OpportunityId = opportunity.ToEntityReference();
				oppnote2.adx_AssignedTo = (opportunity.msa_contact_opportunity != null) ? opportunity.msa_contact_opportunity.FullName : "";
				context.AddObject(oppnote2);
			}
			var oppID = opportunity.OpportunityId;

			context.UpdateObject(partner);
			context.UpdateObject(opportunity);
			context.SaveChanges();

			if (!(opportunity.adx_PartnerCreated ?? false))
			{
				context.AddObject(calculatePartnerDetailsAction);
			}

			context.SaveChanges();

			var opp = context.OpportunitySet.Where(o => o.OpportunityId == oppID).FirstOrDefault();

			if (opp != null)
			{
				CurrentlyAssignedToLabel.Text = (opp.msa_contact_opportunity != null) ? opp.msa_contact_opportunity.FullName : "";
				PipelinePhaseText.Text = opp.StepName;
			}

			DisableControlsBasedOnPipelinePhaseAndAccessPermissions();

			BindPipelinePhaseDetails();

			GetLeadHistory();

			GetContactList();

			ConfirmationMessage.Visible = true;
		}

		private void AddHistoryDiv(OpportunityHistory history)
		{
			var div = new HtmlGenericControl("div")
			{
				InnerHtml = string.Format(@"<span class=""stage-date"">{0}</span><span class=""stage-name"">{1}</span>{2}{3}",
					history.NoteCreatedOn.ToString("ddd, dd MMM yyyy HH:mm:ss 'GMT'"),
					history.Name.Substring(history.Name.IndexOf('-') + 1),
					!string.IsNullOrEmpty(history.PartnerAssignedTo)
						? string.Format(@"<span class=""stage-assigned-to"">{0}{1}</span>", "Assigned To:  ", Server.HtmlEncode(history.PartnerAssignedTo))
						: string.Empty,
					!string.IsNullOrEmpty(history.Details)
						? string.Format(@"<div class=""stage-details"">{0}</div>", Server.HtmlEncode(history.Details))
						: string.Empty)
			};

			// add div at the beginning for reverse chronological order
			OpportunityHistoryPlaceHolder.Controls.AddAt(0, div);
		}

		private void AddContactDiv(Contact contact)
		{
			var account = (contact.ParentCustomerId != null) ?
				ServiceContext.AccountSet.Where(a => a.AccountId == contact.ParentCustomerId.Id).FirstOrDefault() : null;

			var companyName = account != null ? account.Name : contact.Adx_OrganizationName;

			HtmlGenericControl div;

			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			var channelWriteAccess = (channelPermission != null && channelPermission.adx_Write.GetValueOrDefault());

			var channelReadAccess = (channelPermission != null && channelPermission.adx_Read.GetValueOrDefault());

			var parentAccount = (account != null && account.msa_managingpartnerid != null) ?
				ServiceContext.AccountSet.Where(a => a.GetAttributeValue<Guid>("accountid") == account.msa_managingpartnerid.Id).FirstOrDefault() : null;

			string contactFormattedString = "";

			if ((parentAccount != null && channelPermission != null && (channelPermission.adx_AccountId.Id == parentAccount.AccountId)) ||
					(contact.msa_managingpartnerid != null && channelPermission != null && contact.msa_managingpartnerid.Id == channelPermission.adx_AccountId.Id))
			{
				if (channelWriteAccess)
				{
					contactFormattedString = string.Format(@"<i class=""icon-pencil""></i><a href=""{0}"" class=""Edit"">{1}</a>",
						EditContactUrl(contact.ContactId),
						contact.FullName);
				}
				else if (channelReadAccess)
				{
					contactFormattedString = string.Format(@"<a href=""{0}"">{1}</a>",
						ReadOnlyContactUrl(contact.ContactId),
						contact.FullName);
				}
			}
			else
			{
				contactFormattedString = contact.FullName;
			}

			div = new HtmlGenericControl("div")
			{

				InnerHtml = string.Format(@"<span class=""contact-name"">{0}</span>{1}",
					contactFormattedString,
					!string.IsNullOrEmpty(companyName)
					? string.Format(@"<span class=""contact-company-name"">{0}</span>", Server.HtmlEncode(companyName)) : string.Empty)
			};

			// add div at the beginning for reverse chronological order
			OpportunityContactsPlaceHolder.Controls.AddAt(0, div);
		}

		private void GetLeadHistory()
		{
			foreach (var history in XrmContext.GetOpportunityHistories(OpenOpportunity))
			{
				AddHistoryDiv(history);
			}
		}

		private void GetContactList()
		{
			var contacts = OpenOpportunity.adx_opportunity_contact;

			foreach (var contact in contacts)
			{
				AddContactDiv(contact);
			}
		}

		//TODO: Refactor this!
		private void BindProductsLeadNotesContactsAndAssignedTo()
		{
			CurrentlyAssignedToLabel.Text = (OpenOpportunity.msa_contact_opportunity != null) ? OpenOpportunity.msa_contact_opportunity.FullName : "";

			if (IsPostBack)
			{
				return;
			}

			Products.Text = string.Join(", ", OpenOpportunity.adx_opportunity_product.Select(product => product.Name));

			OpportunityNotes.Text = GetFormattedDescription(OpenOpportunity.Description);

			//LeadAssignedTo.Text = OpenOpportunity.adx_PartnerAssignedTo;

			var empli = new ListItem();

			AssignToList.Items.Add(empli);

			//var contacts = XrmContext.GetContactsForContact(Contact).Cast<Contact>();

			AssertContactHasParentAccount();

			var contacts = XrmContext.ContactSet.Where(c => c.ParentCustomerId.Id == Contact.ParentCustomerId.Id);

			foreach (var contact in contacts)
			{
				if (contact.StateCode == 0)
				{
					var li = new ListItem()
					{
						Text = contact.FullName,
						Value = contact.ContactId.ToString()
					};

					if ((OpenOpportunity.msa_partneroppid != null) && (li.Value == OpenOpportunity.msa_partneroppid.Id.ToString()))
					{
						li.Selected = true;
					}

					AssignToList.Items.Add(li);
				}
			}

			var partnerAccount = ServiceContext.AccountSet.FirstOrDefault(a => a.AccountId == Contact.ParentCustomerId.Id);

			if (partnerAccount == null)
			{
				throw new Exception("No Parnter Account Found");
			}

			var customerContacts = ServiceContext.ContactSet;  //TODO: Refactor this Query

			//var currentContacts = OpenOpportunity.adx_opportunity_contact;

			//.Where(c => c.contact_customer_accounts.msa_managingpartnerid.Id == partnerAccount.Id 
			//                                                || c.msa_managingpartnerid.Id == partnerAccount.Id);

			//TODO: REFACTOR THIS CODE!!!!

			foreach (var customer in customerContacts)
			{
				if (
					((customer.contact_customer_accounts != null) && (customer.contact_customer_accounts.msa_managingpartnerid != null) &&
					customer.contact_customer_accounts.msa_managingpartnerid.Id == partnerAccount.Id)
					||
					((customer.msa_managingpartnerid != null) && customer.msa_managingpartnerid.Id == partnerAccount.Id))
				{
					Contact customer1 = customer;
					var contactCrossover = OpenOpportunity.adx_opportunity_contact.FirstOrDefault(c => c.ContactId == customer1.ContactId);

					if (contactCrossover == null)
					{
						var li = new ListItem()
						{
							Text = customer.FullName,
							Value = customer.ContactId.ToString()
						};

						AddContactList.Items.Add(li);
					}
				}
			}

			if (AddContactList.Items.Count < 1)
			{
				AddContactList.Visible = false;
				AddContactCheckBox.Visible = false;
			}

		}

		private void BindPipelinePhaseDetails()
		{

			PipelinePhase.Items.Clear();

			var response = (RetrieveAttributeResponse)ServiceContext.Execute(new RetrieveAttributeRequest
			{
				EntityLogicalName = "opportunity",
				LogicalName = "salesstagecode"
			});

			var picklist = response.AttributeMetadata as PicklistAttributeMetadata;
			if (picklist == null)
			{
				return;
			}

			var phase = 0;

			foreach (var option in picklist.OptionSet.Options)
			{
				var text = option.Label.UserLocalizedLabel.Label;
				var value = option.Value.Value.ToString();

				if (text == OpenOpportunity.StepName)
				{
					phase = option.Value.Value;
				}
			}

			foreach (var option in picklist.OptionSet.Options)
			{
				var li = new ListItem()
				{
					Text = option.Label.UserLocalizedLabel.Label,
					Value = option.Value.Value.ToString()
				};

				if (option.Value.Value >= phase)
				{
					bool GoodToGo = true;

					foreach (ListItem item in PipelinePhase.Items)
					{
						if (item.Text == li.Text)
						{
							GoodToGo = false;
						}
					}

					if (GoodToGo)
					{
						PipelinePhase.Items.Add(li);
					}
				}

				if (li.Text == OpenOpportunity.StepName)
				{
					li.Selected = true;
				}

			}
			DisableControlsBasedOnPipelinePhaseAndAccessPermissions();
		}

		private CrmDataSource CreateDataSource(string dataSourceId, string entityName, string entityIdAttribute, Guid? entityId)
		{
			var formViewDataSource = new CrmDataSource
			{
				ID = dataSourceId,
				FetchXml = string.Format(@"<fetch mapping='logical'> <entity name='{0}'> <all-attributes /> <filter type='and'> <condition attribute = '{1}' operator='eq' value='{{{2}}}'/> </filter> </entity> </fetch>",
					entityName,
					entityIdAttribute,
					entityId)
			};

			CrmEntityFormViewsPanel.Controls.Add(formViewDataSource);

			return formViewDataSource;
		}

		private void DisableControlsBasedOnPipelinePhaseAndAccessPermissions()
		{
			//TODO: Disable based on status and also permissions

			var accessPermissions = XrmContext.GetOpportunityAccessByContact(Contact).Cast<adx_opportunitypermissions>();

			//CreateCaseLink.Visible = false;
			AssignToList.Visible = false;
			//AssignToContact.Visible = false;
			OpportunityStatusPanel.Visible = false;
			SubmitButton.Visible = false;
			CancelOpportunity.Visible = false;

			foreach (var access in accessPermissions)
			{
				if (access.adx_Write.GetValueOrDefault())
				{
					//CreateCaseLink.Visible = true;
					SubmitButton.Visible = true;
					OpportunityStatusPanel.Visible = true;
				}

				if (access.adx_Assign.GetValueOrDefault())
				{
					AssignToList.Visible = true;
					//AssignToContact.Visible = true;
				}

				if (access.adx_Delete.GetValueOrDefault())
				{
					CancelOpportunity.Visible = true;
				}

			}

			CurrentlyAssignedToLabel.Visible = !AssignToList.Visible;

			if ((OpenOpportunity.StateCode != null) &&
				((OpenOpportunity.StateCode.Value == (int)Adxstudio.Xrm.Partner.Enums.OpportunityState.Lost) ||
				(OpenOpportunity.StateCode.Value == (int)Adxstudio.Xrm.Partner.Enums.OpportunityState.Won)))
			{
				CrmEntityFormViewsPanel.Enabled = false;
				OpportunityStatusPanel.Enabled = false;
			}

			//TODO: Adding contacts should be based on channel permissions

		}

		private static string GetFormattedDescription(string description)
		{
			if (string.IsNullOrWhiteSpace(description))
			{
				return string.Empty;
			}

			var numbering = new Regex(@"(?= \d?\d\) )");

			return numbering.Replace(description, "\n\n");
		}

		/*TODO:Refactor this*/
		private Contact GetPrimaryContactAndSetCompanyName()
		{
			if (OpenOpportunity == null)
			{
				return null;
			}

			Contact primaryContact = null;

			var customer = OpenOpportunity.CustomerId;

			if (customer.LogicalName == "account")
			{
				var account = XrmContext.AccountSet.First(a => a.Id == customer.Id);

				CompanyName.Text = account.Name;

				primaryContact = account.account_primary_contact;

				var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

				var channelWriteAccess = (channelPermission != null && channelPermission.adx_Write.GetValueOrDefault());

				var channelReadAccess = (channelPermission != null && channelPermission.adx_Read.GetValueOrDefault());

				var parentAccount = (account.msa_managingpartnerid != null) ? XrmContext.AccountSet.Where(a => a.GetAttributeValue<Guid>("accountid") == account.msa_managingpartnerid.Id).FirstOrDefault() : null;

				if (parentAccount != null && ((channelPermission != null) && channelPermission.adx_AccountId.Id == parentAccount.AccountId)
					&& parentAccount.AccountClassificationCode.GetValueOrDefault() == 100000000)
				{
					if (channelWriteAccess)
					{
						CompanyName.Text = string.Format(@"<a href=""{0}"" class=""Edit"">{1}</a>",
						EditAccountUrl(account.AccountId),
						CompanyName.Text);
					}
					else if (channelReadAccess)
					{
						CompanyName.Text = string.Format(@"<a href=""{0}"" class=""Edit"">{1}</a>",
						ReadOnlyAccountUrl(account.AccountId),
						CompanyName.Text);
					}
				}


				//CompanyName.Attributes.Add("style", "white-space: nowrap;");

			}
			else if (customer.LogicalName == "contact")
			{
				primaryContact = XrmContext.ContactSet.First(c => c.Id == customer.Id);

				var account = primaryContact.account_primary_contact.FirstOrDefault();

				CompanyName.Text = account != null ? account.Name : primaryContact.Adx_OrganizationName;
			}

			return primaryContact;
		}

		protected string EditAccountUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Edit Customer Account");

			if (page == null) { return " "; }

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("AccountID", id.ToString());

			return url;
		}

		protected string ReadOnlyAccountUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Read Only Account View");

			if (page == null) { return " "; }

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("AccountID", id.ToString());

			return url;
		}

		protected QueryStringCollection CreateCustomerContactQueryString()
		{
			var queryStringCollection = new QueryStringCollection("");

			var oppId = OpenOpportunity.OpportunityId;

			var accountid = OpenOpportunity.CustomerId.Id;

			queryStringCollection.Set("OpportunityId", oppId.ToString());

			queryStringCollection.Set("AccountId", accountid.ToString());

			return queryStringCollection;
		}

		protected string EditContactUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Edit Contact");

			if (page == null) { return " "; }

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("ContactID", id.ToString());

			return url;
		}

		protected string ReadOnlyContactUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Read Only Contact View");

			if (page == null) { return " "; }

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("ContactID", id.ToString());

			return url;
		}

	}
}