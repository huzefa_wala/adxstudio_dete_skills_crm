﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Data;
using Adxstudio.Xrm.Partner;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using Site.Helpers;
using Xrm;

namespace Site.Areas.Opportunities.Pages
{
	public partial class AcceptedOpportunities : PortalPage
	{
		private DataTable _opportunities;

		protected string SortDirection
		{
			get { return ViewState["SortDirection"] as string ?? "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		protected string SortExpression
		{
			get { return ViewState["SortExpression"] as string ?? "Accepted"; }
			set { ViewState["SortExpression"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			AssertContactHasParentAccount();

			var opportunities = Enumerable.Empty<Entity>();

			if (string.Equals(CustomerFilter.Text, "My", StringComparison.InvariantCulture))
			{
				opportunities = ServiceContext.GetOpportunitiesSpecificToContact(Contact)
					.Where(o => o.GetAttributeValue<int>("msa_allocatedtopartner") == (int)Adxstudio.Xrm.Partner.Enums.AllocatedToPartner.Yes);
			}
			else //if (string.Equals(CustomerFilter.Text, "All", StringComparison.InvariantCulture))
			{
				opportunities = ServiceContext.GetOpportunitiesForContact(Contact)
					.Where(o => o.GetAttributeValue<int>("msa_allocatedtopartner") == (int)Adxstudio.Xrm.Partner.Enums.AllocatedToPartner.Yes &&
					o.GetAttributeValue<Guid>("msa_partnerid") == Contact.ParentCustomerId.Id);
			}
			//var searchQuery = Request["query"];

			if (!IsPostBack)
			{
				PopulateCustomerFilter(ServiceContext, Contact);
			}

			HideControlsBasedOnAccess(ServiceContext, Contact);

			if (string.Equals(StatusDropDown.Text, "Open", StringComparison.InvariantCulture))
			{
				opportunities = opportunities.Where(o => o.GetAttributeValue<int>("statecode") == (int)Adxstudio.Xrm.Partner.Enums.OpportunityState.Open);
			}
			else if (string.Equals(StatusDropDown.Text, "Won", StringComparison.InvariantCulture))
			{
				opportunities = opportunities.Where(o => o.GetAttributeValue<int>("statecode") == (int)Adxstudio.Xrm.Partner.Enums.OpportunityState.Won);
			}
			else if (string.Equals(StatusDropDown.Text, "Lost", StringComparison.InvariantCulture))
			{
				opportunities = opportunities.Where(o => o.GetAttributeValue<int>("statecode") == (int)Adxstudio.Xrm.Partner.Enums.OpportunityState.Lost);
			}

			if (!string.IsNullOrEmpty(SearchText.Text))
			{
				opportunities =
					from o in opportunities
					join a in ServiceContext.CreateQuery("account") on o.GetAttributeValue<Guid>("customerid") equals
						a.GetAttributeValue<Guid>("accountid")
					where
						a.GetAttributeValue<string>("name").ToLower().Contains(SearchText.Text.ToLower()) ||
							(!string.IsNullOrEmpty(o.GetAttributeValue<string>("adx_referencecode")) &&
								o.GetAttributeValue<string>("adx_referencecode").IndexOf(SearchText.Text.ToLower(), StringComparison.OrdinalIgnoreCase) >= 0)
					select o;
			}

			_opportunities = EnumerableExtensions.CopyToDataTable(opportunities.Cast<Opportunity>().Select(opp => new
			{
				opportunityid = opp.Id,
				ID = opp.adx_ReferenceCode,
				Accepted = opp.adx_AcceptedDate.HasValue ? opp.adx_AcceptedDate.Value.ToString("yyyy/MM/dd") : null,
				CompanyName = opp.opportunity_customer_accounts.Name,
				City = opp.opportunity_customer_accounts.Address1_City,
				Territory = (opp.adx_territory_opportunity != null) ? opp.adx_territory_opportunity.Name : " ",
				Products = string.Join(", ", opp.adx_opportunity_product.Select(product => product.Name)),
				EstRevenue = opp.EstimatedValue.HasValue ? opp.EstimatedValue.Value.ToString("C") : null,
				EstClose = opp.EstimatedCloseDate.HasValue ? opp.EstimatedCloseDate.Value.ToString("yyyy/MM/dd") : null,
				AssignedTo = (opp.msa_contact_opportunity != null) ? opp.msa_contact_opportunity.FullName : " ",
				Status = Enum.GetName(typeof(Adxstudio.Xrm.Partner.Enums.OpportunityState), opp.StateCode),
			}).OrderBy(opp => opp.CompanyName));

			_opportunities.Columns["ID"].ColumnName = "Topic";
			_opportunities.Columns["CompanyName"].ColumnName = "Company Name";
			_opportunities.Columns["EstRevenue"].ColumnName = "Est. Revenue";
			_opportunities.Columns["EstClose"].ColumnName = "Est. Purchase";
			_opportunities.Columns["AssignedTo"].ColumnName = "Assigned To";

			AcceptedOpportunitiesList.DataKeyNames = new[] { "opportunityid" };
			AcceptedOpportunitiesList.DataSource = _opportunities;
			AcceptedOpportunitiesList.DataBind();
		}

		protected void LeadsList_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header ||
				e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Cells[0].Visible = false;
			}

			if (e.Row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var dataKey = AcceptedOpportunitiesList.DataKeys[e.Row.RowIndex].Value;

			e.Row.Cells[1].Text = string.Format(@"<i class=""{0}""></i> <a href=""{1}"" >{2}</a>",
					GetAlertType(dataKey),
					OpportunityDetailsUrl(dataKey),
					e.Row.Cells[1].Text);

			//e.Row.Cells[1].Attributes.Add("style", "white-space: nowrap;");

			foreach (var cell in e.Row.Cells.Cast<DataControlFieldCell>().Where(cell => cell.ContainingField.HeaderText == "Est. Revenue"))
			{
				decimal parsedDecimal;

				if (decimal.TryParse(cell.Text, out parsedDecimal))
				{
					cell.Text = parsedDecimal.ToString("C");
				}
			}
		}

		protected string OpportunityDetailsUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Opportunity Details");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("OpportunityID", id.ToString());

			return url.PathWithQueryString;
		}

		protected void ExportButton_Click(object sender, EventArgs args)
		{
			_opportunities.ExportToCsv("AcceptedOpportunities.csv", Context, new[] { "opportunityid" });
		}

		protected void AcceptedOpportunitiesList_Sorting(object sender, GridViewSortEventArgs e)
		{
			SortDirection = e.SortExpression == SortExpression
				? (SortDirection == "ASC" ? "DESC" : "ASC")
				: "ASC";

			SortExpression = e.SortExpression;

			_opportunities.DefaultView.Sort = string.Format("{0} {1}", SortExpression, SortDirection);

			AcceptedOpportunitiesList.DataSource = _opportunities;
			AcceptedOpportunitiesList.DataBind();
		}

		private string GetAlertType(object dataKey)
		{
			Guid id;

			Guid.TryParse(dataKey.ToString(), out id);

			var alertType = ServiceContext.GetAlertType(id, Website);

			if (alertType.ToString() == Enums.AlertType.Overdue.ToString()) return "icon-exclamation-sign";

			if (alertType.ToString() == Enums.AlertType.PotentiallyStalled.ToString()) return "icon-warning-sign";

			return "";
		}

		private void PopulateCustomerFilter(OrganizationServiceContext context, Entity contact)
		{
			CustomerFilter.Items.Clear();

			//var accounts = ServiceContext.GetAccountsRelatedToOpportunityAccessForContact(Contact).Cast<Account>().OrderBy(a => a.Name);

			//var accessPermissions = context.GetOpportunityAccessByContact(contact).Cast<adx_opportunitypermissions>();


			CustomerFilter.Items.Add(new ListItem("All"));

			CustomerFilter.Items.Add(new ListItem("My"));

			//foreach (var account in accounts)
			//{
			//    CustomerFilter.Items.Add(new ListItem(account.Name, account.AccountId.ToString()));
			//}
		}

		private void HideControlsBasedOnAccess(OrganizationServiceContext context, Entity contact)
		{
			var accessPermissions = context.GetOpportunityAccessByContact(contact).Cast<adx_opportunitypermissions>();

			//CreateCaseLink.Visible = false;
			StatusDropDown.Visible = false;
			CustomerFilter.Visible = false;
			ViewLabel.Visible = false;
			NoOpportunityAccessLabel.Visible = true;
			CreateButton.Visible = false;

			foreach (var access in accessPermissions)
			{
				if (access.adx_Create.GetValueOrDefault())
				{
					//CreateCaseLink.Visible = true;
					StatusDropDown.Visible = true;
					ViewLabel.Visible = true;
					NoOpportunityAccessLabel.Visible = false;

					if (access.adx_Scope != null && access.adx_Scope.Value == (int)Adxstudio.Xrm.Partner.Enums.OpportunityAccessScope.Account)
					{
						CustomerFilter.Visible = true;
					}

					CreateButton.Visible = true;
				}

				if (access.adx_Read.GetValueOrDefault())
				{
					StatusDropDown.Visible = true;
					ViewLabel.Visible = true;
					NoOpportunityAccessLabel.Visible = false;

					if (access.adx_Scope != null && access.adx_Scope.Value == (int)Adxstudio.Xrm.Partner.Enums.OpportunityAccessScope.Account)
					{
						CustomerFilter.Visible = true;
					}
				}
			}

			AcceptedOpportunitiesList.Visible = !NoOpportunityAccessLabel.Visible;
			SearchText.Visible = !NoOpportunityAccessLabel.Visible;
			SearchButton.Visible = !NoOpportunityAccessLabel.Visible;
			ExportBtn.Visible = !NoOpportunityAccessLabel.Visible;
		}

	}
}