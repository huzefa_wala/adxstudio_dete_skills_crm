﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebForms.master" CodeBehind="OpportunityDetails.aspx.cs" Inherits="Site.Areas.Opportunities.Pages.OpportunityDetails" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Opportunities/css/opportunity-details.css") %>">
</asp:Content>

<asp:Content  ContentPlaceHolderID="PageHeader" ViewStateMode="Enabled" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity1" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header form-inline" >
		<div id="opportunity-assigned-to" class="form-inline">
			<crm:Snippet runat="server" SnippetName="opportunity-details/label/partner-assigned-to" DefaultText="Assigned to:" Editable="true" EditType="text" Literal="true" />
			<asp:Label ID="CurrentlyAssignedToLabel" runat="server"></asp:Label>
			<asp:DropDownList ID="AssignToList" runat="server" ClientIDMode="Static" />
		</div>
		<h1>
			<crm:Property DataSourceID="CurrentEntity1" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
<div id="opportunity-ui" style="display: none">
	<br/><br/>

	<div class="row">
			
	<asp:Panel ID="OpportunityStatusPanel" CssClass="span4 pull-right" runat="server">
		
		<div id="opportunities-status-tabs">
			<ul class="nav nav-tabs" >
				<li id="opportunities-details-status-tab">
					<a href="#opportunities-details-status">
						<crm:Snippet runat="server" SnippetName="opportunitiesdetails/label/status-section" DefaultText="Opportunity Status" Editable="true" EditType="text" /></a>
				</li>
			</ul>
			<div id="opportunities-details-status" class="well">
				<div>
					<h5>
					<asp:Literal Text="<%$ Snippet: current_pipeline_phase, Current Phase: %>" runat="server" />
					<asp:Literal ID="PipelinePhaseText" runat="server"></asp:Literal>
					</h5>
				</div>
				<div class="form-inline radio">
					<asp:RadioButton ID="UpdatePipelinePhase" Text="Update Pipeline Phase" GroupName="StatusReason" ClientIDMode="Static" runat="server" />
					<div class="clearfix"></div>
					<asp:DropDownList ID="PipelinePhase" runat="server" ClientIDMode="Static" />
					<asp:TextBox ID="PipelineUpdateDetails" runat="server" TextMode="MultiLine" ClientIDMode="Static" />
				</div>
				<div class="form-inline radio">
					<asp:RadioButton ID="WinOpportunity" Text="Declare as Won" GroupName="StatusReason" ClientIDMode="Static" runat="server" />
					<div class="clearfix"></div>
					<asp:TextBox ID="WonDetails" runat="server" TextMode="MultiLine" ClientIDMode="Static" />
					<div id="won-opportunity-message" title="Declare Opportunity as Won" style="display: none;">
						<crm:Snippet runat="server" SnippetName="opportunities-details/win-opportunity/warning" DefaultText="This action will declare the opportunity as Won." Editable="false" />
					</div>
				</div>
				<div class="form-inline radio" id="return-parent-div">
					<asp:RadioButton ID="ReturnToNetwork" Text="Return to Network" GroupName="StatusReason" ClientIDMode="Static" runat="server" />
					<div class="clearfix"></div>
					<crm:CrmMetadataDataSource ID="ReasonForReturnSource" runat="server" AttributeName="adx_reasonforreturn" EntityName="opportunity" />
					<asp:DropDownList ID="ReasonForReturn" runat="server" DataSourceID="ReasonForReturnSource" DataTextField="OptionLabel" DataValueField="OptionValue" ClientIDMode="Static" />
					<div id="return-to-network-message" title="Return Opportunity to Distributor" style="display: none;">
						<crm:Snippet runat="server" SnippetName="opportunities-details/return-to-network/warning" DefaultText="opportunities-details/return-to-network/warning" Editable="false" />
					</div>
				</div>
				<div class="form-inline radio">
					<asp:RadioButton ID="CancelOpportunity" Text="Cancel Opportunity" GroupName="StatusReason" ClientIDMode="Static" runat="server" />
					<div class="clearfix"></div>
					<asp:TextBox ID="CancelDetails" runat="server" TextMode="MultiLine" ClientIDMode="Static" />
					<div id="cancel-opportunity-message" title="This cancels the opportunity." style="display: none;">
						<crm:Snippet runat="server" SnippetName="opportunities-details/win-opportunity/warning" DefaultText="This action will declare the opportunity as Won." Editable="false" />
					</div>
				</div>
			</div>
		</div>
	</asp:Panel>

	<asp:Panel ID="CrmEntityFormViewsPanel" CssClass="span8 pull-left" runat="server">
		<div id="opportunities-details-tabs" >
			<ul class="nav nav-tabs">
				<li id="opportunities-details-form-views-tab">
					<a href="#opportunities-details-form-views" data-toggle="tab">
					<crm:Snippet runat="server" SnippetName="opportunities-details/label/details-tab" DefaultText="Opportunity Details" Editable="true" EditType="text" /></a>
				</li>
				<li id="opportunities-history-tab">
					<a href="#opportunities-history" data-toggle="tab">
					<crm:Snippet runat="server" SnippetName="opportunities-details/label/history-tab" DefaultText="Opportunity History" Editable="true" EditType="text" /></a>
				</li>
				<li id="opportunities-notes-tab">
					<a href="#opportunities-notes" data-toggle="tab">
					<crm:Snippet runat="server" SnippetName="opportunities-details/label/notes-tab" DefaultText="Opportunity Notes" Editable="true" EditType="text" /></a>
				</li>
				<li id="opportunities-contacts-tab">
					<a href="#opportunities-contacts" data-toggle="tab">
					<crm:Snippet runat="server" SnippetName="opportunities-details/label/contacts-tab" DefaultText="Opportunity Contacts" Editable="true" EditType="text" /></a>
				</li>
			</ul>
			<div id="opportunities-details-content-pane" class ="tab-content">
				<div id="opportunities-details-form-views" class="tab-pane fade">
					<div class="cell">
						<div class="info"><label><crm:Snippet runat="server" SnippetName="opportunities-details/label/company-name" DefaultText="Customer" Editable="true" EditType="text" /></label></div>
						<div class="control"><i class="icon-pencil"></i><asp:Label ID="CompanyName" runat="server"></asp:Label></div>
					</div>
					<adx:CrmEntityFormView ID="ContactFormView" runat="server"
						EntityName="contact"
						FormName="Opportunity Contact Details Form"
						Mode="Edit"
						ValidationGroup="UpdateOpportunity"
						OnItemUpdating="ContactUpdating">
						<UpdateItemTemplate/>
					</adx:CrmEntityFormView>
					<div class="cell">
						<div class="info"><label><crm:Snippet runat="server" SnippetName="opportunities-details/label/products" DefaultText="Products" Editable="true" EditType="text" /></label></div>
						<div class="control"><asp:TextBox ID="Products" runat="server" ReadOnly="true" CssClass="readonly"></asp:TextBox></div>
					</div>
					<adx:CrmEntityFormView ID="OpportunityFormView" runat="server"
						EntityName="opportunity"
						FormName="Opportunity Details Web Form"
						Mode="Edit"
						ValidationGroup="UpdateOpportunity"
						OnItemUpdated="OpportunityUpdated"
						OnItemUpdating="OpportunityUpdating">
						<UpdateItemTemplate/>
					</adx:CrmEntityFormView>
				</div>
				<div id="opportunities-history" class="tab-pane fade">
					<asp:PlaceHolder ID="OpportunityHistoryPlaceHolder" runat="server"></asp:PlaceHolder>
				</div>
				<div id="opportunities-notes" class="tab-pane fade">
					<asp:TextBox ID="OpportunityNotes" runat="server" TextMode="MultiLine" />
				</div>
				<div id="opportunities-contacts" class="tab-pane fade">
					<asp:PlaceHolder ID="OpportunityContactsPlaceHolder" runat="server"></asp:PlaceHolder>
					<div class="form-inline">
						<div class="info"><label><crm:Snippet runat="server" SnippetName="addingcontacts/instructions" DefaultText="You may add an existing contact to this opportunity if any exist, or create a new contact." Editable="true" EditType="text" /></label></div>
						<asp:CheckBox ID="AddContactCheckBox" Text="Add an existing contact to this opportunity" TextAlign="Right" ClientIDMode="Static" CssClass="form-inline checkbox" runat="server" />
						<div class="clearfix"></div>
						<asp:DropDownList ID="AddContactList" runat="server" ClientIDMode="Static" />
						<adx:SiteMarkerLinkButton ID="AddContactButton"  runat="server" SiteMarkerName="Create Customer Contact" CssClass="btn btn-success" >
							<i class="icon-white icon-plus-sign"></i> Create New
						</adx:SiteMarkerLinkButton>
					</div>
				</div>
			</div>
		</div>
	</asp:Panel>

	</div>

	<asp:Panel ID="ConfirmationMessage" runat="server" Visible="false" CssClass="alert alert-success opportunity-saved-message">
			<crm:Snippet runat="server" SnippetName="opportunities-details/message/saved-successfully" DefaultText="Opportunity saved successfully." Editable="true" EditType="text" />
	</asp:Panel>
	<asp:ValidationSummary runat="server" ValidationGroup="UpdateOpportunity" CssClass="alert alert-error" DisplayMode="List" />
	<asp:Panel ID="ErrorMessage" runat="server" Visible="false">
		<p>
			There is a problem with this Opportunity. The information for this opportunity appears to be corrupt or missing.</p>
	</asp:Panel>
		<br/>
	<div class="form-actions form-inline">
			<asp:Button ID="SubmitButton" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="SubmitButton_Click" ValidationGroup="UpdateOpportunity" />
			<asp:Button ID="CancelButton" runat="server" CssClass="btn" Text="Cancel" OnClick="CancelButton_Click" />
	</div>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script type="text/javascript" src="~/js/jquery.cookie.js"></script>
	<script type="text/javascript">
		$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
		$(function () {
			//$("#opportunities-details-tabs").tabs({ cookie: {} });
			//$("#opportunities-status-tabs").tabs();
			

			$("#opportunities-details-tabs").tab();
			$("#opportunities-details-form-views-tab a").tab("show");
			$("#opportunities-status-tabs").tab();
			$("#opportunities-details-status-tab a").tab("show");

			$("#opportunities-details-tabs").show();
			$("#opportunities-status-tabs").show();

			$("#opportunities-history div span.stage-date").each(function () {
				var dateTime = new Date($(this).text());
				$(this).text(dateTime.toString("MMM d, yyyy h:mm tt"));
			});

			$("form").submit(function () {
				if (Page_IsValid) {
					$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
				}
			});

			setTimeout(function () {
				$("div.opportunity-saved-message").hide("fade", {}, 1000);
			}, 5000);

			$("#ReasonForReturn").hide();

			$("#PipelinePhase").hide();
			$("#PipelineUpdateDetails").hide();
			$("#WonDetails").hide();
			$("#CancelDetails").hide();

			$("#UpdatePipelinePhase").click(function () {
				$("#WonDetails").hide();
				$("#CancelDetails").hide();
				$("#ReasonForReturn").hide();

				$("#PipelinePhase").show("slide");
				$("#PipelineUpdateDetails").show("slide");
			});

			$("#WinOpportunity").click(function () {
				//				$("#won-opportunity-message").dialog({
				//					modal: true,
				//					close: function () {
				//						$("#WonDetails").show("slide");
				//					}
				//				});
				$("#ReasonForReturn").hide();
				$("#PipelinePhase").hide();
				$("#PipelineUpdateDetails").hide();
				$("#CancelDetails").hide();

				$("#WonDetails").show("slide");
			});
			$("#CancelOpportunity").click(function () {
				//				$("#cancel-opportunity-message").dialog({
				//					modal: true,
				//					close: function () {
				//						$("#CancelDetails").show("slide");
				//					}
				//				});
				$("#ReasonForReturn").hide();
				$("#PipelinePhase").hide();
				$("#PipelineUpdateDetails").hide();
				$("#WonDetails").hide();

				$("#CancelDetails").show("slide");
			});
			$("#ReturnToNetwork").click(function () {
				//				$("#return-to-network-message").dialog({
				//					modal: true,
				//					close: function() {
				//						$("#ReasonForReturn").show("slide");
				//					}
				//				});
				$("#PipelinePhase").hide();
				$("#PipelineUpdateDetails").hide();
				$("#WonDetails").hide();
				$("#CancelDetails").hide();

				$("#ReasonForReturn").show("slide");
			});

			$("#opportunities-details-form-views-tab a").click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});

			$("#opportunities-history-tab a").click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});

			$("#opportunities-notes-tab a").click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});

			$("#opportunities-contacts-tab a").click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});

			$('input[type="submit"]').click(function () {
				$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
			});

			$("#opportunity-ui").show();
			$.unblockUI();
		});

		function clickRadioAndSlide(radio) {
			radio.click();
			var label = $("#" + radio.attr("id") + " + label");
			$("#ReasonForReturn").hide();
			$("#PipelinePhase").hide();
			$("#PipelineUpdateDetails").hide();
			$("#WonDetails").hide();
			$("#CancelDetails").hide();
			label.hide();
			label.show("slide");
		}
	</script>
</asp:Content>
