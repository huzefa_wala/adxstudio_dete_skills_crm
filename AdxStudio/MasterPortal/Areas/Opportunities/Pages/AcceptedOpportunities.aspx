﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="AcceptedOpportunities.aspx.cs" Inherits="Site.Areas.Opportunities.Pages.AcceptedOpportunities" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Opportunities/css/opportunities.css") %>">
</asp:Content>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<asp:LinkButton ID="ExportBtn" CssClass="btn pull-right" style="margin-left: 5px;" runat="server" OnClick="ExportButton_Click" >
		<i class="icon icon-list-alt"></i><asp:Literal runat="server" Text="<%$ Snippet: accepted-opportunities/export-to-excel-link, Export to Excel %>"></asp:Literal>
		</asp:LinkButton>
		<adx:SiteMarkerLinkButton ID="CreateButton" runat="server" SiteMarkerName="Create Opportunity" CssClass="btn btn-success pull-right" >
			<i class="icon-white icon-plus-sign"></i> Create New
		</adx:SiteMarkerLinkButton>
		<h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<div id="filters" class="pull-right form-inline">
		<asp:Label ID="ViewLabel" Font-Bold="true" runat="server">View: </asp:Label>
		<asp:DropDownList ID="CustomerFilter" AutoPostBack="true" runat="server">
		</asp:DropDownList>
		<asp:DropDownList ID="StatusDropDown" AutoPostBack="true" Width="100px" runat="server">
			<asp:ListItem>Open</asp:ListItem>
			<asp:ListItem>All</asp:ListItem>
			<asp:ListItem>Won</asp:ListItem>
			<asp:ListItem>Lost</asp:ListItem>
		</asp:DropDownList>
	</div>
	<div id="search" class="form-search">
		<asp:TextBox ID="SearchText" runat="server" CssClass="input-medium search-query pull-left"/>&nbsp;
		<asp:Button ID="SearchButton" runat="server" Text="<%$ Snippet: SearchButton/Search, Search%>" CssClass="btn" />
	</div>
	<div class="clearfix"></div>
	<crm:Snippet ID="NoOpportunityAccessLabel" runat="server" SnippetName="accepted-opportunities/no_access" DefaultText="You do not have Opportunity  Permissions." Editable="true" EditType="html" />
	<div id="accepted-opportunities">
		<asp:GridView ID="AcceptedOpportunitiesList" runat="server"
			 CssClass="table table-striped" GridLines="None" AlternatingRowStyle-CssClass="alternate-row" 
			AllowSorting="true" OnSorting="AcceptedOpportunitiesList_Sorting" OnRowDataBound="LeadsList_OnRowDataBound" >
			<EmptyDataRowStyle CssClass="empty" />
			<EmptyDataTemplate>
				<crm:Snippet runat="server" SnippetName="accepted-opportunities/list/empty" DefaultText="There are no items to display." Editable="true" EditType="html" />
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
	<crm:Snippet runat="server" SnippetName="accepted-opportunities/legend" DefaultText="accepted-opportunities/legend" Editable="true" EditType="html" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
		<script type="text/javascript">
		$(function () {
			$(".tabular-data tr").not(":has(th)").click(function () {
				window.location.href = $(this).find("a").attr("href");
			});

			$(".tabular-data td.accepted-date").each(function () {
				var dateTime = new Date($(this).text());
				$(this).text(dateTime.toString("yyyy/MM/dd HH:mm"));
			});

			if ($("#search input.text").val().length == 0) {
				$("#icon_clear").hide();
			}

			$("#search input.text").keyup(function () {
				if ($("#search input.text").val().length > 0) {
					$("#icon_clear").fadeIn(300);
				}
				else {
					$("#icon_clear").fadeOut(300);
				}
			});

			$("#icon_clear").click(function () {
				$("#search input.text").val("");
				$("#search input.button").click();
			});

			$("form").submit(function () {
				blockUI();
			});

			$("#filters select").change(function () {
				blockUI();
			});

			$(".tabular-data th a").click(function () {
				blockUI();
			});
		});

		function blockUI() {
			$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
		}
	</script>
</asp:Content>