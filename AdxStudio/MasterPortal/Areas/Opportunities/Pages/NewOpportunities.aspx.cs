﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Data;
using Adxstudio.Xrm.Partner;
using Microsoft.Xrm.Sdk;
using Site.Pages;
using Xrm;

namespace Site.Areas.Opportunities.Pages
{
	public partial class NewOpportunities : PortalPage
	{

		private List<string> _acceptChecked = new List<string>();
		private List<string> _declineChecked = new List<string>();

		//private DataTable _opportunities;

		protected DataTable Opportunities
		{
			get { return ViewState["NewOpportunities"] as DataTable ?? GetOpportunities(); }
			set { ViewState["NewOpportunities"] = value; }
		}

		protected string SavedOpportunities
		{
			get { return ViewState["SavedOpportunities"] as string ?? string.Empty; }
			set { ViewState["SavedOpportunities"] = value; }
		}

		protected string SortDirection
		{
			get { return ViewState["SortDirection"] as string ?? "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		protected string SortExpression
		{
			get { return ViewState["SortExpression"] as string ?? "Delivered"; }
			set { ViewState["SortExpression"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			AssertContactHasParentAccount();

			Opportunities.DefaultView.Sort = string.Format("{0} {1}", SortExpression, SortDirection);

			NewOpportunitiesList.DataKeyNames = new[] { "opportunityid" };
			NewOpportunitiesList.DataSource = Opportunities;
			NewOpportunitiesList.DataBind();
		}

		protected void OpportunitiesList_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header)
			{
				e.Row.Cells[0].Visible = false;

				e.Row.Cells.Add(new TableHeaderCell { Text = "Accept" });
				e.Row.Cells.Add(new TableHeaderCell { Text = "Decline" });

				return;
			}

			if (e.Row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			e.Row.Cells[0].Visible = false;

			var dataKey = NewOpportunitiesList.DataKeys[e.Row.RowIndex].Value.ToString();

			//e.Row.Cells[1].Attributes.Add("style", "white-space: nowrap;");

			foreach (var cell in e.Row.Cells.Cast<DataControlFieldCell>().Where(cell => cell.ContainingField.HeaderText == "Est. Revenue"))
			{
				decimal parsedDecimal;

				if (decimal.TryParse(cell.Text, out parsedDecimal))
				{
					cell.Text = parsedDecimal.ToString("C");
				}
			}

			var oppPermissions = XrmContext.GetOpportunityAccessByContactForParentAccount(Contact).Where(oa =>
	oa.GetAttributeValue<bool?>("adx_acceptdecline") ?? false == true);

			var acceptCell = new TableCell { HorizontalAlign = HorizontalAlign.Center, Width = new Unit("1%") };
			var acceptCheckBox = new CheckBox { ID = dataKey + "_accept", CssClass = "accept" };
			acceptCheckBox.InputAttributes["class"] = dataKey;
			acceptCell.Controls.Add(acceptCheckBox);
			e.Row.Cells.Add(acceptCell);

			var declineCell = new TableCell { HorizontalAlign = HorizontalAlign.Center, Width = new Unit("1%") };
			var declineCheckBox = new CheckBox { ID = dataKey + "_decline", CssClass = "decline" };
			declineCheckBox.InputAttributes["class"] = dataKey;
			declineCell.Controls.Add(declineCheckBox);
			e.Row.Cells.Add(declineCell);

			if (!oppPermissions.ToList().Any())
			{
				acceptCheckBox.Enabled = false;
				declineCheckBox.Enabled = false;
			}
		}

		protected void NewOpportunitiesList_Sorting(object sender, GridViewSortEventArgs e)
		{
			StoreCheckedValues();

			SortDirection = e.SortExpression == SortExpression
				? (SortDirection == "ASC" ? "DESC" : "ASC")
				: "ASC";

			SortExpression = e.SortExpression;

			Opportunities.DefaultView.Sort = string.Format("{0} {1}", SortExpression, SortDirection);

			NewOpportunitiesList.DataSource = Opportunities;
			NewOpportunitiesList.DataBind();

			RestoreCheckedValues();
		}

		protected void SaveButton_Click(object sender, EventArgs args)
		{
			foreach (GridViewRow row in NewOpportunitiesList.Rows)
			{
				var dataKey = new Guid(NewOpportunitiesList.DataKeys[row.RowIndex].Value.ToString());

				var cellCount = row.Cells.Count;

				var acceptCell = row.Cells[cellCount - 2];

				var accept = acceptCell.FindControl(dataKey + "_accept") as CheckBox;

				var declineCell = row.Cells[cellCount - 1];

				var decline = declineCell.FindControl(dataKey + "_decline") as CheckBox;

				var opportunity = XrmContext.OpportunitySet.Where(o => o.OpportunityId == dataKey).FirstOrDefault();

				if (SavedOpportunities.Contains(dataKey.ToString()) ||
					(!accept.Checked && !decline.Checked) || (accept.Checked && decline.Checked))
				{
					continue;
				}

				var partner = XrmContext.AccountSet.First(a => a.Id == opportunity.msa_partnerid.Id);

				var oppPermissions = XrmContext.GetOpportunityAccessByContactForParentAccount(Contact).Where(oa =>
					oa.GetAttributeValue<bool?>("adx_acceptdecline") ?? false == true);

				if ((partner.adx_NumberofOpportunitiesDelivered ?? 0) == 0)
				{
					partner.adx_NumberofOpportunitiesDelivered = 1;
				}


				if (accept.Checked)
				{
					if (oppPermissions.ToList().Any())
					{

						//we mark this opportunity as accepted
						opportunity.MSA_AllocatedtoPartner = (int)Adxstudio.Xrm.Partner.Enums.AllocatedToPartner.Yes;
						opportunity.StatusCode = (int)Adxstudio.Xrm.Partner.Enums.OpportunityStatusReason.Accepted;
						opportunity.StepName = OpportunityHistory.PipelinePhaseAccepted;
						opportunity.adx_AcceptedDate = DateTime.UtcNow;

						if (partner.adx_NumberofOpportunitiesAccepted == null)
						{
							partner.adx_NumberofOpportunitiesAccepted = 0;
						}
						partner.adx_NumberofOpportunitiesAccepted = partner.adx_NumberofOpportunitiesAccepted + 1;
						partner.adx_TouchRate =
							(double)(partner.adx_NumberofOpportunitiesAccepted + (partner.adx_NumberofOpportunitiesDeclined ?? 0.0)) /
							(partner.adx_NumberofOpportunitiesDelivered ?? 1.0);
					}
				}
				else if (decline.Checked)
				{
					if (oppPermissions.ToList().Any())
					{
						//we mark this opportunity as declined
						opportunity.MSA_AllocatedtoPartner = (int)Adxstudio.Xrm.Partner.Enums.AllocatedToPartner.No; ;
						opportunity.StatusCode = (int)Adxstudio.Xrm.Partner.Enums.OpportunityStatusReason.Declined;
						opportunity.StepName = OpportunityHistory.PipelinePhaseDeclined;

						if (partner.adx_NumberofOpportunitiesDeclined == null)
						{
							partner.adx_NumberofOpportunitiesDeclined = 0;
						}
						partner.adx_NumberofOpportunitiesDeclined = partner.adx_NumberofOpportunitiesDeclined + 1;
						partner.adx_ActiveOpportunityCount = partner.adx_ActiveOpportunityCount - 1;
						partner.adx_TouchRate =
							(double)((partner.adx_NumberofOpportunitiesAccepted ?? 0) + partner.adx_NumberofOpportunitiesDeclined) /
							(partner.adx_NumberofOpportunitiesDelivered ?? 1.0);

						XrmContext.AddLink(opportunity, new Relationship("adx_account_declinedopportunity"), partner);
					}
				}

				opportunity.adx_ExpiryDate = null;

				XrmContext.UpdateObject(opportunity);
				XrmContext.UpdateObject(partner);
				XrmContext.SaveChanges();

				SavedOpportunities += dataKey + ",";
			}

			RestoreCheckedValues();
			//Response.Redirect(Request.RawUrl);
		}

		private DataTable GetOpportunities()
		{
			var opportunities = ServiceContext.GetOpportunitiesForContact(Contact)
			.Where(o => o.GetAttributeValue<int>("msa_allocatedtopartner") != 1 &&
				o.GetAttributeValue<Guid>("msa_partnerid") == Contact.ParentCustomerId.Id &&
				o.GetAttributeValue<int>("statuscode") == (int)Adxstudio.Xrm.Partner.Enums.OpportunityStatusReason.Delivered);

			Opportunities = EnumerableExtensions.CopyToDataTable(opportunities.Cast<Opportunity>().Select(opp => new
			{
				opportunityid = opp.Id,
				ID = opp.adx_ReferenceCode,
				Delivered = opp.adx_DeliveredDate.HasValue ? opp.adx_DeliveredDate.Value.ToString("yyyy/MM/dd") : null,
				CompanyName = opp.opportunity_customer_accounts.Name,
				City = opp.opportunity_customer_accounts.Address1_City,
				Territory = (opp.adx_territory_opportunity != null) ? opp.adx_territory_opportunity.Name : " ",
				Products = string.Join(", ", opp.adx_opportunity_product.Select(product => product.Name)),
				EstRevenue = opp.EstimatedValue.HasValue ? opp.EstimatedValue.Value.ToString("C") : null,
				/*EstClose = opp.EstimatedCloseDate.HasValue ? opp.EstimatedCloseDate.Value.ToString("yyyy/MM/dd") : null,*/
				Expires = opp.adx_ExpiryDate.HasValue ? opp.adx_ExpiryDate.Value.ToString("yyyy/MM/dd") : null,
			}).OrderBy(opp => opp.Delivered).ThenBy(opp => opp.CompanyName));

			Opportunities.Columns["ID"].ColumnName = "Topic";
			Opportunities.Columns["CompanyName"].ColumnName = "Company Name";
			Opportunities.Columns["EstRevenue"].ColumnName = "Est. Revenue";
			//_opportunities.Columns["EstClose"].ColumnName = "Est. Purchase";

			return Opportunities;
		}

		private void RestoreCheckedValues()
		{
			foreach (GridViewRow row in NewOpportunitiesList.Rows)
			{
				var dataKey = NewOpportunitiesList.DataKeys[row.RowIndex].Value.ToString();

				var cellCount = row.Cells.Count;

				if (_acceptChecked.Contains(dataKey))
				{
					var acceptCell = row.Cells[cellCount - 2];

					var accept = acceptCell.FindControl(dataKey + "_accept") as CheckBox;

					accept.Checked = true;
				}

				if (_declineChecked.Contains(dataKey))
				{
					var declineCell = row.Cells[cellCount - 1];

					var decline = declineCell.FindControl(dataKey + "_decline") as CheckBox;

					decline.Checked = true;
				}

				if (SavedOpportunities.Contains(dataKey))
				{
					row.CssClass = "saved" + (row.RowIndex % 2 == 1 ? " alternate-row" : string.Empty);
				}
			}
		}

		private void StoreCheckedValues()
		{
			foreach (GridViewRow row in NewOpportunitiesList.Rows)
			{
				var dataKey = NewOpportunitiesList.DataKeys[row.RowIndex].Value.ToString();

				var cellCount = row.Cells.Count;

				var acceptCell = row.Cells[cellCount - 2];

				var accept = acceptCell.FindControl(dataKey + "_accept") as CheckBox;

				if (accept.Checked)
				{
					_acceptChecked.Add(dataKey);
				}

				var declineCell = row.Cells[cellCount - 1];

				var decline = declineCell.FindControl(dataKey + "_decline") as CheckBox;

				if (decline.Checked)
				{
					_declineChecked.Add(dataKey);
				}
			}
		}
	}
}