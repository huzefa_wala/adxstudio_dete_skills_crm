﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebForms.master" CodeBehind="CreateNewOpportunity.aspx.cs" Inherits="Site.Areas.Opportunities.Pages.CreateNewOpportunity" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled"  runat="server">
	<asp:Label runat="server" ID="InsufficientPermissions" Visible="false" />
	<div id="create-opportunity-section">
		<asp:Panel ID="OpportunityDetailsPanel"  runat="server">
			<asp:Label ID="InstructionTxt" CssClass="control-group" runat="server"><crm:Snippet runat="server" SnippetName="CreateOpp/SelectClientMsg" DefaultText="Select a customer account as the potential client for the opportunity:" /></asp:Label>
			<div class="control-group well">
				<asp:LinkButton ID="CreateCustomerButton"  
					runat="server" 
					CssClass="btn btn-success pull-right" 
					OnClick="CreateCustomerButton_Click" ><i class="icon-white icon-plus-sign"></i>
					<asp:Literal runat="server" Text="<%$ Snippet: CreateOpp/CreateNewCustomer, Create New Customer %>"></asp:Literal></asp:LinkButton>
				<asp:DropDownList runat="server" ID="Account_dropdown" 
					AppendDataBoundItems="true" >
					<asp:ListItem Selected="True" Text="- Accounts -" Value="" />
				</asp:DropDownList>
				<%--<crm:CrmHyperLink SiteMarkerName="Create Customer Account" Text="<%$ Snippet: links/create-customer, Create New Customer %>" runat="server"></crm:CrmHyperLink>--%>
			</div>
			<adx:CrmDataSource ID="WebFormDataSource" runat="server" />
			<div id="createOpportunity">
				<adx:CrmEntityFormView ID="createOpp" runat="server" DataSourceID="WebFormDataSource" EntityName="opportunity" FormName="Opportunity Create Form"
					ValidationGroup="CreateOpportunity" OnItemInserting="OnItemInserting" CssClass="crmEntityFormView" OnItemInserted="OnItemInserted" Mode="Insert" >
					<InsertItemTemplate>
						<div class="actions">
							<asp:Button Text='<%$ Snippet: CreateOpp/Submit, Submit %>' CssClass="btn btn-primary" CommandName="Insert" CausesValidation="true" ValidationGroup="CreateOpportunity" runat="server" />
						</div>
					</InsertItemTemplate>
				</adx:CrmEntityFormView>
			</div>
		</asp:Panel>
	</div>
	<script type="text/javascript">
		$(function () {
			$("#createOpportunity").hide();
			$("#MainContent_ContentBottom_Account_dropdown").change(function () {
				$("#createOpportunity").show("slide");
			});
		});
	</script>
</asp:Content>
