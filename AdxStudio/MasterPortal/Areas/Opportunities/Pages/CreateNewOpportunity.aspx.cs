﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Partner;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Portal.Web.UI.WebControls;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Site.Pages;
using Xrm;

namespace Site.Areas.Opportunities.Pages
{
	public partial class CreateNewOpportunity : PortalPage
	{

		public TextBox PotentialCustomer
		{
			get { return (TextBox)createOpp.FindControl("PotentialCustomer"); }
		}

		private Xrm.Account _account;

		public Xrm.Account ParentCustomerAccount
		{
			get
			{
				if (_account != null)
				{
					return _account;
				}

				Guid accountId;

				if (!Guid.TryParse(Request["AccountID"], out accountId))
				{
					return null;
				}

				_account = XrmContext.AccountSet.FirstOrDefault(c => c.Id == accountId);

				return _account;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			if (Page.IsPostBack)
			{
				return;
			}

			//createOpp.Visible = false;

			var partnerContact = XrmContext.MergeClone(Contact);

			var partnerCustomerAccount = partnerContact.contact_customer_accounts;

			HideCreateButtonIfNoChannelAccess(partnerCustomerAccount);

			if (!ConfirmCreatePermissions(partnerCustomerAccount)) return;

			var accounts = ServiceContext.AccountSet
				.Where(a => a.msa_managingpartnerid.Id == partnerCustomerAccount.AccountId)
				.Where(a => a.PrimaryContactId != null)
				.OrderBy(a => a.Name)
				.Select(a => a);

			if (!accounts.ToList().Any())
			{
				Account_dropdown.Visible = false;

				InstructionTxt.Text = "No Customer accounts with Primary contacts set are availalbe to choose from.";
			}
			else
			{
				Account_dropdown.DataSource = accounts;

				Account_dropdown.DataTextField = "name";

				Account_dropdown.DataValueField = "accountid";

				Account_dropdown.DataBind();

				if (ParentCustomerAccount != null)
				{
					Account_dropdown.ClearSelection();

					foreach (ListItem li in Account_dropdown.Items)
					{
						Guid id;

						if (Guid.TryParse(li.Value, out id) && id == ParentCustomerAccount.AccountId)
						{
							li.Selected = true;
						}
					}
				}
			}

			if (string.IsNullOrEmpty(CreateCustomerAccountUrl()))
			{
				CreateCustomerButton.Visible = false;
			}

		}

		private void HideCreateButtonIfNoChannelAccess(Xrm.Account partnerCustomerAccount)
		{
			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			var channelCreateAccess = (channelPermission != null && channelPermission.adx_Create.GetValueOrDefault());

			if (!channelCreateAccess || partnerCustomerAccount == null ||
				(channelPermission.adx_AccountId.Id != partnerCustomerAccount.AccountId)
				|| partnerCustomerAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
			{
				CreateCustomerButton.Visible = false;
			}
		}

		private bool ConfirmCreatePermissions(Xrm.Account partnerCustomerAccount)
		{
			var opportunityPermissions = XrmContext.GetOpportunityAccessByContact(Contact).Cast<adx_opportunitypermissions>();

			bool createAccess = false;

			foreach (var access in opportunityPermissions)
			{
				var opportunityCreateAccess = (access != null && access.adx_Create.GetValueOrDefault());

				if (opportunityCreateAccess)
				{
					createAccess = true;
				}
			}

			if (!createAccess || partnerCustomerAccount == null ||
				partnerCustomerAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
			{
				InsufficientPermissions.Text = ServiceContext.GetSnippetValueByName(Website, "opportunity/create/nopermissions");

				InsufficientPermissions.Visible = true;

				//SelectClientPanel.Visible = false;

				return false;
			}

			OpportunityDetailsPanel.Visible = !InsufficientPermissions.Visible;
			return true;
		}

		protected void SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!String.IsNullOrEmpty(Account_dropdown.SelectedValue))
			{
				createOpp.Visible = true;
				CreateCustomerButton.Visible = false;
			}

		}

		protected void OnItemInserting(object sender, CrmEntityFormViewInsertingEventArgs e)
		{
			var currentContact = XrmContext.MergeClone(Contact);

			if (currentContact == null)
			{
				return;
			}

			var contactParentCustomerAccount = currentContact.contact_customer_accounts;

			if (contactParentCustomerAccount != null)
			{
				e.Values["msa_partnerid"] = contactParentCustomerAccount.ToEntityReference();
			}

			e.Values["msa_partneroppid"] = currentContact.ToEntityReference();

			e.Values["msa_allocatedtopartner"] = new OptionSetValue(1);

			e.Values["adx_createdbyusername"] = Contact.FullName;

			e.Values["adx_createdbyipaddress"] = Request.UserHostAddress ?? "";

			e.Values["adx_partnercreated"] = true;

			// If no estimated revenue was submitted, leave as system-calculated.

			e.Values["isrevenuesystemcalculated"] = (!e.Values.ContainsKey("estimatedvalue")) || (e.Values["estimatedvalue"] == null);


			var accountID = new Guid(Account_dropdown.SelectedItem.Value);
			var account = XrmContext.AccountSet.FirstOrDefault(a => a.AccountId == accountID);

			if (account != null)
			{
				e.Values["customerid"] = account.ToEntityReference();
			}

		}

		protected void OnItemInserted(object sender, CrmEntityFormViewInsertedEventArgs e)
		{
			var opportunity = XrmContext.OpportunitySet.First(o => o.Id == e.EntityId);

			var opportunityProductsFromLead = opportunity.adx_OpportuntiyProductsfromLead;

			List<Product> productsList = new List<Product>();

			if (!String.IsNullOrEmpty(opportunityProductsFromLead))
			{
				var products = XrmContext.ProductSet;

				string[] words = opportunityProductsFromLead.Split(',');
				foreach (var word in words)
				{
					//var myWord = word.Trim().ToUpper();

					foreach (var product in products)
					{
						if (product.Name.Trim().ToUpper() == word.Trim().ToUpper())
						{
							productsList.Add(product);
						}
					}
				}
			}

			foreach (var leadProduct in productsList)
			{
				if (!XrmContext.IsAttached(leadProduct))
				{
					XrmContext.Attach(leadProduct);
				}

				var adx_opportunity_productRelationship = new Relationship("adx_opportunity_product");

				XrmContext.AddLink(opportunity, adx_opportunity_productRelationship, leadProduct);
			}

			opportunity.adx_ReferenceCode = GetOpportunityReferenceCode();

			var salesStage = opportunity.SalesStageCode ?? 0;

			var response = (RetrieveAttributeResponse)ServiceContext.Execute(new RetrieveAttributeRequest
			{
				EntityLogicalName = "opportunity",
				LogicalName = "salesstagecode"
			});

			var picklist = response.AttributeMetadata as PicklistAttributeMetadata;
			if (picklist == null)
			{
				return;
			}

			foreach (var option in picklist.OptionSet.Options)
			{
				if (option.Value.Value == salesStage)
				{
					opportunity.StepName = option.Label.UserLocalizedLabel.Label;
				}
			}

			var leadType = XrmContext.adx_leadtypeSet.Where(lt => lt.adx_name == "Partner Created").FirstOrDefault();

			if (leadType != null)
			{
				opportunity.adx_LeadTypeId = leadType.ToEntityReference();
			}


			XrmContext.UpdateObject(opportunity);

			XrmContext.SaveChanges();

			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Accepted Opportunities");

			if (page == null)
			{
				throw new Exception("Please contact your System Administrator.  Required Site Marker titled 'My Opportunities' is missing.");
			}

			Response.Redirect(ServiceContext.GetUrl(page));
		}

		protected string CreateCustomerAccountUrl()
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Create Customer Account");

			if (page == null) { return null; }

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("FromCreateOpportunity", "true");

			return url;
		}

		protected void CreateCustomerButton_Click(object sender, EventArgs args)
		{
			var url = CreateCustomerAccountUrl();

			if (url != null)
			{
				Response.Redirect(url);
			}
		}

		public string GetOpportunityReferenceCode()
		{
			string str = "OPP-";
			Random random = new Random();
			for (var i = 0; i < 12; i++)
			{
				var r = random.Next(0x10);

				char s = ' ';

				switch (r)
				{
					case 0: s = '0'; break;
					case 1: s = '1'; break;
					case 2: s = '2'; break;
					case 3: s = '3'; break;
					case 4: s = '4'; break;
					case 5: s = '5'; break;
					case 6: s = '6'; break;
					case 7: s = '7'; break;
					case 8: s = '8'; break;
					case 9: s = '9'; break;
					case 10: s = 'A'; break;
					case 11: s = 'B'; break;
					case 12: s = 'C'; break;
					case 13: s = 'D'; break;
					case 14: s = 'E'; break;
					case 15: s = 'F'; break;
				}

				str = str + s;
			}

			var uuid = str.ToString();

			return uuid;
		}
	}
}