<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" Inherits="Site.Areas.Careers.Pages.Careers" Codebehind="Careers.aspx.cs" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/Areas/Careers/css/careers.css" />
</asp:Content>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<div class="pull-right">
			<a href="<%: Url.RouteUrl("JobPostingsFeed") %>" title="<%: Html.SnippetLiteral("Careers Subscribe Heading", "Subscribe to Job Postings") %>"><img src="<%: Url.Content("~/img/feed-icon-28x28.png") %>" alt=""/></a>
		</div>
		<h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<asp:ListView ID="JobPostings" runat="server">
		<LayoutTemplate>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
		</LayoutTemplate>
		<ItemTemplate>
			<div class="job-posting">
				<h3><%# Eval("Adx_name") %></h3>
				<p>
					<crm:CrmHyperLink ID="JobApplication" runat="server" CssClass="btn btn-small" Text="<%$ Snippet: links/apply, Apply Now %>" SiteMarkerName="Job Application" QueryString='<%# Eval("Adx_jobpostingId", "jobid={0}") %>'/>
					<asp:PlaceHolder Visible='<%# Eval("Adx_closingon") != null %>' runat="server">
						&ndash;
						<crm:Snippet SnippetName="Job Posting Closing Date Text" DefaultText="Closing" runat="server" EditType="text" />
						<span><%# Eval("Adx_closingon", "{0:MMMM dd, yyyy}") %></span>
					</asp:PlaceHolder>
				</p>
				<%# Eval("Adx_description") %>
			</div>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>
