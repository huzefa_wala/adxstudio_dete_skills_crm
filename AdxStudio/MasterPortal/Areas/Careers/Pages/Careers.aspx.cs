using System;
using System.Linq;
using Site.Pages;

namespace Site.Areas.Careers.Pages
{
	public partial class Careers : PortalPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			JobPostings.DataSource = from jp in Website.adx_website_jobposting
				orderby jp.Adx_name
				select new
				{
					jp.Adx_jobpostingId,
					jp.Adx_name,
					jp.Adx_Description,
					jp.Adx_closingon
				};

			JobPostings.DataBind();
		}
	}
}