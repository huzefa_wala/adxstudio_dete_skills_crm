﻿using System;
using System.Globalization;
using System.Linq;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Xrm;

namespace Site.Areas.Service311
{
	public static class ServiceRequestHelpers
	{
		public static string BuildServiceRequestTypeThumbnailImageTag(XrmServiceContext context, adx_servicerequest entity)
		{
			var imageUrl = GetServiceRequestTypeThumbnailImageUrl(context, entity);

			return string.IsNullOrWhiteSpace(imageUrl) ? string.Empty : string.Format("<img alt='' src='{0}' runat='server' />", imageUrl);
		}

		public static string BuildServiceRequestTypeThumbnailImageTag(XrmServiceContext context, adx_servicerequesttype entity)
		{
			var imageUrl = GetServiceRequestTypeThumbnailImageUrl(context, entity);

			return string.IsNullOrWhiteSpace(imageUrl) ? string.Empty : string.Format("<img alt='' src='{0}' runat='server' />", imageUrl);
		}

		public static string GetServiceRequestTypeThumbnailImageUrl(XrmServiceContext context, adx_servicerequest entity)
		{
			var serviceRequestTypeReference = entity.adx_servicerequesttype;

			if (serviceRequestTypeReference == null)
			{
				return string.Empty;
			}

			var type = context.adx_servicerequesttypeSet.FirstOrDefault(s => s.GetAttributeValue<Guid>("adx_servicerequesttypeid") == serviceRequestTypeReference.Id);

			return GetServiceRequestTypeThumbnailImageUrl(context, type);
		}

		public static string GetServiceRequestTypeThumbnailImageUrl(XrmServiceContext context, adx_servicerequesttype entity)
		{
			if (entity == null)
			{
				return string.Empty;
			}

			var url = entity.adx_thumbnailimageurl;

			if (string.IsNullOrWhiteSpace(url))
			{
				return string.Empty;
			}

			if (url.StartsWith("http", true, CultureInfo.InvariantCulture))
			{
				return url;
			}

			var portalContext = PortalCrmConfigurationManager.CreatePortalContext();

			return WebsitePathUtility.ToAbsolute(portalContext.Website, url);
		}

		public static string GetPushpinImageUrl(XrmServiceContext context, adx_servicerequest entity)
		{
			var serviceRequestType = entity.adx_servicerequesttype;

			if (serviceRequestType == null)
			{
				return string.Empty;
			}

			var type = context.adx_servicerequesttypeSet.FirstOrDefault(s => s.GetAttributeValue<Guid>("adx_servicerequesttypeid") == serviceRequestType.Id);

			if (type == null)
			{
				return string.Empty;
			}

			var url = type.adx_mapiconimageurl;

			if (string.IsNullOrWhiteSpace(url))
			{
				return string.Empty;
			}

			if (url.StartsWith("http", true, CultureInfo.InvariantCulture))
			{
				return url;
			}

			var portalContext = PortalCrmConfigurationManager.CreatePortalContext();

			return WebsitePathUtility.ToAbsolute(portalContext.Website, url);
		}

		public static string GetAlertPushpinImageUrl()
		{
			var portalContext = PortalCrmConfigurationManager.CreatePortalContext();
			var context = PortalCrmConfigurationManager.CreateServiceContext();
			var website = context.CreateQuery("adx_website").FirstOrDefault(w => w.GetAttributeValue<Guid>("adx_websiteid") == portalContext.Website.Id);
			var alertMapIconUrl = context.GetSiteSettingValueByName(website, "ServiceRequestMap/AlertMapIconUrl");

			if (string.IsNullOrWhiteSpace(alertMapIconUrl))
			{
				return string.Empty;
			}

			if (alertMapIconUrl.StartsWith("http", true, CultureInfo.InvariantCulture))
			{
				return alertMapIconUrl;
			}

			return WebsitePathUtility.ToAbsolute(website, alertMapIconUrl);
		}
	}
}