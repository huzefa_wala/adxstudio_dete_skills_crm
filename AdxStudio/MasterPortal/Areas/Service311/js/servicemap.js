﻿if (typeof ADX == "undefined" || !ADX) {
	var ADX = {};
}
ADX.serviceMap = (function ($) {
	var _export = {};
	var _elementId = '';
	var _map = null;
	var _mapOptions = {};
	var _searchActionURL = "";

	_mapOptions = { credentials: ADX.settings.mapSettings.key, mapTypeId: Microsoft.Maps.MapTypeId.road, center: new Microsoft.Maps.Location(ADX.settings.mapSettings.latitude, ADX.settings.mapSettings.longitude), zoom: ADX.settings.mapSettings.zoom, width: getWidth(), height: getHeight() };
	//_mapOptions = { credentials: ADX.settings.mapSettings.key, mapTypeId: Microsoft.Maps.MapTypeId.road, center: new Microsoft.Maps.Location(ADX.settings.mapSettings.latitude, ADX.settings.mapSettings.longitude), zoom: ADX.settings.mapSettings.zoom, width: ADX.settings.mapSettings.width, height: ADX.settings.mapSettings.height };
	var _mapViewOptions = { mapTypeId: Microsoft.Maps.MapTypeId.road, center: new Microsoft.Maps.Location(ADX.settings.mapSettings.latitude, ADX.settings.mapSettings.longitude), zoom: ADX.settings.mapSettings.zoom };
	var _pushpinSettings = ADX.settings.mapSettings.pushpin;
	var _infoboxSettings = ADX.settings.mapSettings.infobox;
	var _dateFormat = "m/d/yyyy";
	var _statusColorsString = ADX.settings.serviceRequestSettings.statusColors.toString().replace(/\s/g, '');
	var _priorityColorsString = ADX.settings.serviceRequestSettings.priorityColors.toString().replace(/\s/g, '');
	var _statusColors = _statusColorsString.split(";");
	var _statusColorsArray = new Array();
	for (var i = 0; i < _statusColors.length; i++) {
		// split each string into key/value pair
		var statusItem = _statusColors[i].toString().split(":");
		_statusColorsArray[statusItem[0]] = statusItem[1];
	}
	var _priorityColors = _priorityColorsString.split(";");
	var _priorityColorsArray = new Array();
	for (var j = 0; j < _priorityColors.length; j++) {
		// split each string into key/value pair
		var priorityItem = _priorityColors[j].toString().split(":");
		_priorityColorsArray[priorityItem[0]] = priorityItem[1];
	}

	function initialize(elementId, searchActionUrl, autosize) {
		if (!elementId && elementId != '') {
			log('Map elementId was not provided. Map initialization failed.', 'error', 'ADX.serviceMap.initialize');
			return;
		}
		_searchActionURL = searchActionUrl;
		$(document).ready(function () {
			createStatusOverlay($("#" + elementId).position().left + 100, $("#" + elementId).position().top + 100);
			$("#dateFrom").datepicker();
			$("#dateTo").datepicker();
			$("#dates").change(function () {
				if ($(this).val() == 3) {
					$("#datesFilter").show();
				} else {
					$("#datesFilter").hide();
				}
			});
			_elementId = elementId;
			_map = getMap();
			resetSearchOptions();
			if (autosize) {
				resizeMap();
				$(window).resize(function () {
					resizeMap();
				});
			}
			mapIt();
		});
	}

	function resizeMap() {
		var map = null;
		if (_map == null) {
			map = getMap();
		} else {
			map = _map;
		}
		var width = getWidth();
		var height = getHeight();
		_mapOptions.width = width;
		_mapOptions.height = height;
		map.setOptions({ height: height, width: width });
	}

	function getHeight() {
		var height = 240;
		var frameHeight = $(window).height();
		if (frameHeight > 0) {
			height = frameHeight;
		}
		return height;
	}

	function getWidth() {
		var width = 320;
		var frameWidth = $("#" + _elementId).parent().width();//$(window).width();
		if ((frameWidth) > 0) {
			width = frameWidth - 4;
		}
		return width;
	}

	function createStatusOverlay(left, top) {
		$("<div id='mapStatus' style='display:none; border: 1px solid #333; padding: 5px; background: #fff; position: absolute; left: " + left + "px; top: " + top + "px; z-index: 10000;'><img src='~/xrm-adx/samples/images/loader.gif' alt='Loading' />&nbsp;<span id='statusText'></span></div>").appendTo("body");
	}

	function reset() {
		resetMapView();
		resetSearchOptions();
	}

	function resetMapView() {
		var map = null;
		if (_map == null) {
			map = getMap();
		} else {
			map = _map;
		}
		map.entities.clear();
		map.setView(_mapViewOptions);
	}

	function resetSearchOptions() {
		// clear the inputs
		$("#dateFrom").val('');
		$("#dateTo").val('');
		$("#datesFilter").hide();
		$("#searchOptions #dates").removeAttr("selected");
		$("#searchOptions #status").removeAttr("selected");
		$("#searchOptions #priority").removeAttr("selected");
		$("#searchOptions #types").removeAttr("selected");
		// set the defaults
		$("#searchOptions #dates option[value='1']").attr("selected", "selected");
		$("#searchOptions #status option[value='0']").attr("selected", "selected");
		$("#searchOptions #priority option[value='0']").attr("selected", "selected");
		$("#searchOptions #types option[value='00000000-0000-0000-0000-000000000000']").attr("selected", "selected");
		$("#adx-map-show-alerts").prop('checked', true);
	}

	function getMap() {
		return new Microsoft.Maps.Map(document.getElementById(_elementId), _mapOptions);
	}

	function log(msg, cat, src) {
		try {
			if ((typeof (YAHOO) != 'undefined') && YAHOO && YAHOO.widget && YAHOO.widget.Logger && YAHOO.widget.Logger.log) {
				return YAHOO.widget.Logger.log(msg, cat, src);
			}

			if ((typeof (console) == 'undefined') || !console) {
				return false;
			}

			var source = src ? ('[' + src + ']') : '';

			if (cat == 'warn' && console.warn) {
				console.warn(msg, source);
			}
			else if (cat == 'error' && console.error) {
				console.error(msg, source);
			}
			else if (cat == 'info' && console.info) {
				console.info(msg, source);
			}
			else if (console.log) {
				console.log(msg, source);
			}

			return true;
		}
		catch (e) {
			return false;
		}
	}

	function addPushpin(map, content, latitude, longitude, tether, pushpinImageUrl) {
		var location = new Microsoft.Maps.Location(latitude, longitude);
		var pushpinOptions = {};
		if (pushpinImageUrl != '') {
			pushpinOptions = { icon: pushpinImageUrl, width: _pushpinSettings.width, height: _pushpinSettings.height };
		}
		var pushpin = new Microsoft.Maps.Pushpin(location, pushpinOptions);
		var infoboxContent = "<div class='infobox right'>"
								+ "<div class='infobox-arrow'>"
									+ "<div class='infobox-arrow-inner'></div>"
								+ "</div>"
								+ "<div class='infobox-content'>"
									+ content
								+ "</div>"
							+ "</div>";
		var pushpinInfobox = new Microsoft.Maps.Infobox(pushpin.getLocation(),
		{
			htmlContent: infoboxContent,
			visible: false,
			offset: new Microsoft.Maps.Point(_infoboxSettings.offset.x, _infoboxSettings.offset.y), // right
			showCloseButton: true
		});
		if (!tether) {
			Microsoft.Maps.Events.addHandler(map, 'viewchange', function (e) { pushpinInfobox.setOptions({ visible: false }); });
		}
		Microsoft.Maps.Events.addHandler(pushpin, 'mouseover', function (e) { pushpinInfobox.setOptions({ visible: true }); });
		Microsoft.Maps.Events.addHandler(pushpin, 'mouseout', function (e) { pushpinInfobox.setOptions({ visible: false }); });
		map.entities.push(pushpin);
		map.entities.push(pushpinInfobox);
	}


	function mapIt() {
		$(document).ready(function () {
			var map = null;
			if (_map == null) {
				map = getMap();
			} else {
				map = _map;
			}
			var dateFilterCode = parseInt($("#searchOptions #dates option:selected").val());
			var dateFrom = $("#dateFrom").val();
			var dateTo = $("#dateTo").val();
			if ($("#searchOptions #dates option[value='3']").is(":checked")) {
				if ((!dateFrom || dateFrom == '') || (!dateTo || dateTo == '')) {
					return;
				}
			}
			var statusFilterCode = parseInt($("#searchOptions #status option:selected").val());
			var priorityFilterCode = parseInt($("#searchOptions #priority option:selected").val());
			var types = new Array();
			$("#searchOptions #types option:selected").each(function (index) {
				types[index] = $(this).val();
			});
			$("#statusText").html("Searching for Service Requests...");
			$("#mapStatus").fadeIn();
			map.entities.clear();
			var includeAlerts = false;
			if ($("#adx-map-show-alerts").prop('checked') == true) {
				includeAlerts = true;
			}
			queryServiceRequestsMapEntities(dateFilterCode, dateFrom, dateTo, statusFilterCode, priorityFilterCode, types, includeAlerts,
				function (data) {
					//success
					//var locations = new Array();
					$.each(data, function (index, entity) {
						//var location = new Microsoft.Maps.Location(entity.Latitude, entity.Longitude);
						//locations.push(location);

						var content = "";

						if (entity.Title != null && entity.Title != '') {
							content = "<span class='title'>" + entity.Title + "</span>";
						}
						if (entity.Description != null && entity.Description != '') {
							content = content + "<p>" + entity.Description + "</p>";
						}
						if (entity.Location != null && entity.Location != '') {
							content = content + "<p>" + entity.Location + "</p>";
						}

						switch (entity.ItemNodeType) {
							case 1:
								content = content
									+ "<p><strong>Service Request #:</strong> " + entity.ServiceRequestNumber + "</p>"
									+ "<p><strong>Status:</strong> <span class='status' style='background:" + getStatusColor(entity.StatusId) + ";'>" + entity.Status + "</span></p>"
									+ "<p><strong>Priority:</strong> <span class='priority' style='color:" + getPriorityColor(entity.PriorityId) + ";'>" + entity.Priority + "</span></p>"
									+ "<p><strong>Incident Date:</strong> " + formatJsonDate(entity.IncidentDate, _dateFormat) + "</p>"
									+ "<p><strong>Scheduled Date:</strong> " + formatJsonDate(entity.ScheduledDate, _dateFormat) + "</p>"
									+ "<p><strong>Closed Date:</strong> " + formatJsonDate(entity.ClosedDate, _dateFormat) + "</p>";
								break;
							case 2:
								var scheduledStartDateString = "";
								var scheduledEndDateString = "";
								if (entity.ScheduledStartDate != null) {
									try {
										scheduledStartDateString = "<p><strong>Scheduled Start Date:</strong> " + formatJsonDate(entity.ScheduledStartDate, _dateFormat) + "</p>"
									} catch (e) {
									}
								}
								if (entity.ScheduledEndDate != null) {
									try {
										scheduledEndDateString = "<p><strong>Scheduled End Date:</strong> " + formatJsonDate(entity.ScheduledEndDate, _dateFormat) + "</p>";
									} catch (e) {
									}
								}
								content = content + scheduledStartDateString + scheduledEndDateString;
						}
						addPushpin(map, content, entity.Latitude, entity.Longitude, true, entity.PushpinImageUrl);
					});
					//if (locations.length > 0) {
					//		map.setView({ bounds: Microsoft.Maps.LocationRect.fromLocations(locations) });
					//}
				},
				function () {
					//error
				},
				function () {
					//completed
					$("#mapStatus").fadeOut();
				}
			);
		});
	}

	function queryServiceRequestsMapEntities(dateFilterCode, dateFrom, dateTo, statusFilterCode, priorityFilterCode, types, includeAlerts, success, error, complete) {
		success = $.isFunction(success) ? success : function () { };
		error = $.isFunction(error) ? error : function () { };
		complete = $.isFunction(complete) ? complete : function () { };
		var url = _searchActionURL;
		var data = {};
		data.dateFilterCode = dateFilterCode;
		data.dateFrom = dateFrom;
		data.dateTo = dateTo;
		data.statusFilterCode = statusFilterCode;
		data.priorityFilterCode = priorityFilterCode;
		data.types = types;
		data.includeAlerts = includeAlerts;
		var jsonData = JSON.stringify(data);
		$.ajax({
			type: 'POST',
			dataType: "json",
			contentType: 'application/json',
			url: url,
			data: jsonData,
			global: false,
			success: success,
			error: function (event, xhr, ajaxOptions, thrownError) {
				//errorHandler(event, xhr, ajaxOptions, thrownError);
				error.call(this, event, xhr, ajaxOptions, thrownError);
			},
			complete: complete
		});
	}

	function getStatusColor(id) {
		if (!id || id == '') {
			return '';
		}
		var color = _statusColorsArray[id];
		if (color == undefined) {
			return '';
		}
		return color;
	}

	function getPriorityColor(id) {
		if (!id || id == '') {
			return '';
		}
		var color = _priorityColorsArray[id];
		if (color == undefined) {
			return '';
		}
		return color;
	}

	function formatJsonDate(dateString, format) {
		if (!dateString) {
			return '';
		}
		dateString = dateString.dateFromJSON();
		var date = new Date(dateString);
		return date.format(format);
	}

	String.prototype.dateFromJSON = function () {
		return eval(this.replace(/\/Date\((.*?)\)\//gi, "new Date($1)"));
	};

	_export.initialize = initialize;

	_export.getMap = getMap;

	_export.mapIt = mapIt;

	_export.resetSearchOptions = resetSearchOptions;

	_export.resetMapView = resetMapView;

	_export.reset = reset;

	return _export;

})(jQuery);
