﻿<%@ Page Language="C#" ContentType="text/javascript" Trace="false" %>
<%@ OutputCache Duration="28800" VaryByParam="None" VaryByContentEncoding="gzip;x-gzip;deflate" %>
if (typeof ADX == "undefined" || !ADX) {
	var ADX = {};
}
ADX.settings = (function () {
	var _export = {};
	var _mapSettings = {
		key: '<asp:Literal runat="server" Text="<%$ SiteSetting: map_credentials %>" />',
		zoom: <asp:Literal runat="server" Text="<%$ SiteSetting: map_zoom %>" />,
		latitude: <asp:Literal runat="server" Text="<%$ SiteSetting: map_latitude %>" />,
		longitude: <asp:Literal runat="server" Text="<%$ SiteSetting: map_longitude %>" />,
		restServiceUrl: '<asp:Literal runat="server" Text="<%$ SiteSetting: map_rest_url %>" />',
		width: <asp:Literal runat="server" Text="<%$ SiteSetting: map_width %>" />,
		height: <asp:Literal runat="server" Text="<%$ SiteSetting: map_height %>" />,
		pushpin: {
			width: <asp:Literal runat="server" Text="<%$ SiteSetting: map_pushpin_width %>" />,
			height: <asp:Literal runat="server" Text="<%$ SiteSetting: map_pushpin_height %>" />
		},
		infobox: {
			offset: {
				x: <asp:Literal runat="server" Text="<%$ SiteSetting: map_infobox_offset_x %>" />,
				y: <asp:Literal runat="server" Text="<%$ SiteSetting: map_infobox_offset_y %>" />
			}
		}
	};
	var _serviceRequestSettings = {
		statusColors: '<asp:Literal runat="server" Text="<%$ SiteSetting: map_service_status_colors %>" />',
		priorityColors: '<asp:Literal runat="server" Text="<%$ SiteSetting: map_service_priority_colors %>" />'
	};
	_export.mapSettings = _mapSettings;
	_export.serviceRequestSettings = _serviceRequestSettings;
	return _export;
})();