﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="ServiceRequest.aspx.cs" Inherits="Site.Areas.Service311.Pages.ServiceRequest" %>
<asp:Content runat="server" ContentPlaceHolderID="Head">
	<link rel="stylesheet" type="text/css" href="~/css/webforms.css" />
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Service311/css/311.css") %>" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<div class="page-header">
		<h1>
			<asp:Literal ID="ServiceRequestIcon" runat="server" /><asp:Literal ID="ServiceRequestTitle" runat="server" ></asp:Literal>
		</h1>
	</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentBottom" runat="server" ViewStateMode="Enabled">
	<asp:Panel ID="PanelServiceRequest" runat="server">
		<crm:CrmDataSource ID="WebFormDataSource" runat="server" />
	</asp:Panel>
	<div class="row">
		<div class="span8">
			<asp:ListView ID="LatestArticlesList" runat="server">
				<LayoutTemplate>
					<div class="activity activity-grid">
						<div class="header">
							<h2>
								<crm:Snippet runat="server" SnippetName="311 Service Request Latest Knowledge Base Articles Title Text" DefaultText="Latest Knowledge Base Articles" />
							</h2>
						</div>
						<ul>
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
						</ul>
				</LayoutTemplate>
				<ItemTemplate>
					<li>
						<asp:HyperLink NavigateUrl='<%# GetKbArticleUrl(Eval("kbarticleid")) %>' Text='<%# HtmlEncode(Eval("Title")) %>' runat="server" />
					</li>
				</ItemTemplate>
			</asp:ListView>
		</div>
	</div>
</asp:Content>

