﻿using System;
using System.Linq;
using System.Web.UI;
using Adxstudio.Xrm.Cms;
using Site.Pages;

namespace Site.Areas.Service311.Pages
{
	public partial class ServiceRequest : PortalPage
	{
		public string EntityLogicalName;

		protected void Page_Load(object sender, EventArgs e)
		{
			var typeId = Request.QueryString["typeid"];
			Guid serviceRequestTypeId;

			if (!Guid.TryParse(typeId, out serviceRequestTypeId))
			{
				throw new ApplicationException(string.Format("Service Request Type could not be found. The provided typeid '{0}' is not valid.", typeId));
			}
	
			var serviceRequestType = ServiceContext.adx_servicerequesttypeSet.FirstOrDefault(s => s.adx_servicerequesttypeId == serviceRequestTypeId);

			if (serviceRequestType == null)
			{
				throw new ApplicationException(string.Format("Service Request Type could not be found for the typeid '{0}'", typeId));
			}

			if (string.IsNullOrWhiteSpace(serviceRequestType.adx_entityname))
			{
				throw new NullReferenceException("Entity Name.");
			}

			EntityLogicalName = serviceRequestType.adx_entityname;

			ServiceRequestTitle.Text = serviceRequestType.adx_name ?? string.Empty;

			var webform = serviceRequestType.adx_webform;

			ServiceRequestIcon.Text = ServiceRequestHelpers.BuildServiceRequestTypeThumbnailImageTag(ServiceContext, serviceRequestType);

			var webformControl = new Adxstudio.Xrm.Web.UI.WebControls.WebForm
				              {
								  WebFormReference = webform,
					              ClientIDMode = ClientIDMode.Static,
					              FormCssClass = "crmEntityFormView form",
					              PreviousButtonCssClass = "btn",
					              NextButtonCssClass = "btn btn-primary",
					              SubmitButtonCssClass = "btn btn-primary"
				              };

			PanelServiceRequest.Controls.Add(webformControl);

			var maxLatestArticlesSetting = ServiceContext.GetSiteSettingValueByName(Website, "service_request_max_kb_articles");

			int maxLatestArticles;

			maxLatestArticles = int.TryParse(maxLatestArticlesSetting, out maxLatestArticles) ? maxLatestArticles : 3;

			var latestArticles = Enumerable.Empty<Xrm.KbArticle>().AsQueryable();

			var subjectId = serviceRequestType.adx_subject != null ? serviceRequestType.adx_subject.Id : Guid.Empty;

			if (subjectId != Guid.Empty)
			{
				latestArticles = XrmContext.KbArticleSet.Where(k => k.StateCode == (int)Enums.KbArticleState.Published && k.msa_publishtoweb == true && k.SubjectId.Id == subjectId).OrderByDescending(k => k.CreatedOn).Take(maxLatestArticles);
			}

			LatestArticlesList.DataSource = latestArticles;

			LatestArticlesList.DataBind();
		}

		protected string GetKbArticleUrl(object kbarticleid)
		{
			if (kbarticleid == null)
			{
				return null;
			}

			var articleUrl = GetUrlForRequiredSiteMarker("KB Article");

			articleUrl.QueryString.Set("id", kbarticleid.ToString());

			return articleUrl.ToString();
		}

		public static string HtmlEncode(object value)
		{
			return value == null ? string.Empty : Microsoft.Security.Application.Encoder.HtmlEncode(value.ToString());
		}
	}
}