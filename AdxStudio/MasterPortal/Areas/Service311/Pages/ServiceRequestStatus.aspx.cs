﻿using System;
using System.Collections.Generic;
using System.Linq;
using Adxstudio.Xrm.Web.UI;
using Adxstudio.Xrm.Cms;
using Site.Pages;
using Xrm;

namespace Site.Areas.Service311.Pages
{
	public partial class ServiceRequestStatus : PortalPage
	{
		private const string ViewName = "Status Web View";

		private string ServiceRequestNumberQueryStringKey
		{
			get
			{
				var setting = ServiceContext.GetSiteSettingValueByName(Website, "311/ServiceRequests/StatusCheckQuerystringKey");

				return string.IsNullOrWhiteSpace(setting) ? "refnum" : setting;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var servicerequestnumber = Request.QueryString[ServiceRequestNumberQueryStringKey];
			
			if (string.IsNullOrWhiteSpace(servicerequestnumber))
			{
				StatusPanel.Visible = false;
				ErrorPanel.Visible = true;
				return;
			}

			var serviceRequest = ServiceContext.adx_servicerequestSet.FirstOrDefault(s => s.adx_servicerequestnumber == servicerequestnumber);

			if (serviceRequest == null)
			{
				StatusPanel.Visible = false;
				ErrorPanel.Visible = true;
				return;
			}

			ErrorPanel.Visible = false;

			StatusPanel.Visible = true;

			var status = new List<adx_servicerequest> { serviceRequest };

			var savedQuery = ServiceContext.SavedQuerySet.FirstOrDefault(s => s.Name == ViewName && s.ReturnedTypeCode == adx_servicerequest.EntityLogicalName);

			if (savedQuery == null)
			{
				throw new ApplicationException(string.Format("Required <em>View</em> titled '{0}' could not be found for '{1}'.", ViewName, adx_servicerequest.EntityLogicalName));
			}

			var columnsGenerator = new SavedQueryColumnsGenerator(XrmContext, savedQuery);

			StatusDetailsView.DataKeyNames = new[] { "adx_servicerequestid" };

			StatusDetailsView.DataSource = columnsGenerator.ToDataTable(status);

			StatusDetailsView.RowsGenerator = columnsGenerator;

			StatusDetailsView.DataBind();
		}
	}
}