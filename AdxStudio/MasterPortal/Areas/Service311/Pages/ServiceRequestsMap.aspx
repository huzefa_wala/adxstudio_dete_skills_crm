﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="ServiceRequestsMap.aspx.cs" Inherits="Site.Areas.Service311.Pages.ServiceRequestsMap" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Site.Areas.Service311" %>
<%@ Import Namespace="Xrm" %>

<asp:Content runat="server" ContentPlaceHolderID="Head">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Service311/css/311.css") %>" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<div class="page-heading">
		<% Html.RenderPartial("Breadcrumbs"); %>
		<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
		<div class="page-header">
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
		</div>
	</div>
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<asp:ScriptManagerProxy runat="server">
		<Scripts>
			<asp:ScriptReference Path="~/xrm-adx/js/jquery-ui-1.8.18.min.js" />
			<asp:ScriptReference Path="~/Areas/Service311/js/json2.min.js" />
			<asp:ScriptReference Path="~/Areas/Service311/js/date.format.min.js" />
			<asp:ScriptReference Path="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0" />
			<asp:ScriptReference Path="~/Areas/Service311/js/settings.js.aspx" />
			<asp:ScriptReference Path="~/Areas/Service311/js/servicemap.js?v=1" />
		</Scripts>
	</asp:ScriptManagerProxy>
	<div id="searchOptions">
		<h3>
			<crm:Snippet runat="server" SnippetName="311 Map Search Title Text" DefaultText="Search" Editable="true" EditType="text" />
		</h3>
		<div class="options">
			<strong><crm:Snippet runat="server" SnippetName="311 Map Search Date Title Text" DefaultText="Date:" Editable="true" EditType="text" /></strong><br/>
			<select id="dates">
				<option value="0">Last 7 days</option>
				<option value="1">Last 30 days</option>
				<option value="2">Last 12 months</option>
				<option value="3">Other</option>
			</select>
			<div id="datesFilter"><table><tr><td>From</td><td>To</td></tr><tr><td><input id="dateFrom" type="text" /></td><td><input id="dateTo" type="text" /></td></tr></table></div>
		</div>
		<div class="options">
			<strong><crm:Snippet runat="server" SnippetName="311 Map Search Status Title Text" DefaultText="Status:" Editable="true" EditType="text" /></strong><br/>
			<asp:ListView ID="ServiceRequestStatusList" runat="server">
				<LayoutTemplate>
					<select id="status">
						<asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
					</select>
				</LayoutTemplate>
				<ItemTemplate>
					<option value='<%# Eval("Id") %>'><%# Eval("Name") %></option>
				</ItemTemplate>
			</asp:ListView>
		</div>
		<div class="options">
			<strong><crm:Snippet runat="server" SnippetName="311 Map Search Priority Title Text" DefaultText="Priority:" Editable="true" EditType="text" /></strong><br/>
			<asp:ListView ID="ServiceRequestPriorityList" runat="server">
				<LayoutTemplate>
					<select id="priority">
						<asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
					</select>
				</LayoutTemplate>
				<ItemTemplate>
					<option value='<%# Eval("Id") %>'><%# Eval("Name") %></option>
				</ItemTemplate>
			</asp:ListView>
		</div>
		<div class="options">
			<strong><crm:Snippet runat="server" SnippetName="311 Map Search Type Title Text" DefaultText="Type:" Editable="true" EditType="text" /></strong><br/>
			<asp:ListView ID="ServiceRequestTypesList" runat="server">
				<LayoutTemplate>
					<select id="types">
						<asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder>
					</select>
				</LayoutTemplate>
				<ItemTemplate>
					<option value='<%# Eval("Id") %>'><%# Eval("Name") %></option>
				</ItemTemplate>
			</asp:ListView>
		</div>
		<div class="options"><strong>Show Alerts:</strong><br><input id="adx-map-show-alerts" name="adx-map-show-alerts" type="checkbox" /> </div>
		<div class="actions">
			<input type="button" class="btn" value="<%$ Snippet: 311 Map Search Reset Button Text, Reset %>" onclick="ADX.serviceMap.reset();" runat="server" /> <input type="button" class="btn btn-primary" value="<%$ Snippet: 311 Map Search Button Text, Search %>" onclick="ADX.serviceMap.mapIt();" runat="server"/>
		</div>
	</div>
	<div id="mapContainer">
		<div id="serviceMap"></div>
	</div>
	<div id="mapLegend">
		<h3>
			<crm:Snippet runat="server" SnippetName="311 Map Legend Title Text" DefaultText="Legend" Editable="true" EditType="text" />
		</h3>
		<asp:ListView ID="ServiceRequestTypesLegendList" runat="server">
			<LayoutTemplate>
				<ul><asp:PlaceHolder ID="ItemPlaceHolder" runat="server"></asp:PlaceHolder></ul>
			</LayoutTemplate>
			<ItemTemplate>
				<li><asp:Literal Text="<%# ServiceRequestHelpers.BuildServiceRequestTypeThumbnailImageTag(ServiceContext, Container.DataItem as adx_servicerequesttype) %>" runat="server" /><%# Eval("adx_name") %></li>
			</ItemTemplate>
		</asp:ListView>
	</div>
	<div class="clearfix"></div>
	<script type="text/javascript">
		ADX.serviceMap.initialize('serviceMap', "<%: Url.Action("Search", "Map", new{ area = "Service311" }) %>", true);
	</script>
</asp:Content>
