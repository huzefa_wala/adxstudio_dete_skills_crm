<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="True" CodeBehind="Home.aspx.cs" Inherits="Site.Areas.Service311.Pages.Home" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Service311/css/311.css") %>" />
</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script src="<%: Url.Content("~/Areas/Service311/js/service311.js") %>"></script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Breadcrumbs" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<asp:ListView ID="ServiceRequestsListView" runat="server">
		<LayoutTemplate>
			<ul class="service311 thumbnails">
				<li id="itemPlaceholder" runat="server"/>
			</ul>
		</LayoutTemplate>
		<ItemTemplate>
			<li class="span2" runat="server">
				<a class="well thumbnail service text-center" href="<%# Eval("Url") %>">
					<asp:Image ImageUrl='<%# GetThumbnailUrl(Eval("Entity")) %>' runat="server"/>
					<h6><%# Eval("Title") %></h6>
				</a>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>
<asp:Content ContentPlaceHolderID="SidebarAbove" runat="server">
	<div class="well sidebar">
		<div id="status-tracker" class="section form-search">
			<crm:Snippet runat="server" SnippetName="311 Service Request Status Tracker Title" EditType="html" Editable="true" DefaultText="<h4>Track Your Service Request</h4>" />
			<div class="input-append">
				<input id="ServiceRequestTrackingNumber" class="search-query input-medium" type="text" placeholder="Service Request #" onkeypress="return ADX.service311.disableEnterKey(event);" />
				<a href="#" class="btn" onclick="<%= string.Format("ADX.service311.checkStatus('ServiceRequestTrackingNumber', '{0}', 'refnum');", Html.SiteMarkerUrl("check-status")) %>"><i class="icon-search"></i></a>
			</div>
		</div>
	</div>
	<div class="well sidebar">
		<div class="section">
			<crm:Snippet runat="server" SnippetName="311 Home Service Requests Map Heading Text" EditType="text" Editable="true" DefaultText="<h4>Service Requests Map</h4>" />
			<crm:Snippet runat="server" SnippetName="311 Home View Service Requests Map Caption" EditType="html" Editable="true" DefaultText="<p>Service requests can be searched for and rendered on a map.</p>" />
			<a class="btn btn-info" href="<%= Html.SiteMarkerUrl("Service Requests Map") %>"><i class="icon-map-marker icon-white"></i> <crm:Snippet runat="server" SnippetName="311 Home View Service Requests Map Button" EditType="text" Editable="true" DefaultText="View Map" /></a>
		</div>
	</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="SidebarBelow" runat="server">
	<asp:Panel ID="TwitterFeedPanel" runat="server">
		<crm:Snippet SnippetName="311 Home Twitter Widget" EditType="text" Literal="True" DefaultText="311 Home Twitter Widget" runat="server"/>
	</asp:Panel>
</asp:Content>
