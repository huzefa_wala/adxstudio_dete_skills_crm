﻿using System;
using System.Linq;
using Site.Pages;

namespace Site.Areas.Service311.Pages
{
	public partial class ServiceRequests : PortalPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var serviceRequestTypes = ServiceContext.adx_servicerequesttypeSet.Where(s => s.statecode == 0).OrderBy(s => s.adx_name);

			ServiceRequestTypesListView.DataSource = serviceRequestTypes;

			ServiceRequestTypesListView.DataBind();
		}
	}
}