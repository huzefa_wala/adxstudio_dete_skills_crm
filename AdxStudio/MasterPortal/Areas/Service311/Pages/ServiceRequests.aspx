﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="ServiceRequests.aspx.cs" Inherits="Site.Areas.Service311.Pages.ServiceRequests" %>
<%@ Import Namespace="Site.Areas.Service311" %>
<%@ Import Namespace="Xrm" %>

<asp:Content runat="server" ContentPlaceHolderID="Head">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Service311/css/311.css") %>" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div id="service-requests" class="activity">
		<div class="header">
			<h2>
				<crm:Snippet runat="server" SnippetName="311 Service Requests List Title Text" EditType="text" Editable="true" DefaultText="Submit a New Service Request" ClientIDMode="Static" />
			</h2>
		</div>
		<asp:ListView runat="server" ID="ServiceRequestTypesListView">
			<LayoutTemplate>
				<ul>
					<asp:PlaceHolder runat="server" ID="ItemPlaceHolder"></asp:PlaceHolder>
				</ul>
			</LayoutTemplate>
			<ItemTemplate>
				<li>
					<asp:Literal Text="<%# ServiceRequestHelpers.BuildServiceRequestTypeThumbnailImageTag(ServiceContext, Container.DataItem as adx_servicerequesttype) %>" runat="server" />
					<crm:CrmHyperLink SiteMarkerName="New Service Request" QueryString='<%# string.Format("typeid={0}", Eval("adx_servicerequesttypeId")) %>' Text='<%# Eval("adx_name") %>' runat="server"></crm:CrmHyperLink>
					<p><%# Eval("adx_description") %></p>
				</li>
			</ItemTemplate>
		</asp:ListView>
	</div>
	<div class="help-block">
		<crm:Snippet runat="server" SnippetName="311 Service Requests Types Not Listed Message" EditType="html" Editable="true" DefaultText="" />
	</div>
</asp:Content>