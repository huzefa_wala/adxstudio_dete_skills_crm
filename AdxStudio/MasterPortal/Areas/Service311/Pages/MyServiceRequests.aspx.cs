﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Web.UI;
using Site.Pages;
using Adxstudio.Xrm.Data;

namespace Site.Areas.Service311.Pages
{
	public partial class MyServiceRequests : PortalPage
	{
		private const string SavedQueryName = "Status Web View";
		private const string EntityName = "adx_servicerequest";

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			if (Contact == null)
			{
				return;
			}

			var savedQuery = XrmContext.SavedQuerySet.FirstOrDefault(s => s.Name == SavedQueryName && s.ReturnedTypeCode == EntityName);

			if (savedQuery == null)
			{
				throw new ApplicationException(string.Format("Required View titled '{0}' could not be found for entity '{1}'.", SavedQueryName, EntityName));
			}

			var serviceRequests = XrmContext.adx_servicerequestSet.Where(s => s.adx_regardingcontact.Id == Contact.Id);

			MyServiceRequestsGridView.DataSource = serviceRequests.ToDataTable(XrmContext, savedQuery, true);

			MyServiceRequestsGridView.ColumnsGenerator = new SavedQueryColumnsGenerator(XrmContext, EntityName, SavedQueryName);
			
			MyServiceRequestsGridView.DataBind();
		}

		protected void MyServiceRequestsGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow || e.Row.Cells.Count < 1)
			{
				return;
			}

			foreach (TableCell cell in e.Row.Cells)
			{
				DateTime cellAsDateTime;

				if (DateTime.TryParse(cell.Text, out cellAsDateTime))
				{
					cell.Text = cellAsDateTime.ToLocalTime().ToString("g");
				}
			}
		}
	}
}