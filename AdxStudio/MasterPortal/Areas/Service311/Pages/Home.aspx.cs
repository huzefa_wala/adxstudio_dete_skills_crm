﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Site.Pages;

namespace Site.Areas.Service311.Pages
{
	public partial class Home : PortalPage
	{
		protected void Page_Load(object sender, EventArgs args)
		{
			var setting = ServiceContext.CreateQuery("adx_sitesetting").FirstOrDefault(s => s.GetAttributeValue<string>("adx_name") == "twitter_feed_enabled");

			var twitterEnabled = false;

			if (setting != null)
			{
				bool.TryParse(setting.GetAttributeValue<string>("adx_value"), out twitterEnabled);
			}

			TwitterFeedPanel.Visible = twitterEnabled;

			ServiceRequestsListView.DataSource = GetServiceRequests();
			ServiceRequestsListView.DataBind();
		}

		private IEnumerable<SiteMapNode> GetServiceRequests()
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Service Requests List");

			if (page == null)
			{
				return new SiteMapNode[] { };
			}

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			var serviceRequestsNode = System.Web.SiteMap.Provider.FindSiteMapNodeFromKey(url);

			if (serviceRequestsNode == null)
			{
				return new SiteMapNode[] { };
			}

			var nodes = serviceRequestsNode.ChildNodes.Cast<SiteMapNode>().ToList();

			return nodes;
		}

		protected string GetThumbnailUrl(object webpageObject)
		{
			var webpageEntity = webpageObject as Entity;

			if (webpageEntity == null) return null;

			var webfile = ServiceContext.CreateQuery<Xrm.Adx_webfile>().FirstOrDefault(file => file.Id == webpageEntity.GetAttributeValue<Guid>("adx_image"));

			if (webfile == null) return null;

			var url = new UrlBuilder(ServiceContext.GetUrl(webfile));

			return url.Path;
		}
	}
}
