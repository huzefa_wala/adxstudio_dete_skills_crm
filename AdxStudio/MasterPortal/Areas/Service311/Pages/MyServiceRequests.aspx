﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Profile.master" AutoEventWireup="true" CodeBehind="MyServiceRequests.aspx.cs" Inherits="Site.Areas.Service311.Pages.MyServiceRequests" %>
<%@ OutputCache CacheProfile="User" %>

<asp:Content runat="server" ContentPlaceHolderID="Head">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Service311/css/311.css") %>" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-heading">
		<div class="page-header">
			<div class="pull-right">
				<crm:CrmHyperLink runat="server" SiteMarkerName="Service Requests" CssClass="btn btn-primary">
					<i class="icon-plus-sign icon-white"></i>
					<crm:Snippet runat="server" SnippetName="311 New Service Request Button Label" Literal="True" DefaultText="Create a new service request" />
				</crm:CrmHyperLink>
			</div>
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
		</div>
	</div>
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
	<div id="my-service-requests">
		<asp:GridView ID="MyServiceRequestsGridView" runat="server" CssClass="table table-striped" OnRowDataBound="MyServiceRequestsGridView_OnRowDataBound" GridLines="None">
			<EmptyDataTemplate>
				<div class="alert"><crm:Snippet runat="server" SnippetName="311 My Service Requests List Empty Message" EditType="html" Editable="true" DefaultText="You currently do not have any Service Requests." /></div>
				<p>
					<crm:CrmHyperLink runat="server" SiteMarkerName="Service Requests" CssClass="btn btn-primary">
						<i class="icon-plus-sign icon-white"></i>
						<crm:Snippet runat="server" SnippetName="311 New Service Request Button Label" Literal="True" DefaultText="Create a new service request" />
					</crm:CrmHyperLink>
				</p>
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
</asp:Content>
