﻿using System;
using System.Linq;
using Adxstudio.Xrm.Web.UI.WebForms;
using Xrm;

namespace Site.Areas.Service311.Controls
{
	public partial class WebFormServiceRequestSuccess : WebFormUserControl
	{
		protected void Page_PreRender(object sender, EventArgs e)
		{
			WebForm.ShowHideNextButton(false);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (PreviousStepEntityID == Guid.Empty)
			{
				throw new NullReferenceException("The ID of the previous web form step's created entity is null.");
			}

			CustomSuccessMessage.Text = WebForm.SuccessMessage ?? string.Empty;

			DefaultSuccessMessageSnippet.Visible = string.IsNullOrWhiteSpace(WebForm.SuccessMessage);
			
			var context = new XrmServiceContext();

			var entity = context.CreateQuery(CurrentStepEntityLogicalName).FirstOrDefault(o => o.GetAttributeValue<Guid>(PreviousStepEntityPrimaryKeyLogicalName) == PreviousStepEntityID);

			if (entity == null)
			{
				throw new NullReferenceException(string.Format("The {0} record with primary key '{1}' equal to '{2}' could not be found.", PreviousStepEntityLogicalName, PreviousStepEntityPrimaryKeyLogicalName, PreviousStepEntityID));
			}

			var type = context.adx_servicerequesttypeSet.FirstOrDefault(s => s.adx_entityname == PreviousStepEntityLogicalName);

			if (type == null)
			{
				throw new NullReferenceException(string.Format("The {0} record could not be found with Entity Name equal to '{1}'.", adx_servicerequesttype.EntityLogicalName, PreviousStepEntityLogicalName));
			}

			var field = type.adx_servicerequestnumberfieldname;

			if (!string.IsNullOrWhiteSpace(field))
			{
				try
				{
					var serviceRequestNumber = entity.GetAttributeValue<string>(field);

					if (!string.IsNullOrWhiteSpace(serviceRequestNumber))
					{
						ServiceRequestNumber.Text = serviceRequestNumber;

						ServiceRequestNumberPanel.Visible = true;
					}
				}
				catch (Exception)
				{
					ServiceRequestNumberPanel.Visible = false;
				}
			}

			PanelSuccess.Visible = true;
		}
	}
}