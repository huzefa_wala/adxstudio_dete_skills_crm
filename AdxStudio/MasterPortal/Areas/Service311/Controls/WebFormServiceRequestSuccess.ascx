﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebFormServiceRequestSuccess.ascx.cs" Inherits="Site.Areas.Service311.Controls.WebFormServiceRequestSuccess" %>
<asp:Panel ID="PanelSuccess" runat="server" Visible="False" CssClass="row">
	<div class="span8">
		<div class="alert alert-success">
			<crm:Snippet ID="DefaultSuccessMessageSnippet" runat="server" Visible="False" SnippetName="311 Service Request Default Success Message" EditType="html" Editable="true" DefaultText="<p><strong><em>Thank you.</em></strong></p><p>Your service request has been successfully submitted.</p><p>Your request will be reviewed within 24 hours and you will receive confirmation and a follow-up message.</p>" ClientIDMode="Static" />
			<asp:Label ID="CustomSuccessMessage" runat="server" />
		</div>
		<asp:Panel ID="ServiceRequestNumberPanel" runat="server" Visible="False">
			<p>
				<crm:Snippet ID="ServiceRequestNumberLabel" runat="server" SnippetName="311 Service Request Number Label Text" EditType="text" Editable="true" DefaultText="Service Request Number:" ClientIDMode="Static" />&nbsp;<asp:Label ID="ServiceRequestNumber" runat="server" ClientIDMode="Static" ></asp:Label>
			</p>
		</asp:Panel>
	</div>
</asp:Panel>