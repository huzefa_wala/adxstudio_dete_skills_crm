﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Profile.master" AutoEventWireup="true" CodeBehind="MyAccount.aspx.cs" Inherits="Site.Areas.Retail.Pages.MyAccount" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/css/webforms.css") %>">
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="EntityControls">
	<script type="text/javascript">
		function entityFormClientValidate() {
			// Custom client side validation. Method is called by the submit button's onclick event.
			// Must return true or false. Returning false will prevent the form from submitting.
			return true;
		}
		$(document).ready(function () {
			$("time").each(function () {
				var dateTime = Date.parse($(this).attr("datetime"));
				if (dateTime) {
					$(this).text(dateTime.toString("MM/dd/yyyy h:mm tt"));
				}
			});
		});
		function webFormClientValidate() {
			// Custom client side validation. Method is called by the next/submit button's onclick event.
			// Must return true or false. Returning false will prevent the form from submitting.
			return true;
		}
	</script>
	<adx:EntityForm ID="EntityFormControl" runat="server" FormCssClass="crmEntityFormView readonly centered" PreviousButtonCssClass="btn" NextButtonCssClass="btn btn-primary" SubmitButtonCssClass="btn btn-primary" ClientIDMode="Static" />
	<asp:Panel runat="server" ID="Household">
		<fieldset>
			<legend>
				<crm:Snippet SnippetName="My Account Household Form Legend" DefaultText="Household" EditType="text" runat="server"/>
			</legend>
			<adx:CrmDataSource ID="HouseholdDataSource" runat="server"/>
			<adx:CrmEntityFormView ID="HouseholdFormView" runat="server"
				DataSourceID="HouseholdDataSource"
				CssClass="crmEntityFormView readonly"
				EntityName="account"
				FormName="Household Summary Web Form"
				Mode="ReadOnly">
			</adx:CrmEntityFormView>
		</fieldset>
		<fieldset>
			<legend>
				<crm:Snippet SnippetName="My Account Household Members Legend" DefaultText="Household Members" EditType="text" runat="server"/>
			</legend>
			<adx:EntityList ID="EntityListControl" runat="server" ListCssClass="table table-striped" DefaultEmptyListText="There are no items to display." ClientIDMode="Static" />
		</fieldset>
	</asp:Panel>
</asp:Content>