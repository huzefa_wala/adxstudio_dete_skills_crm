﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Site.Areas.Retail.Pages.Home" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Retail/css/retail.css") %>" />
	<link rel="stylesheet" href="<%: Url.Content("~/css/rateit.css") %>" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Scripts">
	<script type="text/javascript" src="<%: Url.Content("~/js/jquery.rateit.min.js") %>"></script>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentBottom">
	<div class="row">
		<div class="span9">
			<asp:Panel ID="FeaturedProductsPanel" runat="server">
				<asp:ObjectDataSource ID="FeaturedProductsDataSource" TypeName="Adxstudio.Xrm.Products.CampaignProductsDataAdapter" OnObjectCreating="CreateCampaignProductsDataAdapter" SelectMethod="SelectProducts" SelectCountMethod="SelectProductCount" EnablePaging="True" runat="server" />
				<asp:ListView ID="FeaturedProducts" DataSourceID="FeaturedProductsDataSource" runat="server">
					<LayoutTemplate>
						<div class="featured-products">
							<div class="page-header">
								<h3><crm:Snippet SnippetName="Retail Featured Products Title" EditType="text" DefaultText="Featured Products" runat="server"/></h3>
							</div>
							<ul class="nav thumbnails product-grid">
								<li id="itemPlaceholder" runat="server" />
							</ul>
						</div>
					</LayoutTemplate>
					<ItemTemplate>
						<li>
							<a class="thumbnail" href="<%# Url.Action("Product", "Products", new { productIdentifier = Eval("PartialURL") ?? Eval("SKU"), area = "Products" }) %>"> <%--href='<%# string.Format("~/products/product/{0}/", Eval("PartialURL") ?? Eval("SKU")) %>'>--%>
								<img src="<%# string.IsNullOrWhiteSpace(Eval("ImageThumbnailURL").ToString()) ? "/image-not-available-150x150.png/" : Eval("ImageThumbnailURL") %>" alt="<%# Eval("Name") %>" />
								<div class="caption text-center">
									<div>
										<%# Eval("Name") %>
									</div>
									<div>
										<strong><%# Eval("ListPrice", "{0:C2}") %></strong>
									</div>
									<div>
										<div data-rateit-readonly="true" data-rateit-ispreset="true" data-rateit-value="<%# Eval("RatingInfo.Average") %>" class="rateit"></div>
									</div>
								</div>
							</a>
						</li>
					</ItemTemplate>
				</asp:ListView>
			</asp:Panel>
			<% var featuredCollections = Html.WebLinkSet("Featured Collections"); %>
			<% if (featuredCollections != null) { %>
				<div class="featured-collections">
					<div class="page-header">
						<h3><%: Html.TextAttribute(featuredCollections, "adx_title") ?? new HtmlString("Featured Categories") %></h3>
					</div>
					<div class="weblinks <%: featuredCollections.Editable ? "xrm-entity xrm-editable-adx_weblinkset" : string.Empty %>" data-weblinks-maxdepth="1">
						<ul class="nav thumbnails product-collection-grid">
							<% foreach (var webLink in featuredCollections.WebLinks) { %>
								<li>
									<a class="thumbnail" href="<%: webLink.Url %>" title="<%: webLink.ToolTip %>">
										<%: Html.WebLinkImage(webLink) %>
										<div class="caption text-center"><%: webLink.Name %></div>
									</a>
								</li>
							<% } %>
						</ul>
						<% if (featuredCollections.Editable) { %>
							<%: Html.WebLinkSetEditingMetadata(featuredCollections) %>
						<% } %>
					</div>
				</div>
			<% } %>
		</div>
		<div class="span3">
			<div class="well sidebar">
				<crm:WebLinks WebLinkSetName="Secondary Navigation" CssClass="weblinks section" runat="server"/>
			</div>
			<adx:PollPlacement runat="server" PlacementName="Sidebar" CssClass="poll">
				<LayoutTemplate>
					<div class="well">
						<div class="header">
							<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: SiteMarker=Poll Archives, Return=Url %>' Text='<%$ Snippet: polls/archiveslabel, Poll Archives %>' runat="server" />
							<h4>
								<i class="icon-question-sign"></i>
								<crm:Snippet SnippetName="polls/title" EditType="text" DefaultText="Poll" runat="server"/>
							</h4>
						</div>
						<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
					</div>
				</LayoutTemplate>
				<PollTemplate>
					<div class="poll-question">
						<asp:PlaceHolder ID="PollQuestion" runat="server"/>
					</div>
					<div class="poll-options">
						<asp:RadioButtonList ID="PollOptions" RepeatLayout="UnorderedList" runat="server"/>
					</div>
					<div class="poll-actions">
						<asp:Button ID="PollSubmit" CssClass="btn btn-primary" Text="Submit" runat="server"/>
						<asp:LinkButton ID="PollViewResults" CssClass="btn" Text='<%$ Snippet: polls/resultslabel, View Results %>' runat="server"/>
					</div>
				</PollTemplate>
				<ResultsTemplate>
					<div class="poll-question">
						<asp:PlaceHolder ID="PollQuestion" runat="server"/>
					</div>
					<div class="poll-results">
						<asp:PlaceHolder ID="PollResults" runat="server"/>
					</div>
					<div>
						<crm:Snippet SnippetName="polls/totalslabel" EditType="text" DefaultText="Total Votes:" runat="server"/>
						<asp:PlaceHolder ID="PollTotalVotes" runat="server"/>
					</div>
				</ResultsTemplate>
			</adx:PollPlacement>
			<adx:AdPlacement runat="server" PlacementName="Sidebar Bottom" CssClass="ad" />
		</div>
	</div>
</asp:Content>
