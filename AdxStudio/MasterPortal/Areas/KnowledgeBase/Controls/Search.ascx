﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="Site.Areas.KnowledgeBase.Controls.Search" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<div class="well kb-search">
	<asp:Panel ID="SearchForm" CssClass="input-append" DefaultButton="SubmitSearch" ViewStateMode="Enabled" runat="server">
		<asp:PlaceHolder ID="Subject" Visible="False" runat="server">
			<div class="btn-group btn-select" data-target="#KnowledgeBaseSubjectFilter" data-focus="#KnowledgeBaseQuery">
				<a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
					<span class="selected"><%: SubjectName %></span>
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a data-value="<%: SubjectId %>"><%: SubjectName %></a>
					</li>
					<li>
						<a data-value=""><%: Html.SnippetLiteral("Knowledge Base Default Search Filter Text", "All Articles") %></a>
					</li>
				</ul>
			</div>
			<asp:DropDownList ID="KnowledgeBaseSubjectFilter" ClientIDMode="Static" CssClass="input-medium btn-select" DataTextField="Text" DataValueField="Value" runat="server" />
		</asp:PlaceHolder>
		<asp:TextBox ID="KnowledgeBaseQuery" ClientIDMode="Static" CssClass="span4" runat="server"/>
		<asp:LinkButton ID="SubmitSearch" CssClass="btn" OnClick="SubmitSearch_Click" runat="server">
			<i class="icon-search"></i>
		</asp:LinkButton>
	</asp:Panel>
</div>
