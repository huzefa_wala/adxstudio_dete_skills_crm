﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="Subject.aspx.cs" Inherits="Site.Areas.KnowledgeBase.Pages.Subject" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import namespace="Adxstudio.Xrm" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Register src="../Controls/Search.ascx" tagname="Search" tagprefix="adx" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/KnowledgeBase/css/knowledgebase.css") %>">
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarAbove" runat="server"/>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<adx:Search runat="server" />
	
	<% var childNodes = Html.SiteMapChildNodes().ToArray(); %>
	<% if (childNodes.Any()) { %>
		<div class="kb-subjects">
			<div class="page-header">
				<h3><%: Html.TextSnippet("Knowledge Base Subjects Heading") ?? new HtmlString("Subjects") %></h3>
			</div>
			<% foreach (var batch in childNodes.Batch(2)) { %>
				<div class="row">
					<% foreach (var child in batch) { %>
						<div class="span4">
							<h4><a href="<%: child.Url %>"><%: child.Title %></a></h4>
						</div>
					<% } %>
				</div>
			<% } %>
		</div>
	<% } %>

	<asp:Panel ID="SubjectSearch" CssClass="search-results" Visible="False" runat="server">
		<adx:SearchDataSource ID="SearchData" Query="<%$ SiteSetting: knowledgebase/subject/query %>" LogicalNames="kbarticle" OnSelected="SearchData_OnSelected" runat="server">
			<SelectParameters>
				<asp:QueryStringParameter Name="PageNumber" QueryStringField="page" />
				<asp:Parameter Name="PageSize" DefaultValue="10" />
			</SelectParameters>
		</adx:SearchDataSource>
		
		<asp:Repeater DataMember="Info" DataSourceID="SearchData" runat="server">
			<ItemTemplate>
				<asp:Panel Visible='<%# ((int)Eval("Count")) > 0 %>' runat="server">
					<div class="page-header">
						<h3 class="summary">Results <%# Eval("FirstResultNumber") %>&ndash;<%# Eval("LastResultNumber") %> of <%# Eval("ApproximateTotalHits") %></h3>
					</div>
					
					<asp:ListView DataSourceID="SearchData" ID="SearchResults" OnDataBound="SearchResults_DataBound" runat="server">
						<LayoutTemplate>
							<ul>
								<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
							</ul>
						</LayoutTemplate>
						<ItemTemplate>
							<li runat="server">
								<h4 class="title">
									<asp:HyperLink Text='<%# Eval("Title") %>' NavigateUrl='<%# Eval("Url") %>' runat="server" />
								</h4>
							</li>
						</ItemTemplate>
					</asp:ListView>
					
					<div class="pagination">
						<adx:UnorderedListDataPager ID="SearchResultPager" PagedControlID="SearchResults" QueryStringField="page" PageSize="10" runat="server">
							<Fields>
								<adx:ListItemNextPreviousPagerField ShowNextPageButton="false" ShowFirstPageButton="True" FirstPageText="&laquo;" PreviousPageText="&lsaquo;" />
								<adx:ListItemNumericPagerField ButtonCount="10" PreviousPageText="&hellip;" NextPageText="&hellip;" />
								<adx:ListItemNextPreviousPagerField ShowPreviousPageButton="false" ShowLastPageButton="True" LastPageText="&raquo;" NextPageText="&rsaquo;" />
							</Fields>
						</adx:UnorderedListDataPager>
					</div>
				</asp:Panel>
				<asp:Panel Visible='<%# ((int)Eval("Count")) == 0 %>' runat="server">
					<div class="alert alert-block">
						<%: Html.HtmlSnippet("Knowledge Base Subject No Articles") ?? new HtmlString("<p>There are no published knowledge base articles for this subject.</p>") %>
					</div>
				</asp:Panel>
			</ItemTemplate>
		</asp:Repeater>
	</asp:Panel>
	
	<adx:SearchDataSource ID="MostPopularArticlesSearchData" LogicalNames="kbarticle" OnSelected="SearchData_OnSelected" runat="server">
		<SelectParameters>
			<asp:Parameter Name="PageSize" DefaultValue="10" />
		</SelectParameters>
	</adx:SearchDataSource>
		
	<asp:ListView ID="MostPopularArticles" DataSourceID="MostPopularArticlesSearchData" runat="server">
		<LayoutTemplate>
			<div class="search-results well kb-popular">
				<h4>
					<crm:Snippet SnippetName="Knowledge Base Most Popular Articles" DefaultText="Most Popular Articles" EditType="text" runat="server"/>
				</h4>
				<ol>
					<asp:PlaceHolder ID="itemPlaceHolder" runat="server"/>
				</ol>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<li runat="server">
				<h5 class="title">
					<asp:HyperLink Text='<%# Eval("Title") %>' NavigateUrl='<%# Eval("Url") %>' runat="server" />
				</h5>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>
