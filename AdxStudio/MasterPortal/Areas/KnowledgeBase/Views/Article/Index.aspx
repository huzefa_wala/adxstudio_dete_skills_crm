﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" Inherits="System.Web.Mvc.ViewPage<Site.Areas.KnowledgeBase.ViewModels.ArticleViewModel>" %>
<%@ OutputCache CacheProfile="User" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/KnowledgeBase/css/knowledgebase.css") %>">
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<ul class="breadcrumb">
		<% foreach (var node in Html.SiteMapPath().Where(e => e.Item2 != SiteMapNodeType.Current)) { %>
			<li>
				<a href="<%: node.Item1.Url %>"><%: node.Item1.Title %></a>
				<span class="divider">/</span>
			</li>
		<% } %>
		<li class="active">
			<%: Model.Number %>
		</li>
	</ul>
	<div class="kb-article">
		<div>
			<%: Model.Content %>
		</div>
		<% if (Model.RelatedArticles.Any()) { %>
			<div class="page-header">
				<h4><%: Html.TextSnippet("Knowledge Base Related Articles Heading") ?? new HtmlString("Related Articles") %></h4>
			</div>
			<ul class="nav">
				<% foreach (var relatedArticle in Model.RelatedArticles) { %>
					<li>
						<a href="<%: relatedArticle.Url %>"><%: relatedArticle.Title %></a>
					</li>
				<% } %>
			</ul>
		<% } %>
	</div>
</asp:Content>
