﻿<%@ Page Title="" Language="C#" MasterPageFile="../Shared/Account.Master" Inherits="System.Web.Mvc.ViewPage<Site.Areas.Account.ViewModels.PasswordRecoveryViewModel>" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="form-horizontal">
		<% using (Html.BeginForm("PasswordRecoveryEmail", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
			<%= Html.AntiForgeryToken() %>
			<fieldset>
				<legend><%: Html.SnippetLiteral("Account/PasswordRecovery/UserNameFormHeading", "Reset your password") %></legend>
				<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"}) %>
				<div class="control-group">
					<label class="control-label required" for="UserName"><%: Html.SnippetLiteral("Account/PasswordRecovery/UserNameLabel", "Username") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.Username) %>
						<p class="help-block"><%: Html.SnippetLiteral("Account/PasswordRecovery/UserNameInstructionsText", "Enter your username to reset your password.") %></p>
					</div>
				</div>
				<div class="form-actions">
					<input id="submit-username" type="submit" class="btn btn-primary" value="<%: Html.SnippetLiteral("Account/PasswordRecovery/UserNameFormButtonText", "Submit") %>"/>
				</div>
			</fieldset>
		<% } %>
	</div>
</asp:Content>
