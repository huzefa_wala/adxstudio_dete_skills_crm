﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Site.Areas.Account.ViewModels.RedeemViewModel>" %>

<% using (Html.BeginForm("RedeemLocal", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
	<%= Html.AntiForgeryToken() %>
	<%= Html.HiddenFor(model => model.InvitationCode) %>
	<%= Html.HiddenFor(model => model.ChallengeAnswer) %>
	<div class="form-horizontal">
		<fieldset>
			<legend><%: Html.SnippetLiteral("Account/Redeem/LocalSignUpFormHeading", "Sign up for a new local account") %></legend>
			<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"}) %>
			<div class="control-group">
				<label class="control-label required" for="UserName"><%: Html.SnippetLiteral("Account/Redeem/UserNameLabel", "Username") %></label>
				<div class="controls">
					<%= Html.TextBoxFor(model => model.Username) %>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label required" for="Password"><%: Html.SnippetLiteral("Account/Redeem/PasswordLabel", "Password") %></label>
				<div class="controls">
					<%= Html.PasswordFor(model => model.Password) %>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label required" for="ConfirmPassword"><%: Html.SnippetLiteral("Account/Redeem/ConfirmPasswordLabel", "Confirm Password") %></label>
				<div class="controls">
					<%= Html.PasswordFor(model => model.ConfirmPassword) %>
				</div>
			</div>
			<% if (ViewBag.RequiresUniqueEmail && !ViewBag.RequiresConfirmation) { %>
				<div class="control-group">
					<label class="control-label required" for="Email"><%: Html.SnippetLiteral("Account/Redeem/EmailLabel", "Email") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.Email) %>
					</div>
				</div>
			<% } %>
			<% if (ViewBag.RequiresQuestionAndAnswer) { %>
				<div class="control-group">
					<label class="control-label required" for="Question"><%: Html.SnippetLiteral("Account/Redeem/QuestionLabel", "Question") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.Question) %>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label required" for="Answer"><%: Html.SnippetLiteral("Account/Redeem/AnswerLabel", "Answer") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.Answer) %>
					</div>
				</div>
			<% } %>
			<div class="form-actions">
				<input id="submit-redeem-local" type="submit" class="btn btn-primary" value="<%: Html.SnippetLiteral("Account/Redeem/LocalSignUpFormButtonText", "Create Local Account") %>"/>
			</div>
		</fieldset>
	</div>
<% } %>
