﻿<%@ Page Language="C#" MasterPageFile="../Shared/Account.master" Inherits="System.Web.Mvc.ViewPage<Site.Areas.Account.ViewModels.RedeemViewModel>" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<% if (!string.IsNullOrWhiteSpace(ViewBag.ResultCode as string)) { %>
		<div class="validation-summary">
			<% if (string.Equals(ViewBag.ResultCode, "confirm")) { %>
				<span class="alert alert-success"><%: Html.SnippetLiteral("Account/Redeem/ConfirmMessage", "Please check your email for a confirmation link or an invitation code.") %></span>
			<% } else if (string.Equals(ViewBag.ResultCode, "unregistered")) { %>
				<span class="alert alert-info"><%: Html.SnippetLiteral("Account/Redeem/UnregisteredMessage", "This account is not eligible for transfer. Please sign in with a registered Windows Live ID account.") %></span>
			<% } else if (string.Equals(ViewBag.ResultCode, "inactive")) { %>
				<span class="alert alert-danger"><%: Html.SnippetLiteral("Account/Redeem/InactiveMessage", "This account is pending registration.") %></span>
			<% } else if (string.Equals(ViewBag.ResultCode, "invalid-invitation-code")) { %>
				<span class="alert alert-error"><%: Html.SnippetLiteral("Account/Redeem/InvalidMessage", "The invitation code is invalid. Please enter the code and try again.") %></span>
			<% } %>
		</div>
	<% } %>
	<% using (Html.BeginForm("RedeemInvitationCode", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
		<%= Html.AntiForgeryToken() %>
		<%= Html.HiddenFor(model => model.wa) %>
		<%= Html.HiddenFor(model => model.wresult) %>
		<%= Html.HiddenFor(model => model.openauthresult) %>
		<div class="form-horizontal">
			<fieldset>
				<legend><%: Html.SnippetLiteral("Account/Redeem/InvitationCodeFormHeading", "Sign up with an invitation code") %></legend>
				<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"}) %>
				<div class="control-group">
					<label class="control-label required" for="InvitationCode"><%: Html.SnippetLiteral("Account/Redeem/InvitationCodeLabel", "Invitation Code") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.InvitationCode) %>
					</div>
				</div>
				<div class="form-actions">
					<input id="submit-invitation-code" type="submit" class="btn btn-primary" value="<%: Html.SnippetLiteral("Account/Redeem/InvitationCodeFormButtonText", "Submit") %>"/>
				</div>
			</fieldset>
		</div>
	<% } %>
</asp:Content>
