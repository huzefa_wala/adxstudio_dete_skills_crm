﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Site.Areas.Account.ViewModels.EmailConfirmationViewModel>" %>

<% using (Html.BeginForm("SignUpByEmail", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
	<%= Html.AntiForgeryToken() %>
	<div class="form-horizontal">
		<fieldset>
			<legend><%: Html.SnippetLiteral("Account/SignUp/EmailConfirmationFormHeading", "Sign up for a new account") %></legend>
			<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"}) %>
			<div class="control-group">
				<label class="control-label" for="Email"><%: Html.SnippetLiteral("Account/SignUp/EmailLabel", "Provide a valid email") %></label>
				<div class="controls">
					<%= Html.TextBoxFor(model => model.Email) %>
				</div>
			</div>
			<div class="form-actions">
				<input id="submit-email-confirmation" type="submit" class="btn btn-primary" value="<%: Html.SnippetLiteral("Account/SignUp/EmailConfirmationFormButtonText", "Submit") %>"/>
			</div>
		</fieldset>
	</div>
<% } %>
