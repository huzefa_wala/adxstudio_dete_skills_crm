﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Site.Areas.Account.ViewModels.SignInLocalViewModel>" %>
<%@ Import namespace="Adxstudio.Xrm.Collections.Generic" %>

<% using (Html.BeginForm("SignInLocal", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
	<%= Html.AntiForgeryToken() %>
	<div class="form-horizontal">
		<fieldset>
			<legend><%: Html.SnippetLiteral("Account/SignIn/LocalSignInFormHeading", "Sign in with a local account") %></legend>
			<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"}) %>
			<div class="control-group">
				<label class="control-label required" for="UserName"><%: Html.SnippetLiteral("Account/SignIn/UserNameLabel", "Username") %></label>
				<div class="controls">
					<%= Html.TextBoxFor(model => model.UserName) %>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label required" for="Password"><%: Html.SnippetLiteral("Account/SignIn/PasswordLabel", "Password") %></label>
				<div class="controls">
					<%= Html.PasswordFor(model => model.Password) %>
				</div>
			</div>
			<% if (ViewBag.DisplayRememberMe) { %>
				<div class="control-group">
					<label class="control-label" for="RememberMe"><%: Html.SnippetLiteral("Account/SignIn/RememberMeLabel", "Remember me?") %></label>
					<div class="controls">
						<label class="checkbox"><%= Html.CheckBoxFor(model => model.RememberMe) %></label>
					</div>
				</div>
			<% } %>
			<div class="form-actions">
				<input id="submit-signin-local" type="submit" class="btn btn-primary" value="<%: Html.SnippetLiteral("Account/SignIn/LocalSignInFormButtonText", "Sign in") %>"/>
				<% if (Membership.EnablePasswordReset) { %>
					<a class="help-link" href="<%= Html.SiteMarkerUrl("Login") %>PasswordRecovery?<%: (ViewBag.PasswordRecoveryQueryString as NameValueCollection).ToQueryString() %>"><%: Html.SnippetLiteral("Account/SignIn/PasswordRecovery", "Forgot Your Password?") %></a>
				<% } %>
			</div>
		</fieldset>
	</div>
<% } %>
