﻿<%@ Page Title="" Language="C#" MasterPageFile="../Shared/Account.Master" Inherits="System.Web.Mvc.ViewPage<Site.Areas.Account.ViewModels.RedeemViewModel>" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<div class="row">
		<% if (ViewBag.MembershipProviderEnabled) { %>
			<div class="span6">
				<% Html.RenderPartial("RedeemLocal", Model); %>
			</div>
		<% } %>
		<% if (ViewBag.FederatedAuthEnabled) { %>
			<div class="span6">
				<% Html.RenderPartial("RedeemFederated", Model); %>
			</div>
		<% } %>
	</div>
</asp:Content>
