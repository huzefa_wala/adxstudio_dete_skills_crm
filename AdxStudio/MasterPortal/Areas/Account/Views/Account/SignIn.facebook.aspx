﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<!DOCTYPE html>
<html>
	<head runat="server">
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title><%: Html.AttributeLiteral("adx_title") ?? Html.AttributeLiteral("adx_name") %> <%= Html.SnippetLiteral("Browser Title Suffix") %></title>
		<%: Html.ContentStyles(only: new Dictionary<string, string>
			{
				{"bootstrap.min.css", Url.Content("~/css/bootstrap.min.css")}
			}) %>
		<link rel="stylesheet" href="<%: Url.Content("~/css/portal.css") %>">
		<link rel="stylesheet" href="<%: Url.Content("~/xrm-adx/css/yui-skin-sam-2.9.0/skin.css") %>">
		<!-- HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<% Html.RenderPartialFromSetting("Head/Template"); %>
		<%: Html.ContentStyles(except: new [] { "bootstrap.min.css" }) %>
	</head>
	<body>
		<% if (Request.IsAuthenticated && !string.IsNullOrWhiteSpace((string)ViewBag.ReturnUrl)) { %>
			<script type="text/javascript">
				window.location.href = "<%: (string)ViewBag.ReturnUrl %>";
			</script>
		<% } %>
		<% Html.RenderPartialFromSetting("Header/Template", "HeaderNavbar"); %>
		<script src="<%: Url.Content("~/xrm-adx/js/jquery-1.8.3.min.js") %>"></script>
		<script src="<%: Url.Content("~/xrm-adx/js/yui-2.9.0-combo.min.js") %>"></script>
		<div class="container">
			<% var signInPrompt = Html.HtmlSnippet("Facebook/SignIn/Prompt"); %>
			<% if (signInPrompt == null) { %>
				<div class="alert alert-block alert-info">
					<p>You must be signed in into Facebook to use this application.</p>
				</div>
			<% } else { %>
				<%: signInPrompt %>
			<% } %>
		</div>
		<% Html.RenderPartialFromSetting("Footer/Template", "FooterMenuWithCopy"); %>
		<%: Html.EntityEditingMetadata() %>
		<%: Html.EditingStyles(new []
			{
				"~/xrm-adx/css/yui-skin-sam-2.9.0/skin.css"
			}) %>
		<%: Html.EditingScripts(dependencyScriptPaths: new []
			{
				"~/xrm-adx/js/jquery-ui-1.10.0.min.js",
				"~/xrm-adx/js/tinymce/tinymce.min.js"
			}, extensionScriptPaths: new string[] {}) %>
		<script src="<%: Url.Content("~/js/bootstrap.min.js") %>"></script>
		<script src="<%: Url.Content("~/js/date.js") %>"></script>
		<script src="<%: Url.Content("~/js/jquery.timeago.js") %>"></script>
		<script src="<%: Url.Content("~/js/google-code-prettify/prettify.js") %>"></script>
		<script src="<%: Url.Content("~/js/portal.js") %>"></script>
		<%= Html.SnippetLiteral("Tracking Code") %>
	</body>
</html>
