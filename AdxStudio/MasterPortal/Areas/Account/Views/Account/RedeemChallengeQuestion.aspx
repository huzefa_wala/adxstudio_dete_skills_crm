﻿<%@ Page Title="" Language="C#" MasterPageFile="../Shared/Account.Master" Inherits="System.Web.Mvc.ViewPage<Site.Areas.Account.ViewModels.RedeemViewModel>" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<% using (Html.BeginForm("RedeemChallengeAnswer", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
		<%= Html.AntiForgeryToken() %>
		<%= Html.HiddenFor(model => model.wa) %>
		<%= Html.HiddenFor(model => model.wresult) %>
		<%= Html.HiddenFor(model => model.openauthresult) %>
		<%= Html.HiddenFor(model => model.InvitationCode) %>
		<%= Html.HiddenFor(model => model.ChallengeQuestion) %>
		<div class="form-horizontal">
			<fieldset>
				<legend><%: Html.SnippetLiteral("Account/Redeem/ChallengeQuestionFormHeading", "Answer the challenge question") %></legend>
				<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"}) %>
				<div class="control-group">
					<label class="control-label" for="ChallengeQuestion"><%: Html.SnippetLiteral("Account/Redeem/ChallengeQuestionLabel", "Question") %></label>
					<div class="controls">
						<span class="help-inline"><%: Model.ChallengeQuestion %></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label required" for="ChallengeAnswer"><%: Html.SnippetLiteral("Account/Redeem/ChallengeAnswerLabel", "Answer") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.ChallengeAnswer) %>
					</div>
				</div>
				<div class="form-actions">
					<input id="submit-challenge-answer" type="submit" class="btn btn-primary" value="<%: Html.SnippetLiteral("Account/Redeem/ChallengeQuestionFormButtonText", "Submit") %>"/>
				</div>
			</fieldset>
		</div>
	<% } %>
</asp:Content>
