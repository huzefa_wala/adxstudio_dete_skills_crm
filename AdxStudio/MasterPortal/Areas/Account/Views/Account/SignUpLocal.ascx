﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Site.Areas.Account.ViewModels.SignUpLocalViewModel>" %>

<% using (Html.BeginForm("SignUpLocal", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
	<%= Html.AntiForgeryToken() %>
	<div class="form-horizontal">
		<fieldset>
			<legend><%: Html.SnippetLiteral("Account/SignUp/LocalSignUpFormHeading", "Sign up for a new local account") %></legend>
			<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"}) %>
			<div class="control-group">
				<label class="control-label required" for="UserName"><%: Html.SnippetLiteral("Account/SignUp/UserNameLabel", "Username") %></label>
				<div class="controls">
					<%= Html.TextBoxFor(model => model.Username) %>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label required" for="Password"><%: Html.SnippetLiteral("Account/SignUp/PasswordLabel", "Password") %></label>
				<div class="controls">
					<%= Html.PasswordFor(model => model.Password) %>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label required" for="ConfirmPassword"><%: Html.SnippetLiteral("Account/SignUp/ConfirmPasswordLabel", "Confirm Password") %></label>
				<div class="controls">
					<%= Html.PasswordFor(model => model.ConfirmPassword) %>
				</div>
			</div>
			<% if (ViewBag.RequiresUniqueEmail) { %>
				<div class="control-group">
					<label class="control-label required" for="Email"><%: Html.SnippetLiteral("Account/SignUp/EmailLabel", "Email") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.Email) %>
					</div>
				</div>
			<% } %>
			<% if (ViewBag.RequiresQuestionAndAnswer) { %>
				<div class="control-group">
					<label class="control-label required" for="Question"><%: Html.SnippetLiteral("Account/SignUp/QuestionLabel", "Question") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.Question) %>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label required" for="Answer"><%: Html.SnippetLiteral("Account/SignUp/AnswerLabel", "Answer") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.Answer) %>
					</div>
				</div>
			<% } %>
			<div class="form-actions">
				<input id="submit-signup-local" type="submit" class="btn btn-primary" value="<%: Html.SnippetLiteral("Account/SignUp/LocalSignUpFormButtonText", "Sign Up") %>"/>
			</div>
		</fieldset>
	</div>
<% } %>
