﻿<%@ Page Title="" Language="C#" MasterPageFile="../Shared/Account.Master" Inherits="System.Web.Mvc.ViewPage<Site.Areas.Account.ViewModels.PasswordRecoveryViewModel>" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<div class="form-horizontal">
		<% using (Html.BeginForm("PasswordRecoverySecurityAnswer", "Account", ViewBag.ReturnUrlRouteValues as RouteValueDictionary)) { %>
			<%= Html.AntiForgeryToken() %>
			<%= Html.HiddenFor(model => model.Username) %>
			<%= Html.HiddenFor(model => model.Question) %>
			<fieldset>
				<legend><%: Html.SnippetLiteral("Account/PasswordRecovery/SecurityQuestionFormHeading", "Reset your password") %></legend>
				<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"}) %>
				<div class="control-group">
					<label class="control-label" for="UserName"><%: Html.SnippetLiteral("Account/PasswordRecovery/UserNameLabel", "Username") %></label>
					<div class="controls">
						<span class="help-inline"><%: Model.Username %></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Question"><%: Html.SnippetLiteral("Account/PasswordRecovery/QuestionLabel", "Question") %></label>
					<div class="controls">
						<span class="help-inline"><%: Model.Question %></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label required" for="Answer"><%: Html.SnippetLiteral("Account/PasswordRecovery/AnswerLabel", "Answer") %></label>
					<div class="controls">
						<%= Html.TextBoxFor(model => model.Answer) %>
						<p class="help-block"><%: Html.SnippetLiteral("Account/PasswordRecovery/AnswerInstructionsText", "Answer the question correctly to receive your password.") %></p>
					</div>
				</div>
				<div class="form-actions">
					<input id="submit-security-answer" type="submit" class="btn btn-primary" value="<%: Html.SnippetLiteral("Account/PasswordRecovery/SecurityQuestionFormButtonText", "Submit") %>"/>
				</div>
			</fieldset>
		<% } %>
	</div>
</asp:Content>
