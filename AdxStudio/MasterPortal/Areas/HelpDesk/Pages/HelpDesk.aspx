﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HelpDesk.aspx.cs" Inherits="Site.Areas.HelpDesk.Pages.HelpDesk" MasterPageFile="~/MasterPages/WebForms.master" %>
<%@ OutputCache VaryByParam="*" VaryByCustom="user" Duration="60" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm.Collections.Generic" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Microsoft.Xrm.Portal.IdentityModel.Configuration" %>
<%@ Register src="../Controls/CaseDeflection.ascx" tagname="CaseDeflection" tagprefix="adx" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/HelpDesk/css/helpdesk.css") %>">
</asp:Content>

<asp:Content ContentPlaceHolderID="Breadcrumbs" runat="server"/>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server" ViewStateMode="Enabled">
	<adx:CaseDeflection ID="CaseDeflection" runat="server"/>
	
	<div class="cases">
		<asp:Panel ID="NoCaseAccessWarning" CssClass="alert alert-block" Visible="False" runat="server">
			<asp:LoginView runat="server">
				<AnonymousTemplate>
					<crm:Snippet runat="server" SnippetName="cases/access/signin" DefaultText="You must sign in to view your cases." Editable="true" EditType="html"/>
					<% Html.RenderPartial("SignInLink"); %>
				</AnonymousTemplate>
				<LoggedInTemplate>
					<crm:Snippet runat="server" SnippetName="cases/access/nopermissions" DefaultText="You do not have permission to view cases." Editable="true" EditType="html"/>
				</LoggedInTemplate>
			</asp:LoginView>
		</asp:Panel>

		<asp:Panel ID="CaseControls" CssClass="controls form-inline" runat="server">
			<asp:Button ID="CreateCase" CssClass="pull-right btn btn-primary" Text='<%$ Snippet: cases/createbuttontext, Open a New Support Request %>' OnClick="CreateCase_Click" runat="server"/>
			<asp:Panel ID="CaseFilters" runat="server">
				<div class="input-prepend">
					<div class="add-on">
						<i class="icon-filter"></i>
					</div>
					<asp:DropDownList ID="CustomerFilter" AutoPostBack="true" runat="server"/>
				</div>
				<asp:DropDownList ID="StatusDropDown" AutoPostBack="true" runat="server">
					<asp:ListItem>Active</asp:ListItem>
					<asp:ListItem>Closed</asp:ListItem>
				</asp:DropDownList>
			</asp:Panel>
		</asp:Panel>
		<asp:GridView ID="CaseList" runat="server" CssClass="table table-striped" GridLines="None" AlternatingRowStyle-CssClass="alternate-row" OnRowDataBound="CaseList_OnRowDataBound" >
			<EmptyDataTemplate>
				<crm:Snippet  runat="server" SnippetName="cases/view/empty" DefaultText="There are no cases for the selected filter." Editable="true" EditType="html"/>
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
	
	<asp:ListView ID="ChildView" runat="server">
		<LayoutTemplate>
			<div class="listing">
				<h4>
					<i class="icon-folder-open"></i>
					<crm:Snippet SnippetName="Page Children Heading" DefaultText="In This Section" EditType="text" runat="server"/>
				</h4>
				<ul>
					<li id="itemPlaceholder" runat="server"/>
				</ul>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<li runat="server">
				<h4>
					<a href="<%# Eval("Url") %>"><%# Eval("Title") %></a>
				</h4>
				<crm:CrmEntityDataSource ID="ChildEntity" DataItem='<%# Eval("Entity") %>' runat="server"/>
				<crm:Property DataSourceID="ChildEntity" PropertyName='<%# GetSummaryPropertyName(Eval("Entity.LogicalName", "{0}")) %>' EditType="html" runat="server"/>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>
