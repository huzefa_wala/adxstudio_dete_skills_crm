﻿<%@ Control Language="C#" ViewStateMode="Enabled" EnableViewState="true" AutoEventWireup="true" CodeBehind="CaseDeflection.ascx.cs" Inherits="Site.Areas.HelpDesk.Controls.CaseDeflection" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm.Collections.Generic" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Microsoft.Xrm.Portal.IdentityModel.Configuration" %>

<div class="case-deflection">
	<div class="search well form-inline">
		<crm:Snippet runat="server" SnippetName="cases/casedeflection/header" DefaultText="<h3>What can we help you with?</h3>" Editable="true" EditType="html"/>
		<asp:TextBox ID="Subject" CssClass="input-xlarge subject" placeholder="e.g. User login is failing" runat="server"/>
		<asp:DropDownList ID="Product" CssClass="product" runat="server"/>
		<asp:Button CssClass="btn btn-primary" Text="<%$ Snippet: cases/casedeflection/searchbuttontext, Search %>" OnClick="Submit_Click" runat="server"/>
		<asp:HyperLink Text="<%$ Snippet: cases/casedeflection/searchresetbuttontext, Clear %>" CssClass="btn" NavigateUrl='<%$ CrmSiteMap: Current, Url %>' runat="server"/>
	</div>

	<asp:Panel ID="Deflection" Visible="False" CssClass="search-results" runat="server">
		<adx:SearchDataSource ID="SearchData" Query="(+_logicalname:incident~0.9^2 +statecode:@resolvedincidentstatecode +(@subject)) OR (-_logicalname:incident~0.9 +(@subject))" LogicalNames="incident,adx_issue,adx_webpage,adx_communityforumthread,adx_communityforumpost,adx_blogpost,kbarticle" OnSelected="SearchData_OnSelected" runat="server">
			<SelectParameters>
				<asp:QueryStringParameter Name="subject" QueryStringField="subject"/>
				<asp:QueryStringParameter Name="product" QueryStringField="product"/>
				<asp:Parameter Name="resolvedincidentstatecode" DefaultValue="1" />
				<asp:Parameter Name="PageNumber" DefaultValue="1" />
				<asp:Parameter Name="PageSize" DefaultValue="10" />
			</SelectParameters>
		</adx:SearchDataSource>
		<asp:ListView DataSourceID="SearchData" ID="SearchResults" runat="server">
			<LayoutTemplate>
				<ul>
					<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
				</ul>
			</LayoutTemplate>
			<ItemTemplate>
				<li runat="server">
					<h3><asp:HyperLink Text='<%# Eval("Title") %>' NavigateUrl='<%# Eval("Url") %>' runat="server" /></h3>
					<p class="fragment"><%# Eval("Fragment") %></p>
					<div>
						<asp:Label Visible='<%# (string)Eval("EntityLogicalName") == "adx_communityforum" || (string)Eval("EntityLogicalName") == "adx_communityforumthread" || (string)Eval("EntityLogicalName") == "adx_communityforumpost" %>' CssClass="label" Text="Forums" runat="server"/>
						<asp:Label Visible='<%# (string)Eval("EntityLogicalName") == "adx_blog" || (string)Eval("EntityLogicalName") == "adx_blogpost" || (string)Eval("EntityLogicalName") == "adx_blogpostcomment" %>' CssClass="label label-info" Text="Blogs" runat="server"/>
						<asp:Label Visible='<%# (string)Eval("EntityLogicalName") == "adx_event" || (string)Eval("EntityLogicalName") == "adx_eventschedule" %>' CssClass="label label-info" Text="Events" runat="server"/>
						<asp:Label Visible='<%# (string)Eval("EntityLogicalName") == "adx_idea" %>' CssClass="label label-success" Text="Ideas" runat="server"/>
						<asp:Label Visible='<%# (string)Eval("EntityLogicalName") == "adx_issue" %>' CssClass="label label-important" Text="Issues" runat="server"/>
						<asp:Label Visible='<%# (string)Eval("EntityLogicalName") == "incident" %>' CssClass="label label-warning" Text="Help Desk" runat="server"/>
						<asp:Label Visible='<%# (string)Eval("EntityLogicalName") == "kbarticle" %>' CssClass="label label-info" Text="Knowledge Base" runat="server"/>
						<asp:HyperLink Text='<%# GetDisplayUrl(Eval("Url")) %>' NavigateUrl='<%# Eval("Url") %>' runat="server" />
					</div>
				</li>
			</ItemTemplate>
		</asp:ListView>
		<div class="open-request alert alert-block alert-info">
			<div class="controls pull-right">
				<asp:Panel ID="NoCaseAccessWarning" Visible="False" runat="server">
					<asp:LoginView runat="server">
						<AnonymousTemplate>
							<crm:Snippet runat="server" SnippetName="cases/access/create/signin" DefaultText="You must sign in to open a new support request. " Editable="true" EditType="html"/>
							<% Html.RenderPartial("SignInLink"); %>
						</AnonymousTemplate>
						<LoggedInTemplate>
							<crm:Snippet runat="server" SnippetName="cases/access/create/nopermissions" DefaultText="You do not have permission to create cases." Editable="true" EditType="html"/>
						</LoggedInTemplate>
					</asp:LoginView>
				</asp:Panel>
				<asp:Button ID="OpenNewSupportRequest" CssClass="btn btn-large btn-primary" runat="server" Text="<%$ Snippet: cases/casedeflection/createbuttontext, Open a New Support Request %>" OnClick="OpenNewSupportRequest_OnClick" />
			</div>
			<crm:Snippet runat="server" SnippetName="cases/casedeflection/answernotfound" DefaultText="<p>Didn't find anything helpful?</p>" Editable="true" EditType="html"/>
		</div>
	</asp:Panel>
</div>