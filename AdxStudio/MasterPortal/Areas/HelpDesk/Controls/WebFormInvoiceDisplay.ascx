﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebFormInvoiceDisplay.ascx.cs" Inherits="Site.Areas.HelpDesk.Controls.WebFormInvoiceDisplay" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
	<Scripts>
		<asp:ScriptReference Path="~/js/jquery.formatCurrency_1.4.0.min.js" />
	</Scripts>
</asp:ScriptManagerProxy>

<script type="text/javascript">
	$(document).ready(function () {
		$(".money").formatCurrency();
	});
</script>

<crm:Snippet runat="server" SnippetName="cases/HelpDesk/InvoicePageHeader" DefaultText="<div class='alert alert-success'>Thank you, your support plan order has been approved!</div>" Editable="true" EditType="html"/>
<h3><asp:Literal  runat="server" Text="<%$ Snippet: ccases/HelpDesk/InvoiceNumberLabel, Invoice Number:  %>" /> <%= InvoiceNumber %></h3>
<h3><crm:Snippet  runat="server" SnippetName="cases/HelpDesk/InvoiceSummaryLabel" DefaultText="Order Summary: " Editable="true" EditType="html"/></h3>
<div class="span6">
	<p><%= InvoiceProduct%> <strong class="pull-right money"><%= InvoicePreTaxTotal%></strong></p>
	<p><asp:Literal runat="server" Text="<%$ Snippet: cases/HelpDesk/InvoiceTaxLabel, Total Tax:  %>" /> <strong class="pull-right money"><%= InvoiceTaxTotal %></strong></p>
	<hr/>
	<p><asp:Literal runat="server" Text="<%$ Snippet: cases/HelpDesk/InvoiceTotalLabel, Total Charge: %>" /> <strong class="pull-right money"><%= InvoiceTotal %></strong></p>
</div>
<div class="clearfix"></div>
