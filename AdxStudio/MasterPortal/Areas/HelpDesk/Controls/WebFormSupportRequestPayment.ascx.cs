﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Adxstudio.Xrm.Commerce;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Controls;
using Xrm;
using Adxstudio.Xrm.Cms;
using AuthorizeNet;

namespace Site.Areas.HelpDesk.Controls
{
	public partial class WebFormSupportRequestPayment : WebFormPortalUserControl
	{
		private const bool _testModeEnabled = false;
		private const string TestCreditCardNumber = "4111111111111111";
		private const string TestCreditCardExpiry = "0130";
		private const string TestCreditCardExpiryYear = "2030";
		private const string TestCreditCardExpiryMonth = "01";
		private const string TestCreditCardVerificationValue = "123";
		private ShoppingCart _cart;

		protected string FirstName;
		protected string LastName;
		protected string Email;
		protected string Address;
		protected string City;
		protected string Province;
		protected string Country;
		protected string PostalCode;
		protected string CreditCardNumber;
		protected string CreditCardExpiryMonth;
		protected string CreditCardExpiryYear;
		protected string CreditCardExpiry;
		protected string CreditCardVerificationValue;
		protected string FingerprintHash;
		protected string FingerprintSequence;
		protected string FingerprintTimestamp;
		protected string ApiLogin;
		protected string Amount;
		protected decimal DecimalAmount;
		protected string Tax;
		protected string TotalAmount;
		protected string RelayURL;
		protected string RelayResponse;
		protected string OrderID;
		protected bool AuthorizeNet;

		protected bool TestModeEnabled
		{
			get { return (_testModeEnabled || IsPaymentDemo); }
		}

		protected string VisitorID
		{
			get { return Context.Profile.UserName; }
		}

		protected Guid ShoppingCartId
		{

			get
			{
				var supportRequest = XrmContext.CreateQuery("adx_supportrequest")
					.FirstOrDefault(sr => sr.GetAttributeValue<Guid>("adx_supportrequestid") == CurrentStepEntityID);

				return supportRequest == null ? Guid.Empty : supportRequest.GetAttributeValue<EntityReference>("adx_shoppingcartid").Id;
			}
		}
		
		protected Guid SessionId
		{
			get
			{
				Guid sessionId;
				return !Guid.TryParse(Request["sessionid"], out sessionId) ? Guid.Empty : sessionId;
			}
		}

		//TODO: switch to an enum instead of Bool.
		public bool IsPaymentPaypal
		{
			get
			{
				var paymentProviderSetting = ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/PaymentProvider") ?? "PayPal";

				return paymentProviderSetting == "PayPal";
			}
		}

		//TODO: switch to an enum instead of Bool.
		public bool IsPaymentAuthorizeNet
		{
			get
			{
				var paymentProviderSetting = ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/PaymentProvider") ?? "PayPal";

				return paymentProviderSetting == "Authorize.Net";
			}
		}

		//TODO: switch to an enum instead of Bool.
		public bool IsPaymentDemo
		{
			get
			{
				var paymentProviderSetting = ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/PaymentProvider") ?? "PayPal";

				return paymentProviderSetting == "Demo";
			}
		}

		protected bool Paid
		{
			get
			{
				if (Request["Payment"] != "Successful")
				{
					return false;
				}

				var supportRequest = XrmContext.CreateQuery("adx_supportrequest")
					.FirstOrDefault(sr => sr.GetAttributeValue<Guid>("adx_supportrequestid") == CurrentStepEntityID);

				if (supportRequest == null)
				{
					return false;
				}

				var order =
					XrmContext.SalesOrderSet.FirstOrDefault(o => o.SalesOrderId == supportRequest.GetAttributeValue<Guid>("adx_orderid"));

				return order != null;
			}
		}

		public bool IsPaymentError
		{
			get { return Request["Payment"] == "Unsuccessful"; }
		}

		public string AuthorizeNetError
		{
			get { return Request["AuthorizeNetError"]; }
		}

		protected ShoppingCart ShoppingCart
		{
			get { return _cart ?? (_cart = GetShoppingCart(ShoppingCartId)); }
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			if (!Request.IsSecureConnection && !IsPaymentDemo)
			{
				RedirectToHttpsIfNecessary();
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Paid)
			{
				MoveToNextStep();
			}
			else if (ShoppingCart != null)
			{
				if (IsPaymentError)
				{
					SetErrorFields();
				}

				SetMerchantFields();

				PopulateContactInfo(Contact);

				PopulatePurchaseDetails();

				EnableTestMode(TestModeEnabled);
			}
		}

		protected void PayPalButton_OnClick(object sender, EventArgs e)
		{
			//validation that the order is going to be correct.
			var total = _cart.GetCartTotal();

			if (total < 0)
			{
				throw new Exception("Either, you are submitting an empty cart, or this is some other error processign the cart");
			}

			//if some other payment method is used, put a redirect here instead of using paypal!

			//Note that the demo "fake" paypal processing is still considered the paypal method and will still use the HandlePayPalRedirection() 
			//method.  Only a completley custom payment method should have this site setting set to false; 
			//in which case completely custom code must be written
			//var isPaymentPaypal = bool.Parse(ServiceContext.GetSiteSettingValueByName(Website, "Paypal/isPaypalUsed") ?? "true");

			if (IsPaymentPaypal)
			{
				HandlePaypalPayment(total);
			}
			else
			{
				//Do something else......
			}
		}

		protected ShoppingCart GetShoppingCart(Guid shoppingCartId)
		{
			var cart = XrmContext.Adx_shoppingcartSet.FirstOrDefault(sc => sc.Adx_shoppingcartId == shoppingCartId);

			var myCart = cart == null ? null : new ShoppingCart(cart as Entity, XrmContext as OrganizationServiceContext);

			return myCart;
		}

		protected void SetMerchantFields()
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Open New Support Request");
			var pageUrl = ServiceContext.GetUrl(page);
			var returnUrl = new UrlBuilder(pageUrl);
			returnUrl = Uri.UriSchemeHttp + Uri.SchemeDelimiter + returnUrl.Uri.Authority + returnUrl.Uri.PathAndQuery;
			var handlerPath = "/PaymentProcessing.axd";
			var handlerUrl = Uri.UriSchemeHttp + Uri.SchemeDelimiter + returnUrl.Uri.Authority + handlerPath;
			var apiLogin = ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/Authorize.Net/ApiLogin");
			var transactionKey = ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/Authorize.Net/TransactionKey");
			var amount = ShoppingCart.GetCartTotal();
			//var tax = GetTaxGivenTotal(amount);
			//_tax = tax;
			//var totalAmount = amount + tax;
			var sequence = !IsPaymentDemo ? Crypto.GenerateSequence() : null;
			var timestamp = Crypto.GenerateTimestamp();
			var fingerprint = !IsPaymentDemo ? Crypto.GenerateFingerprint(transactionKey, apiLogin, amount, sequence, timestamp.ToString(CultureInfo.InvariantCulture)) : null;
			FingerprintHash = fingerprint;
			FingerprintSequence = sequence;
			FingerprintTimestamp = timestamp.ToString(CultureInfo.InvariantCulture);
			ApiLogin = apiLogin;
			Amount = amount.ToString(CultureInfo.InvariantCulture);
			DecimalAmount = amount;
			//Tax = tax.ToString(CultureInfo.InvariantCulture);
			//TotalAmount = totalAmount.ToString(CultureInfo.InvariantCulture);
			RelayURL = handlerUrl;
			RelayResponse = "TRUE";
			OrderID = CurrentStepEntityID + "&" + SessionId + "&" + CurrentStepEntityLogicalName;

			BtnSubmit.PostBackUrl = GetFormAction(TestModeEnabled);
		}

		protected  void SetErrorFields()
		{
			if (!string.IsNullOrEmpty(AuthorizeNetError))
			{
				AuthorizeNetErrorMessage.Text = AuthorizeNetError;
			}
		}

		protected void PopulateContactInfo(Contact contact)
		{
			if (contact == null)
			{
				return;
			}
			FirstName = contact.FirstName;
			LastName = contact.LastName;
			Email = contact.EMailAddress1;
			Address = contact.Address1_Line1;
			City = contact.Address1_City;
			Province = contact.Address1_StateOrProvince;
			Country = contact.Address2_Country;
			PostalCode = contact.Address1_PostalCode;
		}

		protected void PopulatePurchaseDetails()
		{
			if (ShoppingCart == null)
			{
				return;
			}

			var items = ShoppingCart.GetCartItems().Select(o => o.Entity).Cast<Adx_shoppingcartitem>().ToList();

			if (!items.Any())
			{
				return;
			}

			var purchase = items.First();

			PurchaseTitle.Text = GetShoppingCartItemTitle(purchase);

			var amount = ShoppingCart.GetCartTotal();

			PurchaseAmount.Text = string.Format("{0:C}", amount);

		}

		protected string GetShoppingCartItemTitle(Adx_shoppingcartitem item)
		{
			if (item == null)
			{
				return string.Empty;
			}

			var product = item.adx_product_shoppingcartitem;

			return product == null ? string.Empty : product.Name;
		}

		protected string GetFormAction()
		{
			return GetFormAction(TestModeEnabled);
		}

		protected string GetFormAction(bool testModeEnabled)
		{
			if (IsPaymentDemo)
			{
				return RelayURL;
			}

			if (testModeEnabled)
			{
				return string.IsNullOrEmpty(PostBackUrl) ? Gateway.TEST_URL : PostBackUrl;
			}

			return string.IsNullOrEmpty(PostBackUrl) ? Gateway.LIVE_URL : PostBackUrl;
		}

		protected void EnableTestMode(bool enable)
		{
			TestModePanel.Visible = enable;

			if (!enable)
			{
				return;
			}

			CreditCardNumber = TestCreditCardNumber;
			ExpiryMonthDefault.Value = TestCreditCardExpiryMonth;
			ExpiryYearDefault.Value = TestCreditCardExpiryYear;
			CreditCardExpiry = TestCreditCardExpiry;
			CreditCardVerificationValue = TestCreditCardVerificationValue;
		}

		private void HandlePaypalPayment(decimal total)
		{
			//var paypalBaseUrl = PayPalHelper.GetPaypalBaseUrl(Portal);

			//ServiceContext.GetSiteSettingValueByName(Website, "Paypal/GetPaypalBaseUrl");

			PayPalHelper PayPal = new PayPalHelper(Portal);

			// *** Save any values you might need when you return here
			//Session["PayPal_OrderAmount"] = this.OrderAmount;  // already saved above

			var currencyCode = ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/Paypal/CurrencyCode");

			//Paypal Item Data for aggregateddata
			var aggregateData = bool.Parse(ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/Paypal/aggregateData") ?? "false");
			var itemizedData = bool.Parse(ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/Paypal/itemizedData") ?? "true");
			var addressOverride =
				bool.Parse(ServiceContext.GetSiteSettingValueByName(Website, "Ecommerce/Paypal/AddressOverride") ?? "true");

			//CreateQuote(itemizedData, aggregateData);

			HandlePayPalRedirection(PayPal, total, PayPal.PayPalAccountEmail, currencyCode, itemizedData, aggregateData, addressOverride);
		}

		/// <summary>
		/// Redirects the current request to the PayPal site by passing a querystring.
		/// PayPal then should return to this page with ?PayPal=Cancel or ?PayPal=Success
		/// This routine stores all the form vars so they can be restored later
		/// </summary>
		private void HandlePayPalRedirection(PayPalHelper payPal, decimal total, string accountEmail,
			string currencyCode, bool itemizedData, bool aggregateData, bool addressOverride)
		{

			// *** Set a flag so we know we redirected
			Session["PayPal_Redirected"] = "True";

			//the specifics can be changed as needed

			var args = GetPaypalArgs(total, accountEmail, itemizedData, currencyCode, aggregateData, false, addressOverride);

			Response.Redirect(payPal.GetSubmitUrl(args));

		}

		/// <summary>
		/// Creates the dictionary of arguments for constructing the query string to send to paypal
		/// </summary>
		private Dictionary<string, string> GetPaypalArgs(decimal total, string accountEmail, bool itemizedData,
			string currencyCode, bool aggregateData, bool sendPaypalAddress, bool addressOverride)
		{
			var args = new Dictionary<string, string>();

			//use cmd = "_cart" for the cart upload option
			args.Add("cmd", "_cart");
			args.Add("upload", "1");
			args.Add("business", accountEmail);

			//Paypal STANDARD DATA -> does this have to be done after the items, or does it not matter?
			args.Add("no_note", "0");

			if (!string.IsNullOrEmpty(currencyCode)) { args.Add("currency_code", currencyCode); }

			//TODO: Invoice or Order ID

			args.Add("email", accountEmail);

			if (aggregateData)
			{
				args.Add("item_name", "Aggregated Items");
				args.Add("amount", total.ToString("#.00"));
			}
			//Paypal Item Data for itemized data
			else if (itemizedData)
			{
				var cartItems = _cart.GetCartItems().Select(sci => sci.Entity).Cast<Adx_shoppingcartitem>();

				int counter = 0;
				foreach (var item in cartItems)
				{
					counter++;

					args.Add(string.Format("item_name_{0}", counter), item.Adx_name);
					args.Add(string.Format("amount_{0}", counter),
							 (item.Adx_QuotedPrice != null) ? ((decimal)item.Adx_QuotedPrice).ToString("#.00") : "0.00");
					args.Add(string.Format("quantity_{0}", counter),
							 (item.Adx_Quantity != null) ? ((int)item.Adx_Quantity).ToString() : "0");
					//add arguments for shipping/handling cost?
					args.Add(string.Format("item_number_{0}", counter), item.Adx_shoppingcartitemId.ToString());
				}
				//if we are calculating the tax; this is done and added as an arg.
			}
			else
			{
				/*some other method is being used*/
			}


			var portal = PortalCrmConfigurationManager.CreatePortalContext();

			var context = portal.ServiceContext as OrganizationServiceContext;

			var page = context.GetPageBySiteMarkerName(portal.Website, "Open New Support Request");

			var successUrl = new UrlBuilder(context.GetUrl(page));

			successUrl.QueryString.Set("id", CurrentStepEntityID.ToString());

			successUrl.QueryString.Set("sessionid", SessionId.ToString());

			successUrl.QueryString.Set("Payment", "Successful");

			/****If a quote was created, pass in the quote ID*****/

			args.Add("invoice", _cart.Id.ToString());

			// *** Have paypal return back to this URL
			var cancelUrl = Request.Url + "?PayPal=Cancel";

			args.Add("return", successUrl);
			args.Add("cancel_return", cancelUrl);

			return args;
		}




	}
}
