﻿using System;
using System.Linq;
using Site.Controls;

namespace Site.Areas.HelpDesk.Controls
{
	public partial class WebFormInvoiceDisplay :  WebFormPortalUserControl
	{
		protected string InvoiceNumber;
		protected string ReceiptNumber;
		protected decimal InvoicePreTaxTotal;
		protected decimal InvoiceTaxTotal;
		protected decimal InvoiceTotal;
		protected string InvoiceProduct;

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToHttpIfNecessary();

			var supportRequest = XrmContext.adx_supportrequestSet
				.FirstOrDefault(sr => sr.GetAttributeValue<Guid>("adx_supportrequestid") == CurrentStepEntityID);

			var invoice = XrmContext.InvoiceSet.FirstOrDefault(i => i.InvoiceId == supportRequest.adx_invoice.Id);

			InvoiceNumber = invoice.InvoiceNumber;
			InvoicePreTaxTotal = invoice.TotalLineItemAmount ?? (decimal)0.0;
			InvoiceTaxTotal = invoice.TotalTax ?? (decimal)0.0;
			InvoiceTotal = invoice.TotalAmount ?? (decimal)0.0;
			InvoiceProduct = invoice.invoice_details.First().product_invoice_details.Name;
		}
	}
}