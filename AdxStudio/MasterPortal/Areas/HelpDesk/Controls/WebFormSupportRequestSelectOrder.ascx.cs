﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Commerce;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Site.Controls;
using Xrm;

namespace Site.Areas.HelpDesk.Controls
{
	public partial class WebFormSupportRequestSelectOrder : WebFormPortalUserControl
	{
		private const string DefaultPriceListName = "Web";

		protected string VisitorID
		{
			get { return Context.Profile.UserName; }
		}

		public string PriceListName
		{
			get
			{
				var priceListName = DefaultPriceListName;

				if (Contact != null)
				{
					var accountPriceListName = ServiceContext.GetPriceListNameForParentAccount(Contact);

					if (string.IsNullOrWhiteSpace(accountPriceListName))
					{
						var websitePriceListName = ServiceContext.GetDefaultPriceListName(Website);

						if (!string.IsNullOrWhiteSpace(websitePriceListName))
						{
							priceListName = websitePriceListName;
						}
					}
					else
					{
						priceListName = accountPriceListName;
					}

				}

				return priceListName;
			}
		}

		private Guid _sessionId;

		public Guid SessionId
		{
			get
			{
				if (!Guid.TryParse(Request["sessionid"], out _sessionId))
				{
					return Guid.Empty;
				}
				return _sessionId;
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			PlanPackageList.ValidationGroup = PlanPackageListRequiredFieldValidator.ValidationGroup = ValidationGroup;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				PopulatePlanPackageList();
			}
		}

		private void PopulatePlanPackageList()
		{
			PlanPackageList.Items.Clear();

			var supportRequest = XrmContext.CreateQuery("adx_supportrequest")
				.FirstOrDefault(sr => sr.GetAttributeValue<Guid>("adx_supportrequestid") == CurrentStepEntityID);
			if (supportRequest == null)
			{
				throw new ApplicationException(string.Format("Could not find support request record with id equal to {0}", CurrentStepEntityID));
			}
			var supportRequestProductReference = supportRequest.GetAttributeValue<EntityReference>("adx_product");
			var parentProduct = supportRequestProductReference == null ? null : XrmContext.ProductSet.FirstOrDefault(pp => pp.ProductId == supportRequestProductReference.Id);
			IQueryable<Product> supportProducts;
			if (parentProduct != null && parentProduct.ProductId != null)
			{
				supportProducts = from product in XrmContext.ProductSet
								join supportedset in XrmContext.adx_supportedproduct_productplanSet on product.ProductId equals supportedset.productidOne
								join supportedProduct in XrmContext.ProductSet on supportedset.productidTwo equals supportedProduct.ProductId
								where product.StateCode == 0
								where product.ProductId == parentProduct.ProductId
								where supportedProduct.ProductTypeCode == (int) ProductTypeCode.SupportPlan
								select supportedProduct;
			}
			else
			{
				supportProducts = XrmContext.ProductSet.Where(p => p.StateCode == 0 && p.ProductTypeCode == (int)ProductTypeCode.SupportPlan);
			}
			
			foreach (var supportProduct in supportProducts)
			{
				var supportUnit =
					XrmContext.UoMScheduleSet.FirstOrDefault(unit => unit.UoMScheduleId == supportProduct.DefaultUoMScheduleId.Id);

				var baseUom = XrmContext.UoMSet.FirstOrDefault(baseuom => baseuom.Name == supportUnit.BaseUoMName);

				var uoms = XrmContext.UoMSet.Where(uom => uom.BaseUoM.Id == baseUom.Id);

				foreach (var u in uoms)
				{
					var amount = new Money(0);
					var priceListItem = XrmContext.GetPriceListItemByPriceListNameAndUom(supportProduct, u.Id, PriceListName);
					
					if (priceListItem != null)
					{
						amount = priceListItem.GetAttributeValue<Money>("amount");
					}

					PlanPackageList.Items.Add(new ListItem(
												string.Format("{0} - {1} - {2}", supportProduct.Name, u.Name, amount.Value.ToString("c0")),
												string.Format("{0}&{1}", supportProduct.ProductId.ToString(), u.UoMId.ToString())
												));
				}
			}
		}

		protected override void OnSubmit(object sender, Adxstudio.Xrm.Web.UI.WebControls.WebFormSubmitEventArgs e)
		{
			AddToCart();

			base.OnSubmit(sender, e);
		}

		protected void AddToCart()
		{
			const string nameFormat = "Case Order for {0}";

			Guid guid = Guid.NewGuid();

			var cart = Contact == null
				? new Adx_shoppingcart
				{
					Adx_name = string.Format(nameFormat, VisitorID),
					Adx_VisitorID = VisitorID,
					adx_websiteid = Website.ToEntityReference(),
					Adx_shoppingcartId = guid,
					adx_system = true
				}
				: new Adx_shoppingcart
				{
					Adx_name = string.Format(nameFormat, Contact.FullName),
					adx_contactid = Contact.ToEntityReference(),
					adx_websiteid = Website.ToEntityReference(),
					Adx_shoppingcartId = guid,
					adx_system = true
				};

			XrmContext.AddObject(cart);

			XrmContext.SaveChanges();

			//Choose Parent Product

			var selectedValue = PlanPackageList.SelectedValue;

			string[] guids = selectedValue.Split('&');

			var supportProductId = new Guid(guids[0]);
			var uomId = new Guid(guids[1]);

			var caseProduct = XrmContext.ProductSet.FirstOrDefault(p => p.ProductId == supportProductId);
			var myUom = XrmContext.UoMSet.FirstOrDefault(uom => uom.UoMId == uomId);

			Guid productID = (caseProduct != null) ? caseProduct.Id : Guid.Empty;

			cart = XrmContext.Adx_shoppingcartSet.FirstOrDefault(sc => sc.Adx_shoppingcartId == guid);

			var myCart = cart == null ? null : new ShoppingCart(cart, XrmContext);

			if (myCart != null)
				myCart.AddProductToCart(productID, myUom, PriceListName);
			else
				throw new Exception("Error Processing Cart.");

			var total = myCart.GetCartTotal();

			if (total < 0)
			{
				throw new Exception("Either, you are submitting an empty cart, or this is some other error processign the cart");
			}

			AddCartToSupportRequest(myCart);
		}

		private void AddCartToSupportRequest(ShoppingCart myCart)
		{
			var context = new CrmOrganizationServiceContext(new CrmConnection("Xrm"));

			var supportRequest = new Entity("adx_supportrequest") {Id = CurrentStepEntityID};

			supportRequest.Attributes["adx_shoppingcartid"] = myCart.Entity.ToEntityReference();

			context.Attach(supportRequest);

			context.UpdateObject(supportRequest);

			context.SaveChanges();
		}
	}
}