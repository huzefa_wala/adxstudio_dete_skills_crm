﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebFormSupportRequestPayment.ascx.cs" Inherits="Site.Areas.HelpDesk.Controls.WebFormSupportRequestPayment" %>

<asp:ScriptManagerProxy runat="server">
	<Scripts>
		<asp:ScriptReference Path="~/js/jquery.blockUI.js" />
		<asp:ScriptReference Path="~/js/jquery.validate.min.js" />
		<asp:ScriptReference Path="~/js/jquery.formatCurrency_1.4.0.min.js" />
	</Scripts>
</asp:ScriptManagerProxy>

<script type="text/javascript">
	$(document).ready(function () {
		$.unblockUI();
		
		if (("<%= IsPaymentAuthorizeNet %>".toLowerCase()=='true') || ("<%= IsPaymentDemo %>".toLowerCase()=='true')) {
		   $("#content_form").attr("action", "<%= GetFormAction() %>");
		   $("#content_form").attr("method", "POST");
		}

		if ("<%=IsPaymentError %>".toLowerCase()=='true') {
			$("#PaypalButtonPanel").hide();
			$("#AuthorizeNetErrorDiv").show();
		}
		
		if ("<%=IsPaymentPaypal %>".toLowerCase()=='false') {
			$("#PaypalButtonPanel").hide();

			$("#PreviousButton").addClass("cancel");
			
			$.validator.addMethod("validateExpiryMonthYear", function() {
				var expiryMonth = $("#ExpiryMonth").val();
				var expiryYear = $("#ExpiryYear").val();
				return !((expiryMonth == null || expiryMonth.length == 0) || (expiryYear == null || expiryYear.length == 0));
			}, "Month & Year are required.");
			$.validator.addMethod("validateCountryDropDown", function() {
				var countrySelected = $("#countryDropDown").val();
				if (countrySelected != "!1") {
					return true;
				}
				return false;
			}, "This field is required.");
			$.validator.addMethod("validateProvinceDropDown", function() {
				var provinceSelected = $("#stateDropDown").val();
				if (provinceSelected != "!1" && provinceSelected != "!2" && provinceSelected != "!3") {
					return true;
				}
				return false;
			}, "This field is required.");
			$("#x_state_other").hide();
			$("#x_country_other").hide();
			$("#TaxTitle").hide();
			$("#TaxAmount").hide();
			$("#TotalTitle").hide();
			$("#TotalAmountLabel").hide();
			$("#TaxBR").hide();
			$("#TaxHR").hide();
			$("#content_form").validate({
				rules: {
					x_first_name: { required: true },
					x_last_name: { required: true },
					x_email: {
						required: true,
						email: true
					},
					x_address: { required: true },
					x_city: { required: true },
					x_zip: { required: true },
					countryDropDown: {
						validateCountryDropDown: true
					},
					stateDropDown: {
						validateProvinceDropDown: true
					},
					x_state: {
						required: true
					},
					x_country: {
						required: true
					},
					ExpiryMonth: {
						validateExpiryMonthYear: true
					},
					ExpiryYear: {
						validateExpiryMonthYear: true
					},
					x_card_num: {
						required: true,
						digits: true
					},
					x_card_code: {
						required: true,
						minlength: 3,
						digits: true
					}
				},
				errorClass: "help-inline error",
				groups: { nameGroup: "ExpiryMonth ExpiryYear" },
				errorPlacement: function(error, element) {
					if (element.attr("name") == "ExpiryMonth" || element.attr("name") == "ExpiryYear") {
						error.insertAfter("#ExpiryYear");
					} else if (element.attr("name") == "x_card_code") {
						error.insertAfter("#cvv-help-link");
					} else {
						error.insertAfter(element);
					}
				},
				highlight: function(label) {
					$(label).closest('.control-group').removeClass('success').addClass('error');
				},
				success: function(label) {
					$(label).closest('.control-group').removeClass('error').addClass('success');
				},
				debug: true
			});
			$("#ExpiryMonth").val($("#ExpiryMonthDefault").val());
			
			$(document).on("change", "#ExpiryMonth", function () {
				updateExpiry();
			});
			populateExpiryYear();
			$("#ExpiryYear").val($("#ExpiryYearDefault").val());
			$(document).on("change", "#ExpiryYear", function () {
				updateExpiry();
			});
			$(document).on("change", "#countryDropDown", function () {
				updateCountry();
			});
			$(document).on("change", "#stateDropDown", function () {
				updateProvince();
			});

		} else {
			$("#mainPaymentDiv").hide();
		}

		function populateExpiryYear() {
			var d = new Date();
			var currentyear = d.getFullYear();
			for (var i = 0; i < 20; i++) {
				var year = currentyear + i;
				$("#ExpiryYear")
					.append($("<option></option>")
							.attr("value", year)
							.text(year));
			}
		}

		function updateExpiry() {
			var expdate = $("#x_exp_date");
			var expiryMonth = $("#ExpiryMonth").val();
			var expiryYear = $("#ExpiryYear").val();
			if ((expiryMonth == null || expiryMonth.length == 0) || (expiryYear == null || expiryYear.length == 0)) {
				expdate.val('');
				return;
			}
			var exp = expiryMonth + expiryYear.substr(2);
			expdate.val(exp);
		}

		function updateCountry() {
			var x_country = $("#x_country");
			var countrySelected = $("#countryDropDown").val();
			x_country.val('');
			if (countrySelected != "!1") {
				x_country.val(countrySelected);
				if (countrySelected == "CAN") {
					$("#x_country_other").hide();
					$("#x_state_dropdown").show();
					$("#x_state_other").hide();
					populateProvinces();
				} else if (countrySelected == "USA") {
					$("#x_country_other").hide();
					$("#x_state_dropdown").show();
					$("#x_state_other").hide();
					populateStates();
				} else {
					x_country.val('');
					$("#x_state").val('');
					$("#x_country_other").show();
					$("#x_state_dropdown").hide();
					$("#x_state_other").show();
				}
			}
			calculateTax();
		}

		function populateProvinces() {
			var stateDropDown = $("#stateDropDown");
			stateDropDown.empty();
			var provinceList = [
				{ value: "AB", name: "Alberta" },
				{ value: "BC", name: "British Columbia" },
				{ value: "MB", name: "Manitoba" },
				{ value: "NB", name: "New Brunswick" },
				{ value: "NL", name: "Newfoundland and Labrador" },
				{ value: "NT", name: "Northwest Territories" },
				{ value: "NS", name: "Nova Scotia" },
				{ value: "NU", name: "Nunavut" },
				{ value: "ON", name: "Ontario" },
				{ value: "PE", name: "Prince Edward Island" },
				{ value: "QC", name: "Quebec" },
				{ value: "SK", name: "Saskatchewan" },
				{ value: "YT", name: "Yukon" }
			];
			
			stateDropDown
					.append($("<option></option>")
							.attr("value", "!1")
							.text("---Select a Province-------"));
		
			for (var i = 0; i < provinceList.length; i++) {
				stateDropDown
					.append($("<option></option>")
							.attr("value", provinceList[i].value)
							.text(provinceList[i].name));
			}
		}
		
		function populateStates() {
			var stateDropDown = $("#stateDropDown");
			stateDropDown.empty();

			var provinceList = [
				{ value: "AL", name: "Alabama" },
				{ value: "AK", name: "Alaska" },
				{ value: "AZ", name: "Arizona" },
				{ value: "AR", name: "Arkansas" },
				{ value: "CA", name: "California" },
				{ value: "CO", name: "Colorado" },
				{ value: "CT", name: "Connecticut" },
				{ value: "DC", name: "District of Columbia" },
				{ value: "DE", name: "Delaware" },
				{ value: "FL", name: "Florida" },
				{ value: "GA", name: "Georgia" },
				{ value: "HI", name: "Hawaii" },
				{ value: "ID", name: "Idaho" },
				{ value: "IL", name: "Illinois" },
				{ value: "IN", name: "Indiana" },
				{ value: "IA", name: "Iowa" },
				{ value: "KS", name: "Kansas" },
				{ value: "KY", name: "Kentucky" },
				{ value: "LA", name: "Louisiana" },
				{ value: "ME", name: "Maine" },
				{ value: "MD", name: "Maryland" },
				{ value: "MA", name: "Massachusetts" },
				{ value: "MI", name: "Michigan" },
				{ value: "MN", name: "Minnesota" },
				{ value: "MS", name: "Mississippi" },
				{ value: "MO", name: "Missouri" },
				{ value: "MT", name: "Montana" },
				{ value: "NE", name: "Nebraska" },
				{ value: "NV", name: "Nevada" },
				{ value: "NH", name: "New Hampshire" },
				{ value: "NS", name: "Nova Scotia" },
				{ value: "NJ", name: "New Jersey" },
				{ value: "NM", name: "New Mexico" },
				{ value: "NY", name: "New York" },
				{ value: "NC", name: "North Carolina" },
				{ value: "ND", name: "North Dakota" },
				{ value: "OH", name: "Ohio" },
				{ value: "OK", name: "Oklahoma" },
				{ value: "OR", name: "Oregon" },
				{ value: "PA", name: "Pennsylvania" },
				{ value: "RI", name: "Rhode Island" },
				{ value: "SC", name: "South Carolina" },
				{ value: "SD", name: "South Dakota" },
				{ value: "TN", name: "Tennessee" },
				{ value: "TX", name: "Texas" },
				{ value: "UT", name: "Utah" },
				{ value: "VT", name: "Vermont" },
				{ value: "VA", name: "Virginia" },
				{ value: "WA", name: "Washington" },
				{ value: "WV", name: "West Virginia" },
				{ value: "WI", name: "Wisconsin" },
				{ value: "WY", name: "Wyoming" }
			];
		
			stateDropDown
					.append($("<option></option>")
							.attr("value", "!1")
							.text("---Select a State-------"));

			for (var i = 0; i < provinceList.length; i++) {
				stateDropDown
					.append($("<option></option>")
							.attr("value", provinceList[i].value)
							.text(provinceList[i].name));
			}
			
			stateDropDown
					.append($("<option></option>")
							.attr("value", "Other")
							.text("Other"));
		}

		function updateProvince() {
			var x_state = $("#x_state");
			var provinceSelected = $("#stateDropDown").val();
			x_state.val('');
			if (provinceSelected != "!1" && provinceSelected != "!2" && provinceSelected != "!3") {
				if (provinceSelected == "Other") {
					x_state.val('');
					$("#x_state_other").show();
				} else {
					x_state.val(provinceSelected);
					$("#x_state_other").hide();
				}
			}
			calculateTax();
		}
		function calculateTax() {
			var tax = 0.0;
			var amount = <%= Amount %>;
			var x_country = $("#x_country").val();
			var x_state = $("#x_state").val();
			var x_tax = $("#x_tax");
			var GST = false;
			var HST = false;
			var PST = false;

			if (x_country == "CAN")
			{
				if (x_state == "AB" || x_state == "MB" || x_state == "NT" || x_state == "NU" || x_state == "QC" || x_state == "PE" || x_state == "YT")
				{
					GST = true;
					tax = amount* 0.05;
				}
				if (x_state == "ON")
				{
					HST = true;
					tax = amount*0.13;
				}
				if (x_state == "BC")
				{
					HST = true;
					tax = amount*0.12;
				}
				if (x_state == "NB")
				{
					HST = true;
					tax = amount* 0.13;
				}
				if (x_state == "NL")
				{
					HST = true;
					tax = amount* 0.13;
				}
				if (x_state == "NS")
				{
					HST = true;
					tax = amount* 0.15;
				}
				if (x_state == "SK")
				{
					GST = true;
					PST = true;
					tax = amount* 0.10;
				}
				x_tax.val(tax);
			}
			
			if (tax > 0.0) {
				
				if (GST && PST)
				{
					$("#TaxTitle").text("GST/PST");
				}
				else if (GST)
				{
					$("#TaxTitle").text("GST");
				}
				else if (HST)
				{
					$("#TaxTitle").text("HST");
				}

				$("#TaxTitle").show();
				$("#TaxAmount").show();
				$("#TotalTitle").show();
				$("#TotalAmountLabel").show();
				$("#TaxBR").show();
				$("#TaxHR").show();
				$("#TaxAmount").text(tax).formatCurrency();
				$("#TotalAmountLabel").text(tax + amount).formatCurrency();
			} else {
				x_tax.val("0");
				$("#TaxTitle").hide();
				$("#TaxAmount").hide();
				$("#TotalTitle").hide();
				$("#TotalAmountLabel").hide();
				$("#TaxBR").hide();
				$("#TaxHR").hide();
				$("#TotalAmountLabel").text(amount).formatCurrency();
			}
		}
	});
	function webFormClientValidate() {
		var isValid = $("#content_form").valid();
		if (isValid) {
			$.blockUI({ message: $("#progress-message") }); 
		}
		return isValid;
	}
	function isTrue(input) {
    if (typeof input == 'string') {
        return input.toLowerCase() == 'true';
    }

    return !!input;
	}

</script>

<input type="hidden" id="x_fp_hash" name="x_fp_hash" value="<%= FingerprintHash %>" />
<input type="hidden" id="x_fp_sequence" name="x_fp_sequence" value="<%= FingerprintSequence %>" />
<input type="hidden" id="x_fp_timestamp" name="x_fp_timestamp" value="<%= FingerprintTimestamp %>" />
<input type="hidden" id="x_login" name="x_login" value="<%= ApiLogin %>" />
<input type="hidden" id="x_amount" name="x_amount" value="<%= Amount %>" />
<input type="hidden" id="x_tax" name="x_tax" value="<%= Tax %>" />
<input type="hidden" id="x_relay_url" name="x_relay_url" value="<%= RelayURL %>" />
<input type="hidden" id="x_relay_response" name="x_relay_response" value="<%= RelayResponse %>" />
<input type="hidden" id="order_id" name="order_id" value="<%= OrderID %>" />

<div id="AuthorizeNetErrorDiv" class="alert alert-error hide">
   <crm:Snippet ID="PaymentErrorPreface" SnippetName="Payment Error Message" DefaultText="Sorry, there was an error during your request." Editable="True" EditType="html" runat="server"/> 
   <p><asp:Label ID="AuthorizeNetErrorMessage" runat="server"></asp:Label></p>
</div>

<asp:Panel ID="TestModePanel" CssClass="alert alert-error" Visible="False" runat="server">
	<i class="icon-exclamation-sign"></i>&nbsp;<strong>Payment Provider TEST mode is enabled!</strong> Credit card fields have been auto populated with test values.
</asp:Panel>

<div id="mainPaymentDiv" class="row-fluid payment">
	<div class="span6 form-horizontal">
		<fieldset>
			<legend>
				<crm:Snippet SnippetName="Help Desk Payment Billing Contact Title" DefaultText="Billing Contact" Editable="True" EditType="text" runat="server"/>
			</legend>
			<div class="control-group">
				<label for="x_first_name" class="control-label required">First Name</label>
				<div class="controls">
					<input id="x_first_name" name="x_first_name" type="text" value="<%= FirstName %>" />
				</div>
			</div>
			<div class="control-group">
				<label for="x_last_name" class="control-label required">Last Name</label>
				<div class="controls">
					<input id="x_last_name" name="x_last_name" type="text" value="<%= LastName %>" />
				</div>
			</div>
			<div class="control-group">
				<label for="x_email" class="control-label required">Email</label>
				<div class="controls">
					<input id="x_email" name="x_email" type="text" value="<%= Email %>" />
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>
				<crm:Snippet SnippetName="Help Desk Payment Billing Address Title" DefaultText="Billing Address" Editable="True" EditType="text" runat="server"/>
			</legend>
			<div class="control-group">
				<label for="x_address" class="control-label required">Address</label>
				<div class="controls">
					<input id="x_address" name="x_address" type="text" value="<%= Address %>" />
				</div>
			</div>
			<div class="control-group">
				<label for="x_city" class="control-label required">City</label>
				<div class="controls">
					<input id="x_city" name="x_city" type="text" value="<%= City %>" />
				</div>
			</div>
			<div class="control-group">
				<label for="countryDropDown" class="control-label  required">Country</label>
				<div class="controls">
					<select id="countryDropDown" name="countryDropDown">
						<option value="!1">---Select a Country-------</option>
						<option value=CAN>Canada</option>
						<option value=USA>United States</option>
						<option value=Other>Other</option>
					</select>
				</div>
			</div>
			<div id="x_country_other" class="control-group">
					<label id="x_countrylabel" for="x_country" class="control-label required">Other Country</label>
					<div class="controls">
						<input type="text" id="x_country" name="x_country" value="<%= Country %>" />
					</div>
			</div>
			<div id="x_state_dropdown" class="control-group">
				<label for="stateDropDown" class="control-label  required">State/Province</label>
				<div class="controls">
					<select id="stateDropDown" name="stateDropDown">
						<option value="!1">---Select a State-------</option>
						<option value="!2">---Canada---------------</option>
						<option value=AB>Alberta</option>
						<option value=BC>British Columbia</option>
						<option value=MB>Manitoba</option>
						<option value=NB>New Brunswick</option>
						<option value=NL>Newfoundland and Labrador</option>
						<option value=NT>Northwest Territories</option>
						<option value=NS>Nova Scotia</option>
						<option value=NU>Nunavut</option>
						<option value=ON>Ontario</option>
						<option value=PE>Prince Edward Island</option>
						<option value=QC>Quebec</option>
						<option value=SK>Saskatchewan</option>
						<option value=YT>Yukon</option>
						<option value="!3">---United States--------</option>
						<option value=AL>Alabama</option>
						<option value=AK>Alaska</option>
						<option value=AZ>Arizona</option>
						<option value=AR>Arkansas</option>
						<option value=CA>California</option>
						<option value=CO>Colorado</option>
						<option value=CT>Connecticut</option>
						<option Value=DC>District of Columbia</option>
						<option value=DE>Delaware</option>
						<option value=FL>Florida</option>
						<option value=GA>Georgia</option>
						<option Value=HI>Hawaii</option>
						<option value=ID>Idaho</option>
						<option value=IL>Illinois</option>
						<option value=IN>Indiana</option>
						<option value=IA>Iowa</option>
						<option value=KS>Kansas</option>
						<option value=KY>Kentucky</option>
						<option value=LA>Louisiana</option>
						<option value=ME>Maine</option>
						<option value=MD>Maryland</option>
						<option value=MA>Massachusetts</option>
						<option value=MI>Michigan</option>
						<option value=MN>Minnesota</option>
						<option value=MS>Mississippi</option>
						<option value=MO>Missouri</option>
						<option value=MT>Montana</option>
						<option value=NE>Nebraska</option>
						<option Value=NV>Nevada</option>
						<option value=NH>New Hampshire</option>
						<option value=NJ>New Jersey</option>
						<option value=NM>New Mexico</option>
						<option value=NY>New York</option>
						<option value=NC>North Carolina</option>
						<option value=ND>North Dakota</option>
						<option value=OH>Ohio</option>
						<option value=OK>Oklahoma</option>
						<option value=OR>Oregon</option>
						<option value=PA>Pennsylvania</option>
						<option value=RI>Rhode Island</option>
						<option value=SC>South Carolina</option>
						<option value=SD>South Dakota</option>
						<option value=TN>Tennessee</option>
						<option value=TX>Texas</option>
						<option value=UT>Utah</option>
						<option value=VT>Vermont</option>
						<option value=VA>Virginia</option>
						<option value=WA>Washington</option>
						<option value=WV>West Virginia</option>
						<option value=WI>Wisconsin</option>
						<option value=WY>Wyoming</option>
					</select>
				</div>
			</div>
			<div id="x_state_other" class="control-group">
					<label id="x_statelabel" for="x_state" class="control-label required">Other State/Province</label>
					<div class="controls">
						<input type="text" id="x_state" name="x_state" value="<%= Province %>" />
					</div>
			</div>
			<div class="control-group">
				<label for="x_zip" class="control-label required">Zip/Postal Code</label>
				<div class="controls">
					<input id="x_zip" name="x_zip" type="text" value="<%= PostalCode %>" maxlength="7" class="input-mini" />
				</div>
			</div>
		</fieldset>
	</div>

	<div class="span6 form-horizontal">
		<fieldset>
			<legend>
				<crm:Snippet SnippetName="Help Desk Payment Purchase Title" DefaultText="Purchase" Editable="True" EditType="text" runat="server"/>
			</legend>
			<div class="control-group alert alert-block alert-success">
				<strong><asp:Label ID="PurchaseTitle" Text="Support" runat="server"></asp:Label></strong>&nbsp;<strong class="pull-right"><asp:Label ID="PurchaseAmount" runat="server"></asp:Label></strong>
				<br id="TaxBR"/>
				<strong><asp:Label ID="TaxTitle" Text="Tax" runat="server"></asp:Label></strong>&nbsp;<strong class="pull-right"><asp:Label ID="TaxAmount" runat="server"></asp:Label></strong>
				<hr id="TaxHR"/>
				<strong><asp:Label ID="TotalTitle" Text="Total" runat="server"></asp:Label></strong>&nbsp;<strong class="pull-right"><asp:Label ID="TotalAmountLabel" runat="server"></asp:Label></strong>
			</div>
		</fieldset>
		<fieldset>
			<legend>
				<crm:Snippet SnippetName="Help Desk Payment Title" DefaultText="Pay with Credit Card" Editable="True" EditType="text" runat="server"/>
			</legend>
                <crm:Snippet SnippetName="Authorize.Net Verified Merchant Seal" DefaultText="" Editable="False" EditType="html" runat="server" />
			<div class="control-group">
				<crm:Snippet SnippetName="Help Desk Payment Credit Card Instructions" DefaultText="" Editable="True" EditType="html" runat="server"/>
            </div>
                <!-- (c) 2005, 2012. Authorize.Net is a registered trademark of CyberSource Corporation --> 
			<div class="control-group">
				<label for="x_card_num" class="control-label required">Credit Card Number</label>
				<div class="controls">
					<input id="x_card_num" name="x_card_num" type="text" value="<%= CreditCardNumber %>" />
				</div>
			</div>
			<div class="control-group">
				<label for="ExpiryMonth" class="control-label required">Expires</label>
				<div class="controls">
					<input type="hidden" id="ExpiryMonthDefault" clientidmode="Static" runat="server" />
					<select id="ExpiryMonth" name="ExpiryMonth" class="input-small">
						<option value="">Month</option>
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
						<option value="04">4</option>
						<option value="05">5</option>
						<option value="06">6</option>
						<option value="07">7</option>
						<option value="08">8</option>
						<option value="09">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
					</select>
					<input type="hidden" id="ExpiryYearDefault" clientidmode="Static" runat="server" />
					<select id="ExpiryYear" name="ExpiryYear" class="input-small">
						<option value="">Year</option>
					</select>
					<input type="hidden" id="x_exp_date" name="x_exp_date" value="<%= CreditCardExpiry %>" />
				</div>
			</div>
			<div class="control-group">
				<label for="x_card_code" class="control-label required">Card Verification Value</label>
				<div class="controls">
					<input id="x_card_code" name="x_card_code" type="text" value="<%= CreditCardVerificationValue %>" maxlength="4" class="input-mini" /> <a id='cvv-help-link' href='#cvv-help' data-toggle='modal' class="icon-question-sign"></a>
				</div>
			</div>
		</fieldset>
	</div>
</div>

<div ID="PaypalButtonPanel">
	<asp:Button ID="PayPalButton"  Text="<%$ Snippet: paypal_checkout_button, Process Order using Paypal %>" OnClick="PayPalButton_OnClick" runat="server"/>
</div>

<div id="cvv-help" class="modal hide">
	<div class="modal-header"><button class="close" data-dismiss="modal">&times;</button>
		<h3>
			<crm:Snippet runat="server" SnippetName="Payment CVV Help Title" DefaultText="Credit Card Verification Value" EditType="text" />
		</h3>
	</div>
	<div class="modal-body">
		<crm:Snippet runat="server" SnippetName="Payment CVV Help Content" DefaultText="<p>The card verification value is an important security feature for credit card transactions on the internet.</p><p>MasterCard, Visa and Discover credit cards have a 3 digit code printed on the back of the card while American Express cards have a 4 digit code printed on the front side of the card above the card number.</p>" EditType="html" />
	</div>
	<div class="modal-footer"><button class="btn" data-dismiss="modal">Close</button></div>
</div>

<div id="progress-message" style="display:none;">
	<crm:Snippet runat="server" SnippetName="Payment Processing Message" DefaultText="<h2 style='padding: 10px; text-align: center;'><img alt='Loading' src='~/xrm-adx/samples/images/ajax-loader.gif' style='vertical-align: middle;'> Please wait while we process your purchase...</h2>" EditType="html" />
</div>

<%--Hack to get authorize.net to work - without the button the payment provider will throw an error--%>
<asp:Button ID="BtnSubmit" runat="server" Text="" ClientIDMode="Static" Height="0" Width="0" BorderStyle="None" />
