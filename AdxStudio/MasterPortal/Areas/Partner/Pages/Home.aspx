﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Site.Areas.Partner.Pages.Home" %>
<%@ Register TagPrefix="site" Namespace="Site.Controls" Assembly="Site" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Partner/css/partner.css") %>">
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<form id="content_form" runat="server">
		<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
		<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
		
		<asp:Panel ID="PartnerHomePanel" runat="server">
		<div class="row">
			<div id="home-activities-section" class="pull-right span3">
				<h4>
					<asp:Literal Text="<%$ Snippet: home/activities/label, Activity %>" runat="server" /></h4>
				<div id="home-activities">
					<%--<crm:CrmHyperLink runat="server" SiteMarkerName="New Opportunities" >--%>
						<div id="new-opportunity-activity" class="well sidebar">
							<div class="section">
								<h4>
									<crm:CrmHyperLink runat="server" SiteMarkerName="New Opportunities" >
									<crm:Snippet runat="server" SnippetName="home/activities/label/new-opportunities" DefaultText="New Opportunities" Editable="true" EditType="text" />
									</crm:CrmHyperLink>
								</h4>
								<crm:Snippet runat="server" SnippetName="home/activities/label/new-opportunities-count" DefaultText="Opportunities awaiting acceptance" Editable="true" EditType="text" />
								<p><asp:Label ID="NewOpportunityCount" runat="server"></asp:Label></p>
								<crm:Snippet runat="server" SnippetName="home/activities/label/new-opportunities-value" DefaultText="Value of new Opportunities" Editable="true" EditType="text" />
								<p><asp:Label ID="NewOpportunityValue" runat="server"></asp:Label></p>
							</div>
						</div>
					<%--</crm:CrmHyperLink>
					<crm:CrmHyperLink runat="server" SiteMarkerName="Accepted Opportunities" >--%>
						<div id="accepted-opportunity-activity" class="well sidebar">
							<div class="section">
								<h4>
									<crm:CrmHyperLink runat="server" SiteMarkerName="Accepted Opportunities" >
									<crm:Snippet runat="server" SnippetName="home/activities/label/accepted-opportunities" DefaultText="Active Opportunities" Editable="true" EditType="text" />
									</crm:CrmHyperLink>
								</h4>
								<crm:Snippet runat="server" SnippetName="home/activities/label/accepted-opportunities-count" DefaultText="Current Opportunities" Editable="true" EditType="text" />
								<p><asp:Label ID="AcceptedOpportunityCount" runat="server"></asp:Label></p>
								<crm:Snippet runat="server" SnippetName="home/activities/label/accepted-opportunities-value" DefaultText="Value of active Opportunities" Editable="true" EditType="text" />
								<p><asp:Label ID="AcceptedOpportunityValue" runat="server"></asp:Label></p>
							</div>
						</div>
					<%--</crm:CrmHyperLink>--%>
				</div>
			</div>
			<div id="home-alerts-section" class="span9">
				<h4>
					<asp:Literal Text="<%$ Snippet: home/alerts/label, Alerts %>" runat="server" /></h4>
				<div id="home-alerts">
					<crm:Snippet runat="server" SnippetName="home/alerts/legend" DefaultText="home/alerts/legend" Editable="true" EditType="html"/>
					<asp:GridView ID="Alerts" runat="server" CssClass="table table-striped" GridLines="None" AlternatingRowStyle-CssClass="alternate-row" OnRowDataBound="Alerts_OnRowDataBound">
						<EmptyDataRowStyle CssClass="empty" />
						<EmptyDataTemplate>
							<crm:Snippet runat="server" SnippetName="home/alerts/empty" DefaultText="There are currently no alerts." Editable="true" EditType="html" />
						</EmptyDataTemplate>
					</asp:GridView>
				</div>
			</div>
		</div>
		</asp:Panel>
	</form>
</asp:Content>
