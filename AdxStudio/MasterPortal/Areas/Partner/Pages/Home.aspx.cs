﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Data;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Portal.Web.UI;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using Xrm;

namespace Site.Areas.Partner.Pages
{
	public partial class Home : PortalPage
	{
		protected const string HomeAlertsSavedQueryName = "Web - Partner Pipeline - Home Alerts";

		protected List<Opportunity> Opportunities
		{
			get
			{
				return XrmContext.GetOpportunitiesForContact(Contact).Where(o => o.GetAttributeValue<int>("statecode") == 
					(int)Adxstudio.Xrm.Partner.Enums.OpportunityState.Open)
					.Cast<Opportunity>().ToList();
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			//RedirectToLoginIfNecessary();

			var contact = Contact;

			if (contact != null)
			{
				var homeAlertsSavedQuery = XrmContext.SavedQuerySet.FirstOrDefault(query => query.Name == HomeAlertsSavedQueryName);

				var alerts = Opportunities.Where(opp => opp.MSA_AllocatedtoPartner != (int)Adxstudio.Xrm.Partner.Enums.AllocatedToPartner.Yes ||
					XrmContext.GetOpportunityLatestStatusModifiedOn(opp) <= DateTime.Now.AddDays(-ServiceContext.GetInactiveDaysUntilOverdue(Website)))
					.OrderByDescending(opp => GetAlertType(opp.Id)).ThenBy(opp => opp.opportunity_customer_accounts.Name);

				Alerts.DataKeyNames = new[] { "opportunityid" };
				Alerts.DataSource = alerts.ToDataTable(XrmContext, homeAlertsSavedQuery);
				Alerts.ColumnsGenerator = new CrmSavedQueryColumnsGenerator(HomeAlertsSavedQueryName);
				Alerts.DataBind();

				var newOpportunities = Opportunities.Where(opp => !opp.MSA_AllocatedtoPartner.HasValue);

				NewOpportunityCount.Text = newOpportunities.Count().ToString();
				NewOpportunityValue.Text = newOpportunities.Sum(opp => opp.EstimatedValue).Value.ToString("C");

				var acceptedOpportunities = Opportunities.Where(opp => opp.MSA_AllocatedtoPartner == (int)Adxstudio.Xrm.Partner.Enums.AllocatedToPartner.Yes);

				AcceptedOpportunityCount.Text = acceptedOpportunities.Count().ToString();
				AcceptedOpportunityValue.Text = acceptedOpportunities.Sum(opp => opp.EstimatedValue).Value.ToString("C");
			}
			else
			{
				PartnerHomePanel.Visible = false;
			}
		}

		protected void Alerts_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow || e.Row.Cells.Count < 1)
			{
				return;
			}

			var dataKey = Alerts.DataKeys[e.Row.RowIndex].Value;

			var alertType = GetAlertType(dataKey);

			e.Row.Cells[0].Text = string.Format(@"<i class=""{0}""></i> <a href=""{1}"" >{2}</a>",
				GetAlertType(dataKey),
				GetAlertType(dataKey) == "icon-ok-circle" ? NewOpportunitiesUrl() : OpportunityDetailsUrl(dataKey),
				e.Row.Cells[0].Text);
		}

		protected string NewOpportunitiesUrl()
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "New Opportunities");

			return new UrlBuilder(ServiceContext.GetUrl(page)).PathWithQueryString;
		}

		protected string OpportunityDetailsUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Opportunity Details");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("OpportunityID", id.ToString());

			return url.PathWithQueryString;
		}

		private string GetAlertType(object dataKey)
		{
			Guid id;

			Guid.TryParse(dataKey.ToString(), out id);

			var alertType = ServiceContext.GetAlertType(id, Website);

			if (alertType.ToString() == Enums.AlertType.Overdue.ToString()) return "icon-exclamation-sign";

			if (alertType.ToString() == Enums.AlertType.PotentiallyStalled.ToString()) return "icon-warning-sign";

			if (alertType.ToString() == Enums.AlertType.New.ToString()) return "icon-ok-circle";

			return "";
		}

	}
}