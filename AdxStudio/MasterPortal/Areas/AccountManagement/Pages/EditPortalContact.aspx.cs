﻿using System;
using System.Linq;
using Adxstudio.Xrm.Account;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Services;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;
using Xrm;

namespace Site.Areas.AccountManagement.Pages
{
	public partial class EditPortalContact : PortalPage
	{
		private Contact _contact;

		public Contact ContactToEdit
		{
			get
			{
				if (_contact != null)
				{
					return _contact;
				}

				Guid contactId;

				if (!Guid.TryParse(Request["ContactID"], out contactId))
				{
					return null;
				}

				_contact = XrmContext.ContactSet.FirstOrDefault(c => c.Id == contactId);

				return _contact;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			var formViewDataSource = new CrmDataSource { ID = "WebFormDataSource" };

			var fetchXml = string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>", Contact.EntityLogicalName, "contactid", ContactToEdit.ContactId);

			formViewDataSource.FetchXml = fetchXml;

			ContactWebForm.Controls.Add(formViewDataSource);

			ContactFormView.DataSourceID = "WebFormDataSource";

			Xrm.Account partnerAccount = null;

			if (Contact != null && Contact.ParentCustomerId != null)
			{
				AssertContactHasParentAccount();

				partnerAccount = ServiceContext.AccountSet.FirstOrDefault(a => a.AccountId == Contact.ParentCustomerId.Id);	
			}
			
			if ((partnerAccount == null || ContactToEdit == null || ContactToEdit.ParentCustomerId == null) || (ContactToEdit.ParentCustomerId.Id != partnerAccount.AccountId))
			{
				InviteButton.Visible = false;
				ManageRoles.Visible = false;

				var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

				var channelWriteAccess = (channelPermission != null && channelPermission.adx_Write.GetValueOrDefault());

				var partnerContact = ServiceContext.MergeClone(Contact);

				var parentAccount = partnerContact.contact_customer_accounts;

				if (!channelWriteAccess || parentAccount == null || (channelPermission.adx_AccountId.Id != parentAccount.AccountId)
					|| parentAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
				{
					ContactWebForm.Visible = false;

					NoAccountAccessLabel.Visible = true;
				}


				//additionally, at this point we should check channel permissions 
			}
			else
			{
				var contactAccessPermissions = ServiceContext.GetContactAccessByContact(Contact).Cast<Adx_contactaccess>();

				var canWrite = false;

				foreach (var access in contactAccessPermissions)
				{
					if (access.Adx_Write.GetValueOrDefault())
					{
						canWrite = true;
					}

				}

				ContactWebForm.Visible = canWrite;

				NoAccountAccessLabel.Visible = !canWrite;

				//at this point we should check contact access perms
			}

		}

		protected void OnItemUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e) { }

		protected void OnItemUpdated(object sender, CrmEntityFormViewUpdatedEventArgs e)
		{
			ConfirmationMessage.Visible = true;
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			ContactFormView.UpdateItem();
		}

		protected void InviteButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			//create invitation Code

			var contact = XrmContext.ContactSet.FirstOrDefault(c => c.ContactId == ContactToEdit.ContactId);

			if (contact == null)
			{
				throw new ArgumentNullException(string.Format("Unable to find contact with id equal to {0}",ContactToEdit.ContactId));
			}

			contact.Adx_InvitationCode = ServiceContext.CreateInvitationCode();

			var oppPermissions = new adx_opportunitypermissions
				                     {
				adx_name = "Web - Opportunity Permissions",
				adx_ContactId = contact.ToEntityReference(),
				adx_AccountId = contact.ParentCustomerId,
				adx_Scope = 100000000,
				adx_Read = true
			};

			var channelPermissions = new adx_channelpermissions
				                         {
				adx_name = "Web - Channel Permissions",
				adx_ContactId = contact.ToEntityReference(),
				adx_AccountId = contact.ParentCustomerId
			};

			XrmContext.AddObject(oppPermissions);

			XrmContext.AddObject(channelPermissions);

			XrmContext.UpdateObject(contact);

			XrmContext.SaveChanges();

			// Execute workflow to send invitation code in confirmation email

			XrmContext.ExecuteWorkflowByName(ServiceContext.GetSiteSettingValueByName(Website, "Account/EmailConfirmation/WorkflowName") ?? "ADX Sign Up Email Confirmation", contact.Id);
			
			InvitationConfirmationMessage.Visible = true;
		}

		protected void ManagePermissionsButton_Click(object sender, EventArgs e)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Manage Permissions");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			var partnerAccount = ServiceContext.AccountSet.FirstOrDefault(a => a.AccountId == Contact.ParentCustomerId.Id);

			var partnerOppPerms = ServiceContext.adx_opportunitypermissionsSet.FirstOrDefault(op => op.adx_ContactId.Id == Contact.ContactId);

			var partnerChannelPerms = ServiceContext.adx_channelpermissionsSet.FirstOrDefault(cp => cp.adx_ContactId.Id == Contact.ContactId);

			var oppPermissions = ContactToEdit.adx_contact_opportunitypermissions.FirstOrDefault();

			bool changes = false;

			if (oppPermissions == null && partnerAccount != null && partnerOppPerms != null)
			{
				oppPermissions = new adx_opportunitypermissions()
									 {
										 adx_ContactId = ContactToEdit.ToEntityReference(),
										 adx_name = "opportunitity permissions for " + ContactToEdit.FullName
									 };

				ServiceContext.AddObject(oppPermissions);

				changes = true;
			}

			var channelPermissions = ContactToEdit.adx_contact_channelpermissions.FirstOrDefault();

			if (channelPermissions == null && partnerAccount != null && partnerChannelPerms != null)
			{
				channelPermissions = new adx_channelpermissions()
										{
											adx_ContactId = ContactToEdit.ToEntityReference(),
											adx_name = "channel permissions for" + ContactToEdit.FullName
										};

				ServiceContext.AddObject(channelPermissions);

				changes = true;
			}

			if (changes) ServiceContext.SaveChanges();

			var id = ContactToEdit.ContactId;

			url.QueryString.Set("ContactID", id.ToString());

			Response.Redirect(url);
		}
	}
}