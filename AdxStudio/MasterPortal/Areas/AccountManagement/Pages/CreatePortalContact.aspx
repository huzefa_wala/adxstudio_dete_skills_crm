﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="CreatePortalContact.aspx.cs" Inherits="Site.Areas.AccountManagement.Pages.CreatePortalContact" %>
<%@ OutputCache VaryByParam="*" VaryByCustom="user" Duration="60" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content  ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<div class="row">
		<div id="instructions-section" class="pull-right span3 well">
			<h4>
				<asp:Literal Text="<%$ Snippet: profile/instructions, Instructions %>" runat="server" />
			</h4>
			<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
		<div id="create-contact-section" class="span8">
			<div id="profile">
				<asp:Panel ID="ConfirmationMessage" runat="server" CssClass="alert alert-success" Visible="false">
					<crm:Snippet runat="server" SnippetName="Contact Create Success Text" DefaultText="User Contact has been created successfully." Editable="true" EditType="html" />
				</asp:Panel>
				<adx:CrmDataSource ID="WebFormDataSource" runat="server" />
				<asp:Panel ID="ContactWebForm" CssClass="crmEntityFormView" runat="server" Visible="true">
					<adx:CrmEntityFormView runat="server" ID="ContactFormView"
						EntityName="contact"
						FormName="Contact Web Form"
						OnItemInserted="OnItemInserted"
						RecommendedFieldsRequired="True"
						ShowUnsupportedFields="False"
						DataSourceID="WebFormDataSource"
						ToolTipEnabled="False"
						Mode="Insert"
						ValidationGroup="CreatePortalContact">
						<InsertItemTemplate></InsertItemTemplate>
					</adx:CrmEntityFormView>
				</asp:Panel>
				<div class="form-actions">
					<asp:Button ID="SubmitButton" 
						Text='<%$ Snippet: Contact Create Submit Button Text, Create %>' 
						CssClass="btn btn-primary" OnClick="SubmitButton_Click" ValidationGroup="CreatePortalContact" runat="server" />
					<asp:Button ID="InviteAndSave" 
						Text='<%$ Snippet: Contact Create and Invite Button, Create and Invite to Portal %>' 
						CssClass="btn btn-info" 
						OnClick="InviteAndSaveButton_Click" 
						ValidationGroup="CreatePortalContact" 
						runat="server" />
				</div>
			</div>
		</div>
	</div>
</asp:Content>
