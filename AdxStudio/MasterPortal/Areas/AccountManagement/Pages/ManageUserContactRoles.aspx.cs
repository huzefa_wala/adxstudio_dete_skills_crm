﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;
using Xrm;
using CrmDataSource = Adxstudio.Xrm.Web.UI.WebControls.CrmDataSource;

namespace Site.Areas.AccountManagement.Pages
{
	public partial class ManageUserContactRoles : PortalPage
	{
		private Contact _contact;

		//private adx_opportunitypermissions _partnerOppPerms;

		//private adx_channelpermissions _partnerChannelPerms;

		public Contact ContactToEdit
		{
			get
			{
				if (_contact != null)
				{
					return _contact;
				}

				Guid contactId;

				if (!Guid.TryParse(Request["ContactID"], out contactId))
				{
					return null;
				}

				_contact = XrmContext.ContactSet.FirstOrDefault(c => c.Id == contactId);

				return _contact;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			AssertContactHasParentAccount();
			
			var oppPermissions = ContactToEdit.adx_contact_opportunitypermissions.FirstOrDefault();

			var channelPermissions = ContactToEdit.adx_contact_channelpermissions.FirstOrDefault();

			var partnerAccount = ServiceContext.AccountSet.FirstOrDefault(a => a.AccountId == Contact.ParentCustomerId.Id);

			var partnerOppPerms = ServiceContext.adx_opportunitypermissionsSet.FirstOrDefault(op => op.adx_ContactId.Id == Contact.ContactId);

			var partnerChannelPerms = ServiceContext.adx_channelpermissionsSet.FirstOrDefault(cp => cp.adx_ContactId.Id == Contact.ContactId);

			if (oppPermissions == null || channelPermissions == null || partnerOppPerms == null || partnerChannelPerms == null)
			{
				//error message

				ErrorLabel.Visible = true;
				return;

			}

			var formViewDataSource = new CrmDataSource { ID = "WebFormDataSource" };

			var fetchXml =
				string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>",
				adx_opportunitypermissions.EntityLogicalName, "adx_opportunitypermissionsid", oppPermissions.adx_opportunitypermissionsId);

			formViewDataSource.FetchXml = fetchXml;

			OppAccesstWebForm.Controls.Add(formViewDataSource);

			OppAccesstFormView.DataSourceID = "WebFormDataSource";

			var formViewDataSourceChannel = new CrmDataSource { ID = "WebFormDataSourceChannel" };

			var fetchXmlChannel =
				string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>",
				adx_channelpermissions.EntityLogicalName, "adx_channelpermissionsid", channelPermissions.adx_channelpermissionsId);

			formViewDataSourceChannel.FetchXml = fetchXmlChannel;

			OppAccesstWebForm.Controls.Add(formViewDataSourceChannel);

			ChannelAccesstFormView.DataSourceID = "WebFormDataSourceChannel";

			if (!IsPostBack)
			{
				SetControlValues(oppPermissions, channelPermissions);
			}

			HideControlsBasedOnPartnerAccess(partnerAccount, partnerOppPerms, partnerChannelPerms);

		}

		public void SetControlValues(adx_opportunitypermissions opportunitypermissions, adx_channelpermissions channelpermissions)
		{
			var oppCreate = opportunitypermissions.adx_Create ?? false;

			var oppDelete = opportunitypermissions.adx_Delete ?? false;

			var oppAcceptDecline = opportunitypermissions.adx_AcceptDecline ?? false;

			var oppAssign = opportunitypermissions.adx_Assign ?? false;

			var channelWrite = channelpermissions.adx_Write ?? false;

			var channelCreate = channelpermissions.adx_Create ?? false;

			OppCreateCheckBox.Checked = oppCreate;

			OppDeleteCheckBox.Checked = oppDelete;

			OppAcceptDeclineCheckBox.Checked = oppAcceptDecline;

			OppAssignCheckBox.Checked = oppAssign;

			ChannelWriteCheckBox.Checked = channelWrite;

			ChannelCreateCheckBox.Checked = channelCreate;
		}

		public void HideControlsBasedOnPartnerAccess(Xrm.Account partnerAccount, adx_opportunitypermissions partnerOppPerms, adx_channelpermissions partnerChannelPerms)
		{
			// let's just worry about the checkboxes
			var partnerOppCreate = partnerOppPerms.adx_Create ?? false;

			var partnerOppDelete = partnerOppPerms.adx_Delete ?? false;

			var partnerOppAcceptDecline = partnerOppPerms.adx_AcceptDecline ?? false;

			var partnerOppAssign = partnerOppPerms.adx_Assign ?? false;

			var partnerChannelWrite = partnerChannelPerms.adx_Write ?? false;

			var partnerChannelCreate = partnerChannelPerms.adx_Create ?? false;

			OppCreateCheckBox.Visible = partnerOppCreate;

			OppDeleteCheckBox.Visible = partnerOppDelete;

			OppAcceptDeclineCheckBox.Visible = partnerOppAcceptDecline;

			OppAssignCheckBox.Visible = partnerOppAssign;

			ChannelWriteCheckBox.Visible = partnerChannelWrite;

			ChannelCreateCheckBox.Visible = partnerChannelCreate;
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			ChannelAccesstFormView.UpdateItem();

			OppAccesstFormView.UpdateItem();
		}

		protected void OppPermissionsUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e)
		{
			e.Values["adx_create"] = OppCreateCheckBox.Checked;
			e.Values["adx_delete"] = OppDeleteCheckBox.Checked;
			e.Values["adx_acceptdecline"] = OppAcceptDeclineCheckBox.Checked;
			e.Values["adx_assign"] = OppAssignCheckBox.Checked;
		}

		protected void OppPermissionsUpdated(object sender, CrmEntityFormViewUpdatedEventArgs e)
		{
			var opportunity = XrmContext.adx_opportunitypermissionsSet.First(o => o.Id == e.Entity.Id);

			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Edit Portal User");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("ContactID", ContactToEdit.ContactId.ToString());

			Response.Redirect(url);
		}

		protected void ChannelPermissionsUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e)
		{
			e.Values["adx_write"] = ChannelWriteCheckBox.Checked;
			e.Values["adx_create"] = ChannelCreateCheckBox.Checked;
		}

		protected void ChannelPermissionsUpdated(object sender, CrmEntityFormViewUpdatedEventArgs e)
		{

		}


	}
}