﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="EditPortalContact.aspx.cs" Inherits="Site.Areas.AccountManagement.Pages.EditPortalContact" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content  ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<div class="row">
		<div id="instructions-section" class="pull-right well span3">
			<h4>
				<asp:Literal Text="<%$ Snippet: profile/instructions, Instructions %>" runat="server" />
			</h4>
			<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
		<div id="edit-contact-section" class="span8">
			<crm:Snippet ID="NoAccountAccessLabel" runat="server" SnippetName="edit-contact/no_access" DefaultText="You do not have permissions to perform this action" Editable="true" EditType="html" Visible="False" />
			<asp:Panel ID="ConfirmationMessage" runat="server" Visible="false" CssClass="alert alert-success">
				<crm:Snippet runat="server" SnippetName="Contact Update Success Text" DefaultText="Contact has been updated successfully." Editable="true" EditType="html" />
			</asp:Panel>
			<asp:Panel ID="InvitationConfirmationMessage" runat="server" Visible="false" CssClass="alert alert-success">
				<crm:Snippet runat="server" SnippetName="Contact Invitation Success Text" DefaultText="Contact has been invited successfully." Editable="true" EditType="html" />
			</asp:Panel>
			<br/>
			<div id="profile">
				<asp:Panel ID="ContactWebForm" CssClass="crmEntityFormView" runat="server" Visible="true">
					<adx:CrmEntityFormView runat="server" 
						ID="ContactFormView" 
						EntityName="contact" 
						FormName="Contact Web Form" 
						OnItemUpdating="OnItemUpdating" 
						OnItemUpdated="OnItemUpdated" 
						ValidationGroup="Profile" 
						RecommendedFieldsRequired="True" 
						ShowUnsupportedFields="False" 
						ToolTipEnabled="False" Mode="Edit">
						<UpdateItemTemplate>
						</UpdateItemTemplate>
					</adx:CrmEntityFormView>
				</asp:Panel>
				<div class="form-actions">
					<asp:Button ID="SubmitButton" Text='<%$ Snippet: Contact Update Submit Button Text, Update %>' CssClass="btn btn-primary" OnClick="SubmitButton_Click" ValidationGroup="Profile" runat="server" />
					<asp:Button ID="InviteButton" Text='<%$ Snippet: Invite Contact to Portal Text, Invite Contact to Portal %>' CssClass="btn btn-info" OnClick="InviteButton_Click" runat="server" />
					<asp:Button ID="ManageRoles" Text='<%$ Snippet: Manage Roles, Manage Permissions %>' CssClass="btn btn-inverse" OnClick="ManagePermissionsButton_Click" runat="server" />
				</div>
			</div>
		</div>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script type="text/javascript" src="~/js/jquery.cookie.js"></script>
	<script type="text/javascript">
		$(function () {
			$("form").submit(function () {
				if (Page_IsValid) {
					$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
				}
			});
		});
	</script>
</asp:Content>
