﻿using System;
using System.Linq;
using Adxstudio.Xrm.Account;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Services;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Portal.Web.UI.WebControls;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using Xrm;

namespace Site.Areas.AccountManagement.Pages
{
	public partial class CreatePortalContact : PortalPage
	{
		protected bool Invite
		{
			get { return ViewState["Invite"] as bool? ?? false; }
			set { ViewState["Invite"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();
			AssertContactHasParentAccount();
			HideControlsBasedOnAccess(ServiceContext, Contact);
		}

		protected void OnItemInserted(object sender, CrmEntityFormViewInsertedEventArgs e)
		{
			var account = XrmContext.AccountSet.FirstOrDefault(a => a.AccountId == Contact.ParentCustomerId.Id);

			var contact = XrmContext.ContactSet.FirstOrDefault(c => c.ContactId == e.EntityId);

			if (account == null || contact == null)
			{
				throw new Exception("Unable to retrieve account or contact for the logged in user.");
			}

			contact.ParentCustomerId = account.ToEntityReference();

			XrmContext.UpdateObject(account);

			if (Invite)
			{
				InviteContact(contact);
			}

			XrmContext.UpdateObject(contact);

			XrmContext.SaveChanges();

			ConfirmationMessage.Visible = true;

			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Manage Partner Account");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			Response.Redirect(url);
		}

		private void HideControlsBasedOnAccess(OrganizationServiceContext context, Entity contact)
		{
			var accessPermissions = context.GetContactAccessByContact(contact).Cast<Adx_contactaccess>();

			ContactWebForm.Visible = false;


			foreach (var access in accessPermissions)
			{

				if (access.Adx_Create.GetValueOrDefault())
				{
					ContactWebForm.Visible = true;
				}
			}

		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			Invite = false;

			ContactFormView.InsertItem();
		}

		protected void InviteAndSaveButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			Invite = true;

			ContactFormView.InsertItem();
		}

		protected void InviteContact(Contact contact)
		{
			//create invitation Code

			contact.Adx_InvitationCode = ServiceContext.CreateInvitationCode();

			var oppPermissions = new adx_opportunitypermissions
				                     {
				adx_name = "Web - Opportunity Permissions",
				adx_ContactId = contact.ToEntityReference(),
				adx_AccountId = contact.ParentCustomerId,
				adx_Scope = 100000000,
				adx_Read = true
			};

			var channelPermissions = new adx_channelpermissions
				                         {
				adx_name = "Web - Channel Permissions",
				adx_ContactId = contact.ToEntityReference(),
				adx_AccountId = contact.ParentCustomerId
			};

			XrmContext.AddObject(oppPermissions);

			XrmContext.AddObject(channelPermissions);

			XrmContext.UpdateObject(contact);

			XrmContext.SaveChanges();

			// Execute workflow to send invitation code in confirmation email

			XrmContext.ExecuteWorkflowByName(ServiceContext.GetSiteSettingValueByName(Website, "Account/EmailConfirmation/WorkflowName") ?? "ADX Sign Up Email Confirmation", contact.Id);
		}
	}
}