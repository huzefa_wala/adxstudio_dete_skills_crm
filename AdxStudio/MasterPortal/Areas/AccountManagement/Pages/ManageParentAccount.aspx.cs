﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Data;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using Xrm;

namespace Site.Areas.AccountManagement.Pages
{
	public partial class ManageParentAccount : PortalPage
	{

		private DataTable _contacts;

		protected string SortDirection
		{
			get { return ViewState["SortDirection"] as string ?? "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		protected string SortExpression
		{
			get { return ViewState["SortExpression"] as string ?? "Accepted"; }
			set { ViewState["SortExpression"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			AssertContactHasParentAccount();

			var formViewDataSource = new CrmDataSource { ID = "WebFormDataSource" };

			var parentAccountReference = Contact.ParentCustomerId;

			var parentAccount = XrmContext.AccountSet.FirstOrDefault(a => a.GetAttributeValue<Guid>("accountid") == parentAccountReference.Id);

			HideControlsBasedOnAccess(ServiceContext, Contact);

			if (NoAccountAccessLabelPanel.Visible)
			{
				return;
			}

			var fetchXml = string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>", Xrm.Account.EntityLogicalName, "accountid", parentAccount.AccountId);

			formViewDataSource.FetchXml = fetchXml;

			AccountForm.Controls.Add(formViewDataSource);

			FormView.DataSourceID = "WebFormDataSource";

			var contacts = ServiceContext.GetContactsForContact(Contact)
					.Where(c => c.GetAttributeValue<Guid>("parentcustomerid") == parentAccount.Id);


			//HideControlsBasedOnAccess(ServiceContext, Contact);

			_contacts = EnumerableExtensions.CopyToDataTable(contacts.Cast<Contact>().Select(c => new
			{
				contactid = c.Id,
				ID = c.FullName,
				CompanyName = c.contact_customer_accounts.Name,
				City = c.Address1_City,
				State = c.Address1_StateOrProvince,
				Phone = c.Address1_Telephone1,
				Email = c.EMailAddress1,
			}).OrderBy(opp => opp.CompanyName));

			_contacts.Columns["City"].ColumnName = "City";
			_contacts.Columns["State"].ColumnName = "State";
			_contacts.Columns["Phone"].ColumnName = "Phone";
			_contacts.Columns["Email"].ColumnName = "E-mail Address";

			AccountContactsList.DataKeyNames = new[] { "contactid" };
			AccountContactsList.DataSource = _contacts;
			AccountContactsList.DataBind();

			HideControlsBasedOnAccess(XrmContext, Contact);
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			//var contact = XrmContext.MergeClone(Contact);

			FormView.UpdateItem();
		}

		protected void OnItemUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e) { }

		protected void OnItemUpdated(object sender, CrmEntityFormViewUpdatedEventArgs e)
		{
			ConfirmationMessage.Visible = true;
		}

		protected void AccountContactsList_Sorting(object sender, GridViewSortEventArgs e)
		{
			SortDirection = e.SortExpression == SortExpression
				? (SortDirection == "ASC" ? "DESC" : "ASC")
				: "ASC";

			SortExpression = e.SortExpression;

			_contacts.DefaultView.Sort = string.Format("{0} {1}", SortExpression, SortDirection);

			AccountContactsList.DataSource = _contacts;
			AccountContactsList.DataBind();
		}

		protected void AccountContactsList_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header ||
				e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Cells[0].Visible = false;
			}

			if (e.Row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var dataKey = AccountContactsList.DataKeys[e.Row.RowIndex].Value;

			e.Row.Cells[1].Text = string.Format(@"<a href=""{0}"">{1}</a>",
					ContactDetailsUrl(dataKey),
					e.Row.Cells[1].Text);

			e.Row.Cells[1].Attributes.Add("style", "white-space: nowrap;");
		}

		protected string ContactDetailsUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Edit Portal User");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("ContactID", id.ToString());

			return url;
		}

		private void HideControlsBasedOnAccess(OrganizationServiceContext context, Entity contact)
		{
			var parentAccountReference = Contact.ParentCustomerId;

			var parentAccount = XrmContext.AccountSet.FirstOrDefault(a => a.GetAttributeValue<Guid>("accountid") == parentAccountReference.Id);

			var accountAccess = XrmContext.GetAccountAccessByContact(Contact).Cast<Adx_accountaccess>();

			var contactAccessPermissions = context.GetContactAccessByContact(contact).Cast<Adx_contactaccess>();

			NoAccountAccessLabelPanel.Visible = true;
			NoContactAccessLabelPanel.Visible = true;
			CreateButton.Visible = false;

			foreach (var access in contactAccessPermissions)
			{
				if (access.Adx_Create.GetValueOrDefault())
				{
					CreateButton.Visible = true;
				}

				if (access.Adx_Read.GetValueOrDefault())
				{
					NoContactAccessLabelPanel.Visible = false;
				}
			}

			if (parentAccount != null)
			{
				foreach (var access in accountAccess)
				{
					if ((access.adx_accountid.Id == parentAccount.Id) && (access.Adx_Read ?? false))
					{
						NoAccountAccessLabelPanel.Visible = false;
					}

				}
			}

			AccountForm.Visible = !NoAccountAccessLabelPanel.Visible;
			AccountContactsList.Visible = !NoContactAccessLabelPanel.Visible;
			AccountContactsListLabel.Visible = AccountContactsList.Visible;
			SubmitButtonPanel.Visible = !NoAccountAccessLabelPanel.Visible;
		}

		protected void CreateButton_Click(object sender, EventArgs args)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Create Portal Contact");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			Response.Redirect(url);
		}


	}
}