﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="ManageUserContactRoles.aspx.cs" Inherits="Site.Areas.AccountManagement.Pages.ManageUserContactRoles" %>
<%@ OutputCache VaryByParam="*" VaryByCustom="user" Duration="60" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
	<link rel="stylesheet" href="~/Areas/AccountManagement/css/account-management.css">
</asp:Content>

<asp:Content  ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<div class="row">
		<div id="instructions-section" class="well span3 pull-right">
			<h4>
				<asp:Literal Text="<%$ Snippet: manage-permissions/instructions, Instructions %>" runat="server" />
			</h4>
			<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
		<div id="edit-contact-section" class="span8">
			<asp:Panel ID="ConfirmationMessage" runat="server" Visible="false" CssClass="alert alert-success">
				<crm:Snippet runat="server" SnippetName="Contact Update Success Text" DefaultText="Contact has been updated successfully." Editable="true" EditType="html" />
			</asp:Panel>
			<asp:Panel ID="InvitationConfirmationMessage" runat="server" Visible="false" CssClass="alert alert-info">
				<crm:Snippet runat="server" SnippetName="Contact Invitation Success Text" DefaultText="Contact has been invited successfully." Editable="true" EditType="html" />
			</asp:Panel>
			<br/>
			<div id="profile">
				<crm:Snippet ID="ErrorLabel" runat="server" SnippetName="manage_permissions/error" DefaultText="Please contact the vendor administrator, there is an error with this contact." Editable="true" Visible="False" EditType="html" />
				<crm:Snippet ID="NoAccountAccessLabel" runat="server" SnippetName="manage_permissions/no_access" DefaultText="You do not have an associated parent account, or do not have account access." Editable="true" Visible="False" EditType="html" />
				<asp:Panel ID="OppAccesstWebForm" CssClass="crmEntityFormView" runat="server" Visible="true">
					<adx:CrmEntityFormView runat="server" ID="OppAccesstFormView" EntityName="adx_opportunitypermissions" FormName="Permissions Management Web Form"
					 OnItemUpdating="OppPermissionsUpdating" OnItemUpdated="OppPermissionsUpdated" 
					 ValidationGroup="Profile" RecommendedFieldsRequired="True" ShowUnsupportedFields="False" ToolTipEnabled="False" Mode="Edit">
						<UpdateItemTemplate>
						</UpdateItemTemplate>
					</adx:CrmEntityFormView>
					<table class="section tab form-inline" id="opportunity-permissions">
						<colgroup><col /><col /></colgroup>
						<tr valign="top">
							<td colspan="1" rowspan="1" class="cell checkbox-cell checkbox"> <asp:CheckBox ID="OppCreateCheckBox" Text="Create" CssClass="padded-cb"  ClientIDMode="Static" runat="server" /> </td>
							<td colspan="1" rowspan="1" class="cell checkbox-cell checkbox"> <asp:CheckBox ID="OppDeleteCheckBox" Text="Delete" CssClass="padded-cb" ClientIDMode="Static" runat="server" /> </td>
						</tr>
						<tr valign="top">
							<td colspan="1" rowspan="1" class="cell checkbox-cell checkbox"> <asp:CheckBox ID="OppAcceptDeclineCheckBox" Text="Accept/Decline" CssClass="padded-cb" ClientIDMode="Static" runat="server" /> </td>
							<td colspan="1" rowspan="1" class="cell checkbox-cell checkbox"><asp:CheckBox ID="OppAssignCheckBox" Text="Assign" CssClass="padded-cb" ClientIDMode="Static" runat="server" /> </td>
						</tr>
					</table>
					<adx:CrmEntityFormView runat="server" ID="ChannelAccesstFormView" EntityName="adx_channelpermissions" 
					FormName="Permissions Management Web Form" OnItemUpdating="ChannelPermissionsUpdating" OnItemUpdated="ChannelPermissionsUpdated"
					 ValidationGroup="Profile" RecommendedFieldsRequired="True" ShowUnsupportedFields="False" ToolTipEnabled="False" Mode="Edit">
						<UpdateItemTemplate>
						</UpdateItemTemplate>
					</adx:CrmEntityFormView>
					<table class="section tab form-inline" id="channel-permissions">
						<colgroup><col /><col /></colgroup>
						<tr valign="top">
							<td colspan="1" rowspan="1" class="cell checkbox-cell checkbox"> <asp:CheckBox ID="ChannelWriteCheckBox" Text="Write" ClientIDMode="Static" runat="server" /> </td>
							<td colspan="1" rowspan="1" class="cell checkbox-cell checkbox"> <asp:CheckBox ID="ChannelCreateCheckBox" Text="Create" ClientIDMode="Static" runat="server" /> </td>
						</tr>
					</table>
				</asp:Panel>
				<div class="form-actions">
					<asp:Button ID="SubmitButton" Text='<%$ Snippet: Contact Update Submit Button Text, Update %>' CssClass="btn btn-primary" OnClick="SubmitButton_Click" ValidationGroup="Profile" runat="server" />
				</div>
			</div>
		</div>
	</div>
</asp:Content>
