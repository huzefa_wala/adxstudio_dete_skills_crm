﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="ManageParentAccount.aspx.cs" Inherits="Site.Areas.AccountManagement.Pages.ManageParentAccount" %>
<%@ OutputCache VaryByParam="*" VaryByCustom="user" Duration="60" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content  ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<div class="row">
		<div id="instructions-section" class="pull-right span3 well">
			<h4>
				<asp:Literal Text="<%$ Snippet: profile/instructions, Instructions %>" runat="server" /></h4>
			<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property  DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
		<div id="manage-account-section" class="span8">
			<asp:Panel ID="ConfirmationMessage" runat="server" Visible="false" CssClass="alert alert-success">
				<crm:Snippet runat="server" SnippetName="Account Update Success Text" DefaultText="Account information has been updated successfully." Editable="true" EditType="html" />
			</asp:Panel>
			<br/>
			<div id="profile">
				<asp:Panel runat="server" ID="NoAccountAccessLabelPanel" CssClass="alert alert-danger">
					<crm:Snippet runat="server" SnippetName="account-profile/no_access" DefaultText="You do not have an associated parent account, or do not have account access." Editable="true" EditType="html"  />
				</asp:Panel>
				<asp:Panel ID="AccountForm" CssClass="crmEntityFormView frame" runat="server" Visible="true">
					<adx:CrmEntityFormView runat="server" 
						ID="FormView" EntityName="account" 
						FormName="Account Web Form" 
						OnItemUpdating="OnItemUpdating" 
						OnItemUpdated="OnItemUpdated" 
						ValidationGroup="Profile" 
						RecommendedFieldsRequired="True" 
						ShowUnsupportedFields="False" 
						ToolTipEnabled="False" 
						Mode="Edit">
						<UpdateItemTemplate>
						</UpdateItemTemplate>
					</adx:CrmEntityFormView>
				</asp:Panel>
				<asp:Panel ID="SubmitButtonPanel" CssClass="form-actions" runat="server">
						<asp:Button ID="SubmitButton" Text='<%$ Snippet: Profile Submit Button Text, Update %>' CssClass="btn btn-primary" OnClick="SubmitButton_Click" ValidationGroup="Profile" runat="server" />
					</asp:Panel>
				<asp:Panel runat="server" ID="NoContactAccessLabelPanel" CssClass="alert alert-danger">
					<crm:Snippet runat="server" SnippetName="accepted-opportunities/no_access" DefaultText="You do not have permissions to edit your contact list." Editable="true" EditType="html" />
				</asp:Panel>
			</div>
		</div>
	</div>
	<hr/>
	<div id="contacts-list" class="form-inline">
		<asp:LinkButton ID="CreateButton" runat="server" CssClass="btn btn-success pull-right" OnClick="CreateButton_Click" >
			<i class="icon-white icon-plus-sign"></i> Create New
		</asp:LinkButton>
		<crm:Snippet ID="AccountContactsListLabel"  runat="server" SnippetName="Account Contacts Associated Text" DefaultText="The following User Contacts are associated with this account:" Editable="true" EditType="html" />
	</div>
	<br/>
	<div id="account-contacts">
		<asp:GridView ID="AccountContactsList" runat="server" CssClass="table table-striped" GridLines="None" AlternatingRowStyle-CssClass="alternate-row" AllowSorting="true" OnSorting="AccountContactsList_Sorting" OnRowDataBound="AccountContactsList_OnRowDataBound" >
			<EmptyDataRowStyle CssClass="empty" />
			<EmptyDataTemplate>
				<crm:Snippet runat="server" SnippetName="account-contacts/list/empty" DefaultText="There are no items to display." Editable="true" EditType="html" />
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script type="text/javascript">
		$(function () {
			$(".table tr").not(":has(th)").click(function () {
				window.location.href = $(this).find("a").attr("href");
			});
			$("form").submit(function () {
				blockUI();
			});
			$(".table th a").click(function () {
				blockUI();
			});
		});

		function blockUI() {
			$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
		}
	</script>
</asp:Content>


