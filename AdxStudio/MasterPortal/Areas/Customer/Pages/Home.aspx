﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="True" CodeBehind="Home.aspx.cs" Inherits="Site.Areas.Customer.Pages.Home" %>
<%@ OutputCache CacheProfile="User" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
	<div class="row">
		<div class="span8">
			<asp:ObjectDataSource ID="ForumsDataSource" TypeName="Adxstudio.Xrm.Forums.IForumAggregationDataAdapter" OnObjectCreating="CreateForumDataAdapter" SelectMethod="SelectForums" runat="server" />
			<asp:ListView DataSourceID="ForumsDataSource" runat="server">
				<LayoutTemplate>
					<div class="activity activity-grid">
						<div class="header">
							<h2>
								<i class="icon-comment"></i>
								<crm:Snippet SnippetName="Home Forum Activity Heading" DefaultText="Forums" EditType="text" runat="server" />
							</h2>
						</div>
						<ul>
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
						</ul>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<li>
						<div class="row">
							<div class="span4">
								<h4><asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Name") %>' runat="server"/></h4>
								<div><%# Eval("Description") %></div>
							</div>
							<div class="span2"><%# Eval("ThreadCount") %> threads</div>
							<div class="span2"><%# Eval("PostCount") %> posts</div>
						</div>
					</li>
				</ItemTemplate>
			</asp:ListView>
		</div>
		<div class="span4">
			<asp:ListView ID="UpcomingEvents" runat="server">
				<LayoutTemplate>
					<div class="activity">
						<div class="header">
							<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: SiteMarker=Events, Return=Url %>' Text='<%$ Snippet: Home All Events Link Text, All Events %>' runat="server" />
							<h2>
								<i class="icon-calendar"></i>
								<crm:Snippet SnippetName="Home Upcoming Events Heading" DefaultText="Events" EditType="text" runat="server" />
							</h2>
						</div>
						<ul>
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
						</ul>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<li class="vevent">
						<crm:CrmEntityDataSource ID="Event" DataItem='<%# Eval("Event") %>' runat="server" />
						<h4>
							<a class="url summary" href="<%# Eval("Url") %>"><crm:Property DataSourceID="Event" PropertyName="Adx_name" Literal="True" runat="server"/></a>
						</h4>
						<div>
							<abbr class="dtstart" title="<%# Eval("Start", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("Start", "{0:r}") %></abbr>&ndash;<abbr class="dtend" title="<%# Eval("End", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("End", "{0:r}") %></abbr>
						</div>
						<crm:Property DataSourceID="Event" PropertyName="Adx_Summary" EditType="html" runat="server"/>
					</li>
				</ItemTemplate>
			</asp:ListView>
		</div>
	</div>
</asp:Content>
