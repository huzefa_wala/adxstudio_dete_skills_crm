﻿<%@ Page Language="C#" MasterPageFile="../MasterPages/Forums.master" AutoEventWireup="true" CodeBehind="Forums.aspx.cs" Inherits="Site.Areas.Forums.Pages.Forums" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Forums" %>
<%@ Import Namespace="Site.Areas.Forums" %>
<%@ Import Namespace="Site.Helpers" %>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity1" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<h1>
			<crm:Property DataSourceID="CurrentEntity1" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />

	<asp:ObjectDataSource ID="ForumsDataSource" TypeName="Adxstudio.Xrm.Forums.IForumAggregationDataAdapter" OnObjectCreating="CreateForumAggregationDataAdapter" SelectMethod="SelectForums" runat="server" />
	<asp:ListView DataSourceID="ForumsDataSource" runat="server">
		<LayoutTemplate>
			<table class="table forums">
				<thead>
					<tr>
						<th class="name">
							<crm:Snippet SnippetName="Forum Name Heading" DefaultText="Forum" EditType="text" runat="server"/>
						</th>
						<th class="last-post">
							<crm:Snippet SnippetName="Forum Last Post Heading" DefaultText="Last Post" EditType="text" runat="server"/>
						</th>
						<th class="count">
							<crm:Snippet SnippetName="Forum Thread Count Heading" DefaultText="Threads" EditType="text" runat="server"/>
						</th>
						<th class="count">
							<crm:Snippet SnippetName="Forum Post Count Heading" DefaultText="Posts" EditType="text" runat="server"/>
						</th>
					</tr>
				</thead>
				<tbody>
					<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
				</tbody>
			</table>
		</LayoutTemplate>
		<ItemTemplate>
			<tr>
				<td class="name">
					<h3><asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Name") %>' runat="server"/></h3>
					<p><%# Eval("Description") %></p>
				</td>
				<td class="last-post">
					<div class="postedon">
						<abbr class="timeago">
							<%# ForumHelpers.PostedOn(Eval("LatestPost") as IForumPostInfo, "r") %>
						</abbr>
					</div>
					<a class="author-link" href='<%# Url.AuthorUrl(Eval("LatestPost.Author") as IForumAuthor) %>'>
						<%# HttpUtility.HtmlEncode(Eval("LatestPost.Author.DisplayName") ?? "") %>
					</a>
				</td>
				<td class="count"><%# Eval("ThreadCount") %></td>
				<td class="count"><%# Eval("PostCount") %></td>
			</tr>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>
