﻿<%@ Page Language="C#" MasterPageFile="../MasterPages/Forums.master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="Forum.aspx.cs" Inherits="Site.Areas.Forums.Pages.Forum" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm.Forums" %>
<%@ Import Namespace="Site.Areas.Forums" %>
<%@ Import Namespace="Site.Helpers" %>

<asp:Content ContentPlaceHolderID="Breadcrumbs" runat="server">
	<% Html.RenderPartial("ForumBreadcrumbs"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<asp:Panel ID="ForumControls" CssClass="forum-controls pull-right" runat="server">
			<a class="btn btn-primary" href="#new">
				<i class="icon-plus-sign icon-white"></i>
				<crm:Snippet SnippetName="Forum Thread Create Heading" DefaultText="Create a new thread" Literal="True" runat="server"/>
			</a>
		</asp:Panel>
		<h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_name" EditType="text" runat="server" />
			<small>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_description" EditType="text" runat="server" />
			</small>
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<asp:ObjectDataSource ID="ForumAccouncementDataSource" TypeName="Adxstudio.Xrm.Forums.IForumDataAdapter" OnObjectCreating="CreateForumDataAdapter" SelectMethod="SelectAnnouncements" runat="server" />
	<asp:ListView DataSourceID="ForumAccouncementDataSource" runat="server">
		<LayoutTemplate>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
		</LayoutTemplate>
		<ItemTemplate>
			<crm:CrmEntityDataSource ID="Announcement" DataItem='<%# Eval("Entity") %>' runat="server"/>
			<div class="alert alert-block alert-info">
				<h4 class="alert-heading">
					<crm:Property DataSourceID="Announcement" PropertyName="Adx_name" EditType="text" runat="server"/>
				</h4>
				<crm:Property DataSourceID="Announcement" PropertyName="Adx_content" EditType="html" runat="server"/>
			</div>
		</ItemTemplate>
	</asp:ListView>

	<asp:ObjectDataSource ID="ForumThreadDataSource" TypeName="Adxstudio.Xrm.Forums.IForumThreadAggregationDataAdapter" OnObjectCreating="CreateForumDataAdapter" SelectMethod="SelectThreads" SelectCountMethod="SelectThreadCount" EnablePaging="True" runat="server" />
	<asp:ListView ID="ForumThreads" DataSourceID="ForumThreadDataSource" OnDataBound="ForumThreads_DataBound" runat="server">
		<LayoutTemplate>
			<table class="table forums forum-threads">
				<thead>
					<tr>
						<th class="labels"></th>
						<th class="name">
							<crm:Snippet SnippetName="Forum Thread Name Heading" DefaultText="Thread" EditType="text" runat="server"/>
						</th>
						<th class="author">
							<crm:Snippet SnippetName="Forum Thread Author Heading" DefaultText="Author" EditType="text" runat="server"/>
						</th>
						<th class="last-post">
							<crm:Snippet SnippetName="Forum Thread Last Post Heading" DefaultText="Last Post" EditType="text" runat="server"/>
						</th>
						<th class="count">
							<crm:Snippet SnippetName="Forum Thread Reply Count Heading" DefaultText="Replies" EditType="text" runat="server"/>
						</th>
					</tr>
				</thead>
				<tbody>
					<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
				</tbody>
			</table>
			
			<div class="pagination">
				<adx:UnorderedListDataPager ID="ForumThreadsPager" PagedControlID="ForumThreads" QueryStringField="page" PageSize="20" runat="server">
					<Fields>
						<adx:ListItemNextPreviousPagerField ShowNextPageButton="false" ShowFirstPageButton="True" FirstPageText="&laquo;" PreviousPageText="&lsaquo;" />
						<adx:ListItemNumericPagerField ButtonCount="10" PreviousPageText="&hellip;" NextPageText="&hellip;" />
						<adx:ListItemNextPreviousPagerField ShowPreviousPageButton="false" ShowLastPageButton="True" LastPageText="&raquo;" NextPageText="&rsaquo;" />
					</Fields>
				</adx:UnorderedListDataPager>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<tr>
				<td class="labels">
					<asp:Label CssClass="label" Visible='<%# Eval("IsSticky") %>' Text="Sticky" runat="server"/>
					<asp:Label CssClass="icon-ok" Visible='<%# Eval("IsAnswered") %>' ToolTip='<%$ Snippet: Forum Thread Is Answered ToolTip, This thread has been marked as answered %>' runat="server"></asp:Label>
					<asp:Label CssClass="icon-question-sign" Visible='<%# (bool)Eval("ThreadType.RequiresAnswer") && (!(bool)Eval("IsAnswered")) %>' ToolTip='<%$ Snippet: Forum Thread Requires Answer ToolTip, This thread requires an answer %>' runat="server"/>	
				</td>
				<td class="name">
					<h4><asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# HttpUtility.HtmlEncode(Eval("Name") ?? "") %>' runat="server"/></h4>
				</td>
				<td class="author">
					<a class="author-link" href='<%# Url.AuthorUrl(Eval("Author") as IForumAuthor) %>'>
						<%# HttpUtility.HtmlEncode(Eval("Author.DisplayName") ?? "") %>
					</a>
				</td>
				<td class="last-post">
					<div class="postedon">
						<abbr class="timeago">
							<%# ForumHelpers.PostedOn(Eval("LatestPost") as IForumPostInfo, "r") %>
						</abbr>
					</div>
					<a class="author-link" href='<%# Url.AuthorUrl(Eval("LatestPost.Author") as IForumAuthor) %>'>
						<%# HttpUtility.HtmlEncode(Eval("LatestPost.Author.DisplayName") ?? "") %>
					</a>
					<asp:HyperLink NavigateUrl='<%# Eval("LatestPostUrl") %>' Visible='<%# Eval("LatestPost") != null && Eval("LatestPostUrl") != null %>' ToolTip="Last post in thread" runat="server">
						<i class="icon-chevron-right"></i>
					</asp:HyperLink>
				</td>
				<td class="count"><%# Eval("ReplyCount") %></td>
			</tr>
		</ItemTemplate>
	</asp:ListView>
	
	<asp:Panel ID="ForumThreadCreateForm" CssClass="form-horizontal form-forum-thread html-editors" ViewStateMode="Enabled" runat="server">
		<a name="new"></a>
		<fieldset>
			<legend>
				<crm:Snippet SnippetName="Forum Thread Create Heading" DefaultText="Create a new thread" EditType="text" runat="server"/>
			</legend>
			<crm:Snippet SnippetName="Forum Thread Create Instructions" EditType="html" runat="server"/>
			<asp:ValidationSummary CssClass="alert alert-error alert-block" ValidationGroup="NewThread" runat="server" />
			<div class="control-group">
				<asp:Label AssociatedControlID="NewForumThreadName" CssClass="control-label" runat="server">* Thread Title</asp:Label>
				<div class="controls">
					<asp:TextBox ID="NewForumThreadName" ValidationGroup="NewThread" MaxLength="95" runat="server"/>
					<asp:RequiredFieldValidator CssClass="validator" ValidationGroup="NewThread" ControlToValidate="NewForumThreadName" ErrorMessage="Name is a required field." Text="*" runat="server"/>
				</div>
			</div>
			<div class="control-group">
				<asp:Label AssociatedControlID="NewForumThreadType" CssClass="control-label" runat="server">* Thread Type</asp:Label>
				<div class="controls">
					<asp:ObjectDataSource ID="ForumThreadTypeDataSource" TypeName="Adxstudio.Xrm.Forums.IForumDataAdapter" OnObjectCreating="CreateForumDataAdapter" SelectMethod="SelectThreadTypeListItems" runat="server" />
					<asp:DropDownList DataSourceID="ForumThreadTypeDataSource" ValidationGroup="NewThread" ID="NewForumThreadType" DataTextField="Text" DataValueField="Value" runat="server"/>
					<asp:RequiredFieldValidator CssClass="validator" ValidationGroup="NewThread" ControlToValidate="NewForumThreadType" ErrorMessage="Content is a required field." Text="*" runat="server"/>
				</div>
			</div>
			<div class="control-group">
				<asp:Label AssociatedControlID="NewForumThreadContent" CssClass="control-label" runat="server">* Content</asp:Label>
				<div class="controls">
					<asp:TextBox ID="NewForumThreadContent" ValidationGroup="NewThread" TextMode="MultiLine" runat="server"/>
					<asp:RequiredFieldValidator CssClass="validator" ValidationGroup="NewThread" ControlToValidate="NewForumThreadContent" ErrorMessage="Content is a required field." Text="*" EnableClientScript="False" runat="server"/>
				</div>
			</div>
			<div class="control-group">
				<asp:Label AssociatedControlID="NewForumThreadAttachment" CssClass="control-label" runat="server">Attach a file</asp:Label>
				<div class="controls">
					<asp:FileUpload ID="NewForumThreadAttachment" ValidationGroup="NewThread" runat="server"/>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<label class="checkbox">
						<asp:CheckBox ID="NewForumThreadSubscribe" Checked="True" runat="server"/>
						<crm:Snippet SnippetName="Forum Thread Create Subscribe Label" DefaultText="Subscribe to this thread" EditType="text" runat="server"/>
					</label>
				</div>
			</div>
			<div class="form-actions">
				<asp:Button CssClass="btn btn-primary" OnClick="CreateThread_Click" Text='<%$ Snippet: Forum Thread Create Button Text, Create this thread %>' ValidationGroup="NewThread" runat="server"/>
			</div>
		</fieldset>
	</asp:Panel>
	
	<crm:Snippet ID="AnonymousMessage" SnippetName="Forum Post Anonymous Message" EditType="html" runat="server"/>
	
	<script type="text/javascript">
		$(function () {
			$('input[type="submit"]').click(function () {
				$.blockUI({ message: null, overlayCSS: { opacity: .3} });
			});
		});
	</script>

</asp:Content>
