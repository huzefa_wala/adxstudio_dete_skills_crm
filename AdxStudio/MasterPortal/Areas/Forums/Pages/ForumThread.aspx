﻿<%@ Page Language="C#" MasterPageFile="../MasterPages/Forums.master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="ForumThread.aspx.cs" Inherits="Site.Areas.Forums.Pages.ForumThread" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm.Forums" %>
<%@ Import Namespace="Site.Areas.Forums" %>
<%@ Import Namespace="Site.Helpers" %>

<asp:Content ContentPlaceHolderID="Breadcrumbs" runat="server">
	<% Html.RenderPartial("ForumThreadBreadcrumbs"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<asp:Panel ID="ForumControls" CssClass="forum-controls pull-right" runat="server">
			<a class="btn btn-primary" href="#new">
				<i class="icon-plus-sign icon-white"></i>
				<crm:Snippet SnippetName="Forum Post Create Heading" DefaultText="Post a reply" Literal="True" runat="server"/>
			</a>
			<asp:LinkButton ID="AddAlert" OnClick="AddAlert_Click" CssClass="btn" Visible="False" runat="server">
				<i class="icon-eye-open"></i>
				<crm:Snippet SnippetName="Forum Thread Alert Create Heading" DefaultText="Subscribe" Literal="True" runat="server"/>
			</asp:LinkButton>
			<asp:LinkButton ID="RemoveAlert" OnClick="RemoveAlert_Click" CssClass="btn btn-danger" Visible="False" runat="server">
				<i class="icon-eye-close icon-white"></i>
				<crm:Snippet SnippetName="Forum Thread Alert Delete Heading" DefaultText="Unsubscribe" Literal="True" runat="server"/>
			</asp:LinkButton>
		</asp:Panel>
		<h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_name" EditType="text" HtmlEncode="True" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<asp:ObjectDataSource ID="ForumPostDataSource" TypeName="Adxstudio.Xrm.Forums.IForumPostAggregationDataAdapter" OnObjectCreating="CreateForumThreadDataAdapter" SelectMethod="SelectPosts" SelectCountMethod="SelectPostCount" EnablePaging="True" runat="server" />
	<asp:ListView ID="ForumPosts" DataSourceID="ForumPostDataSource" OnDataBound="ForumPosts_DataBound" runat="server">
		<LayoutTemplate>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
			<div class="pagination">
				<adx:UnorderedListDataPager ID="ForumPostsPager" PagedControlID="ForumPosts" QueryStringField="page" PageSize="20" runat="server">
					<Fields>
						<adx:ListItemNextPreviousPagerField ShowNextPageButton="false" ShowFirstPageButton="True" FirstPageText="&laquo;" PreviousPageText="&lsaquo;" />
						<adx:ListItemNumericPagerField ButtonCount="10" PreviousPageText="&hellip;" NextPageText="&hellip;" />
						<adx:ListItemNextPreviousPagerField ShowPreviousPageButton="false" ShowLastPageButton="True" LastPageText="&raquo;" NextPageText="&rsaquo;" />
					</Fields>
				</adx:UnorderedListDataPager>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<div class="forum-post">
				<a id="<%# Eval("EntityReference.Id", "{0}") %>" name="<%# Eval("EntityReference.Id", "{0}") %>"></a>
				<asp:PlaceHolder Visible='<%# (bool)Eval("CanEdit") %>' ViewStateMode="Enabled" runat="server">
					<div class="modal hide" id="<%# Eval("EntityReference.Id", "edit-{0}") %>" tabindex="-1" role="dialog" aria-labelledby="<%# Eval("EntityReference.Id", "edit-label-{0}") %>" aria-hidden="true">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h3 id="<%# Eval("EntityReference.Id", "edit-label-{0}") %>">
								<crm:Snippet runat="server" SnippetName="Forums/EditPost/ButtonText" DefaultText="Update Post" Literal="True"/>
							</h3>
						</div>
						<div class="modal-body html-editors">
							<asp:TextBox ID="ForumPostContentUpdate" TextMode="MultiLine" Text='<%# Eval("Content") %>' runat="server"/>
						</div>
						<div class="modal-footer">
							<asp:LinkButton CssClass="btn btn-primary" Text='<%$ Snippet: Forums/EditPost/ButtonText, Update Post %>' CommandArgument='<%# Eval("EntityReference.Id") %>' OnCommand="UpdatePost_OnCommand" runat="server"/>
							<button class="btn" data-dismiss="modal" aria-hidden="true">
								<crm:Snippet runat="server" SnippetName="Forums/EditPost/CancelButtonText" DefaultText="Cancel" Literal="true" EditType="text"/>
							</button>
						</div>
					</div>
				</asp:PlaceHolder>
				<div class="row">
					<div class="span3 metadata">
						<asp:Panel Visible='<%# Eval("IsAnswer") %>' CssClass="alert alert-success" runat="server"><i class="icon-ok"></i> Answer</asp:Panel>
						<div class="postedon">
							<abbr class="timeago">
								<%# ForumHelpers.PostedOn(Container.DataItem as IForumPostInfo, "r") %>
							</abbr>
						</div>
						<a class="author-link" href='<%# Url.AuthorUrl(Eval("Author") as IForumAuthor) %>'>
							<%# HttpUtility.HtmlEncode(Eval("Author.DisplayName") ?? "") %>
						</a>
						<asp:Image CssClass="author-img" ImageUrl='<%# Url.UserImageUrl(Eval("Author") as IForumAuthor) %>' AlternateText='<%# Eval("Author.DisplayName") %>' runat="server"/>
					</div>
					<div class="span9">
						<div class="btn-toolbar">
							<asp:Panel class="btn-group" Visible='<%# Eval("CanMarkAsAnswer") %>' runat="server">
								<asp:LinkButton CssClass="btn btn-success" Visible='<%# !(bool)Eval("IsAnswer") %>' CommandArgument='<%# Eval("EntityReference.Id") %>' OnCommand="MarkAsAnswer_OnCommand" runat="server">
									<i class="icon-ok icon-white"></i>
									<crm:Snippet SnippetName="Forum Post Mark Answer Button Text" Literal="True" DefaultText="Mark as answer" runat="server"/>
								</asp:LinkButton>
								<asp:LinkButton CssClass="btn" Visible='<%# (bool)Eval("IsAnswer") %>' CommandArgument='<%# Eval("EntityReference.Id") %>' OnCommand="UnmarkAsAnswer_OnCommand" runat="server">
									<i class="icon-remove"></i>
									<crm:Snippet SnippetName="Forum Post Unmark Answer Button Text" Literal="True" DefaultText="Unmark as answer" runat="server"/>
								</asp:LinkButton>
							</asp:Panel>
							<asp:Panel CssClass="btn-group" Visible='<%# (bool)Eval("CanEdit") %>'  runat="server" >
								<a class="btn" href="<%# Eval("EntityReference.Id", "#edit-{0}") %>" role="modal" data-toggle="modal"><i class="icon-edit"></i></a>
							</asp:Panel>
							<asp:Panel CssClass="xrm-entity xrm-editable-adx_communityforumpost btn-group" Visible='<%# Eval("Editable") %>' runat="server">
								<a class="btn xrm-edit"><i class="icon-cog"></i></a>
								<a class="btn dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="#" class="xrm-edit"><i class="icon-edit"></i> Edit</a>
									</li>
									<li>
										<a href="#" class="xrm-delete"><i class="icon-remove"></i> Delete</a>
									</li>
								</ul>
								<asp:HyperLink NavigateUrl='<%# Eval("EditPath.AbsolutePath") %>' CssClass="xrm-entity-ref" style="display:none;" runat="server"/>
								<asp:HyperLink NavigateUrl='<%# Eval("DeletePath.AbsolutePath") %>' CssClass="xrm-entity-delete-ref" style="display:none;" runat="server"/>
							</asp:Panel>
						</div>
						<div>
							<%# Eval("Content") %>
						</div>
						<asp:ListView DataSource='<%# Eval("AttachmentInfo") %>' runat="server">
							<LayoutTemplate>
								<div class="attachments alert alert-block alert-info">
									<ul>
										<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
									</ul>
								</div>
							</LayoutTemplate>
							<ItemTemplate>
								<li runat="server" Visible='<%# Eval("Path") != null %>'>
									<i class="icon-file"></i>
									<asp:HyperLink NavigateUrl='<%# Eval("Path.AbsolutePath") %>' Text='<%# HttpUtility.HtmlEncode(string.Format("{0} ({1:1})", Eval("Name"), Eval("Size"))) %>' runat="server"/>
								</li>
							</ItemTemplate>
						</asp:ListView>
					</div>
				</div>
			</div>
		</ItemTemplate>
	</asp:ListView>
	
	<asp:Panel ID="ForumPostCreateForm" CssClass="form-horizontal form-forum-thread html-editors" ViewStateMode="Enabled" runat="server">
		<a name="new"></a>
		<fieldset>
			<legend>
				<crm:Snippet SnippetName="Forum Post Create Heading" DefaultText="Post a reply" EditType="text" runat="server"/>
			</legend>
			<crm:Snippet SnippetName="Forum Post Create Instructions" EditType="html" runat="server"/>
			<asp:ValidationSummary CssClass="alert alert-error alert-block" ValidationGroup="NewPost" runat="server" />
			<div class="control-group">
				<asp:Label AssociatedControlID="NewForumPostContent" CssClass="control-label" runat="server">* Content</asp:Label>
				<div class="controls">
					<asp:TextBox ID="NewForumPostContent" TextMode="MultiLine" runat="server"/>
					<asp:RequiredFieldValidator CssClass="validator" ControlToValidate="NewForumPostContent" EnableClientScript="False" ValidationGroup="NewPost" ErrorMessage="Content is a required field." Text="*" runat="server"/>
				</div>
			</div>
			<div class="control-group">
				<asp:Label AssociatedControlID="NewForumPostAttachment" CssClass="control-label" runat="server">Attach a file</asp:Label>
				<div class="controls">
					<asp:FileUpload ID="NewForumPostAttachment" ValidationGroup="NewPost" runat="server"/>
				</div>
			</div>
			<div class="form-actions">
				<asp:Button ValidationGroup="NewPost" OnClick="CreatePost_Click" CssClass="btn btn-primary" Text="Post this reply" runat="server"/>
			</div>
		</fieldset>
	</asp:Panel>
	
	<crm:Snippet ID="AnonymousMessage" SnippetName="Forum Post Anonymous Message" EditType="html" runat="server"/>
	
	<script type="text/javascript">
		$(function() {
			$('input[type="submit"]').click(function() {
				$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
			});
		});
	</script>

</asp:Content>
