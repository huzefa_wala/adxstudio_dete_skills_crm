<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="True" CodeBehind="Home.aspx.cs" Inherits="Site.Areas.Government.Pages.Home" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Blogs" %>

<asp:Content ID="Head" ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Service311/css/311.css") %>" />
</asp:Content>
<asp:Content ID="Scripts" ContentPlaceHolderID="Scripts" runat="server">
	<script src="<%: Url.Content("~/Areas/Service311/js/service311.js") %>"></script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentPage" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Property DataSourceID="CurrentPage" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" ViewStateMode="Enabled" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="container">
		<div class="row">
			<div class="span8">
				<asp:Panel runat="server" ID="NewsPanel" Visible="False">
					<asp:ObjectDataSource ID="PostDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogDataAdapter" SelectMethod="SelectPosts" runat="server">
						<SelectParameters>
							<asp:Parameter Name="startRowIndex" DefaultValue="0"/>
							<asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Home News Post Count, 4 %>'/>
						</SelectParameters>
					</asp:ObjectDataSource>
					<asp:ListView ID="Posts" DataSourceID="PostDataSource" runat="server">
						<LayoutTemplate>
							<div class="activity activity-grid">
								<asp:ObjectDataSource ID="BlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogDataAdapter" SelectMethod="Select" runat="server" />
								<div class="header">
									<asp:Repeater DataSourceID="BlogDataSource" runat="server">
										<ItemTemplate>
											<asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AbsolutePath") %>' Text='<%$ Snippet: Home All News Link Text, All News %>' runat="server" />
											<h2>
												<asp:HyperLink NavigateUrl='<%# Eval("FeedPath.AbsolutePath") %>' ImageUrl="~/img/feed-icon-14x14.png" ToolTip='<%$ Snippet: Home News Feed Subscribe Tooltip Label, Subscribe %>' runat="server" />
												<%# Eval("Title") %>
											</h2>
										</ItemTemplate>
									</asp:Repeater>
								</div>
								<ul>
									<li id="itemPlaceholder" runat="server" />
								</ul>
							</div>
						</LayoutTemplate>
						<ItemTemplate>
							<li runat="server">
								<h4>
									<asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
								</h4>
								<div>
									<%# Eval("Entity.Adx_date", "{0:f}") %>
									<asp:Panel runat="server" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) != BlogCommentPolicy.None) %>'>
										&ndash;
										<asp:HyperLink NavigateUrl='<%# string.Format("{0}#comments", Eval("ApplicationPath.AbsolutePath")) %>' runat="server">
											<i class="icon-comment"></i> <%# Eval("CommentCount") %>
										</asp:HyperLink>
									</asp:Panel>
								</div>
								<div>
									<%# Eval("Summary") %>
								</div>
							</li>
						</ItemTemplate>
					</asp:ListView>
				</asp:Panel>
			</div>
			<div class="span4">
				<asp:Panel ID="TwitterFeedPanel" runat="server">
					<crm:Snippet SnippetName="Home Twitter Widget" EditType="text" Literal="True" DefaultText="Home Twitter Widget" runat="server"/>
				</asp:Panel>
			</div>
		</div>
	</div>
</asp:Content>
