﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Blogs;
using Microsoft.Xrm.Portal;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Site.Pages;
using PortalContextDataAdapterDependencies = Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies;

namespace Site.Areas.Government.Pages
{
	public partial class Home : PortalPage
	{
		private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext());
		protected Entity NewsBlog
		{
			get
			{
				var newsBlogName = _portal.Value.ServiceContext.GetSiteSettingValueByName(_portal.Value.Website, "News Blog Name");

				if (string.IsNullOrWhiteSpace(newsBlogName))
				{
					return null;
				}

				var newsBlog = _portal.Value.ServiceContext.CreateQuery("adx_blog").FirstOrDefault(b => b.GetAttributeValue<Guid>("adx_websiteid") == _portal.Value.Website.Id && b.GetAttributeValue<string>("adx_name") == newsBlogName);

				return newsBlog;
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			NewsPanel.Visible = (NewsBlog != null);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		
		}

		protected void CreateBlogDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			args.ObjectInstance = new BlogDataAdapter(NewsBlog, new PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
		}
	}
}
