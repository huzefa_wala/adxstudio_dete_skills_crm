﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Adxstudio.Xrm.Issues.IIssue>" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Site.Areas.Issues" %>

<small> <%: Html.SnippetLiteral("Issue Status Label", "Status:")%> </small><span class="label
<%= Model.Status == (int)IssueStatus.Confirmed ? " label-warning" : string.Empty %>
<%= Model.Status == (int)IssueStatus.WorkaroundAvailable ? " label-info" : string.Empty %>
<%= Model.Status == (int)IssueStatus.Resolved ? " label-success" : string.Empty %>
<%= Model.Status == (int)IssueStatus.WillNotFix ? " label-inverse" : string.Empty %>
<%= Model.Status == (int)IssueStatus.ByDesign ? " label-inverse" : string.Empty %>
<%= Model.Status == (int)IssueStatus.UnableToReproduce ? " label-important" : string.Empty %>">
<%: Regex.Replace(Enum.GetName(typeof(IssueStatus), Model.Status), "([a-z])([A-Z])", "$1 $2")%>
</span>
