﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Site.Areas.Issues.ViewModels.IssueViewModel>" %>
<%@ Import Namespace="Site.Helpers" %>

<% if (!Model.CurrentUserHasAlert) { %>
	<%= Ajax.RawActionLink(@"<i class=""icon-eye-open""></i> " + Html.SnippetLiteral("Issue Alert Create Label", "Track"),
		"AlertCreate", "Issue", new { id = Model.Issue.Id },
		new AjaxOptions { HttpMethod = "POST", UpdateTargetId = "issue-tracking" },
		new { @class = "btn" }) %>
<% } else { %>
	<%= Ajax.RawActionLink(@"<i class=""icon-eye-close icon-white""></i> " + Html.SnippetLiteral("Issue Alert Remove Label", "Stop Tracking"),
		"AlertRemove", "Issue", new { id = Model.Issue.Id },
		new AjaxOptions { HttpMethod = "POST", UpdateTargetId = "issue-tracking" },
		new { @class = "btn btn-danger" }) %>			
<% } %>
<script type="text/javascript">
	$(function () {
		$("div#issue-tracking > a").click(function () {
			$(this).block({ message: null, overlayCSS: { opacity: .3} });
		});
	});
</script>