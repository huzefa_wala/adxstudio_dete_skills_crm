﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Adxstudio.Xrm.Issues.IIssueForum>" %>

<% using (Ajax.BeginForm("Create", "Issue", new { id = Model.Id }, new AjaxOptions { UpdateTargetId = "create-issue", OnComplete = "issueCreated"}, new{@class = "form-horizontal html-editors"})) { %>
	<fieldset>
		<legend><%: Html.SnippetLiteral("Issue Add New Label", "Add a New Issue")%></legend>
		<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"})%>
		<% if (!Request.IsAuthenticated) { %>
			<div class="control-group">
				<label class="control-label" for="authorName">* Your Name</label>
				<div class="controls">
					<%= Html.TextBox("authorName", string.Empty)%>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="authorEmail">* E-mail</label>
				<div class="controls">
					<%= Html.TextBox("authorEmail", string.Empty)%>
				</div>
			</div>
		<% } %>
		<div class="control-group">
			<label class="control-label" for="title">* Issue</label>
			<div class="controls">
				<%= Html.TextBox("title", string.Empty, new { @class = "span6" })%>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="copy">Description</label>
			<div class="controls">
				<%= Html.TextArea("copy", string.Empty, new { @class = "span6" })%>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="track">Track</label>
			<div class="controls">
				<label class="checkbox"><%= Html.CheckBox("track", true)%>Be notified when someone updates or comments on this issue.</label>
			</div>
		</div>
		<div class="form-actions">
			<input id="submit-issue" class="btn btn-primary" type="submit" value="<%: Html.SnippetLiteral("Issue Submit Label", "Submit Issue")%>" />
		</div>
	</fieldset>
<% } %>
<script type="text/javascript">
	$(function() {
		$("#submit-issue").click(function() {
			$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
		});
	});

	function issueCreated() {
		if ($("#create-issue .validation-summary-errors").length) {
			portal.initializeHtmlEditors();
			prettyPrint();
			$.unblockUI();
			return;
		}
		window.location.href = '<%= Url.RouteUrl("IssuesFilter", new { issueForumPartialUrl = Model.PartialUrl, filter = "open", status = "new-or-unconfirmed" }) %>';
	}
</script>