﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Site.Areas.Issues.ViewModels.IssueCommentsViewModel>" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Site.Helpers" %>

<% if (Model.Issue.CommentCount > 0) { %>
	<legend><%: Html.SnippetLiteral("Issue Comments Label", "Comments")%> (<%: Model.Issue.CommentCount %>)</legend>
	<ul class="unstyled">
		<% foreach (var comment in Model.Comments) { %>
			<li>
				<h4>
					<i class="icon-comment"></i>
					<% if (comment.Author.EntityReference != null) { %>
						<a href="<%= Url.AuthorUrl(comment) %>"><%= comment.Author.DisplayName%></a>
					<% } else { %>
						<%: comment.Author.DisplayName%>
					<% } %>
					<small>&ndash; <abbr class="timeago"><%: comment.Date.ToString("r") %></abbr></small>
				</h4>
				<%= comment.Content %>
			</li>
		<% } %>
	</ul>
	<% Html.RenderPartial("Pagination", Model.Comments); %>
<% } 
if (Model.Issue.CurrentUserCanComment) { %>
	<% using (Ajax.BeginForm("CommentCreate", "Issue", new { id = Model.Issue.Id }, new AjaxOptions { UpdateTargetId = "comments", OnComplete = "commentCreated"}, new { id="create-comment", @class = "form-horizontal html-editors" } )) { %>
		<fieldset>
			<legend><%: Html.SnippetLiteral("Add Comment Label", "Add a Comment")%></legend>
			<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"})%>
			<% if (!Request.IsAuthenticated) { %>
				<div class="control-group">
					<label class="control-label" for="authorName">* Your Name</label>
					<div class="controls">
						<%= Html.TextBox("authorName", string.Empty)%>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="authorEmail">* E-mail</label>
					<div class="controls">
						<%= Html.TextBox("authorEmail", string.Empty)%>
					</div>
				</div>
			<% } %>
			<div class="control-group">
				<label class="control-label" for="title">* Comment</label>
				<div class="controls">
					<%= Html.TextArea("copy", string.Empty, new { @class = "span6" })%>
				</div>
			</div>
			<div class="form-actions">
				<input id="post-comment" class="btn btn-primary" type="submit" value="<%: Html.SnippetLiteral("Post Comment Label", "Post Comment")%>" />
			</div>
		</fieldset>
	<% }
} %>
<script type="text/javascript">
	$(function () {
		$("#post-comment").click(function () {
			$.blockUI({ message: null, overlayCSS: { opacity: .3} });
		});
	});

	function commentCreated() {
		portal.convertAbbrDateTimesToTimeAgo($);

		if ($("#create-comment .validation-summary-errors").length == 0) {
			$("#create-comment :input").each(function () {
				if (this.type == "text" || this.tagName.toLowerCase() == "textarea") {
					this.value = "";
				}
			});
		}

		prettyPrint();

		portal.initializeHtmlEditors();

		$.unblockUI();
	}
</script>
