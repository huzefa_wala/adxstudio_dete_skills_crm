﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Site.Areas.Products.ViewModels.ProductImageGalleryViewModel>" %>
<% if (Model.ImageGalleryNodes.Count > 0) { %>
	<div id="gallery" class="flexslider">
		<ul class="slides thumbnails">
			<% foreach (var node in Model.ImageGalleryNodes) { %>
				<li>
					<a href="<%= node.ImageURL %>" class="thumbnail">
						<img src="<%= node.ThumbnailImageURL %>" alt="<%= node.Title %>" />
					</a>
				</li>
			<% } %>
		</ul>
	</div>
<% } %>
<script type="text/javascript">
	$(document).ready(function () {
		$('#gallery').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 60, // itemWidth is the primary property for the new carousel options. Without this property, your slider is not considered a carousel. To use itemWidth, give an integer value of the width of your individual slides. This should include borders and paddings applied to your slides; a total width measurement.
			itemMargin: 10 // itemMargin describes the gutter between the slide elements. If each slide has a margin-left of 10px, your itemMargin value would be 10. If elements have margin: 0 10px, your itemMargin would be 20.
		});
	});
</script>