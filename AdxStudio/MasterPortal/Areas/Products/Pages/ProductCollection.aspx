﻿<%@ Page Language="C#" MasterPageFile="../MasterPages/Products.master" AutoEventWireup="true" CodeBehind="ProductCollection.aspx.cs" Inherits="Site.Areas.Products.Pages.ProductCollection" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/css/rateit.css") %>" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Scripts">
	<script src="<%: Url.Content("~/js/jquery.rateit.min.js") %>"></script>
</asp:Content>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			<asp:ObjectDataSource ID="BrandDataSource" TypeName="Adxstudio.Xrm.Products.IBrandDataAdapter" OnObjectCreating="CreateBrandDataAdapter" SelectMethod="SelectBrand" runat="server">
				<SelectParameters>
					<asp:QueryStringParameter Name="id" QueryStringField="brand"/>
				</SelectParameters>
			</asp:ObjectDataSource>
			<asp:ListView DataSourceID="BrandDataSource" runat="server">
				<LayoutTemplate>
					<small>
						<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
					</small>
				</LayoutTemplate>
				<ItemTemplate>
					<%# Eval("Name") %>
				</ItemTemplate>
			</asp:ListView>
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<ul class="nav nav-tabs">
		<li class="dropdown active">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-list"></i> <%= CurrentSortOptionLabel %> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<% foreach (var option in SortOptions) { %>
					<li>
						<a href="<%: GetSortUrl(option.Key) %>"><%= option.Value %></a>
					</li>
				<% } %>
			</ul>
		</li>
	</ul>
	<asp:ObjectDataSource ID="ProductsDataSource" TypeName="Adxstudio.Xrm.Products.IFilterableProductAggregationDataAdapter" OnObjectCreating="CreateSubjectProductsDataAdapter" SelectMethod="SelectProducts" SelectCountMethod="SelectProductCount" EnablePaging="True" runat="server">
		<SelectParameters>
			<asp:QueryStringParameter Name="brand" QueryStringField="brand"/>
			<asp:QueryStringParameter Name="rating" QueryStringField="rating"/>
			<asp:QueryStringParameter Name="sortExpression" QueryStringField="orderby"/>
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ListView ID="Products" DataSourceID="ProductsDataSource" runat="server">
		<LayoutTemplate>
			<ol class="nav thumbnails product-grid">
				<li id="itemPlaceholder" runat="server" />
			</ol>
			<div class="pagination">
				<adx:UnorderedListDataPager PageSize="20" PagedControlID="Products" QueryStringField="page" runat="server">
					<Fields>
						<adx:ListItemNextPreviousPagerField ShowNextPageButton="false" ShowFirstPageButton="True" FirstPageText="&laquo;" PreviousPageText="&lsaquo;" />
						<adx:ListItemNumericPagerField ButtonCount="10" PreviousPageText="&hellip;" NextPageText="&hellip;" />
						<adx:ListItemNextPreviousPagerField ShowPreviousPageButton="false" ShowLastPageButton="True" LastPageText="&raquo;" NextPageText="&rsaquo;" />
					</Fields>
				</adx:UnorderedListDataPager>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<li>
				<a class="thumbnail" href="<%# Url.Action("Product", "Products", new { productIdentifier = Eval("PartialURL") ?? Eval("SKU"), area = "Products" }) %>"> <%--href='<%# string.Format("~/products/product/{0}/", Eval("PartialURL") ?? Eval("SKU")) %>'>--%>
					<img src="<%# string.IsNullOrWhiteSpace(Eval("ImageThumbnailURL").ToString()) ? "/image-not-available-150x150.png/" : Eval("ImageThumbnailURL") %>" alt="<%# Eval("Name") %>" />
					<div class="caption text-center">
						<div>
							<%# Eval("Name") %>
						</div>
						<div>
							<strong><%# Eval("ListPrice", "{0:C2}") %></strong>
						</div>
						<div>
							<div data-rateit-readonly="true" data-rateit-ispreset="true" data-rateit-value="<%# Eval("RatingInfo.Average") %>" class="rateit"></div>
						</div>
					</div>
				</a>
			</li>
		</ItemTemplate>
		<EmptyDataTemplate>
			<crm:Snippet SnippetName="No Products Message" DefaultText="No products match your current filter." EditType="html" runat="server"/>
		</EmptyDataTemplate>
	</asp:ListView>
</asp:Content>

<asp:Content ContentPlaceHolderID="Filters" runat="server">
	<asp:ObjectDataSource ID="BrandsDataSource" TypeName="Adxstudio.Xrm.Products.IBrandDataAdapter" OnObjectCreating="CreateBrandDataAdapter" SelectMethod="SelectBrands" runat="server" />
	<asp:ListView ID="Brands" DataSourceID="BrandsDataSource" runat="server">
		<LayoutTemplate>
			<li class="nav-header">
				<crm:Snippet SnippetName="Brand Filter Header" DefaultText="Brands" runat="server"/>
			</li>
			<asp:PlaceHolder runat="server">
				<li class="<%: NoBrandFilter ? "active" : string.Empty %>">
					<a href="<%: AllBrandsUrl %>"><%: Html.SnippetLiteral("All Brands Brand Filter", "All Brands") %></a>
				</li>
			</asp:PlaceHolder>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
		</LayoutTemplate>
		<ItemTemplate>
			<li class="<%# IsActiveBrandFilter(Eval("Id")) ? "active" : string.Empty %>">
				<asp:HyperLink NavigateUrl='<%# GetBrandFilterUrl(Eval("Id")) %>' Text='<%# Eval("Name") %>' runat="server"/>
			</li>
		</ItemTemplate>
	</asp:ListView>
	<li class="nav-header">
		<crm:Snippet SnippetName="Rating Filter Header" DefaultText="Rating" runat="server"/>
	</li>
	<li class="<%: NoRatingFilter ? "active" : string.Empty %>">
		<a href="<%: AnyRatingUrl %>"><%: Html.SnippetLiteral("All Ratings Filter", "Any Rating") %></a>
	</li>
	<% foreach (var rating in RatingFilterOptions) { %>
		<li class="<%: IsActiveRatingFilter(rating.Key) ? "active" : string.Empty %>">
			<a href="<%: GetRatingFilterUrl(rating.Key) %>">
				<div data-rateit-readonly="true" data-rateit-ispreset="true" data-rateit-value="<%: rating.Key %>" class="rateit"></div>
				<strong><%= rating.Value %></strong>
			</a>
		</li>
	<% } %>
</asp:Content>
