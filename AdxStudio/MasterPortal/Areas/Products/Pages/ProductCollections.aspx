﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="ProductCollections.aspx.cs" Inherits="Site.Areas.Products.Pages.ProductCollections" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Microsoft.Xrm.Sdk" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Products/css/products.css") %>">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="collection-grid">
		<asp:ListView ID="ChildView" runat="server">
			<LayoutTemplate>
				<ul class="thumbnails">
					<li id="itemPlaceholder" runat="server" />
				</ul>
			</LayoutTemplate>
			<ItemTemplate>
				<li>
					<a class="thumbnail" href='<%# Eval("Url") %>'>
						<asp:Literal runat="server" Text='<%# BuildImageTag(Eval("Entity") as Entity) %>'></asp:Literal>
						<div class="caption text-center">
							<%# Eval("Title") %>
						</div>
					</a>
				</li>
			</ItemTemplate>
		</asp:ListView>
	</div>
</asp:Content>
