﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="ManageCustomerAccounts.aspx.cs" Inherits="Site.Areas.CustomerManagement.Pages.ManageCustomerAccounts" %>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<adx:SiteMarkerLinkButton ID="CreateButton" runat="server" SiteMarkerName="Create Customer Account" CssClass="btn btn-success pull-right"  >
			<i class="icon-white icon-plus-sign"></i> Create New
			</adx:SiteMarkerLinkButton>
		<h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content  ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<crm:Snippet ID="AccountLabel" runat="server" SnippetName="Channel Accounts Text" DefaultText="<p>The following Customer Accounts are associated with this account:</p>" Editable="true" EditType="html" />
	<div class="clearfix"></div>
	<asp:Label runat="server" ID="InsufficientPermissions" Visible="false" />
	<div id="customer-accounts-list">
		<asp:GridView ID="CustomerAccountsList" runat="server" CssClass="table table-striped" GridLines="None" AlternatingRowStyle-CssClass="alternate-row" AllowSorting="true" OnSorting="CustomerAccountsList_Sorting" OnRowDataBound="CustomerAccountsList_OnRowDataBound" >
			<EmptyDataRowStyle CssClass="empty" />
			<EmptyDataTemplate>
				<crm:Snippet runat="server" SnippetName="customer-accounts/list/empty" DefaultText="There are no items to display." Editable="true" EditType="html" />
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script type="text/javascript">
				$(function () {
					$(".table tr").not(":has(th)").click(function () {
						window.location.href = $(this).find("a").attr("href");
					});
					$("form").submit(function () {
						blockUI();
					});
					$(".table th a").click(function () {
						blockUI();
					});
				});

				function blockUI() {
					$.blockUI({ message: null, overlayCSS: { opacity: .3} });
				}
			</script>
	</asp:Content>
