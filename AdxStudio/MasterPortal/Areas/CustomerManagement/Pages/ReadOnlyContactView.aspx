﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="ReadOnlyContactView.aspx.cs" Inherits="Site.Areas.CustomerManagement.Pages.ReadOnlyContactView" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content  ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="row">
		<div id="instructions-section" class="span3 pull-right well">
			<h4>
				<asp:Literal  Text="<%$ Snippet: contact/read-only, Contact Details %>" runat="server" />
			</h4>
			<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
		<div id="manage-account-section" class="span8">
			<div id="profile">
				<asp:Panel CssClass="alert alert-info" ID="NoAccountAccessLabelPanel" runat="server">
					<crm:Snippet runat="server" SnippetName="account-profile/no_access" DefaultText="You do not have an associated parent account, or do not have account access." Editable="true" EditType="html" />
				</asp:Panel>
				<asp:Panel ID="ContactForm" CssClass="crmEntityFormView" runat="server" Visible="true">
					<adx:CrmEntityFormView runat="server" 
						ID="FormView" EntityName="contact" 
						FormName="Opportunity Contact Details Form" 
						ValidationGroup="Profile" 
						RecommendedFieldsRequired="True" 
						ShowUnsupportedFields="False" 
						ToolTipEnabled="False" 
						Mode="ReadOnly">
					</adx:CrmEntityFormView>
				</asp:Panel>
			</div>
		</div>
	</div>
</asp:Content>
