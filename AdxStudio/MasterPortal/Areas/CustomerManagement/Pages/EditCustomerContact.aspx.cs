﻿using System;
using System.Linq;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;
using Xrm;

namespace Site.Areas.CustomerManagement.Pages
{
	public partial class EditCustomerContact : PortalPage
	{
		private Contact _contact;

		public Contact ContactToEdit
		{
			get
			{
				if (_contact != null)
				{
					return _contact;
				}

				Guid contactId;

				if (!Guid.TryParse(Request["ContactID"], out contactId))
				{
					return null;
				}

				_contact = XrmContext.ContactSet.FirstOrDefault(c => c.Id == contactId);

				return _contact;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			var formViewDataSource = new CrmDataSource { ID = "WebFormDataSource" };

			var fetchXml = string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>", Contact.EntityLogicalName, "contactid", ContactToEdit.ContactId);

			formViewDataSource.FetchXml = fetchXml;

			ContactWebForm.Controls.Add(formViewDataSource);

			ContactFormView.DataSourceID = "WebFormDataSource";

			Xrm.Account partnerAccount = null;

			if (Contact != null && Contact.ParentCustomerId != null)
			{
				partnerAccount = ServiceContext.AccountSet.FirstOrDefault(a => a.AccountId == Contact.ParentCustomerId.Id);	
			}
			
			if ((partnerAccount == null || ContactToEdit == null || ContactToEdit.ParentCustomerId == null) || (ContactToEdit.ParentCustomerId.Id != partnerAccount.AccountId))
			{
				ManageRoles.Visible = false;

				var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

				var channelWriteAccess = (channelPermission != null && channelPermission.adx_Write.GetValueOrDefault());

				var partnerContact = ServiceContext.MergeClone(Contact);

				var parentAccount = partnerContact.contact_customer_accounts;

				if (!channelWriteAccess || parentAccount == null || (channelPermission.adx_AccountId.Id != parentAccount.AccountId)
					|| parentAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
				{
					ContactWebForm.Visible = false;

					NoAccountAccessLabel.Visible = true;
				}


				//additionally, at this point we should check channel permissions 
			}
			else
			{
				var contactAccessPermissions = ServiceContext.GetContactAccessByContact(Contact).Cast<Adx_contactaccess>();

				var canWrite = false;

				foreach (var access in contactAccessPermissions)
				{
					if (access.Adx_Write.GetValueOrDefault())
					{
						canWrite = true;
					}

				}

				ContactWebForm.Visible = canWrite;

				NoAccountAccessLabel.Visible = !canWrite;

				//at this point we should check contact access perms
			}

		}

		protected void OnItemUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e) { }

		protected void OnItemUpdated(object sender, CrmEntityFormViewUpdatedEventArgs e)
		{
			ConfirmationMessage.Visible = true;
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			ContactFormView.UpdateItem();
		}

		protected void ManagePermissionsButton_Click(object sender, EventArgs e)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Manage Permissions");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			var id = ContactToEdit.ContactId;

			url.QueryString.Set("ContactID", id.ToString());

			Response.Redirect(url);
		}
	}
}