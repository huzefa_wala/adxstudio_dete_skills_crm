﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Data;
using Adxstudio.Xrm.Partner;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;
using Xrm;

namespace Site.Areas.CustomerManagement.Pages
{
	public partial class ManageCustomerAccounts : PortalPage
	{
		private DataTable _accounts;

		protected string SortDirection
		{
			get { return ViewState["SortDirection"] as string ?? "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		protected string SortExpression
		{
			get { return ViewState["SortExpression"] as string ?? "Accepted"; }
			set { ViewState["SortExpression"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			var channelReadAccess = (channelPermission != null && channelPermission.adx_Read.GetValueOrDefault());

			var channelCreateAccess = (channelPermission != null && channelPermission.adx_Create.GetValueOrDefault());

			var partnerContact = ServiceContext.MergeClone(Contact);

			var parentCustomerAccount = partnerContact.contact_customer_accounts;

			CreateButton.Visible = channelCreateAccess;

			if (!channelReadAccess || parentCustomerAccount == null || (channelPermission.adx_AccountId.Id != parentCustomerAccount.AccountId)
				|| parentCustomerAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
			{
				InsufficientPermissions.Text = "You do not have channel Read permissions for this account";

				InsufficientPermissions.Visible = true;

				CustomerAccountsList.Visible = false;

				AccountLabel.Visible = false;

				return;
			}

			var accounts = ServiceContext.AccountSet
				.Where(a => a.msa_managingpartnerid.Id == parentCustomerAccount.AccountId)
				.OrderBy(a => a.Name)
				.Select(a => a);

			//HideControlsBasedOnAccess(ServiceContext, Contact);

			_accounts = EnumerableExtensions.CopyToDataTable(accounts.Select(a => new
			{
				accountid = a.Id,
				ID = a.Name,
				City = a.Address1_City,
				State = a.Address1_StateOrProvince,
				Phone = a.Address1_Telephone1,
				Email = a.EMailAddress1,
			}));

			_accounts.Columns["City"].ColumnName = "City";
			_accounts.Columns["State"].ColumnName = "State";
			_accounts.Columns["Phone"].ColumnName = "Phone";
			_accounts.Columns["Email"].ColumnName = "E-mail Address";

			CustomerAccountsList.DataKeyNames = new[] { "accountid" };
			CustomerAccountsList.DataSource = _accounts;
			CustomerAccountsList.DataBind();

			//HideControlsBasedOnAccess(XrmContext, Contact);
		}

		protected void CustomerAccountsList_Sorting(object sender, GridViewSortEventArgs e)
		{
			SortDirection = e.SortExpression == SortExpression
				? (SortDirection == "ASC" ? "DESC" : "ASC")
				: "ASC";

			SortExpression = e.SortExpression;

			_accounts.DefaultView.Sort = string.Format("{0} {1}", SortExpression, SortDirection);

			CustomerAccountsList.DataSource = _accounts;
			CustomerAccountsList.DataBind();
		}

		protected void CustomerAccountsList_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header ||
				e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Cells[0].Visible = false;
			}

			if (e.Row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var dataKey = CustomerAccountsList.DataKeys[e.Row.RowIndex].Value;

			e.Row.Cells[1].Text = string.Format(@"<a href=""{0}"">{1}</a>",
					EditAccountUrl(dataKey),
					e.Row.Cells[1].Text);

			e.Row.Cells[1].Attributes.Add("style", "white-space: nowrap;");
		}

		protected string EditAccountUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Edit Customer Account");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("AccountID", id.ToString());

			return url;
		}


	}
}