﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Data;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using Xrm;

namespace Site.Areas.CustomerManagement.Pages
{
	public partial class EditCustomerAccount : PortalPage
	{
		private DataTable _contacts;

		private Xrm.Account _account;

		public Xrm.Account AccountToEdit
		{
			get
			{
				if (_account != null)
				{
					return _account;
				}

				Guid accountId;

				if (!Guid.TryParse(Request["AccountID"], out accountId))
				{
					return null;
				}

				_account = XrmContext.AccountSet.FirstOrDefault(c => c.Id == accountId);

				return _account;
			}
		}

		protected string SortDirection
		{
			get { return ViewState["SortDirection"] as string ?? "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		protected string SortExpression
		{
			get { return ViewState["SortExpression"] as string ?? "Accepted"; }
			set { ViewState["SortExpression"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			AssertContactHasParentAccount();

			NoAccountAccessLabel.Visible = false;

			var formViewDataSource = new CrmDataSource { ID = "WebFormDataSource" };

			var parentAccount = ServiceContext.AccountSet.Where(a => a.GetAttributeValue<Guid>("accountid") == AccountToEdit.msa_managingpartnerid.Id).FirstOrDefault();

			//var accountAccess = XrmContext.GetAccountAccessByContact(Contact).Cast<Adx_accountaccess>();

			HideControlsBasedOnAccess(ServiceContext, Contact);

			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			//var channelReadAccess = (channelPermission != null && channelPermission.adx_Read.GetValueOrDefault());

			var channelCreateAccess = (channelPermission != null && channelPermission.adx_Create.GetValueOrDefault());

			var channelWriteAccess = (channelPermission != null && channelPermission.adx_Write.GetValueOrDefault());

			if (!channelWriteAccess || parentAccount == null || (channelPermission.adx_AccountId.Id != parentAccount.AccountId)
	|| parentAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
			{
				NoAccountAccessLabel.Visible = true;

				return;
			}

			SubmitButton.Visible = channelWriteAccess;

			CreateButton.Visible = channelCreateAccess;

			CreateButton.QueryStringCollection = CreateCustomerContactQueryStringCollection();

			var fetchXml = string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>", Xrm.Account.EntityLogicalName, "accountid", AccountToEdit.AccountId);

			formViewDataSource.FetchXml = fetchXml;

			AccountForm.Controls.Add(formViewDataSource);

			FormView.DataSourceID = "WebFormDataSource";

			if (!IsPostBack)
			{

				BindPrimaryContact();


				var contacts = ServiceContext.ContactSet
					.Where(c => c.GetAttributeValue<Guid>("parentcustomerid") == AccountToEdit.Id);


				//HideControlsBasedOnAccess(ServiceContext, Contact);

				_contacts = EnumerableExtensions.CopyToDataTable(contacts.Select(c => new
				{
					contactid = c.Id,
					ID = c.FullName,
					CompanyName = c.contact_customer_accounts.Name,
					City = c.Address1_City,
					State = c.Address1_StateOrProvince,
					Phone = c.Address1_Telephone1,
					Email = c.EMailAddress1,
				}).ToList().OrderBy(c => c.CompanyName));

				_contacts.Columns["City"].ColumnName = "City";
				_contacts.Columns["State"].ColumnName = "State";
				_contacts.Columns["Phone"].ColumnName = "Phone";
				_contacts.Columns["Email"].ColumnName = "E-mail Address";

				AccountContactsList.DataKeyNames = new[] { "contactid" };
				AccountContactsList.DataSource = _contacts;
				AccountContactsList.DataBind();

			}

			//HideControlsBasedOnAccess(XrmContext, Contact);
		}

		protected void SetPrimaryContactButton_Click(object sender, EventArgs e)
		{

			Guid SelectedGuid = new Guid(PrimaryContactList.SelectedItem.Value);

			var contact = XrmContext.ContactSet.Where(c => c.Id == SelectedGuid).FirstOrDefault();

			var account = XrmContext.AccountSet.Where(a => a.AccountId == AccountToEdit.AccountId).FirstOrDefault();

			account.PrimaryContactId = contact.ToEntityReference();

			XrmContext.UpdateObject(contact);
			XrmContext.UpdateObject(account);
			XrmContext.SaveChanges();

			ConfirmationMessage.Visible = true;

		}

		private void BindPrimaryContact()
		{
			var empli = new ListItem()
			{
				Text = "- Empty -",
			};

			PrimaryContactList.Items.Add(empli);

			var contacts =
				ServiceContext.ContactSet.Where(c => c.ParentCustomerId.Id == AccountToEdit.AccountId);

			foreach (var contact in contacts)
			{
				var li = new ListItem()
				{
					Text = contact.FullName,
					Value = contact.ContactId.ToString()
				};

				if ((AccountToEdit.PrimaryContactId != null) && (li.Value == AccountToEdit.PrimaryContactId.Id.ToString()))
				{
					li.Selected = true;
				}

				PrimaryContactList.Items.Add(li);
			}

			if (contacts.ToList().Count < 1)
			{
				PrimaryContactList.Visible = false;
				SetPrimaryLabel.Text = "To Set the Primary contact, first please create a contact for this customer account."; //TODO: change to snipper
			}
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			//var contact = XrmContext.MergeClone(Contact);

			FormView.UpdateItem();
		}

		protected void OnItemUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e)
		{
			Guid SelectedGuid = new Guid(PrimaryContactList.SelectedItem.Value);

			var contact = XrmContext.ContactSet.Where(c => c.Id == SelectedGuid).FirstOrDefault();

			var contactReference = contact.ToEntityReference();

			e.Values["primarycontactid"] = contactReference;

		}

		protected void OnItemUpdated(object sender, CrmEntityFormViewUpdatedEventArgs e)
		{

			ConfirmationMessage.Visible = true;
		}

		protected void AccountContactsList_Sorting(object sender, GridViewSortEventArgs e)
		{
			SortDirection = e.SortExpression == SortExpression
				? (SortDirection == "ASC" ? "DESC" : "ASC")
				: "ASC";

			SortExpression = e.SortExpression;

			_contacts.DefaultView.Sort = string.Format("{0} {1}", SortExpression, SortDirection);

			AccountContactsList.DataSource = _contacts;
			AccountContactsList.DataBind();
		}

		protected void AccountContactsList_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header ||
				e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Cells[0].Visible = false;
			}

			if (e.Row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var dataKey = AccountContactsList.DataKeys[e.Row.RowIndex].Value;

			e.Row.Cells[1].Text = string.Format(@"<a href=""{0}"">{1}</a>",
					ContactDetailsUrl(dataKey),
					e.Row.Cells[1].Text);

			e.Row.Cells[1].Attributes.Add("style", "white-space: nowrap;");
		}

		protected string ContactDetailsUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Edit Customer Contact");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("ContactID", id.ToString());

			return url;
		}

		private void HideControlsBasedOnAccess(OrganizationServiceContext context, Entity contact)
		{
			var accessPermissions = context.GetContactAccessByContact(contact).Cast<Adx_contactaccess>();

			CreateButton.Visible = false;
			NoContactAccessLabelPanel.Visible = true;

			foreach (var access in accessPermissions)
			{
				if (access.Adx_Create.GetValueOrDefault())
				{
					CreateButton.Visible = true;
				}

				if (access.Adx_Read.GetValueOrDefault())
				{
					NoContactAccessLabelPanel.Visible = false;
				}
			}

			AccountForm.Visible = !NoAccountAccessLabel.Visible;
			AccountContactsList.Visible = !NoContactAccessLabelPanel.Visible;

		}

		protected QueryStringCollection CreateCustomerContactQueryStringCollection()
		{
			var id = AccountToEdit.AccountId;

			var queryStringCollection = new QueryStringCollection("");

			queryStringCollection.Set("AccountID", id.ToString());

			queryStringCollection.Set("ReturnToAccount", "true");

			return queryStringCollection;
		}

	}
}