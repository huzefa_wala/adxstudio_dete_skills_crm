﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Web.UI.WebControls;
using Site.Pages;
using Xrm;

namespace Site.Areas.CustomerManagement.Pages
{
	public partial class ReadOnlyAccountView : PortalPage
	{
		private Xrm.Account _account;

		public Xrm.Account AccountToEdit
		{
			get
			{
				if (_account != null)
				{
					return _account;
				}

				Guid accountId;

				if (!Guid.TryParse(Request["AccountID"], out accountId))
				{
					return null;
				}

				_account = XrmContext.AccountSet.FirstOrDefault(c => c.Id == accountId);

				return _account;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			AssertContactHasParentAccount();

			NoAccountAccessLabelPanel.Visible = false;

			var formViewDataSource = new CrmDataSource { ID = "WebFormDataSource" };

			//var accountAccess = XrmContext.GetAccountAccessByContact(Contact).Cast<Adx_accountaccess>();

			var parentAccount = ServiceContext.AccountSet.Where(a => a.AccountId == Contact.ParentCustomerId.Id).FirstOrDefault();

			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			var channelReadAccess = (channelPermission != null && channelPermission.adx_Read.GetValueOrDefault());

			if (!channelReadAccess || parentAccount == null || (channelPermission.adx_AccountId.Id != parentAccount.AccountId)
	|| parentAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
			{
				NoAccountAccessLabelPanel.Visible = true;

				return;
			}

			var fetchXml = string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>", Xrm.Account.EntityLogicalName, "accountid", AccountToEdit.AccountId);

			formViewDataSource.FetchXml = fetchXml;

			AccountForm.Controls.Add(formViewDataSource);

			FormView.DataSourceID = "WebFormDataSource";

			//BindPrimaryContact();

		}
	}
}