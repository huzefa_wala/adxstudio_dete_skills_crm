﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Data;
using Adxstudio.Xrm.Partner;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;
using Xrm;

namespace Site.Areas.CustomerManagement.Pages
{
	public partial class ManageCustomerContacts : PortalPage
	{
		private DataTable _contacts;

		protected string SortDirection
		{
			get { return ViewState["SortDirection"] as string ?? "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		protected string SortExpression
		{
			get { return ViewState["SortExpression"] as string ?? "Accepted"; }
			set { ViewState["SortExpression"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			AssertContactHasParentAccount();
			
			if (!IsPostBack)
			{
				PopulateCustomerFilter();
			}

			var partnerContact = ServiceContext.MergeClone(Contact);

			var parentCustomerAccount = partnerContact.contact_customer_accounts;

			if (!ConfirmPermissions(parentCustomerAccount)) return;

			var partnerAccount = ServiceContext.AccountSet.Where(pa => pa.AccountId == Contact.ParentCustomerId.Id).FirstOrDefault();

			var contacts = new List<Contact>();

			/******************filter contacts by account***************/

			if (string.Equals(CustomerFilter.Text, "All", StringComparison.InvariantCulture))
			{

				var myContacts = ServiceContext.ContactSet.Where(c => (c.msa_managingpartnerid != null) && c.msa_managingpartnerid.Id == partnerAccount.AccountId);

				contacts.AddRange(myContacts);

				var accounts = (partnerAccount != null) ? ServiceContext.AccountSet.Where(a => a.msa_managingpartnerid.Id == partnerAccount.AccountId) : null;

				foreach (var account in accounts)
				{
					var currentContacts = ServiceContext.ContactSet.Where(c => (c.msa_managingpartnerid == null || (c.msa_managingpartnerid != null &&
						c.msa_managingpartnerid.Id != partnerAccount.AccountId)) && c.ParentCustomerId.Id == account.AccountId);

					contacts.AddRange(currentContacts);
				}
			}
			else if (string.Equals(CustomerFilter.Text, "My", StringComparison.InvariantCulture))
			{
				var currentContacts = ServiceContext.ContactSet.Where(c => (c.ParentCustomerId == null) && c.msa_managingpartnerid.Id == partnerAccount.AccountId);

				contacts.AddRange(currentContacts);

			}
			else
			{
				Guid accountid;

				if (Guid.TryParse(CustomerFilter.SelectedValue, out accountid))
				{
					var currentContacts = ServiceContext.ContactSet.Where(c => c.ParentCustomerId.Id == accountid);

					contacts.AddRange(currentContacts);
				}
			}

			//HideControlsBasedOnAccess(ServiceContext, Contact);

			_contacts = EnumerableExtensions.CopyToDataTable(contacts.Select(c => new
			{
				contactid = c.Id,
				ID = c.FullName,
				CompanyName = (c.contact_customer_accounts != null) ? c.contact_customer_accounts.Name : " ",
				City = c.Address1_City,
				State = c.Address1_StateOrProvince,
				Phone = c.Address1_Telephone1,
				Email = c.EMailAddress1,
			}));

			_contacts.Columns["City"].ColumnName = "City";
			_contacts.Columns["State"].ColumnName = "State";
			_contacts.Columns["Phone"].ColumnName = "Phone";
			_contacts.Columns["Email"].ColumnName = "E-mail Address";

			CustomerContactsList.DataKeyNames = new[] { "contactid" };
			CustomerContactsList.DataSource = _contacts;
			CustomerContactsList.DataBind();

			Guid id;

			if (CustomerFilter.SelectedItem != null && Guid.TryParse(CustomerFilter.SelectedItem.Value, out id))
			{
				CreateButton.QueryStringCollection = new QueryStringCollection("");

				CreateButton.QueryStringCollection.Set("AccountID", id.ToString());
			}

			//HideControlsBasedOnAccess(XrmContext, Contact);
		}

		private bool ConfirmPermissions(Xrm.Account parentCustomerAccount)
		{
			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			var channelReadAccess = (channelPermission != null && channelPermission.adx_Read.GetValueOrDefault());

			var channelCreateAccess = (channelPermission != null && channelPermission.adx_Create.GetValueOrDefault());

			CreateButton.Visible = channelCreateAccess;

			if (!channelReadAccess || parentCustomerAccount == null ||
				(channelPermission.adx_AccountId.Id != parentCustomerAccount.AccountId)
				|| parentCustomerAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
			{
				InsufficientPermissions.Text = "You do not have channel Read permissions for this account";

				InsufficientPermissions.Visible = true;

				CustomerContactsList.Visible = false;

				CustomerFilter.Visible = false;

				ViewLabel.Visible = false;

				return false;
			}
			return true;
		}

		protected void CustomerContactsList_Sorting(object sender, GridViewSortEventArgs e)
		{
			SortDirection = e.SortExpression == SortExpression
				? (SortDirection == "ASC" ? "DESC" : "ASC")
				: "ASC";

			SortExpression = e.SortExpression;

			_contacts.DefaultView.Sort = string.Format("{0} {1}", SortExpression, SortDirection);

			CustomerContactsList.DataSource = _contacts;
			CustomerContactsList.DataBind();
		}

		protected void CustomerContactsList_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header ||
				e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Cells[0].Visible = false;
			}

			if (e.Row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var dataKey = CustomerContactsList.DataKeys[e.Row.RowIndex].Value;

			e.Row.Cells[1].Text = string.Format(@"<a href=""{0}"">{1}</a>",
					EditContactUrl(dataKey),
					e.Row.Cells[1].Text);

			e.Row.Cells[1].Attributes.Add("style", "white-space: nowrap;");
		}

		protected string EditContactUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Edit Customer Contact");

			if (page == null) return null; 

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("ContactID", id.ToString());

			return url;
		}

		private void PopulateCustomerFilter()
		{
			if (!CustomerFilter.Visible)
			{
				return;
			}

			CustomerFilter.Items.Clear();

			var partnerAccount =
				ServiceContext.AccountSet.Where(pa => pa.AccountId == Contact.ParentCustomerId.Id).FirstOrDefault();

			if (partnerAccount == null)
			{
				InsufficientPermissions.Visible = true;
				CustomerFilter.Visible = false;
				CustomerContactsList.Visible = false;
				return;
			}

			var accounts = (partnerAccount != null) ?
				ServiceContext.AccountSet.Where(a => a.msa_managingpartnerid.Id == partnerAccount.AccountId)
				.OrderBy(a => a.Name)
				.Select(a => a)
			: null;

			CustomerFilter.Items.Add(new ListItem("All"));

			CustomerFilter.Items.Add(new ListItem("My"));

			foreach (var account in accounts)
			{
				CustomerFilter.Items.Add(new ListItem(account.Name, account.AccountId.ToString()));
			}
		}

	}
}