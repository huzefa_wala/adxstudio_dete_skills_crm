﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="CreateCustomerContact.aspx.cs" Inherits="Site.Areas.CustomerManagement.Pages.CreateCustomerContact" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<asp:Label ID="NoChannelAccessLabel" Visible="false" runat="server"><crm:Snippet runat="server" SnippetName="channel/access/nopermissions" DefaultText="You do not have channel permissions to manage customers" Editable="true" EditType="html"/></asp:Label>
	<div class="row">
		<div id="instructions-section" class="pull-right span3 well">
			<h4>
				<asp:Literal Text="<%$ Snippet: profile/instructions, Instructions %>" runat="server" />
			</h4>
			<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
		<div id="create-contact-section" class="span8">
			<div id="profile">
				<asp:Panel ID="ConfirmationMessage" runat="server" CssClass="alert alert-success" Visible="false">
					<div class="message">
						<crm:Snippet runat="server" SnippetName="Contact Create Success Text" DefaultText="Contact has been created successfully." Editable="true" EditType="html" />
					</div>
				</asp:Panel>
				<adx:CrmDataSource ID="WebFormDataSource" runat="server" />
				<asp:Panel ID="ContactWebForm" CssClass="crmEntityFormView" runat="server" Visible="true">
					<%--TODO: Make this dropdown disabled only if you don't have channel permissions or if you are coming from an account/opportunity--%>
					<div class="cell">
							<div class="info"><label><crm:Snippet runat="server" SnippetName="customer-create/label/account-dropdown" DefaultText="Parent Customer" Editable="true" EditType="text" /></label></div>
							<div class="control"><asp:DropDownList ID="CompanyNameList" runat="server" Enabled="False"></asp:DropDownList></div>
					</div>
					<adx:CrmEntityFormView runat="server" ID="ContactFormView" EntityName="contact" FormName="Contact Web Form"
						OnItemInserting="OnItemInserting" OnItemInserted="OnItemInserted" 
						RecommendedFieldsRequired="True" ShowUnsupportedFields="False" DataSourceID="WebFormDataSource"
					ToolTipEnabled="False" Mode="Insert">
						<InsertItemTemplate></InsertItemTemplate>
					</adx:CrmEntityFormView>
				</asp:Panel>
				<asp:Panel ID="SubmitButtonPanel" runat="server" CssClass="form-actions">
					<asp:Button ID="SubmitButton" Text='<%$ Snippet: Contact Create Submit Button Text, Update %>' CssClass="btn btn-primary" OnClick="SubmitButton_Click" ValidationGroup="Profile" runat="server" />
				</asp:Panel>
			</div>
		</div>
	</div>
</asp:Content>

