﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Partner;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Portal.Web.UI.WebControls;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using Xrm;

namespace Site.Areas.CustomerManagement.Pages
{
	public partial class CreateCustomerContact : PortalPage
	{
		private Xrm.Account _account;

		private Opportunity _opportunity;

		public Xrm.Account ParentCustomerAccount
		{
			get
			{
				if (_account != null)
				{
					return _account;
				}

				Guid accountId;

				if (!Guid.TryParse(Request["AccountID"], out accountId))
				{
					return null;
				}

				_account = XrmContext.AccountSet.FirstOrDefault(c => c.Id == accountId);

				return _account;
			}
		}

		public Opportunity OriginatingOpportunity
		{
			get
			{
				if (_opportunity != null)
				{
					return _opportunity;
				}

				Guid oppId;

				if (!Guid.TryParse(Request["OpportunityID"], out oppId))
				{
					return null;
				}

				_opportunity = XrmContext.OpportunitySet.FirstOrDefault(c => c.Id == oppId);

				return _opportunity;
			}
		}

		public bool ReturnToAccount
		{
			get
			{
				bool b = bool.TryParse(Request["ReturnToAccount"], out b);
				return b;
			}
		}

		public bool SetAsPrimary
		{
			get
			{
				bool b = bool.TryParse(Request["SetAsPrimary"], out b);
				return b;
			}
		}

		public bool FromCreateOpportunity
		{
			get
			{
				bool b = bool.TryParse(Request["FromCreateOpportunity"], out b);
				return b;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();
			
			AssertContactHasParentAccount();
			
			HideControlsBasedOnAccess(ServiceContext, Contact);

			PopulateDropDownList();

		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			//var contact = XrmContext.MergeClone(Contact);

			ContactFormView.InsertItem();
		}

		protected void OnItemInserting(object sender, CrmEntityFormViewInsertingEventArgs e) { }

		protected void OnItemInserted(object sender, CrmEntityFormViewInsertedEventArgs e)
		{
			Guid accountID = (Guid.TryParse(CompanyNameList.SelectedValue, out accountID)) ? accountID : Guid.Empty;

			var opportunityID = (OriginatingOpportunity != null) ? OriginatingOpportunity.OpportunityId : null;

			var account = XrmContext.AccountSet.FirstOrDefault(a => a.AccountId == accountID);

			var contactID = e.EntityId;

			var contact = XrmContext.ContactSet.FirstOrDefault(c => c.ContactId == contactID);

			var parentAccount = XrmContext.AccountSet.FirstOrDefault(pa => pa.AccountId == Contact.ParentCustomerId.Id);

			var opportunity = (opportunityID != null) ? XrmContext.OpportunitySet.FirstOrDefault(a => a.OpportunityId == opportunityID) : null;

			if (opportunity != null)
			{
				var contactCrossover = opportunity.adx_opportunity_contact.FirstOrDefault(c => c.ContactId == contact.ContactId);

				adx_opportunitynote oppnote = new adx_opportunitynote();

				if (contactCrossover == null)
				{
					XrmContext.AddLink(opportunity, new Relationship("adx_opportunity_contact"), contact);

					oppnote.adx_name = "Contact Added: " + contact.FullName;
					oppnote.adx_Date = DateTime.UtcNow;
					oppnote.adx_Description = "Contact Added: " + contact.FullName;
					oppnote.adx_OpportunityId = opportunity.ToEntityReference();
					oppnote.adx_AssignedTo = (opportunity.msa_contact_opportunity != null) ? opportunity.msa_contact_opportunity.FullName : "";
					XrmContext.AddObject(oppnote);
					XrmContext.UpdateObject(opportunity);

				}
			}

			contact.msa_managingpartnerid = parentAccount.ToEntityReference();

			if (account != null)
			{
				contact.ParentCustomerId = account.ToEntityReference();

				XrmContext.UpdateObject(account);
			}

			XrmContext.UpdateObject(contact);

			XrmContext.SaveChanges();

			ConfirmationMessage.Visible = true;

			if (!ServiceContext.IsAttached(Website))
			{
				ServiceContext.Attach(Website);
			}

			if (SetAsPrimary)
			{
				contact = XrmContext.ContactSet.Where(c => c.ContactId == contactID).FirstOrDefault();

				account = XrmContext.AccountSet.Where(a => a.AccountId == accountID).FirstOrDefault();

				if (account != null)
				{
					account.PrimaryContactId = contact.ToEntityReference();

					XrmContext.UpdateObject(account);
				}
				else
				{
					throw new Exception("New Customer must contain primary contact.");
				}

				XrmContext.UpdateObject(contact);

				XrmContext.SaveChanges();
			}

			if (opportunity != null)
			{
				var page = ServiceContext.GetPageBySiteMarkerName(Website, "Opportunity Details");

				var url = new UrlBuilder(ServiceContext.GetUrl(page));

				url.QueryString.Set("OpportunityID", opportunity.OpportunityId.ToString());

				Response.Redirect(url);
			}
			else if (ReturnToAccount)
			{
				var page = ServiceContext.GetPageBySiteMarkerName(Website, "Edit Customer Account");

				var url = new UrlBuilder(ServiceContext.GetUrl(page));

				url.QueryString.Set("AccountID", accountID.ToString());

				Response.Redirect(url);
			}
			else if (FromCreateOpportunity)
			{

				var page = ServiceContext.GetPageBySiteMarkerName(Website, "Create Opportunity");

				var url = new UrlBuilder(ServiceContext.GetUrl(page));

				url.QueryString.Set("AccountId", accountID.ToString());

				Response.Redirect(url);
			}
			else
			{
				var page = ServiceContext.GetPageBySiteMarkerName(Website, "Manage Customer Contacts");

				var url = new UrlBuilder(ServiceContext.GetUrl(page));

				Response.Redirect(url);
			}


			/*REPLACE WITH LOGIN PAGE STUFF*/
		}

		private void HideControlsBasedOnAccess(OrganizationServiceContext context, Entity contact)
		{
			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			var channelCreateAccess = (channelPermission != null && channelPermission.adx_Create.GetValueOrDefault());

			var partnerContact = ServiceContext.MergeClone(Contact);

			var parentAccount = partnerContact.contact_customer_accounts;

			if (!channelCreateAccess || parentAccount == null || (channelPermission.adx_AccountId.Id != parentAccount.AccountId)
				|| parentAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
			{
				ContactWebForm.Visible = false;

				NoChannelAccessLabel.Visible = true;
			}

			SubmitButtonPanel.Visible = !NoChannelAccessLabel.Visible;
		}

		private void PopulateDropDownList()
		{
			var managingPartner = ServiceContext.AccountSet.Where(a => a.AccountId == Contact.ParentCustomerId.Id).FirstOrDefault();

			if (managingPartner == null)
			{
				throw new Exception("account for the logged in user does not exist");
			}

			CompanyNameList.Items.Add(new ListItem("None"));

			var accounts = ServiceContext.AccountSet.Where(a => a.msa_managingpartnerid.Id == managingPartner.Id)
				.OrderBy(a => a.Name)
				.Select(a => a);

			foreach (var account in accounts)
			{
				var li = new ListItem()
				{
					Text = account.Name,
					Value = account.AccountId.ToString()
				};

				if ((ParentCustomerAccount != null) && (account.AccountId == ParentCustomerAccount.AccountId))
				{
					li.Selected = true;
				}

				CompanyNameList.Items.Add(li);
			}
		}


	}
}