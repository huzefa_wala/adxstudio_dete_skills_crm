﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="ManageCustomerContacts.aspx.cs" Inherits="Site.Areas.CustomerManagement.Pages.ManageCustomerContacts" %>

<asp:Content ContentPlaceHolderID="PageHeader" ViewStateMode="Enabled" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-header">
		<adx:SiteMarkerLinkButton ID="CreateButton" runat="server" SiteMarkerName="Create Customer Contact" CssClass="btn btn-success pull-right" >
			<i class="icon-white icon-plus-sign"></i> Create New
			</adx:SiteMarkerLinkButton>
		<h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
	<div class="form-inline">
		<div id="filters" class="pull-right">
				<asp:Label ID="ViewLabel" Font-Bold="true" runat="server">View: </asp:Label>
				<asp:DropDownList ID="CustomerFilter" AutoPostBack="true" runat="server">
				</asp:DropDownList>
		</div>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<div class="clearfix"></div>
	<asp:Label runat="server" ID="InsufficientPermissions" Visible="false" />
	<br/>
	<div id="customer-contacts-list">
		<asp:GridView ID="CustomerContactsList" runat="server" CssClass="table table-striped" GridLines="None"  AlternatingRowStyle-CssClass="alternate-row" AllowSorting="true" OnSorting="CustomerContactsList_Sorting" OnRowDataBound="CustomerContactsList_OnRowDataBound" >
			<EmptyDataRowStyle CssClass="empty" />
			<EmptyDataTemplate>
				<crm:Snippet runat="server" SnippetName="customer-contacts/list/empty" DefaultText="There are no items to display." Editable="true" EditType="html" />
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script type="text/javascript">
				$(function () {
					$(".table tr").not(":has(th)").click(function () {
						window.location.href = $(this).find("a").attr("href");
					});


					$("form").submit(function () {
						blockUI();
					});

					$(".table th a").click(function () {
						blockUI();
					});
				});

				function blockUI() {
					$.blockUI({ message: null, overlayCSS: { opacity: .3} });
				}
	</script>
</asp:Content>
