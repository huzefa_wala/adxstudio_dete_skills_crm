﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Partner;
using Adxstudio.Xrm.Web.UI.WebControls;
using Site.Pages;
using Xrm;

namespace Site.Areas.CustomerManagement.Pages
{
	public partial class ReadOnlyContactView : PortalPage
	{
		private Contact _contact;

		public Contact ContactToEdit
		{
			get
			{
				if (_contact != null)
				{
					return _contact;
				}

				Guid contactId;

				if (!Guid.TryParse(Request["ContactID"], out contactId))
				{
					return null;
				}

				_contact = XrmContext.ContactSet.FirstOrDefault(c => c.Id == contactId);

				return _contact;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();

			NoAccountAccessLabelPanel.Visible = false;

			var formViewDataSource = new CrmDataSource { ID = "WebFormDataSource" };

			//var accountAccess = XrmContext.GetAccountAccessByContact(Contact).Cast<Adx_accountaccess>();

			var partnerAccount = ServiceContext.AccountSet.Where(a => a.AccountId == Contact.ParentCustomerId.Id).FirstOrDefault();

			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			var parentAccount = (ContactToEdit.ParentCustomerId != null) ?
				ServiceContext.AccountSet.Where(a => a.AccountId == ContactToEdit.ParentCustomerId.Id).FirstOrDefault() : null;

			//var channelReadAccess = (channelPermission != null && channelPermission.adx_Read.GetValueOrDefault());

			var channelReadAccess = (channelPermission != null && channelPermission.adx_Read.GetValueOrDefault());

			if (!channelReadAccess || partnerAccount == null || (channelPermission.adx_AccountId.Id != partnerAccount.AccountId) ||
				!((parentAccount != null && parentAccount.msa_managingpartnerid != null && parentAccount.msa_managingpartnerid.Id ==
				partnerAccount.AccountId) || ContactToEdit.msa_managingpartnerid.Id == partnerAccount.AccountId))
			{
				NoAccountAccessLabelPanel.Visible = true;

				ContactForm.Visible = false;

				return;
			}

			var fetchXml = string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>", Xrm.Account.EntityLogicalName, "accountid", ContactToEdit.AccountId);

			formViewDataSource.FetchXml = fetchXml;

			ContactForm.Controls.Add(formViewDataSource);

			FormView.DataSourceID = "WebFormDataSource";

			//BindPrimaryContact();

		}
	}
}