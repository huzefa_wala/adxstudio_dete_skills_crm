﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="EditCustomerAccount.aspx.cs" Inherits="Site.Areas.CustomerManagement.Pages.EditCustomerAccount" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content  ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<div class="row">
		<div id="instructions-section" class="span3 pull-right well">
			<h4>
				<asp:Literal  Text="<%$ Snippet: profile/instructions, Instructions %>" runat="server" />
			</h4>
			<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property  DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
		<div id="manage-account-section" class="span8">
			<asp:Panel ID="ConfirmationMessage" runat="server" Visible="false" CssClass="alert alert-success">
				<crm:Snippet runat="server" SnippetName="Account Update Success Text" DefaultText="Account information has been updated successfully." Editable="true" EditType="html" />
			</asp:Panel>
			<br/>
			<div id="profile">
				<crm:Snippet ID="NoAccountAccessLabel" runat="server" SnippetName="account-profile/no_access" DefaultText="You do not have an associated parent account, or do not have account access." Editable="true" EditType="html" />
				<asp:Panel ID="AccountForm" CssClass="crmEntityFormView" runat="server" Visible="true">
					<adx:CrmEntityFormView runat="server" 
						ID="FormView" 
						EntityName="account" 
						FormName="Account Web Form" 
						OnItemUpdating="OnItemUpdating" 
						OnItemUpdated="OnItemUpdated" 
						ValidationGroup="Profile" 
						RecommendedFieldsRequired="True" 
						ShowUnsupportedFields="False" 
						ToolTipEnabled="False" 
						Mode="Edit">
						<UpdateItemTemplate>
						</UpdateItemTemplate>
					</adx:CrmEntityFormView>
					<div class="well form-inline">
						<asp:Label ID="SetPrimaryLabel" CssClass="control-group" runat="server"><crm:Snippet runat="server" SnippetName="accountform/setcontactprimary" DefaultText="Set Primary Contact for this Account:" /></asp:Label>
						<asp:DropDownList ID="PrimaryContactList" runat="server" ClientIDMode="Static" />
					</div>
				</asp:Panel>
				<div class="form-actions"><asp:Button ID="SubmitButton" 
							Text='<%$ Snippet: Profile Submit Button Text, Update %>' 
							CssClass="btn btn-primary" 
							OnClick="SubmitButton_Click" 
							ValidationGroup="Profile" 
							runat="server" /></div>
				<asp:Panel ID="NoContactAccessLabelPanel" CssClass="alert alert-danger" runat="server">
					<crm:Snippet runat="server" SnippetName="accepted-opportunities/no_access" 
						DefaultText="You do not have permissions to edit your contact list." Editable="true" EditType="html" />
				</asp:Panel>
			</div>
		</div>	
	</div>
	<hr/>
	<div id="contacts-list">
		<adx:SiteMarkerLinkButton ID="CreateButton" SiteMarkerName="Create Customer Contact" runat="server" CssClass="btn btn-success pull-right" >
			<i class="icon-white icon-plus-sign"></i> Create New
		</adx:SiteMarkerLinkButton>
	<crm:Snippet  runat="server" SnippetName="Account Contacts Associated Text" DefaultText="<p>The following Contacts are associated with this account:</p>" Editable="true" EditType="html" />
	</div>
	<br/>
	<div id="account-contacts">
	<asp:GridView ID="AccountContactsList" runat="server" CssClass="table table-striped" GridLines="None"  AlternatingRowStyle-CssClass="alternate-row" AllowSorting="true" OnSorting="AccountContactsList_Sorting" OnRowDataBound="AccountContactsList_OnRowDataBound" >
		<EmptyDataRowStyle CssClass="empty" />
		<EmptyDataTemplate>
			<crm:Snippet runat="server" SnippetName="account-contacts/list/empty" DefaultText="There are no items to display." Editable="true" EditType="html" />
		</EmptyDataTemplate>
	</asp:GridView>
	</div>
	<br/>
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script type="text/javascript" src="~/js/jquery.cookie.js"></script>
			<script type="text/javascript">
				$(function () {
					$("form").submit(function () {
						if (Page_IsValid) {
							$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
						}
					});
				});
	</script>
</asp:Content>
