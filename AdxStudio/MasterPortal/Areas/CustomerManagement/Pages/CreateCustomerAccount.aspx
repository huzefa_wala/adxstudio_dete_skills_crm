﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="CreateCustomerAccount.aspx.cs" Inherits="Site.Areas.CustomerManagement.Pages.CreateCustomerAccount" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<asp:Label ID="NoChannelAccessLabel" Visible="false" runat="server"><crm:Snippet runat="server" SnippetName="channel/access/nopermissions" DefaultText="You do not have channel permissions to manage customers" Editable="true" EditType="html"/></asp:Label>
	<div class="row">
		<div id="instructions-section" class="pull-right span3 well">
			<h4>
				<asp:Literal Text="<%$ Snippet: account-create/instructions, Instructions %>" runat="server" />
			</h4>
			<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property  DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
		<div id="create-contact-section" class="span8">
			<div id="profile">
				<asp:Panel ID="ConfirmationMessage" runat="server" Visible="false">
					<div class="alert alert-success">
						<crm:Snippet  runat="server" SnippetName="Account Create Success Text" DefaultText="Account has been created successfully." Editable="true" EditType="html" />
					</div>
				</asp:Panel>
				<adx:CrmDataSource ID="WebFormDataSource" runat="server" />
				<asp:Panel ID="AccountWebForm" CssClass="crmEntityFormView" runat="server" Visible="true">
					<adx:CrmEntityFormView runat="server" ID="AccountFormView" EntityName="account" FormName="Account Web Form"
					 OnItemInserting="OnItemInserting" OnItemInserted="OnItemInserted" 
					 RecommendedFieldsRequired="True" ShowUnsupportedFields="False" DataSourceID="WebFormDataSource"
					ToolTipEnabled="False" Mode="Insert">
						<InsertItemTemplate>
						</InsertItemTemplate>
					</adx:CrmEntityFormView>
				</asp:Panel>
				<asp:Panel ID="SubmitButtonPanel" runat="server" CssClass="form-actions">
					<asp:Button ID="SubmitButton" 
						Text='<%$ Snippet: Contact Create Submit Button Text, Submit %>' 
						CssClass="btn btn-primary"
						ValidationGroup="Profile" 
						OnClick="SubmitButton_Click" 
						runat="server" />
				</asp:Panel>
			</div>
		</div>
	
	</div>
</asp:Content>
