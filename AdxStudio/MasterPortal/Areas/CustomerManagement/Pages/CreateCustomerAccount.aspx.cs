﻿using System;
using System.Linq;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Partner;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Portal.Web.UI.WebControls;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using Xrm;

namespace Site.Areas.CustomerManagement.Pages
{
	public partial class CreateCustomerAccount : PortalPage
	{
		public bool FromCreateOpportunity
		{
			get
			{
				bool b = bool.TryParse(Request["FromCreateOpportunity"], out b);
				return b;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfNecessary();
			AssertContactHasParentAccount();
			HideControlsBasedOnAccess(ServiceContext, Contact);
		}

		protected void OnItemInserting(object sender, CrmEntityFormViewInsertingEventArgs e) { }

		protected void OnItemInserted(object sender, CrmEntityFormViewInsertedEventArgs e)
		{
			var parentAccount = ServiceContext.AccountSet.FirstOrDefault(a => a.AccountId == Contact.ParentCustomerId.Id);

			var account = ServiceContext.AccountSet.FirstOrDefault(c => c.AccountId == e.EntityId);

			if (parentAccount == null)
			{
				throw new Exception("parent account is null");
			}

			account.msa_managingpartnerid = parentAccount.ToEntityReference();

			var accountId = account.AccountId;

			ServiceContext.UpdateObject(account);

			ServiceContext.SaveChanges();

			ConfirmationMessage.Visible = true;


			if (!ServiceContext.IsAttached(Website))
			{
				ServiceContext.Attach(Website);
			}

			if (FromCreateOpportunity)
			{
				var page = ServiceContext.GetPageBySiteMarkerName(Website, "Create Customer Contact");

				var url = new UrlBuilder(ServiceContext.GetUrl(page));

				url.QueryString.Set("AccountId", accountId.ToString());

				url.QueryString.Set("FromCreateOpportunity", "true");

				url.QueryString.Set("SetAsPrimary", "true");

				Response.Redirect(url);
			}
			else
			{
				var page = ServiceContext.GetPageBySiteMarkerName(Website, "Create Customer Contact");

				var url = new UrlBuilder(ServiceContext.GetUrl(page));

				url.QueryString.Set("AccountId", accountId.ToString());

				url.QueryString.Set("ReturnToAccount", "true");

				url.QueryString.Set("SetAsPrimary", "true");

				Response.Redirect(url);
			}
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			//var contact = XrmContext.MergeClone(Contact);

			AccountFormView.InsertItem();
		}

		private void HideControlsBasedOnAccess(OrganizationServiceContext context, Entity contact)
		{
			var channelPermission = ServiceContext.GetChannelAccessByContact(Contact) as adx_channelpermissions;

			var channelCreateAccess = (channelPermission != null && channelPermission.adx_Create.GetValueOrDefault());

			var partnerContact = ServiceContext.MergeClone(Contact);

			var parentCustomerAccount = partnerContact.contact_customer_accounts;

			if (!channelCreateAccess || parentCustomerAccount == null || (channelPermission.adx_AccountId.Id != parentCustomerAccount.AccountId)
				|| parentCustomerAccount.AccountClassificationCode.GetValueOrDefault() != 100000000)
			{

				NoChannelAccessLabel.Visible = true;

				AccountWebForm.Visible = false;

				return;
			}

			SubmitButtonPanel.Visible = !NoChannelAccessLabel.Visible;
		}

	}
}