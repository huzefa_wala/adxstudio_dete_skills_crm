﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SessionScheduleDetails.ascx.cs" Inherits="Site.Areas.Events.Controls.SessionScheduleDetails" %>
<%@ Import Namespace="Xrm"%>
<div id="schedule">
	<asp:ListView ID="ScheduleListView" runat="server" OnItemCommand="ScheduleListView_ItemCommand" OnItemDataBound="ScheduleListView_ItemDataBound">
		<LayoutTemplate>
			<div id="ItemPlaceholder" runat="server" />
		</LayoutTemplate>
		<ItemTemplate>
			<div class="well details">
				<crm:CrmEntityDataSource ID="SessionEvent" runat="server" />
				<crm:CrmEntityDataSource ID="SessionEventLocation" runat="server" />
				<h3 visible="<%# ShowSessionTitle %>" runat="server">
					<crm:Property ID="EventName" DataSourceID="SessionEvent" PropertyName="Adx_name" EditType="text" runat="server" />
				</h3>
				<crm:DateTimeLiteral Value='<%# Eval("Adx_StartTime") %>' Format="D" runat="server" OutputTimeZoneLabel="false" /> : <crm:DateTimeLiteral ID="DateTimeLiteral2" Value='<%# Eval("Adx_StartTime") %>' Format="hh:mm tt" OutputTimeZoneLabel="false" runat="server" /> - <crm:DateTimeLiteral ID="DateTimeLiteral3" Value='<%# Eval("Adx_EndTime") %>' Format="hh:mm tt" runat="server" />
				<crm:Property ID="EventLocationName" DataSourceID="SessionEventLocation" PropertyName="Adx_name" Format="Location: {0}" Literal="true" runat="server" />
				<div class="actions" visible="<%# Request.IsAuthenticated && Attendee != null %>" runat="server">
					<asp:Button ID="FeedbackButton" runat="server" CssClass="btn" Text="Feedback" CommandName="Feedback" CommandArgument='<%# Eval("Adx_eventscheduleId") %>'/>
					<asp:Button ID="ScheduleButton" runat="server" CssClass="btn btn-primary" Text='<%# CheckIfScheduledForCurrentUser((Adx_eventschedule)Container.DataItem, Portal.User == null ? Guid.Empty : Portal.User.Id) ? "Remove From Schedule" : "Add To Schedule" %>' CommandName='<%# CheckIfScheduledForCurrentUser((Adx_eventschedule)Container.DataItem, Portal.User == null ? Guid.Empty : Portal.User.Id) ? "RemoveFromSchedule" : "AddToSchedule" %>' CommandArgument='<%# Eval("Adx_eventscheduleId") %>' />
				</div>
			</div>
		</ItemTemplate>
	</asp:ListView>
</div>