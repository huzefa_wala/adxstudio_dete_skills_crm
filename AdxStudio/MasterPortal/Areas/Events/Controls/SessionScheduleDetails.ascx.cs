﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Events;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Portal.Web.UI.WebControls;
using Site.Controls;
using Xrm;

namespace Site.Areas.Events.Controls
{
	public partial class SessionScheduleDetails : PortalUserControl
	{
		public Contact Attendee;
		public object DataSource;
		public bool ShowSessionTitle;

		protected void Page_PreRender(object sender, EventArgs e)
		{
			ScheduleListView.DataSource = DataSource;

			ScheduleListView.DataBind();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Attendee = Contact;
		}

		protected void ScheduleListView_ItemCommand(object sender, ListViewCommandEventArgs e)
		{

			if (e == null || e.CommandArgument == null)
			{
				return;
			}

			var id = new Guid(e.CommandArgument.ToString());

			switch (e.CommandName)
			{
				case "AddToSchedule":
					{
						// add this to the user's schedule
						var sessionSchedule = XrmContext.Adx_eventscheduleSet.Where(es => es.Adx_eventscheduleId == id).First();
						var registration = new Adx_eventregistration
						{
							adx_attendeeid = Attendee.ToEntityReference(),
							adx_eventid = sessionSchedule.adx_eventid,
							adx_eventscheduleid = sessionSchedule.ToEntityReference(),
							Adx_RegistrationDate = DateTime.UtcNow
						};
						XrmContext.AddObject(registration);
						XrmContext.SaveChanges();
					}
					break;
				case "RemoveFromSchedule":
					{
						// remove this from the user's schedule
						var sessionSchedule = XrmContext.Adx_eventscheduleSet.Where(es => es.Adx_eventscheduleId == id).First();
						var eventSchedules = sessionSchedule.adx_eventschedule_eventregistration.ToList();
						var registration = eventSchedules.FirstOrDefault(er => er.adx_attendeeid.Id == Attendee.ContactId);
						XrmContext.DeleteObject(registration);
						XrmContext.SaveChanges();
					}
					break;
				case "Feedback":
					{
						var feedbackPage = ServiceContext.GetPageBySiteMarkerName(Website, "Event Feedback");

						if (feedbackPage != null)
						{
							var url = new UrlBuilder(ServiceContext.GetUrl(feedbackPage));

							url.QueryString["id"] = id.ToString();

							Response.Redirect(url.PathWithQueryString);
						}
					}
					break;
			}

			// redirect to current page (to avoid re-post issues)
			if (System.Web.SiteMap.CurrentNode == null) return;
			Response.Redirect(System.Web.SiteMap.CurrentNode.Url);
		}

		protected Boolean CheckIfScheduledForCurrentUser(Adx_eventschedule schedule, Guid contactId)
		{
			if (schedule == null)
			{
				return false;
			}

			var eventSchedule = ServiceContext.Adx_eventscheduleSet.FirstOrDefault(et => et.Adx_eventscheduleId == schedule.Adx_eventscheduleId);

			return eventSchedule == null ? false : ServiceContext.CheckIfScheduledForCurrentUser(eventSchedule, contactId);
		}

		protected void ScheduleListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var dataItem = e.Item as ListViewDataItem;

			if (dataItem == null || dataItem.DataItem == null)
			{
				return;
			}

			var scheduleItem = dataItem.DataItem as Adx_eventschedule;

			if (scheduleItem == null)
			{
				return;
			}

			var schedule = ServiceContext.Adx_eventscheduleSet.FirstOrDefault(es => es.Adx_eventscheduleId == scheduleItem.Adx_eventscheduleId);

			var scheduleEvent = schedule.adx_event_eventschedule;

			var eventLocation = schedule.adx_eventlocation_eventschedule;

			var sessionEventControl = (CrmEntityDataSource)e.Item.FindControl("SessionEvent");

			var eventNameControl = (Property)e.Item.FindControl("EventName");

			var sessionEventLocationControl = (CrmEntityDataSource)e.Item.FindControl("SessionEventLocation");

			var eventLocationNameControl = (Property)e.Item.FindControl("EventLocationName");

			if (sessionEventControl != null && eventNameControl != null)
			{
				sessionEventControl.DataItem = scheduleEvent;

				eventNameControl.DataBind();
			}

			if (sessionEventLocationControl != null && eventLocationNameControl != null)
			{
				sessionEventLocationControl.DataItem = eventLocation;

				eventLocationNameControl.DataBind();
			}

		}
	}
}