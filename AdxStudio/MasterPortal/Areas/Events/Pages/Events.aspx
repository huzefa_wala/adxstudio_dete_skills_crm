﻿<%@ Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="Site.Areas.Events.Pages.Events" %>
<%@ Register TagPrefix="site" TagName="ChildNavigation" Src="~/Controls/ChildNavigation.ascx" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Events/css/events.css") %>">
	<link rel="profile" href="http://microformats.org/profile/hcalendar">
</asp:Content>

<asp:Content ContentPlaceHolderID="Breadcrumbs" runat="server"/>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="tabbable events">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#upcoming" data-toggle="tab">
					<crm:Snippet SnippetName="Event Upcoming Tab Text" DefaultText="Upcoming" Literal="True" runat="server"/>
				</a>
			</li>
			<li>
				<a href="#past" data-toggle="tab">
					<crm:Snippet SnippetName="Event Past Tab Text" DefaultText="Past" Literal="True" runat="server"/>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="upcoming">
				<asp:ListView ID="UpcomingEvents" runat="server">
					<LayoutTemplate>
						<ul>
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
						</ul>
					</LayoutTemplate>
					<ItemTemplate>
						<li>
							<crm:CrmEntityDataSource ID="Event" DataItem='<%# Eval("Event") %>' runat="server" />
							<i class="icon icon-calendar"></i>
							<div class="vevent">
								<h3>
									<a class="url summary" href="<%# Eval("Url") %>"><crm:Property DataSourceID="Event" PropertyName="Adx_name" Literal="True" runat="server"/></a>
								</h3>
								<p>
									<abbr class="dtstart" title="<%# Eval("Start", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("Start", "{0:r}") %></abbr>&ndash;<abbr class="dtend" title="<%# Eval("End", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("End", "{0:r}") %></abbr>
								</p>
								<crm:Property DataSourceID="Event" PropertyName="Adx_Summary" EditType="html" runat="server"/>
							</div>
						</li>
					</ItemTemplate>
				</asp:ListView>
			</div>
			<div class="tab-pane" id="past">
				<asp:ListView ID="PastEvents" runat="server">
					<LayoutTemplate>
						<ul>
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
						</ul>
					</LayoutTemplate>
					<ItemTemplate>
						<li>
							<crm:CrmEntityDataSource ID="Event" DataItem='<%# Eval("Event") %>' runat="server" />
							<i class="icon icon-calendar"></i>
							<div class="vevent">
								<h3>
									<a class="url summary" href="<%# Eval("Url") %>"><crm:Property DataSourceID="Event" PropertyName="Adx_name" Literal="True" runat="server"/></a>
								</h3>
								<p>
									<abbr class="dtstart" title="<%# Eval("Start", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("Start", "{0:r}") %></abbr>&ndash;<abbr class="dtend" title="<%# Eval("End", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("End", "{0:r}") %></abbr>
								</p>
								<crm:Property DataSourceID="Event" PropertyName="Adx_Summary" EditType="html" runat="server"/>
							</div>
						</li>
					</ItemTemplate>
				</asp:ListView>
			</div>
		</div>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarAbove" runat="server">
	<site:ChildNavigation Exclude="adx_event" runat="server"/>
</asp:Content>
