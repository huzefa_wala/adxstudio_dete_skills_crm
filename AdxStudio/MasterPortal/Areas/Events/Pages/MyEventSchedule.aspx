﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Profile.master" AutoEventWireup="true" CodeBehind="MyEventSchedule.aspx.cs" Inherits="Site.Areas.Events.Pages.MyEventSchedule" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Register TagPrefix="adx" TagName="SessionScheduleDetails" Src="~/Areas/Events/Controls/SessionScheduleDetails.ascx" %>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" EnableViewState="True" runat="server">
	<adx:SessionScheduleDetails ID="SessionSchedule" runat="server" ShowSessionTitle="true" />
	<asp:Panel ID="EmptyPanel" runat="server">
		<p>You are currently not registered for events at this time.</p>
		<p><crm:CrmHyperLink SiteMarkerName="Events" runat="server">View Upcoming Events</crm:CrmHyperLink></p>
	</asp:Panel>
</asp:Content>
