﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="EventFeedback.aspx.cs" Inherits="Site.Areas.Events.Pages.EventFeedback" %>
<%@ OutputCache VaryByParam="*" VaryByCustom="user" Duration="60" %>
<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div id="feedback">
		<div class="control-group">
			<h4>
				<asp:Label runat="server" ID="EventName" />
			</h4>
			<asp:Label runat="server" CssClass="date" ID="EventDate" />
				</div>
			<hr/>
		<asp:Panel ID="FeedbackFormPanel" CssClass="crmEntityFormView" runat="server">
			<adx:CrmDataSource ID="WebFormDataSource" runat="server" />
			<adx:CrmEntityFormView runat="server" CssClass="crmEntityFormView"
					DataSourceID="WebFormDataSource" 
					FormName="Web Form"
					EntityName="adx_eventsessionfeedback" 
					OnItemInserting="OnItemInserting"
					OnItemInserted="OnItemInserted" />
		</asp:Panel>
		<asp:Panel ID="ThankYouPanel" runat="server" Visible="false">
			<div class="message">
				<crm:Snippet runat="server" SnippetName="SessionFeedback/ThankyouMessage" DefaultText="Thank you for your feedback." />
			</div>
		</asp:Panel>
		<asp:Panel ID="SessionScheduleNotFoundMessage" Visible="false" runat="server">
			<p><crm:Snippet runat="server" SnippetName="sessionFeedback/NotFound" DefaultText="The requested session schedule was not found." /></p>
		</asp:Panel>
	</div>
</asp:Content>
