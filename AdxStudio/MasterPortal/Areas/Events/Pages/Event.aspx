﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="Event.aspx.cs" Inherits="Site.Areas.Events.Pages.Event" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm" %>
<%@ Import Namespace="Adxstudio.Xrm.Collections.Generic" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Microsoft.Xrm.Portal.IdentityModel.Configuration" %>
<%@ Import Namespace="Microsoft.Xrm.Sdk" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Events/css/events.css") %>">
	<link rel="profile" href="http://microformats.org/profile/hcalendar">
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-heading vevent">
	<% Html.RenderPartial("EventBreadcrumbs"); %>
	<div class="page-header">
		<h1>
				<crm:Property DataSourceID="CurrentEntity" CssClass="summary" PropertyName="Adx_name" EditType="text" runat="server" />
			<% if (RequestEventOccurrence != null) { %>
					<small>
					<% if (RequestEventOccurrence.IsAllDayEvent) { %>
						<abbr class="dtstart" data-format="MMMM dd, yyyy" title="<%: RequestEventOccurrence.Start.ToString("yyyy-MM-ddTHH:mm:ssZ") %>"><%: RequestEventOccurrence.Start.ToString("r") %></abbr>
					<% } else { %>
						<abbr class="dtstart" title="<%: RequestEventOccurrence.Start.ToString("yyyy-MM-ddTHH:mm:ssZ") %>"><%: RequestEventOccurrence.Start.ToString("r") %></abbr> &ndash; <abbr class="dtend" title="<%: RequestEventOccurrence.End.ToString("yyyy-MM-ddTHH:mm:ssZ") %>"><%: RequestEventOccurrence.End.ToString("r") %></abbr>
					<% } %>
					</small>
			<% } %>
			</h1>
		</div>
	</div>
	<div class="vevent">
		<div class="row">
			<div class="span8">
			<% if (IsRegistered) { %>
				<% if (!string.IsNullOrEmpty(Entity.GetAttributeValue<string>("adx_content"))) { %>
					<div class="alert alert-info alert-block">
							<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Content" EditType="html" runat="server" />
					</div>
				<% } else { %>
							<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Content" EditType="html" runat="server" />
					<% }
				} %>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Description" EditType="html" runat="server" />
						</div>
			<div class="span4 controls">
				<% if (RequiresRegistration) { %>
					<% if (CanRegister) { %>
						<% if (!IsRegistered) { %>
							<asp:LinkButton ID="Register" CssClass="btn btn-large btn-primary" OnClick="Register_Click" runat="server">
								<i class="icon-plus-sign icon-white"></i>
								<crm:Snippet SnippetName="Event Register Button Text" DefaultText="Register for this event" Literal="True" runat="server"/>
							</asp:LinkButton>
						<% } else { %>
							<% if (!RegistrationRequiresPayment) { %>
								<asp:LinkButton ID="Unregister" CssClass="btn btn-large btn-danger" OnCommand="Unregister_Click" runat="server">
									<i class="icon-minus-sign icon-white"></i>
									<crm:Snippet SnippetName="Event Unregister Button Text" DefaultText="Unregister from this event" Literal="True" runat="server"/>
								</asp:LinkButton>
							<% } else { %>
								<crm:Snippet SnippetName="Event Registered Text" CssClass="alert alert-info" DefaultText="You are registered for this event." EditType="Html" runat="server"/>
							<% } %>
						<% } %>
					<% } else { %>
						<a class="btn btn-large" href="<%: Url.SignInUrl() %>">
							<i class="icon-lock"></i>
							<%: Html.SnippetLiteral("Event Sign In to Register Button Text", "Sign In to register for this event") %>
						</a>
					<% } %>
				<% } %>
				<% if (RequestEventOccurrence != null) { %>
					<a class="btn btn-large" href="<%: Url.RouteUrl("iCalendar", new {eventScheduleId = RequestEventOccurrence.EventSchedule.Id}) %>">
						<i class="icon-calendar"></i>
						<crm:Snippet SnippetName="Event Export Button Text" DefaultText="Add to outlook" Literal="True" runat="server"/>
						</a>
				<% } %>
			</div>
		</div>
					</div>
	
				<asp:ListView ID="OtherOccurrences" runat="server">
					<LayoutTemplate>
			<div class="event-schedule">
								<h4>
					<i class="icon-calendar"></i>
					<crm:Snippet SnippetName="Event Upcoming Times Heading" DefaultText="Upcoming Times" EditType="text" runat="server"/>
								</h4>
				<ul class="nav nav-tabs nav-stacked">
								<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
				</ul>
						</div>
					</LayoutTemplate>
					<ItemTemplate>
			<li class="vevent">
				<a class="url summary" href="<%# Eval("Url") %>">
							<asp:PlaceHolder Visible='<%# Eval("IsAllDayEvent") %>' runat="server">
								<abbr class="dtstart" data-format="MMMM dd, yyyy" title="<%# Eval("Start", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("Start", "{0:r}") %></abbr>
							</asp:PlaceHolder>
							<asp:PlaceHolder Visible='<%# !(bool)Eval("IsAllDayEvent") %>' runat="server">
								<abbr class="dtstart" title="<%# Eval("Start", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("Start", "{0:r}") %></abbr> &ndash; <abbr class="dtend" title="<%# Eval("End", "{0:yyyy-MM-ddTHH:mm:ssZ}") %>"><%# Eval("End", "{0:r}") %></abbr>
							</asp:PlaceHolder>
						</a>
			</li>
					</ItemTemplate>
				</asp:ListView>
	
	<asp:ListView ID="Speakers" runat="server">
		<LayoutTemplate>
			<hr/>
			<h3><crm:Snippet SnippetName="Speakers Heading" DefaultText="Speakers" EditType="text" runat="server" /></h3>
			<ul class="speakers">
				<li id="ItemPlaceholder" runat="server" />
			</ul>
		</LayoutTemplate>
		<ItemTemplate>
			<li>
				<div class="well row">
					<div class="pull-right">
						<asp:LinqDataSource ID="CrmNoteSource" runat="server" ContextTypeName="Xrm.XrmServiceContext" TableName="AnnotationSet"
						Where="ObjectId.Id == @EventSpeakerID && isdocument == true" OrderBy="CreatedOn" OnSelecting="LinqDataSourceSelecting">
							<WhereParameters>
								<asp:ControlParameter ControlID="CurrentEventSpeakerID" Name="EventSpeakerID" DefaultValue="00000000-0000-0000-0000-000000000000" DbType="Guid" />
							</WhereParameters>
						</asp:LinqDataSource>
						<asp:Repeater DataSourceID="CrmNoteSource" runat="server">
							<ItemTemplate>
								<asp:Image ImageUrl='<%# ((Entity)Container.DataItem).GetFileAttachmentUrl(Website) %>' runat="server" />
							</ItemTemplate>
						</asp:Repeater>
			</div>
					<div>
						<crm:CrmEntityDataSource ID="Speaker" DataItem="<%# Container.DataItem %>" runat="server" />
						<h4>
							<crm:Property DataSourceID="Speaker" PropertyName="Adx_name" EditType="text" runat="server" />
						</h4>
						<asp:HyperLink CssClass="speaker-url" NavigateUrl='<%# Eval("Adx_Url") %>' runat="server">
							<crm:Property DataSourceID="Speaker" PropertyName="Adx_Url" EditType="text" runat="server" />
						</asp:HyperLink>
						<asp:HiddenField runat="server" ID="CurrentEventSpeakerID" Value='<%# Eval("Adx_eventspeakerId") %>' />
						<crm:Property DataSourceID="Speaker" PropertyName="Adx_Description" EditType="html" runat="server" />
		</div>
	</div>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>
