﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Web.UI.WebControls;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using Xrm;

namespace Site.Areas.Events.Pages
{
	public partial class EventFeedback : PortalPage
	{
		protected Guid AttendeeContactID { get { return Contact.ContactId.GetValueOrDefault(); } }

		private Guid? _sessionEventScheduleID;

		protected Guid? SessionEventScheduleID
		{
			get
			{
				if (_sessionEventScheduleID != null) return _sessionEventScheduleID;

				try
				{
					Guid id;

					if (!Guid.TryParse(Request.QueryString["id"], out id))
					{
						return null;
					}

					var sched = XrmContext.Adx_eventscheduleSet.FirstOrDefault(es => es.Adx_eventscheduleId == id);

					if (sched != null)
					{
						_sessionEventScheduleID = sched.Adx_eventscheduleId;
					}
					else
					{
						return null;
					}
				}
				catch
				{
					//SessionEventScheduleID is not a valid Guid and will cause the LINQDataSource Query to fail
					Response.Redirect(Request.RawUrl.Remove(Request.RawUrl.IndexOf("?")));

					return null;
				}
				return _sessionEventScheduleID;
			}
			set
			{
				if (value != null)
				{
					try
					{
						_sessionEventScheduleID = value;
					}
					catch
					{
						throw new InvalidCastException();
					}
				}
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				var returnUrl = string.Format("{0}?id={1}", ServiceContext.GetUrl(Entity), Request.QueryString["id"]);

				Response.Redirect(string.Format("~/login?ReturnUrl={0}", HttpUtility.UrlEncode(returnUrl)));

				return;
			}
			if (SessionEventScheduleID == null)
			{
				SessionScheduleNotFoundMessage.Visible = true;
				FeedbackFormPanel.Visible = false;
			}

			var schedule = XrmContext.Adx_eventscheduleSet.FirstOrDefault(en => en.Adx_eventscheduleId == SessionEventScheduleID);
			EventName.Text = GetEventNameFromSchedule(XrmContext, schedule);
			if (schedule != null)
			{
				EventDate.Text = String.Format("{0:f}", schedule.Adx_StartTime);
			}
		}

		protected void OnItemInserting(object sender, CrmEntityFormViewInsertingEventArgs e)
		{
			var schedule = XrmContext.Adx_eventscheduleSet.FirstOrDefault(sn => sn.Adx_eventscheduleId == SessionEventScheduleID);

			e.Values["adx_name"] = GetEventNameFromSchedule(XrmContext, schedule) + " session feedback from " + Contact.FullName;
			e.Values["adx_attendeeid"] = Contact.ToEntityReference();
			if (schedule != null)
			{
				e.Values["adx_eventscheduleid"] = schedule.ToEntityReference();
			}
		}

		protected void OnItemInserted(object sender, CrmEntityFormViewInsertedEventArgs e)
		{
			ThankYouPanel.Visible = true;
			FeedbackFormPanel.Visible = false;
		}

		protected string GetEventNameFromSchedule(OrganizationServiceContext context, Adx_eventschedule schedule)
		{
			if (schedule == null)
			{
				return "";
			}

			var session = schedule.adx_event_eventschedule;

			return session == null ? "" : session.Adx_name;
		}
	}
}