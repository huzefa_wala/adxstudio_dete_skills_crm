﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Site.Pages;

namespace Site.Areas.Events.Pages
{
	public partial class MyEventSchedule : PortalPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			if (IsPostBack || Contact == null)
			{
				return;
			}

			var query = from eventSchedule in XrmContext.CreateQuery("adx_eventschedule")
				join @event in XrmContext.CreateQuery("adx_event") on eventSchedule.GetAttributeValue<Guid>("adx_eventid") equals @event.GetAttributeValue<Guid>("adx_eventid")
				join eventRegistration in XrmContext.CreateQuery("adx_eventregistration") on @event.GetAttributeValue<Guid>("adx_eventid") equals eventRegistration.GetAttributeValue<Guid>("adx_eventid")
				where @event.GetAttributeValue<EntityReference>("adx_websiteid") == Website.ToEntityReference()
				where eventRegistration.GetAttributeValue<EntityReference>("adx_attendeeid") == Contact.ToEntityReference()
				select eventSchedule;

			var eventSchedules = query.ToArray();

			SessionSchedule.DataSource = eventSchedules;
			EmptyPanel.Visible = !eventSchedules.Any();
		}
	}
}