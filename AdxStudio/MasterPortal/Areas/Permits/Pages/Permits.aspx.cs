﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm.Portal.Cms;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Site.Pages;

namespace Site.Areas.Permits.Pages
{
	public partial class Permits : PortalPage
	{
		protected void Page_Load(object sender, EventArgs args)
		{
			PermitsListView.DataSource = GetPermits();
			PermitsListView.DataBind();
		}

		private IEnumerable<SiteMapNode> GetPermits()
		{
			var currentNode = System.Web.SiteMap.CurrentNode;

			if (currentNode == null)
			{
				return new SiteMapNode[] { };
			}

			var nodes = currentNode.ChildNodes.Cast<SiteMapNode>().ToList();

			return nodes;
		}

		protected string GetThumbnailUrl(object webpageObject)
		{
			var webpageEntity = webpageObject as Entity;

			if (webpageEntity == null) return null;

			var webfile = ServiceContext.CreateQuery<Xrm.Adx_webfile>().FirstOrDefault(file => file.Id == webpageEntity.GetAttributeValue<Guid>("adx_image"));

			if (webfile == null) return null;

			var url = new UrlBuilder(ServiceContext.GetUrl(webfile));

			return url.Path;
		}
	}
}