﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Profile.master" AutoEventWireup="true" CodeBehind="MyPermitApplications.aspx.cs" Inherits="Site.Areas.Permits.Pages.MyPermitApplications" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="System.Security.Policy" %>


<asp:Content runat="server" ContentPlaceHolderID="Head">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Permits/css/permits.css") %>" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<div class="page-heading">
		<div class="page-header">
			<div class="pull-right">
				<crm:CrmHyperLink runat="server" SiteMarkerName="Permits" CssClass="btn btn-primary">
					<i class="icon-plus-sign icon-white"></i>
					<crm:Snippet runat="server" SnippetName="New Permit Application Button Label" Literal="True" DefaultText="Apply for a permit" />
				</crm:CrmHyperLink>
			</div>
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
		</div>
	</div>
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
	<div id="my-service-requests">
		<asp:GridView ID="MyPermitsGridView" runat="server" CssClass="table table-striped" OnRowDataBound="MyPermitsGridView_OnRowDataBound" GridLines="None">
			<EmptyDataTemplate>
				<div class="alert"><crm:Snippet runat="server" SnippetName="My Permits List Empty Message" EditType="html" Editable="true" DefaultText="You currently do not have any Permit Applications." /></div>
				<p>
					<crm:CrmHyperLink runat="server" SiteMarkerName="Permits" CssClass="btn btn-primary">
						<i class="icon-plus-sign icon-white"></i>
						<crm:Snippet runat="server" SnippetName="New Permit Application Button Label" Literal="True" DefaultText="Apply for a permit" />
					</crm:CrmHyperLink>
				</p>
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
</asp:Content>
