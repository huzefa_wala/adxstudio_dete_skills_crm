﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="Permits.aspx.cs" Inherits="Site.Areas.Permits.Pages.Permits" %>
<%@ OutputCache CacheProfile="User" %>
<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Government/css/permits.css") %>" />
</asp:Content>
<asp:Content ContentPlaceHolderID="Breadcrumbs" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<asp:ListView ID="PermitsListView" runat="server">
		<LayoutTemplate>
			<ul class="permits thumbnails">
				<li id="itemPlaceholder" runat="server"/>
			</ul>
		</LayoutTemplate>
		<ItemTemplate>
			<li class="span4" runat="server">
				<a class="well thumbnail permit text-center" href="<%# Eval("Url") %>">
					<asp:Image ImageUrl='<%# GetThumbnailUrl(Eval("Entity")) %>' runat="server"/>
					<h6><%# Eval("Title") %></h6>
				</a>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>
<asp:Content ContentPlaceHolderID="SidebarAbove" runat="server"/>
