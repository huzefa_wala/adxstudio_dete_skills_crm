﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Web.UI;
using Microsoft.Xrm.Portal.Data;
using Site.Pages;

namespace Site.Areas.Permits.Pages
{
	public partial class MyPermitApplications : PortalPage
	{
		private const string SavedQueryName = "Active Permit";  //TODO: Create a web view for permits
		private const string EntityName = "adx_permit";

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			if (Contact == null)
			{
				return;
			}

			var savedQuery = XrmContext.SavedQuerySet.FirstOrDefault(s => s.Name == SavedQueryName && s.ReturnedTypeCode == EntityName);

			if (savedQuery == null)
			{
				throw new ApplicationException(string.Format("Required View titled '{0}' could not be found for entity '{1}'.", SavedQueryName, EntityName));
			}

			var serviceRequests = XrmContext.adx_permitSet.Where(s => s.adx_regardingcontact.Id == Contact.Id);

			MyPermitsGridView.DataSource = serviceRequests.ToDataTable(XrmContext, savedQuery, true);

			MyPermitsGridView.ColumnsGenerator = new SavedQueryColumnsGenerator(XrmContext, EntityName, SavedQueryName);

			MyPermitsGridView.DataBind();
		}

		protected void MyPermitsGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow || e.Row.Cells.Count < 1)
			{
				return;
			}

			foreach (TableCell cell in e.Row.Cells)
			{
				DateTime cellAsDateTime;

				if (DateTime.TryParse(cell.Text, out cellAsDateTime))
				{
					cell.Text = cellAsDateTime.ToLocalTime().ToString("g");
				}
			}
		}
	}
}