﻿<%@ Page Language="C#" MasterPageFile="../Shared/Ideas.master" Inherits="System.Web.Mvc.ViewPage<Site.Areas.Ideas.ViewModels.IdeaViewModel>" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Ideas" %>
<%@ Import Namespace="Site.Helpers" %>

<asp:Content runat="server" ContentPlaceHolderID="Title"><%: Model.Idea.Title %></asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageHeader">
	<ul class="breadcrumb">
		<% Html.RenderPartial("SiteMapPath"); %>
		<li><%: Html.ActionLink(Model.IdeaForum.Title, "Ideas", new { ideaForumPartialUrl = Model.IdeaForum.PartialUrl, ideaPartialUrl = string.Empty }) %><span class="divider">/</span></li>
		<li class="active"><%: Model.Idea.Title %></li>
	</ul>
	<div class="page-header">
		<h1><%: Model.Idea.Title %></h1>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<div id="vote-status-<%: Model.Idea.Id %>">
		<% Html.RenderPartial("Votes", Model.Idea); %>
	</div>
	<div class="idea-container">
		<p><small><%: Html.SnippetLiteral("Idea Author Label", "Suggested by")%> </small>
		<% if (Model.Idea.AuthorId.HasValue) { %>
			<a href="<%= Url.AuthorUrl(Model.Idea) %>"><%= Model.Idea.AuthorName %></a>
						<% } else { %>
							<%: Model.Idea.AuthorName %>
						<% } %>
		&middot;<% Html.RenderPartial("IdeaStatus", Model.Idea); %></p>
		<%= Model.Idea.Copy %>
		<% if (!string.IsNullOrWhiteSpace(Model.Idea.StatusComment)) { %>
			<h3><%: Html.SnippetLiteral("Idea Status Comment Label", "Status Details")%></h3>
			<%= Model.Idea.StatusComment %>
		<% } %>
	</div>
	<div id="comments">
		<% Html.RenderPartial("Comments", Model.Comments); %>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="SideBarContent">
	<div class="section">
		<% if (Model.IdeaForum.VotesPerUser.HasValue) { %>
			<div id="votes-left">
				<p><%: Html.SnippetLiteral("Ideas User Votes Left", "Your number of votes left:")%></p>
				<h2 class="bottom-break"><%: Model.IdeaForum.VotesPerUser - Model.IdeaForum.CurrentUserActiveVoteCount %></h2>
			</div>
		<% }
		if (Model.Idea.VotingType == IdeaForumVotingType.UpOrDown) { %>
			<p>
				<span class="badge badge-success"><%: Model.Idea.VoteUpCount %></span>
				<%: Html.SnippetLiteral("Idea Votes Up Label", "votes up for this idea") %>
			</p>
			<p>
				<span class="badge badge-error"><%: Model.Idea.VoteDownCount %></span>
				<%: Html.SnippetLiteral("Idea Votes Down Label", "votes down for this idea") %>
			</p>
		<% }
		if (Model.Idea.VotesPerIdea > 1 || Model.Idea.VotingType == IdeaForumVotingType.UpOrDown) { %>
			<p>
				<span class="badge badge-info"><%: Model.Idea.VoterCount %></span>
				<%: Html.SnippetLiteral("Idea Voter Count Label", "voters for this idea") %>
			</p>
		<% } %>
	</div>
	<div class="section">
		<% if ((Model.IdeaForum.VotingPolicy == IdeaForumVotingPolicy.OpenToAuthenticatedUsers || Model.Idea.CommentPolicy == IdeaForumCommentPolicy.OpenToAuthenticatedUsers) && !Request.IsAuthenticated) { %>
			<div class="alert alert-info"><%: Html.SnippetLiteral("idea/sign-in-message", "Please sign in to provide all types of feedback for this idea.")%></div>
		<% } %>
	</div>
</asp:Content>
