﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Adxstudio.Xrm.Ideas.IIdea>" %>
<%@ Import Namespace="Adxstudio.Xrm.Ideas" %>
<%@ Import Namespace="Microsoft.Xrm.Client" %>
<%@ Import Namespace="Site.Helpers" %>

<div class="well voting-well">
	<% if (Model.CurrentUserCanVote()) {
		if (Model.VotesPerIdea == 1) { %>
			<%= Ajax.RawActionLink(@"<i class=""icon-arrow-up icon-white""></i>", "Vote", "Idea", new { voteValue = 1, id = Model.Id },
				new AjaxOptions { HttpMethod = "POST", UpdateTargetId = "vote-status-" + Model.Id, OnComplete = "updateUserVoteCount(1)" },
				new { @class = "btn btn-mini btn-info" }) %>
		<% } else { %>
			<a class="btn btn-mini btn-info" id="vote-modal-<%: Model.Id %>-link" data-toggle="modal" href="#vote-modal-<%: Model.Id %>"><i class="icon-arrow-up icon-white"></i></a>
		<% }
	} else { %>
		<a class="btn btn-mini disabled"><i class="icon-arrow-up"></i></a>
	<% } %>
	<h4><%: Model.VoteSum %></h4>
	<% if (Model.VotingType == IdeaForumVotingType.UpOrDown) {
		if (Model.CurrentUserCanVote()) { %>
			<%= Ajax.RawActionLink(@"<i class=""icon-arrow-down icon-white""></i>", "Vote", "Idea", new { voteValue = -1, id = Model.Id },
				new AjaxOptions { HttpMethod = "POST", UpdateTargetId = "vote-status-" + Model.Id, OnComplete = "updateUserVoteCount(-1)" },
				new { @class = "btn btn-mini btn-info" }) %>
		<% } else { %>
			<a class="btn btn-mini disabled"><i class="icon-arrow-down"></i></a>
		<% }
	} %>
</div>
<% if (Model.CurrentUserCanVote() && Model.VotesPerIdea != 1) { %>
	<div class="modal modal-vote" id="vote-modal-<%: Model.Id %>">
		 <div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h3>Vote</h3>
		</div>
		<div class="modal-body">
			<div class="btn-group">
				<% for (var i = 1; i < Model.VotesPerIdea + 1; i++) {
					if (Model.CurrentUserCanVote(i)) { %>
						<%= Ajax.ActionLink("+{0}".FormatWith(i), "Vote", "Idea",
							new RouteValueDictionary(new { voteValue = i, id = Model.Id }),
							new AjaxOptions { HttpMethod = "POST", UpdateTargetId = "vote-status-" + Model.Id, OnComplete = "updateUserVoteCount({0})".FormatWith(i) },
							new Dictionary<string, object> { {"class", "btn vote"}, {"data-dismiss", "modal" } })%>
					<% } else { %>
						<a class="btn disabled">+<%= i %></a>
					<% } %>
				<% } %>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function () {
			$("#vote-modal-<%: Model.Id %>-link").click(function (e) {
				$("#vote-modal-<%: Model.Id %>").css({ top: e.pageY - window.pageYOffset + 'px', left: e.pageX - window.pageXOffset + 'px' });
			});
		});
	</script>
<% } %>	
