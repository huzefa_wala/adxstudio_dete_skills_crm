﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Adxstudio.Xrm.Ideas.IIdea>" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Site.Areas.Ideas" %>

<small> <%: Html.SnippetLiteral("Idea Status Label", "Status:")%> </small><span class="label
<%= Model.Status == (int)IdeaStatus.Accepted ? " label-info" : string.Empty %>
<%= Model.Status == (int)IdeaStatus.Completed ? " label-success" : string.Empty %>
<%= Model.Status == (int)IdeaStatus.Rejected ? " label-inverse" : string.Empty %>"><%: Enum.GetName(typeof(IdeaStatus), Model.Status)%></span>
