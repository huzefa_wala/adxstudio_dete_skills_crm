﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<div class="section">
	<h4><%: Html.SnippetLiteral("Idea Search Heading", "Search Ideas") %></h4>
	<% using (Html.BeginForm("search", "Idea", FormMethod.Get, new { id = "ideas-search-form", @class = "form-search" })) { %>
		<div class="input-append">
			<%= Html.TextBox("q", string.Empty, new { @class = "search-query input-medium" })%>
			<button type="submit" class="btn"><i class="icon-search"></i></button>
		</div>
	<% } %>
</div>
<script type="text/javascript">
	$(function() {
		$("#ideas-search-form").submit(function() {
			if ($("#ideas-search-form #q").val().trim().length) {
				return true;
			}
			return false;
		});
	});
</script>
