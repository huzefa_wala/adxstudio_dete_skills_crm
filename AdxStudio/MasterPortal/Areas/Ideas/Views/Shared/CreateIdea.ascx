﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Adxstudio.Xrm.Ideas.IIdeaForum>" %>

<% using (Ajax.BeginForm("Create", "Idea", new { id = Model.Id }, new AjaxOptions { UpdateTargetId = "create-idea", OnComplete = "ideaCreated"}, new{@class = "form-horizontal html-editors"})) { %>
	<fieldset>
		<legend><%: Html.SnippetLiteral("Idea New Suggestion Label", "Suggest a New Idea")%></legend>
		<%= Html.ValidationSummary(string.Empty, new {@class = "alert alert-block alert-error"})%>
		<% if (!Request.IsAuthenticated) { %>
			<div class="control-group">
				<label class="control-label" for="authorName">* Your Name</label>
				<div class="controls">
					<%= Html.TextBox("authorName", string.Empty)%>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="authorEmail">* E-mail</label>
				<div class="controls">
					<%= Html.TextBox("authorEmail", string.Empty)%>
				</div>
			</div>
		<% } %>
		<div class="control-group">
			<label class="control-label" for="title">* Idea</label>
			<div class="controls">
				<%= Html.TextBox("title", string.Empty, new { @class = "span6" })%>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="title">Description</label>
			<div class="controls">
				<%= Html.TextArea("copy", string.Empty, new { @class = "span6" })%>
			</div>
		</div>
		<div class="form-actions">
			<input id="submit-idea" class="btn btn-primary" type="submit" value="<%: Html.SnippetLiteral("Idea Submit Label", "Submit Idea")%>" />
		</div>
	</fieldset>
<% } %>
<script type="text/javascript">
	$(function() {
		$("#submit-idea").click(function() {
			$.blockUI({ message: null, overlayCSS: { opacity: .3 } });
		});
	});

	function ideaCreated() {
		if ($("#create-idea .validation-summary-errors").length) {
			portal.initializeHtmlEditors();
			prettyPrint();
			$.unblockUI();
			return;
		}
		window.location.href = '<%= Url.RouteUrl("IdeasFilter", new { ideaForumPartialUrl = Model.PartialUrl, filter = "new" }) %>';
	}
</script>