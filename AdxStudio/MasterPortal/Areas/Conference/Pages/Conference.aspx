﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="Conference.aspx.cs" Inherits="Site.Areas.Conference.Pages.Conference" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm.Collections.Generic" %>
<%@ Import Namespace="Microsoft.Xrm.Portal.IdentityModel.Configuration" %>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<h3>Conference Details</h3>
	<div>
		<crm:CrmEntityDataSource ID="ConferenceDataSource" runat="server" />
		<crm:Property DataSourceID="ConferenceDataSource" PropertyName="Adx_Summary" EditType="html" runat="server" />
	</div>
	<% if (Request.IsAuthenticated) { %>
		<% if (!UserIsRegisteredForConference) { %>
			<asp:LinkButton ID="Register" CssClass="btn btn-large btn-primary" OnClick="Register_Click" runat="server">
				<i class="icon-briefcase icon-white"></i>
				<crm:Snippet SnippetName="Conference Register Button Text" DefaultText="Register for the Conference" Literal="True" runat="server"/>
			</asp:LinkButton>
		<% } else { %>
			<crm:Snippet SnippetName="Conference Registered Text" CssClass="alert alert-info" DefaultText="You are registered for this conference." EditType="Html" runat="server"/>
		<% } %>
	<% } else { %>
		<a class="btn btn-large" href="<%: Html.SiteMarkerUrl("Login").AppendQueryString(FederationCrmConfigurationManager.GetUserRegistrationSettings().ReturnUrlKey ?? "ReturnURL", Request.Url.PathAndQuery) %>">
			<i class="icon-lock"></i>
			<crm:Snippet SnippetName="Conference Sign in to Register Button Text" DefaultText="Sign In to register for the Conference" EditType="text" runat="server"/>
		</a>
		<% } %>
	<asp:ListView ID="ChildView" runat="server">
		<LayoutTemplate>
			<div class="listing">
				<h4>
					<i class="icon-folder-open"></i>
					<crm:Snippet SnippetName="Page Children Heading" DefaultText="In This Section" EditType="text" runat="server"/>
				</h4>
				<ul>
					<li id="itemPlaceholder" runat="server"/>
				</ul>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<li runat="server">
				<h4>
					<a href="<%# Eval("Url") %>"><%# Eval("Title") %></a>
				</h4>
				<crm:CrmEntityDataSource ID="ChildEntity" DataItem='<%# Eval("Entity") %>' runat="server"/>
				<crm:Property DataSourceID="ChildEntity" PropertyName='<%# GetSummaryPropertyName(Eval("Entity.LogicalName", "{0}")) %>' EditType="html" runat="server"/>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarAbove" runat="server">
	<div class="page-children">
		<% if (Shortcuts.Any()) {%>
			<h4>
				<i class="icon-share"></i>
				<crm:Snippet SnippetName="Page Related Heading" DefaultText="Related Topics" EditType="text" runat="server"/>
			</h4>
			<ul class="nav nav-tabs nav-stacked">
				<% foreach (var node in Shortcuts) { %>
					<li>
						<a href="<%= node.Url %>">
							<i class="icon-share-alt"></i>
							<%= node.Title %>
						</a>
					</li>
				<% } %>
			</ul>
		<% } %>
	</div>
</asp:Content>


