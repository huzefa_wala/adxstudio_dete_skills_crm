﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="Sponsors.aspx.cs" Inherits="Site.Areas.Conference.Pages.Sponsors" %>
<%@ OutputCache VaryByParam="*" VaryByCustom="user" Duration="60" %>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<crm:CrmMetadataDataSource ID="SponsorCategories" runat="server" AttributeName="adx_sponsorshipcategory" EntityName="adx_eventsponsor" />
	<asp:Repeater DataSourceID="SponsorCategories" runat="server">
		<ItemTemplate>
			<h4><%# Eval("OptionLabel") %></h4>
			<asp:Repeater DataSource='<%# GetSponsorsByCategory(Eval("OptionValue") as int?) %>' runat="server">
				<ItemTemplate>
					<div>
						<crm:CrmEntityDataSource ID="Sponsor" DataItem='<%# Container.DataItem %>' runat="server" />
						<asp:HyperLink NavigateUrl='<%# Eval("Adx_url") %>' Target="_blank" runat="server">
							<crm:Property DataSourceID="Sponsor" PropertyName="Adx_name" EditType="text" runat="server" />
						</asp:HyperLink>
						<crm:Property DataSourceID="Sponsor" PropertyName="Adx_Description" EditType="html" runat="server" />
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</ItemTemplate>
	</asp:Repeater>
</asp:Content>
