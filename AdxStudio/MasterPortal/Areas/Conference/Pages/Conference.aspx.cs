﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using Microsoft.Xrm.Portal.Web;
using Adxstudio.Xrm.Cms;

namespace Site.Areas.Conference.Pages
{
	public partial class Conference : ConferencePage
	{
		protected const string ConferenceIdQueryStringParameterName = "conferenceid";

		private readonly Lazy<Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>> _childNodes = new Lazy<Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>>(GetChildNodes, LazyThreadSafetyMode.None);

		protected IEnumerable<SiteMapNode> Children
		{
			get { return _childNodes.Value.Item1; }
		}

		protected IEnumerable<SiteMapNode> Shortcuts
		{
			get { return _childNodes.Value.Item2; }
		}

		protected void Page_Init(object sender, EventArgs args)
		{
			ConferenceDataSource.DataItem = PortalConference;
		}

		protected void Page_Load(object sender, EventArgs args)
		{
			ChildView.DataSource = Children;
			ChildView.DataBind();
		}

		protected void Register_Click(object sender, EventArgs args)
		{
			if (PortalConference == null)
			{
				return;
			}

			var registrationUrl = GetRegistrationUrl(PortalConference.Id);

			Response.Redirect(registrationUrl);
		}

		protected string GetRegistrationUrl(Guid conferenceId)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Conference Registration");

			if (page == null)
			{
				throw new ApplicationException("Page could not be found for Site Marker named 'Conference Registration'");
			}

			var url = ServiceContext.GetUrl(page);

			if (string.IsNullOrWhiteSpace(url))
			{
				throw new ApplicationException("Url could not be determined for Site Marker named 'Conference Registration'");
			}

			var urlBuilder = new UrlBuilder(url);

			urlBuilder.QueryString.Add(ConferenceIdQueryStringParameterName, conferenceId.ToString());

			return urlBuilder.PathWithQueryString;
		}

		private static Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>> GetChildNodes()
		{
			var currentNode = System.Web.SiteMap.CurrentNode;

			if (currentNode == null)
			{
				return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(new SiteMapNode[] { }, new SiteMapNode[] { });
			}

			var shortcutNodes = new List<SiteMapNode>();
			var otherNodes = new List<SiteMapNode>();

			foreach (SiteMapNode childNode in currentNode.ChildNodes)
			{
				var entityNode = childNode as CrmSiteMapNode;

				if (entityNode != null && entityNode.HasCrmEntityName("adx_shortcut"))
				{
					shortcutNodes.Add(childNode);
				}
				else
				{
					otherNodes.Add(childNode);
				}
			}

			return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(otherNodes, shortcutNodes);
		}

		protected string GetSummaryPropertyName(string entityLogicalName)
		{
			if (string.IsNullOrEmpty(entityLogicalName)) return null;

			switch (entityLogicalName)
			{
				case "adx_shortcut":
					return "Adx_description";
				case "adx_webfile":
					return "Adx_Summary";
				case "adx_webpage":
					return "Adx_Summary";
				default:
					return null;
			}
		}
	}
}