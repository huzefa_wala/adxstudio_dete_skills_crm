﻿using System;
using System.Linq;
using Microsoft.Xrm.Client.Security;
using Microsoft.Xrm.Portal.Configuration;
using Site.Pages;

namespace Site.Areas.Conference.Pages
{
	public partial class ConferenceSpeakers : PortalPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var securityProvider = PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();

			Speakers.DataSource = ServiceContext.Adx_eventspeakerSet.Where(es => es.adx_websiteid.Id == Website.Id).ToArray()
				.Where(es => securityProvider.TryAssert(ServiceContext, es, CrmEntityRight.Read))
				.OrderBy(es => es.Adx_name)
				.ToArray();

			Speakers.DataBind();
		}
	}
}
