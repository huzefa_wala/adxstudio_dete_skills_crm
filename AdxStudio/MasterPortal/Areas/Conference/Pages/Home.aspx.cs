﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Web.Mvc.Html;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Site.Helpers;
using PortalContextDataAdapterDependencies = Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies;

namespace Site.Areas.Conference.Pages
{
	public partial class Home : ConferencePage
	{
		protected const string ConferenceIdQueryStringParameterName = "conferenceid";

		protected Entity NewsBlog
		{
			get
			{
				var newsBlogName = Html.Setting("News Blog Name");

				if (string.IsNullOrWhiteSpace(newsBlogName))
				{
					return null;
				}

				var newsBlog = ServiceContext.CreateQuery("adx_blog").FirstOrDefault(b => b.GetAttributeValue<Guid>("adx_websiteid") == Website.Id && b.GetAttributeValue<string>("adx_name") == newsBlogName);

				return newsBlog;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			NewsPanel.Visible = (NewsBlog != null);
		}

		protected void CreateBlogDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			args.ObjectInstance = new BlogDataAdapter(NewsBlog, new PortalContextDataAdapterDependencies(Portal, requestContext: Request.RequestContext));
		}

		protected void Register_Click(object sender, EventArgs args)
		{
			if (PortalConference == null)
			{
				return;
			}

			var registrationUrl = GetRegistrationUrl(PortalConference.Id);

			Response.Redirect(registrationUrl);
		}

		protected string GetRegistrationUrl(Guid conferenceId)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Conference Registration");

			if (page == null)
			{
				throw new ApplicationException("Page could not be found for Site Marker named 'Conference Registration'");
			}

			var url = ServiceContext.GetUrl(page);

			if (string.IsNullOrWhiteSpace(url))
			{
				throw new ApplicationException("Url could not be determined for Site Marker named 'Conference Registration'");
			}

			var urlBuilder = new UrlBuilder(url);

			urlBuilder.QueryString.Add(ConferenceIdQueryStringParameterName, conferenceId.ToString());

			return urlBuilder.PathWithQueryString;
		}
	}
}
