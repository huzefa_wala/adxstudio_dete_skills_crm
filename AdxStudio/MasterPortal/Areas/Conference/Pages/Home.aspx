﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Site.Areas.Conference.Pages.Home" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Blogs" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Adxstudio.Xrm.Collections.Generic" %>
<%@ Import Namespace="Microsoft.Xrm.Portal.IdentityModel.Configuration" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
	<div class="row">
		<div class="span4">
			<div class="activity activity-grid">
				<div>
					<crm:CrmEntityDataSource ID="ConferenceDataSource" runat="server" />
					<crm:Property DataSourceID="ConferenceDataSource" PropertyName="Adx_Summary" EditType="html" runat="server" />
				</div>
				<div class="activity header">
					<h2>
						<i class="icon-exclamation-sign"></i>
						<crm:Snippet SnippetName="Conference Home Blob Title" EditType="text" DefaultText="About" runat="server"/>
					</h2>
				</div>
				<div class="activity">
					<% if (Request.IsAuthenticated) { %>
						<% if (!UserIsRegisteredForConference) { %>
							<asp:LinkButton ID="Register" CssClass="btn btn-large btn-primary" OnClick="Register_Click" runat="server">
								<i class="icon-briefcase icon-white"></i>
								<crm:Snippet SnippetName="Conference Register Button Text" DefaultText="Register for the Conference" Literal="True" runat="server"/>
							</asp:LinkButton>
						<% } else { %>
							<crm:Snippet SnippetName="Conference Registered Text" CssClass="alert alert-info" DefaultText="You are registered for this conference." EditType="Html" runat="server"/>
						<% } %>
					<% } else { %>
		<a class="btn btn-large" href="<%: Html.SiteMarkerUrl("Login").AppendQueryString(FederationCrmConfigurationManager.GetUserRegistrationSettings().ReturnUrlKey ?? "ReturnURL", Request.Url.PathAndQuery) %>">
			<i class="icon-lock"></i>
			<crm:Snippet SnippetName="Conference Sign in to Register Button Text" DefaultText="Sign In to register for the Conference" EditType="text" runat="server"/>
		</a>
		<% } %>
					<p>&nbsp;</p>
					<crm:Snippet SnippetName="Conference Home Blob" EditType="html" DefaultText="Welcome." runat="server"/>
				</div>
			</div>
	
		</div>
		<div class="span4">
			<asp:Panel runat="server" ID="NewsPanel" Visible="False">
				<asp:ObjectDataSource ID="PostDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogDataAdapter" SelectMethod="SelectPosts" runat="server">
					<SelectParameters>
						<asp:Parameter Name="startRowIndex" DefaultValue="0"/>
						<asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Home News Post Count, 4 %>'/>
					</SelectParameters>
				</asp:ObjectDataSource>
				<asp:ListView ID="Posts" DataSourceID="PostDataSource" runat="server">
					<LayoutTemplate>
						<div class="activity">
							<asp:ObjectDataSource ID="BlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogDataAdapter" SelectMethod="Select" runat="server" />
							<div class="header">
								<asp:Repeater DataSourceID="BlogDataSource" runat="server">
									<ItemTemplate>
										<asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AbsolutePath") %>' Text='<%$ Snippet: Home All News Link Text, All News %>' runat="server" />
										<h2>
											<asp:HyperLink NavigateUrl='<%# Eval("FeedPath.AbsolutePath") %>' ImageUrl="~/img/feed-icon-14x14.png" ToolTip='<%$ Snippet: Home News Feed Subscribe Tooltip Label, Subscribe %>' runat="server" />
											<%# Eval("Title") %>
										</h2>
									</ItemTemplate>
								</asp:Repeater>
							</div>
							<ul>
								<li id="itemPlaceholder" runat="server" />
							</ul>
						</div>
					</LayoutTemplate>
					<ItemTemplate>
						<li runat="server">
							<h4>
								<asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
							</h4>
							<div>
								<abbr class="timeago"><%# Eval("PublishDate", "{0:r}") %></abbr>
								<asp:Panel runat="server" Visible='<%# (((BlogCommentPolicy)Eval("CommentPolicy")) != BlogCommentPolicy.None) %>'>
									&ndash;
									<asp:HyperLink NavigateUrl='<%# string.Format("{0}#comments", Eval("ApplicationPath.AbsolutePath")) %>' runat="server">
										<i class="icon-comment"></i> <%# Eval("CommentCount") %>
									</asp:HyperLink>
								</asp:Panel>
							</div>
							<div>
								<%# Eval("Summary") %>
							</div>
						</li>
					</ItemTemplate>
				</asp:ListView>
			</asp:Panel>
		</div>
		<div class="span4">
			<adx:PollPlacement runat="server" PlacementName="Home" CssClass="activity poll">
				<LayoutTemplate>
					<div class="header">
						<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: SiteMarker=Poll Archives, Return=Url %>' Text='<%$ Snippet: polls/archiveslabel, Poll Archives %>' runat="server" />
						<h2>
							<i class="icon-question-sign"></i>
							<crm:Snippet SnippetName="polls/title" EditType="text" DefaultText="Poll" runat="server"/>
						</h2>
					</div>
					<div class="well">
						<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
					</div>
				</LayoutTemplate>
				<PollTemplate>
					<div class="poll-question">
						<asp:PlaceHolder ID="PollQuestion" runat="server"/>
					</div>
					<div class="poll-options">
						<asp:RadioButtonList ID="PollOptions" RepeatLayout="UnorderedList" runat="server"/>
					</div>
					<div class="poll-actions">
						<asp:Button ID="PollSubmit" CssClass="btn btn-primary" Text="Submit" runat="server"/>
						<asp:LinkButton ID="PollViewResults" CssClass="btn" Text='<%$ Snippet: polls/resultslabel, View Results %>' runat="server"/>
					</div>
				</PollTemplate>
				<ResultsTemplate>
					<div class="poll-question">
						<asp:PlaceHolder ID="PollQuestion" runat="server"/>
					</div>
					<div class="poll-results">
						<asp:PlaceHolder ID="PollResults" runat="server"/>
					</div>
					<div>
						<crm:Snippet SnippetName="polls/totalslabel" EditType="text" DefaultText="Total Votes:" runat="server"/>
						<asp:PlaceHolder ID="PollTotalVotes" runat="server"/>
					</div>
				</ResultsTemplate>
			</adx:PollPlacement>
		</div>
	</div>
</asp:Content>
