﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebFormsContent.master" AutoEventWireup="true" CodeBehind="ConferenceSpeakers.aspx.cs" Inherits="Site.Areas.Conference.Pages.ConferenceSpeakers" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm" %>
<%@ Import Namespace="Microsoft.Xrm.Sdk" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Conference/css/events.css") %>">
	<link rel="profile" href="http://microformats.org/profile/hcalendar">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<asp:ListView ID="Speakers" runat="server">
		<LayoutTemplate>
			<h3><crm:Snippet SnippetName="Speakers Heading" DefaultText="Speakers" EditType="text" runat="server" /></h3>
			<ul class="speakers">
					<li id="ItemPlaceholder" runat="server" />
				</ul>
		</LayoutTemplate>
		<ItemTemplate>
			
				<div class="well row">
				<div class="pull-right">
						<asp:LinqDataSource ID="CrmNoteSource" runat="server" ContextTypeName="Xrm.XrmServiceContext" TableName="AnnotationSet"
						Where="ObjectId.Id == @EventSpeakerID && isdocument == true" OrderBy="CreatedOn" OnSelecting="LinqDataSourceSelecting">
							<WhereParameters>
								<asp:ControlParameter ControlID="CurrentEventSpeakerID" Name="EventSpeakerID" DefaultValue="00000000-0000-0000-0000-000000000000" DbType="Guid" />
							</WhereParameters>
						</asp:LinqDataSource>
						<asp:Repeater DataSourceID="CrmNoteSource" runat="server">
						<ItemTemplate>
							<asp:Image CssClass="thumbnail" ImageUrl='<%# ((Entity)Container.DataItem).GetFileAttachmentUrl(Website) %>' runat="server" />
						</ItemTemplate>
					</asp:Repeater>
				</div>
					<div class="">
				<crm:CrmEntityDataSource ID="Speaker" DataItem="<%# Container.DataItem %>" runat="server" />
						<h4>
							<crm:Property DataSourceID="Speaker" PropertyName="Adx_name" EditType="text" runat="server" />
						</h4>
						<asp:HyperLink CssClass="speaker-url" NavigateUrl='<%# Eval("Adx_Url") %>' runat="server">
							<crm:Property DataSourceID="Speaker" PropertyName="Adx_Url" EditType="text" runat="server" />
						</asp:HyperLink>
						<asp:HiddenField runat="server" ID="CurrentEventSpeakerID" Value='<%# Eval("Adx_eventspeakerId") %>' />
						<crm:Property DataSourceID="Speaker" PropertyName="Adx_Description" EditType="html" runat="server" />
					</div>
				</div>
				<br/>
			
		</ItemTemplate>
	</asp:ListView>
	<div class="clearfix"></div>
</asp:Content>
