﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Xrm.Client.Security;
using Microsoft.Xrm.Portal.Configuration;
using Site.Pages;
using Xrm;

namespace Site.Areas.Conference.Pages
{
	public partial class Sponsors : PortalPage
	{
		private Adx_eventsponsor[] _sponsors;

		protected void Page_Init(object sender, EventArgs e)
		{
			var securityProvider = PortalCrmConfigurationManager.CreateCrmEntitySecurityProvider();

			_sponsors = ServiceContext.Adx_eventsponsorSet.Where(es => es.adx_websiteid.Id == Website.Id).ToArray()
				.Where(es => securityProvider.TryAssert(ServiceContext, es, CrmEntityRight.Read))
				.ToArray();
		}

		protected IEnumerable<Adx_eventsponsor> GetSponsorsByCategory(int? category)
		{
			return category == null ? new Adx_eventsponsor[] { } : _sponsors.Where(es => es.Adx_SponsorshipCategory == category);
		}
	}
}