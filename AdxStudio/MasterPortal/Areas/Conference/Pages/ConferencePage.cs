﻿using System;
using System.Linq;
using Adxstudio.Xrm.Conferences;
using Site.Pages;
using Xrm;

namespace Site.Areas.Conference.Pages
{
	public class ConferencePage : PortalPage
	{
		protected enum ConferenceRegistrationStatusCode
		{
			Started = 1,
			Completed = 756150000
		}

		public Adx_conference PortalConference
		{
			get { return ServiceContext.GetPortalConference(Website) as Adx_conference; }
		}

		public Adx_conferenceregistration UserRegistration
		{
			get
			{
				return PortalConference == null ? null : 
					ServiceContext.Adx_conferenceregistrationSet
					.FirstOrDefault(cr => (cr.adx_conferenceid.Id == PortalConference.Id)
						&& cr.statuscode == (int)ConferenceRegistrationStatusCode.Completed
						&& (cr.adx_contactid.Id == (Contact == null ? Guid.Empty : Contact.Id)));
			}
		}

		public bool UserIsRegisteredForConference
		{
			get { return UserRegistration != null; }
		}
	}
}
