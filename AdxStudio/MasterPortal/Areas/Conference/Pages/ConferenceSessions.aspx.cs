﻿using System;
using System.Linq;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Conferences;

namespace Site.Areas.Conference.Pages
{
	public partial class ConferenceSessions : ConferencePage
	{
		protected void Page_Load(object sender, EventArgs args)
		{
			var dataAdapter = new ConferenceEventDataAdapter(new PortalContextDataAdapterDependencies(Portal, PortalName), PortalConference);

			var occurrences = dataAdapter.SelectEventOccurrences(
				PortalConference.Adx_StartingDate ?? DateTime.MinValue,
				PortalConference.Adx_EndDate ?? DateTime.MaxValue)
				.ToArray();

			UpcomingEvents.DataSource = occurrences
				.OrderBy(e => e.Start);

			UpcomingEvents.DataBind();
		}
	}
}
