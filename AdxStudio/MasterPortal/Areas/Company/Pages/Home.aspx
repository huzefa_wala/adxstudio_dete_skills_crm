<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" Codebehind="Home.aspx.cs" Inherits="Site.Areas.Company.Pages.HomePage" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
	
	<div class="row">
		<div class="span4">
			<div class="activity">
				<div class="header">
					<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: SiteMarker=Products, Return=Url %>' Text='<%$ Snippet: Home All Products Link Text, All Products %>' runat="server" />
					<h2>
						<crm:Snippet SnippetName="Products Home Title" EditType="text" DefaultText="Products" runat="server"/>
					</h2>
				</div>
				<asp:SiteMapDataSource ID="ProductsData" StartingNodeUrl='<%$ CrmSiteMap: SiteMarker=Products, Url %>' ShowStartingNode="False" runat="server"/>
				<asp:ListView DataSourceID="ProductsData" runat="server">
					<LayoutTemplate>
						<ul>
							<li ID="itemPlaceholder" runat="server"/>
						</ul>
					</LayoutTemplate>
					<ItemTemplate>
						<li>
							<crm:CrmEntityDataSource ID="ProductPage" DataItem="<%# Container.DataItem %>" runat="server" />
							<h4>
								<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' runat="server">
									<crm:Property DataSourceID="ProductPage" PropertyName="Adx_Title,Adx_name" Literal="true" runat="server" />
								</asp:HyperLink>
							</h4>
							<crm:Property DataSourceID="ProductPage" PropertyName="Adx_Summary" EditType="html" runat="server" />
						</li>
					</ItemTemplate>
				</asp:ListView>
			</div>
		</div>
		<div class="span4">
			<div class="activity">
				<div class="header">
					<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: SiteMarker=News, Return=Url %>' Text='<%$ Snippet: Home All News Link Text, All News %>' runat="server" />
					<h2>
						<a href="<%: Url.RouteUrl("NewsFeed") %>" title="<%: Html.SnippetLiteral("News Subscribe Heading", "Subscribe") %>"><img src="<%: Url.Content("~/img/feed-icon-14x14.png") %>" alt=""/></a>
						<crm:Snippet SnippetName="News Home Title" EditType="text" DefaultText="News" runat="server"/>
					</h2>
				</div>
				<asp:SiteMapDataSource ID="NewsData" StartingNodeUrl='<%$ CrmSiteMap: SiteMarker=News, Url %>' ShowStartingNode="False" runat="server"/>
				<asp:ListView DataSourceID="NewsData" runat="server">
					<LayoutTemplate>
						<ul>
							<li ID="itemPlaceholder" runat="server"/>
						</ul>
					</LayoutTemplate>
					<ItemTemplate>
						<li>
							<crm:CrmEntityDataSource ID="NewsPage" DataItem="<%# Container.DataItem %>" runat="server" />
							<h4>
								<asp:HyperLink NavigateUrl='<%# Eval("Url") %>' runat="server">
									<crm:Property DataSourceID="NewsPage" PropertyName="Adx_Title,Adx_name" Literal="true" runat="server" />
								</asp:HyperLink>
							</h4>
							<div>
								<abbr class="timeago"><%# Eval("Entity.Adx_DisplayDate", "{0:r}") %></abbr>
							</div>
							<crm:Property DataSourceID="NewsPage" PropertyName="Adx_Summary" EditType="html" runat="server" />
						</li>
					</ItemTemplate>
				</asp:ListView>
			</div>
		</div>
		<div class="span4">
			<adx:PollPlacement runat="server" PlacementName="Home" CssClass="activity poll">
				<LayoutTemplate>
					<div class="header">
						<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: SiteMarker=Poll Archives, Return=Url %>' Text='<%$ Snippet: polls/archiveslabel, Poll Archives %>' runat="server" />
						<h2>
							<i class="icon-question-sign"></i>
							<crm:Snippet SnippetName="polls/title" EditType="text" DefaultText="Poll" runat="server"/>
						</h2>
					</div>
					<div class="well">
						<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
					</div>
				</LayoutTemplate>
				<PollTemplate>
					<div class="poll-question">
						<asp:PlaceHolder ID="PollQuestion" runat="server"/>
					</div>
					<div class="poll-options">
						<asp:RadioButtonList ID="PollOptions" RepeatLayout="UnorderedList" runat="server"/>
					</div>
					<div class="poll-actions">
						<asp:Button ID="PollSubmit" CssClass="btn btn-primary" Text="Submit" runat="server"/>
						<asp:LinkButton ID="PollViewResults" CssClass="btn" Text='<%$ Snippet: polls/resultslabel, View Results %>' runat="server"/>
					</div>
				</PollTemplate>
				<ResultsTemplate>
					<div class="poll-question">
						<asp:PlaceHolder ID="PollQuestion" runat="server"/>
					</div>
					<div class="poll-results">
						<asp:PlaceHolder ID="PollResults" runat="server"/>
					</div>
					<div>
						<crm:Snippet SnippetName="polls/totalslabel" EditType="text" DefaultText="Total Votes:" runat="server"/>
						<asp:PlaceHolder ID="PollTotalVotes" runat="server"/>
					</div>
				</ResultsTemplate>
			</adx:PollPlacement>
		</div>
	</div>
</asp:Content>
