﻿<%@ Page Language="C#" MasterPageFile="../MasterPages/Blogs.master" AutoEventWireup="true" CodeBehind="BlogPost.aspx.cs" ValidateRequest="false" Inherits="Site.Areas.Blogs.Pages.BlogPost" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Blogs" %>
<%@ Import Namespace="Adxstudio.Xrm.Web" %>
<%@ Import Namespace="Site.Helpers" %>
<%@ Register src="~/Controls/Comments.ascx" tagname="Comments" tagprefix="adx" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<asp:ObjectDataSource ID="PostHeaderDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogPostDataAdapter" OnObjectCreating="CreateBlogPostDataAdapter" SelectMethod="Select" runat="server" />
	<asp:ListView ID="PostHeader" DataSourceID="PostHeaderDataSource" runat="server">
		<LayoutTemplate>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
		</LayoutTemplate>
		<ItemTemplate>
			<crm:CrmEntityDataSource ID="Post" DataItem='<%# Eval("Entity") %>' runat="server" />
			<div class="page-header">
				<div class="pull-right">
					<asp:HyperLink CssClass="user-avatar" NavigateUrl='<%# Url.AuthorUrl(Eval("Author") as IBlogAuthor) %>' ImageUrl='<%# Gravatar.Url(Eval("Author.EmailAddress")) %>' ToolTip='<%# HttpUtility.HtmlEncode(Eval("Author.Name") ?? "") %>' runat="server"/>
				</div>
				<h1><crm:Property DataSourceID="Post" PropertyName="Adx_name" EditType="text" runat="server" /></h1>
			</div>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<asp:ObjectDataSource ID="PostDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogPostDataAdapter" OnObjectCreating="CreateBlogPostDataAdapter" SelectMethod="Select" runat="server" />
	<asp:ListView ID="Post" DataSourceID="PostDataSource" runat="server">
		<LayoutTemplate>
			<asp:PlaceHolder ID="itemPlaceholder" runat="server" />
		</LayoutTemplate>
		<ItemTemplate>
			<div class='<%# "blog-post" + ((bool)Eval("IsPublished") ? "" : " unpublished") %>' runat="server">
				<asp:Panel CssClass="alert alert-block alert-info" Visible='<%# !(bool)Eval("IsPublished") %>' runat="server" >
					<asp:Label Text='<%$ Snippet: Unpublished Post Message, This post is not published. It is only visible to you. %>' runat="server"></asp:Label>
				</asp:Panel>
				<crm:CrmEntityDataSource ID="Post" DataItem='<%# Eval("Entity") %>' runat="server" />
				<div class="metadata">
					<asp:HyperLink NavigateUrl='<%# Url.AuthorUrl(Eval("Author") as IBlogAuthor) %>' Text='<%# Eval("Author.Name") %>' runat="server" />
					&ndash;
					<abbr class="timeago"><%# Eval("Entity.Adx_date", "{0:r}") %></abbr>
					<asp:Label Visible='<%# ((BlogCommentPolicy)Eval("CommentPolicy")) != BlogCommentPolicy.None %>' runat="server">
						&ndash;
						<a href="#comments">
							<i class="icon-comment"></i> <%# Eval("CommentCount") %>
						</a>
					</asp:Label>
					<adx:MultiRatingControl ID="Rating" InitEventName="OnLoad" RatingType="rating" runat="server" />
				</div>
				<div>
					<asp:Panel Visible='<%# Eval("HasExcerpt") %>' runat="server">
						<crm:Property DataSourceID="Post" PropertyName="Adx_summary" EditType="html" runat="server" />
					</asp:Panel>
					<a class="anchor" name="extended"></a>
					<crm:Property DataSourceID="Post" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
				</div>
				<div>
					<asp:ListView runat="server" DataSource='<%# Eval("Tags") %>'>
						<LayoutTemplate>
							<ul class="tags">
								<li><i class="icon-tags"></i></li>
								<li id="itemPlaceholder" runat="server" />
							</ul>
						</LayoutTemplate>
						<ItemTemplate>
							<li runat="server">
								<asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' Text='<%# Eval("Name") %>' runat="server"></asp:HyperLink>
							</li>
						</ItemTemplate>
					</asp:ListView>
				</div>
			</div>
		</ItemTemplate>
	</asp:ListView>
	<adx:Comments EnableRatings="true" runat="server" />
</asp:Content>
