﻿<%@ Page Language="C#" MasterPageFile="../MasterPages/Blogs.master" AutoEventWireup="true" CodeBehind="Blogs.aspx.cs" Inherits="Site.Areas.Blogs.Pages.Blogs" %>
<%@ OutputCache CacheProfile="User" %>

<asp:Content ContentPlaceHolderID="Breadcrumbs" runat="server"/>

<asp:Content ContentPlaceHolderID="PageHeader" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<asp:ObjectDataSource ID="BlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogAggregationDataAdapter" SelectMethod="Select" runat="server" />
	<div class="page-header">
		<asp:ListView DataSourceID="BlogDataSource" runat="server">
			<LayoutTemplate>
				<div class="pull-right">
					<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
				</div>
			</LayoutTemplate>
			<ItemTemplate>
				<asp:HyperLink NavigateUrl='<%# Eval("FeedPath.AbsolutePath") %>' ImageUrl="~/img/feed-icon-28x28.png" ToolTip='<%$ Snippet: Blog Subscribe Heading, Subscribe %>' runat="server" />
			</ItemTemplate>
		</asp:ListView>
		<h1>
			<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
		</h1>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarTop" runat="server">
	<asp:ObjectDataSource ID="BlogAggregationDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogAggregationDataAdapter" OnObjectCreating="CreateBlogAggregationDataAdapter" SelectMethod="SelectBlogs" runat="server" />
	<asp:ListView DataSourceID="BlogAggregationDataSource" runat="server">
		<LayoutTemplate>
			<ul class="nav nav-list">
				<li class="nav-header">
					<crm:Snippet SnippetName="Blogs Heading" DefaultText="Blogs" runat="server" />
				</li>
				<li id="itemPlaceholder" runat="server" />
			</ul>
		</LayoutTemplate>
		<ItemTemplate>
			<li runat="server">
				<asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AbsolutePath") %>' Text='<%# Eval("Title") %>' runat="server"/>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>
