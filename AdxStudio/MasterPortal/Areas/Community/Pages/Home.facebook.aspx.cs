﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Forums;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using PortalContextDataAdapterDependencies = Adxstudio.Xrm.Forums.PortalContextDataAdapterDependencies;

namespace Site.Areas.Community.Pages
{
	public partial class Home_facebook : PortalPage
	{
		private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext());

		protected void Page_Load(object sender, EventArgs e) {}

		protected void CreateBlogDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var newsBlogName = _portal.Value.ServiceContext.GetSiteSettingValueByName(_portal.Value.Website, "News Blog Name");

			args.ObjectInstance = 
				new WebsiteBlogAggregationDataAdapter(
					new Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies(_portal.Value, requestContext:Request.RequestContext),
					null,
					serviceContext => GetAllBlogPostsInWebsiteExceptNews(ServiceContext, Website.Id, newsBlogName));
		}

		protected void CreateForumDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			args.ObjectInstance = new WebsiteForumDataAdapter(new PortalContextDataAdapterDependencies(
				_portal.Value,
				new PaginatedLatestPostUrlProvider("page", 20)));
		}

		protected IQueryable<Entity> GetAllBlogPostsInWebsiteExceptNews(OrganizationServiceContext serviceContext, Guid websiteId, string newsBlogName)
		{
			var query = from post in serviceContext.CreateQuery("adx_blogpost")
				join blog in serviceContext.CreateQuery("adx_blog") on post.GetAttributeValue<Guid>("adx_blogid") equals blog.GetAttributeValue<Guid>("adx_blogid")
				where blog.GetAttributeValue<EntityReference>("adx_websiteid").Id == websiteId
				where blog.GetAttributeValue<string>("adx_name") != newsBlogName
				where post.GetAttributeValue<bool?>("adx_published") == true
				orderby post.GetAttributeValue<DateTime?>("adx_date") descending
				select post;

			return query;
		}
	}
}