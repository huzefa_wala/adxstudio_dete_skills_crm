﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Blogs;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Events;
using Adxstudio.Xrm.Forums;
using Adxstudio.Xrm.Web.Mvc.Html;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Site.Pages;
using PortalContextDataAdapterDependencies = Adxstudio.Xrm.Blogs.PortalContextDataAdapterDependencies;

namespace Site.Areas.Community.Pages
{
	public partial class Home : PortalPage
	{
		private readonly Lazy<IPortalContext> _portal = new Lazy<IPortalContext>(() => PortalCrmConfigurationManager.CreatePortalContext());

		protected Entity NewsBlog
		{
			get
			{
				var newsBlogName = _portal.Value.ServiceContext.GetSiteSettingValueByName(_portal.Value.Website, "News Blog Name");

				if (string.IsNullOrWhiteSpace(newsBlogName))
				{
					return null;
				}

				var newsBlog = _portal.Value.ServiceContext.CreateQuery("adx_blog").FirstOrDefault(b => b.GetAttributeValue<Guid>("adx_websiteid") == _portal.Value.Website.Id && b.GetAttributeValue<string>("adx_name") == newsBlogName);

				return newsBlog;
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			NewsPanel.Visible = (NewsBlog != null);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var dataAdapter = new WebsiteEventDataAdapter(new Adxstudio.Xrm.Cms.PortalContextDataAdapterDependencies(_portal.Value));
			var now = DateTime.UtcNow;

			var future = Html.TimeSpanSetting("Events/DisplayTimeSpan/Future").GetValueOrDefault(TimeSpan.FromDays(90));

			UpcomingEvents.DataSource = dataAdapter.SelectEventOccurrences(now, now.Add(future)).Take(3);
			UpcomingEvents.DataBind();
		}
		protected void CreateNewsDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			args.ObjectInstance = new BlogDataAdapter(NewsBlog, new PortalContextDataAdapterDependencies(_portal.Value, requestContext: Request.RequestContext));
		}

		protected void CreateBlogDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			var newsBlogName = _portal.Value.ServiceContext.GetSiteSettingValueByName(_portal.Value.Website, "News Blog Name");

			args.ObjectInstance = 
				new WebsiteBlogAggregationDataAdapter(
					new PortalContextDataAdapterDependencies(_portal.Value, requestContext:Request.RequestContext),
					null,
					serviceContext => GetAllBlogPostsInWebsiteExceptNews(ServiceContext, Website.Id, newsBlogName)
					);
		}

		protected void CreateForumDataAdapter(object sender, ObjectDataSourceEventArgs args)
		{
			args.ObjectInstance = new WebsiteForumDataAdapter(new Adxstudio.Xrm.Forums.PortalContextDataAdapterDependencies(
				_portal.Value,
				new PaginatedLatestPostUrlProvider("page", 20)));
		}

		protected IQueryable<Entity> GetAllBlogPostsInWebsiteExceptNews(OrganizationServiceContext serviceContext, Guid websiteId, string newsBlogName)
		{
			var query = from post in serviceContext.CreateQuery("adx_blogpost")
						join blog in serviceContext.CreateQuery("adx_blog") on post.GetAttributeValue<Guid>("adx_blogid") equals blog.GetAttributeValue<Guid>("adx_blogid")
						where blog.GetAttributeValue<EntityReference>("adx_websiteid").Id == websiteId
						where blog.GetAttributeValue<string>("adx_name") != newsBlogName
						where post.GetAttributeValue<bool?>("adx_published") == true
						orderby post.GetAttributeValue<DateTime?>("adx_date") descending
						select post;

			return query;
		}
	}
}
