﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="Home.facebook.aspx.cs" Inherits="Site.Areas.Community.Pages.Home_facebook" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Adxstudio.Xrm.Blogs" %>
<%@ Import Namespace="Adxstudio.Xrm.Web" %>
<%@ Import namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Register src="~/Areas/HelpDesk/Controls/CaseDeflection.ascx" tagname="CaseDeflection" tagprefix="adx" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/HelpDesk/css/helpdesk.css") %>">
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server" ViewStateMode="Enabled">
	<%: Html.HtmlSnippet("Facebook/Home/Content") %>
	<adx:CaseDeflection ID="CaseDeflection" runat="server"/>
	<div class="row">
		<div class="span8">
			<asp:ObjectDataSource ID="PostDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogDataAdapter" SelectMethod="SelectPosts" runat="server">
				<SelectParameters>
					<asp:Parameter Name="startRowIndex" DefaultValue="0"/>
					<asp:Parameter Name="maximumRows" DefaultValue='<%$ SiteSetting: Home Blog Post Count, 4 %>'/>
				</SelectParameters>
			</asp:ObjectDataSource>
			<asp:ListView ID="Posts" DataSourceID="PostDataSource" runat="server">
				<LayoutTemplate>
					<div class="activity activity-grid">
						<asp:ObjectDataSource ID="BlogDataSource" TypeName="Adxstudio.Xrm.Blogs.IBlogDataAdapter" OnObjectCreating="CreateBlogDataAdapter" SelectMethod="Select" runat="server" />
						<div class="header">
							<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: SiteMarker=Blog Home, Return=Url %>' Text='<%$ Snippet: Home All Blogs Link Text, All Blogs %>' runat="server" />
							<h2>
								<asp:Repeater DataSourceID="BlogDataSource" runat="server">
									<ItemTemplate>
										<asp:HyperLink NavigateUrl='<%# Eval("FeedPath.AbsolutePath") %>' ImageUrl="~/img/feed-icon-14x14.png" ToolTip='<%$ Snippet: Blog Subscribe Heading, Subscribe %>' runat="server" />
									</ItemTemplate>
								</asp:Repeater>
								<crm:Snippet SnippetName="Home Blog Activity Heading" DefaultText="Blogs" EditType="text" runat="server" />
							</h2>
						</div>
						<ul>
							<li id="itemPlaceholder" runat="server" />
						</ul>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<li runat="server">
						<asp:HyperLink CssClass="user-avatar" NavigateUrl='<%# Url.AuthorUrl(Eval("Author") as IBlogAuthor) %>' ImageUrl='<%# Gravatar.Url(Eval("Author.EmailAddress")) %>' ToolTip='<%# HttpUtility.HtmlEncode(Eval("Author.Name") ?? "") %>' runat="server"/>
						<h4>
							<asp:HyperLink NavigateUrl='<%# Eval("ApplicationPath.AppRelativePath") %>' runat="server"><%# Eval("Title") %></asp:HyperLink>
						</h4>
						<div>
							<abbr class="timeago"><%# Eval("Entity.Adx_date", "{0:r}") %></abbr>
							&ndash;
							<asp:HyperLink NavigateUrl='<%# Url.AuthorUrl(Eval("Author") as IBlogAuthor) %>' Text='<%# HttpUtility.HtmlEncode(Eval("Author.Name") ?? "") %>' runat="server" />
							&ndash;
							<asp:HyperLink NavigateUrl='<%# string.Format("{0}#comments", Eval("ApplicationPath.AbsolutePath")) %>' runat="server">
								<i class="icon-comment"></i> <%# Eval("CommentCount") %>
							</asp:HyperLink>
						</div>
					</li>
				</ItemTemplate>
			</asp:ListView>

			<asp:ObjectDataSource ID="ForumsDataSource" TypeName="Adxstudio.Xrm.Forums.IForumAggregationDataAdapter" OnObjectCreating="CreateForumDataAdapter" SelectMethod="SelectForums" runat="server" />
			<asp:ListView DataSourceID="ForumsDataSource" runat="server">
				<LayoutTemplate>
					<div class="activity activity-grid">
						<div class="header">
							<h2>
								<i class="icon-comment"></i>
								<crm:Snippet SnippetName="Home Forum Activity Heading" DefaultText="Forums" EditType="text" runat="server" />
							</h2>
						</div>
						<ul>
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
						</ul>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<li>
						<div class="row">
							<div class="span4">
								<h4><asp:HyperLink NavigateUrl='<%# Eval("Url") %>' Text='<%# Eval("Name") %>' runat="server"/></h4>
								<div><%# Eval("Description") %></div>
							</div>
							<div class="span2"><%# Eval("ThreadCount") %> threads</div>
							<div class="span2"><%# Eval("PostCount") %> posts</div>
						</div>
					</li>
				</ItemTemplate>
			</asp:ListView>
		</div>
		<div class="span4">
			<adx:PollPlacement PlacementName="Home" runat="server">
				<LayoutTemplate>
					<div class="activity poll">
						<div class="header">
							<asp:HyperLink NavigateUrl='<%$ CrmSiteMap: SiteMarker=Poll Archives, Return=Url %>' Text='<%$ Snippet: polls/archiveslabel, Poll Archives %>' runat="server" />
							<h2>
								<i class="icon-question-sign"></i>
								<crm:Snippet SnippetName="polls/title" EditType="text" DefaultText="Poll" runat="server"/>
							</h2>
						</div>
						<div class="well">
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
						</div>
					</div>
				</LayoutTemplate>
				<PollTemplate>
					<div class="poll-question">
						<asp:PlaceHolder ID="PollQuestion" runat="server"/>
					</div>
					<div class="poll-options">
						<asp:RadioButtonList ID="PollOptions" RepeatLayout="UnorderedList" runat="server"/>
					</div>
					<div class="poll-actions">
						<asp:Button ID="PollSubmit" CssClass="btn btn-primary" Text="Submit" runat="server"/>
						<asp:LinkButton ID="PollViewResults" CssClass="btn" Text='<%$ Snippet: polls/resultslabel, View Results %>' runat="server"/>
					</div>
				</PollTemplate>
				<ResultsTemplate>
					<div class="poll-question">
						<asp:PlaceHolder ID="PollQuestion" runat="server"/>
					</div>
					<div class="poll-results">
						<asp:PlaceHolder ID="PollResults" runat="server"/>
					</div>
					<div>
						<crm:Snippet SnippetName="polls/totalslabel" EditType="text" DefaultText="Total Votes:" runat="server"/>
						<asp:PlaceHolder ID="PollTotalVotes" runat="server"/>
					</div>
				</ResultsTemplate>
			</adx:PollPlacement>
		</div>
	</div>
</asp:Content>
