﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebFormsContent.master" CodeBehind="ContactUs.aspx.cs" Inherits="Site.Areas.ContactUs.Pages.ContactUs" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha, Version=1.0.5.0, Culture=neutral, PublicKeyToken=9afc4d65b28c38c2" %>
<%@ Register TagPrefix="site" Namespace="Site.Controls" Assembly="Site" %>
<%@ Import Namespace="Site.Helpers" %>

<%--
To enable the Recaptcha validation control:
1. Sign up for an API key at http://www.google.com/recaptcha/whyrecaptcha
2. Add two new site settings to the website with the names "/Recaptcha/PublicKey" and "/Recaptcha/PrivateKey" to hold the public and private keys
--%>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/css/webforms.css">
</asp:Content>

<asp:Content  ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	
		<crm:CrmDataSource ID="WebFormDataSource" runat="server" />
		<adx:CrmEntityFormView runat="server" ID="FormView" DataSourceID="WebFormDataSource" 
			CssClass="crmEntityFormView" 
			FormName="Web Form" 
			EntityName="lead" 
			OnItemInserted="OnItemInserted" 
			ValidationGroup="ContactUs"
			ValidationSummaryCssClass="alert alert-error alert-block">
			<InsertItemTemplate>
				<div class="row">
					<div class="cell">
						<div class="info">
							<asp:CustomValidator
								ID="RecaptchaValidator"
								runat="server"
								ValidationGroup="ContactUs"
								ErrorMessage="reCAPTCHA is invalid. Please try again."
								OnServerValidate="RecaptchaValidator_ServerValidate" Display="None" />
						</div>
						<div class="control">
							<recaptcha:RecaptchaControl
								ID="RecaptchaImage"
								runat="server"
								PublicKey='<%$ SiteSetting: Recaptcha/PublicKey, null %>'
								PrivateKey='<%$ SiteSetting: Recaptcha/PrivateKey, null %>' />
						</div>
					</div>
				</div>
				<div class="actions">
					<asp:Button ID="SubmitButton" Text='<%$ Snippet: ContactUs/Submit, Submit %>' CssClass="btn btn-primary" 
						CommandName="Insert" ValidationGroup="ContactUs" runat="server" />
				</div>
			</InsertItemTemplate>
		</adx:CrmEntityFormView>

	<asp:Panel ID="ConfirmationMessage" runat="server" Visible="false">
		<crm:Snippet runat="server" SnippetName="ContactUs/ConfirmationMsg" DefaultText="Thank you." EditType="html" />
	</asp:Panel>
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarAbove" runat="server">
	<site:ChildNavigation runat="server"/>
	
	<div class="well sidebar">
		<div class="section">
			<h4>
				<asp:Literal Text="<%$ Snippet: contact-us/instructions, Instructions %>" runat="server" /></h4>
			<crm:CrmEntityDataSource ID="CurrentEntity1" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
			<crm:Property DataSourceID="CurrentEntity1" PropertyName="Adx_Copy" CssClass="page-copy" EditType="html" runat="server" />
		</div>
	</div>
</asp:Content>

