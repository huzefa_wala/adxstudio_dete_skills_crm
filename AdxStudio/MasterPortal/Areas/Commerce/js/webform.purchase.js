﻿
$(function () {

  $.unblockUI();
  
  function getRules() {
    var rules = {};

    $('input.required').each(function() {
      rules[$(this).attr('id')] = { required: true };
    });

    return rules;
  }
  
  $("#content_form").validate({
    rules: getRules(),
    errorClass: "help-inline error",
    errorPlacement: function (error, element) {
      error.insertAfter(element);
    },
    highlight: function (label) {
      $(label).closest('.control-group').removeClass('success').addClass('error');
    },
    success: function (label) {
      $(label).closest('.control-group').removeClass('error').addClass('success');
    },
    debug: true
  });

});
