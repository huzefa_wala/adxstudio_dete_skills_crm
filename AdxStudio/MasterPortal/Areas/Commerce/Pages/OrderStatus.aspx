﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Profile.master" CodeBehind="OrderStatus.aspx.cs" Inherits="Site.Areas.Commerce.Pages.OrderStatus" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>
<%@ Import namespace="Adxstudio.Xrm" %>
<%@ Import Namespace="Adxstudio.Xrm.Core" %>
<%@ Import Namespace="Adxstudio.Xrm.Web.Mvc.Html" %>
<%@ Import Namespace="Microsoft.Xrm.Client" %>
<%@ Import Namespace="Microsoft.Xrm.Portal.Core" %>
<%@ Import Namespace="Microsoft.Xrm.Sdk" %>
<%@ Import Namespace="Xrm" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/css/webforms.css") %>" />
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Commerce/css/commerce.css") %>" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" ViewStateMode="Enabled" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	
	<asp:Panel ID="GenericError" Visible="False" CssClass="alert alert-block alert-error" runat="server">
		<crm:Snippet SnippetName="Ecommerce/OrderGenericErrorMessage" DefaultText="There is an error viewing your order. Please contact support." EditType="html" runat="server"/>
	</asp:Panel>
	
	<asp:Panel ID="UpdateSuccessMessage" runat="server" CssClass="alert alert-success alert-block" Visible="False">
		<a class="close" data-dismiss="alert" href="#">×</a>
		<crm:Snippet runat="server" SnippetName="Ecommerce/OrderUpdateSuccessMessage" DefaultText="Your order has been updated successfully." Editable="true" EditType="html" />
	</asp:Panel>

	<asp:Panel ID="OrderHeader" runat="server">
		<ul class="breadcrumb">
			<% foreach (var node in Html.SiteMapPath()) { %>
				<% if (node.Item2 == SiteMapNodeType.Current) { %>
					<li class="active"><%: OrderToEdit.OrderNumber %></li>
				<% } else { %>
					<li>
						<a href="<%: node.Item1.Url %>"><%: node.Item1.Title %></a>
						<span class="divider">/</span>
					</li>
				<% } %>
			<% } %>
		</ul>

		<div class="modal hide" id="add-note" tabindex="-1" role="dialog" aria-labelledby="add-note-modal-label" aria-hidden="true">
					<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="add-note-modal-label">
					<crm:Snippet  runat="server" SnippetName="Ecommerce/Order/AddNote/ButtonText" DefaultText="Add Note" Editable="true" EditType="text"/>
				</h3>
					</div>
					<div class="modal-body form-horizontal">
				<div class="control-group">
					<asp:Label AssociatedControlID="NewNoteText" CssClass="control-label" runat="server">
						<crm:Snippet runat="server" SnippetName="Ecommerce/Order/AddNote/Text" DefaultText="Note" />
							</asp:Label>
					<div class="controls">
						<asp:TextBox runat="server" ID="NewNoteText" TextMode="MultiLine" Rows="6" CssClass="input-xlarge"/>
							</div>
						</div>
				<div class="control-group">
					<asp:Label AssociatedControlID="NewNoteText" CssClass="control-label" runat="server">
						<crm:Snippet runat="server" SnippetName="cEcommerce/Order/AddNote/File" DefaultText="Attach a file" />
							</asp:Label>
					<div class="controls">
									<asp:FileUpload ID="NewNoteAttachment" runat="server"/>
								</div>
							</div>
						</div>
					<div class="modal-footer">
						<asp:Button CssClass="btn btn-primary" OnClick="AddNote_Click" Text='<%$ Snippet: Ecommerce/Order/AddNote/ButtonText, Add Note %>' runat="server" />
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<crm:Snippet  runat="server" SnippetName="Ecommerce/Order/AddNote/CancelButtonText" DefaultText="Cancel" Literal="True" EditType="text"/>
						</button>
					</div>
				</div>
		
		<div class="page-header">
		<asp:Panel ID="OrderControls" CssClass="pull-right btn-toolbar" runat="server">
			<asp:Panel ID="AddNote" CssClass="btn-group" runat="server">
					<a href="#add-note" role="modal" class="btn" data-toggle="modal">
						<i class="icon-plus-sign"></i>
						<crm:Snippet runat="server" SnippetName="Ecommerce/Order/AddNote/ButtonText" DefaultText="Add Note" Literal="true" EditType="text"/>
				</a>
			</asp:Panel>
		</asp:Panel>
		<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			<small>
					<%: OrderToEdit.OrderNumber %>
			</small>
		</h1>
		</div>

		<div class="commerce-status clearfix">
			<span class="<%: "label {0}".FormatWith(GetLabelClassForOrder(OrderToEdit)) %>"><%: XrmContext.GetOptionSetValueLabel("salesorder", "statuscode", OrderToEdit.StatusCode) %></span>
			<span>
				<crm:Snippet SnippetName="Created Label" DefaultText="Created" EditType="text" runat="server"/>
				<abbr class="timeago"><%: "{0:r}".FormatWith(OrderToEdit.CreatedOn) %></abbr>
			</span>
		</div>
		
		<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />
	</asp:Panel>

	<asp:Panel ID="OrderDetails" CssClass="commerce-details" runat="server" Visible="true">
		<asp:Panel ID="ShoppingCartSummary" CssClass="well shopping-cart-summary" runat="server">
			<ul class="unstyled">
				<asp:Repeater ID="CartRepeater" runat="server">
					<ItemTemplate>
						<li class="shopping-cart-item clearfix">
							<div class="pull-right">
								<asp:Label ID="Quantity" runat="server" Text='<%# ((Adx_shoppingcartitem)Container.DataItem).Adx_Quantity.GetValueOrDefault().ToString("N0") %>' />
								&times;
								<%# ((Adx_shoppingcartitem)Container.DataItem).Adx_QuotedPrice.GetValueOrDefault().ToString("C2") %>
							</div>
							<div class="title"><%# GetCartItemTitle((Adx_shoppingcartitem)Container.DataItem) %></div>
							<asp:TextBox ReadOnly="True" ID="CartItemID" runat="server" Visible="false" Text='<%# ((Adx_shoppingcartitem)Container.DataItem).Adx_shoppingcartitemId %>' />
						</li>
					</ItemTemplate>
				</asp:Repeater>
			</ul>
			<div class="total">
				<crm:Snippet SnippetName="Total" DefaultText="Total:" runat="server" EditType="text" />
				<asp:Label ID="Total" runat="server" />
			</div>
		</asp:Panel>
		
		<asp:Panel ID="Order" Visible="False" runat="server">
			<div class="well purchase-summary">
				<asp:ListView ID="OrderItems" runat="server">
					<LayoutTemplate>
						<ul class="unstyled">
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
						</ul>
					</LayoutTemplate>
					<ItemTemplate>
						<li class="line-item">
							<div class="pull-right">
								<asp:Label Visible='<%# ((decimal)Eval("Quantity")) > 1 %>' runat="server" >
									<asp:Label ID="Quantity" runat="server" Text='<%# Eval("Quantity", "{0:N0}") %>' />
									&times;
								</asp:Label>
								<%# Eval("PricePerUnit", "{0:C2}") %>
							</div>
							<div class="column title"><%# Eval("Name") %></div>
						</li>
					</ItemTemplate>
				</asp:ListView>
				<asp:Panel ID="OrderTotalTax" CssClass="total" runat="server">
					<crm:Snippet SnippetName="Ecommerce/Purchase/TotalTaxLabel" DefaultText="Tax:" runat="server" EditType="text" />
					<asp:Label ID="OrderTotalTaxAmount" runat="server"/>
				</asp:Panel>
				<asp:Panel ID="OrderTotalShipping" CssClass="total" runat="server">
					<crm:Snippet SnippetName="Ecommerce/Purchase/TotalShippingLabel" DefaultText="Shipping:" runat="server" EditType="text" />
					<asp:Label ID="OrderTotalShippingAmount" runat="server"/>
				</asp:Panel>
				<asp:Panel ID="OrderTotalDiscount" CssClass="total discount" runat="server">
					<crm:Snippet SnippetName="Ecommerce/Purchase/TotalDiscountLabel" DefaultText="Discount:" runat="server" EditType="text" />
					&minus;<asp:Label ID="OrderTotalDiscountAmount" runat="server"/>
				</asp:Panel>
				<asp:Panel ID="OrderTotal" CssClass="total" runat="server">
					<crm:Snippet SnippetName="Ecommerce/Purchase/TotalLabel" DefaultText="Total:" runat="server" EditType="text" />
					<asp:Label ID="OrderTotalAmount" runat="server"/>
				</asp:Panel>
			</div>
		</asp:Panel>

		<asp:Panel ID="Invoice" Visible="False" runat="server">
			<div class="well purchase-summary">
				<asp:ListView ID="InvoiceItems" runat="server">
					<LayoutTemplate>
						<ul class="unstyled">
							<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
						</ul>
					</LayoutTemplate>
					<ItemTemplate>
						<li class="line-item">
							<div class="pull-right">
								<asp:Label Visible='<%# ((decimal)Eval("Quantity")) > 1 %>' runat="server" >
									<asp:Label ID="Quantity" runat="server" Text='<%# Eval("Quantity", "{0:N0}") %>' />
									&times;
								</asp:Label>
								<%# Eval("PricePerUnit", "{0:C2}") %>
							</div>
							<div class="column title"><%# Eval("Name") %></div>
						</li>
					</ItemTemplate>
				</asp:ListView>
				<asp:Panel ID="InvoiceTotalTax" CssClass="total" runat="server">
					<crm:Snippet SnippetName="Ecommerce/Purchase/TotalTaxLabel" DefaultText="Tax:" runat="server" EditType="text" />
					<asp:Label ID="InvoiceTotalTaxAmount" runat="server"/>
				</asp:Panel>
				<asp:Panel ID="InvoiceTotalShipping" CssClass="total" runat="server">
					<crm:Snippet SnippetName="Ecommerce/Purchase/TotalShippingLabel" DefaultText="Shipping:" runat="server" EditType="text" />
					<asp:Label ID="InvoiceTotalShippingAmount" runat="server"/>
				</asp:Panel>
				<asp:Panel ID="InvoiceTotalDiscount" CssClass="total discount" runat="server">
					<crm:Snippet SnippetName="Ecommerce/Purchase/TotalDiscountLabel" DefaultText="Discount:" runat="server" EditType="text" />
					&minus;<asp:Label ID="InvoiceTotalDiscountAmount" runat="server"/>
				</asp:Panel>
				<asp:Panel ID="InvoiceTotal" CssClass="total" runat="server">
					<crm:Snippet SnippetName="Ecommerce/Purchase/TotalLabel" DefaultText="Total:" runat="server" EditType="text" />
					<asp:Label ID="InvoiceTotalAmount" runat="server"/>
				</asp:Panel>
			</div>
		</asp:Panel>

		<asp:Panel ID="OrderForm" runat="server">
			<adx:CrmEntityFormView CssClass="crmEntityFormView well readonly" runat="server" ID="FormView" EntityName="salesorder" FormName="Order Web Form" OnItemUpdating="OnItemUpdating" OnItemUpdated="OnItemUpdated" ValidationGroup="Profile" RecommendedFieldsRequired="True" ShowUnsupportedFields="False" ToolTipEnabled="False" Mode="ReadOnly"
				SubmitButtonCssClass="btn btn-primary button submit"
				SubmitButtonText='<%$ Snippet: Ecommerce/Order/UpdateOrder, Update Order %>'
				DataBindOnPostBack="True">
			</adx:CrmEntityFormView>
		</asp:Panel>
		
		<div class="page-header">
			<h3>
				<crm:Snippet SnippetName="Ecommerce/OrderNotesLabel" DefaultText="Order Notes" runat="server" EditType="text" />
			</h3>
		</div>
		
		<asp:LinqDataSource ID="CrmNoteSource" runat="server" ContextTypeName="Xrm.XrmServiceContext" TableName="AnnotationSet"
			Where="ObjectId.Id == @OrderID && NoteText.Contains(@Filter)" OrderBy="CreatedOn" OnSelecting="LinqDataSourceSelecting">
			<WhereParameters>
				<asp:QueryStringParameter Name="OrderID" QueryStringField="OrderID" DefaultValue="00000000-0000-0000-0000-000000000000" DbType="Guid" />
			</WhereParameters>
		</asp:LinqDataSource>
		
		<asp:ListView ID="NotesList" DataSourceID="CrmNoteSource" runat="server">
			<LayoutTemplate>
				<asp:PlaceHolder ID="itemPlaceholder" runat="server"/>
			</LayoutTemplate>
			<ItemTemplate>
				<div class="note">
					<div class="row">
						<div class="span3 metadata">
							<abbr class="timeago"><%# Eval("createdon", "{0:r}") %></abbr>
						</div>
						<div class="span9">
							<div class="text">
								<%# FormatNote(Eval("notetext")) %>
							</div>
							<asp:Panel Visible='<%# ((Entity)Container.DataItem).GetAttributeValue<string>("filename") != null %>' CssClass="attachment alert alert-block alert-info" runat="server">
								<span class="fa fa-file" aria-hidden="true"></span>
								<asp:HyperLink NavigateUrl='<%# (Container.DataItem as Entity).GetFileAttachmentUrl(Website) %>' Text='<%# HttpUtility.HtmlEncode(string.Format("{0} ({1:1})", ((Entity)Container.DataItem).GetAttributeValue<string>("filename"), GetFileSize(((Entity)Container.DataItem).GetAttributeValue<int?>("filesize")))) %>' runat="server"/>
							</asp:Panel>
						</div>
					</div>
				</div>
			</ItemTemplate>
		</asp:ListView>
		<asp:Panel ID="AddNoteInline" CssClass="form-actions" runat="server">
			<a href="#add-note" role="modal" class="btn" data-toggle="modal">
				<i class="icon-plus-sign"></i>
				<crm:Snippet runat="server" SnippetName="Ecommerce/Order/AddNote/ButtonText" DefaultText="Add Note" Literal="true" EditType="text"/>
				</a>
		</asp:Panel>
	</asp:Panel>
</asp:Content>
