﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebFormsContent.master" CodeBehind="ProductListing.aspx.cs" Inherits="Site.Areas.Commerce.Pages.ProductListing" %>
<%@ OutputCache CacheProfile="User" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Commerce/css/commerce.css") %>" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script src="<%: Url.Content("~/Areas/Commerce/js/commerce.js") %>"></script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<asp:ListView ID="ChildView" runat="server">
		<LayoutTemplate>
			<div class="listing">
				<h4>
					<i class="icon-folder-open"></i>
					<crm:Snippet SnippetName="Page Children Heading" DefaultText="In This Section" EditType="text" runat="server"/>
				</h4>
				<ul>
					<li id="itemPlaceholder" runat="server"/>
				</ul>
			</div>
		</LayoutTemplate>
		<ItemTemplate>
			<li runat="server">
				<h4>
					<a href="<%# Eval("Url") %>"><%# Eval("Title") %></a>
				</h4>
				<crm:CrmEntityDataSource ID="ChildEntity" DataItem='<%# Eval("Entity") %>' runat="server"/>
				<crm:Property DataSourceID="ChildEntity" PropertyName='<%# GetSummaryPropertyName(Eval("Entity.LogicalName", "{0}")) %>' EditType="html" runat="server"/>
			</li>
		</ItemTemplate>
	</asp:ListView>
</asp:Content>

<asp:Content ContentPlaceHolderID="SidebarAbove" runat="server">
	<div class="page-children">
		<% if (Shortcuts.Any()) {%>
			<h4>
				<i class="icon-share"></i>
				<crm:Snippet SnippetName="Page Related Heading" DefaultText="Related Topics" EditType="text" runat="server"/>
			</h4>
			<ul class="nav nav-tabs nav-stacked">
				<% foreach (var node in Shortcuts) { %>
					<li>
						<a href="<%= node.Url %>">
							<i class="icon-share-alt"></i>
							<%= node.Title %>
						</a>
					</li>
				<% } %>
			</ul>
		<% } %>
	</div>
</asp:Content>
