﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Web.UI;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;
using Xrm;

namespace Site.Areas.Commerce.Pages
{
	public partial class OrderHistory : PortalPage
	{
		protected string SortExpression
		{
			get { return ViewState["SortExpression"] as string ?? "Accepted"; }
			set { ViewState["SortExpression"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			var orders = ServiceContext.GetOrdersForContact(Contact).Select(so => so).Cast<SalesOrder>();

			var columnsGenerator = new SavedQueryColumnsGenerator(ServiceContext, "salesorder", "Order History Web View");

			CustomerOrdersList.DataKeyNames = new[] { "salesorderid" };
			CustomerOrdersList.DataSource = columnsGenerator.ToDataTable(orders);
			CustomerOrdersList.ColumnsGenerator = columnsGenerator;
			CustomerOrdersList.DataBind();
		}

		protected void CustomerOrdersList_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{

			if (e.Row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var dataKey = CustomerOrdersList.DataKeys[e.Row.RowIndex].Value;

			e.Row.Cells[0].Text = string.Format(@"<a href=""{0}"">{1}</a>",
					ViewOrderUrl(dataKey),
					e.Row.Cells[0].Text);

			e.Row.Cells[0].Attributes.Add("style", "white-space: nowrap;");
		}

		protected string ViewOrderUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "View Order") ?? ServiceContext.GetPageBySiteMarkerName(Website, "Order Status");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("OrderID", id.ToString());

			return url;
		}
	}
}