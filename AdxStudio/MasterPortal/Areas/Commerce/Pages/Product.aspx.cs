using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Web.Mvc.Html;
using Microsoft.Xrm.Portal.Cms;
using Microsoft.Xrm.Portal.Core;
using Microsoft.Xrm.Sdk;
using Site.Pages;
using Xrm;

namespace Site.Areas.Commerce.Pages
{
	public partial class ProductPage : PortalPage
	{
		protected string VisitorID
		{
			get { return Context.Profile.UserName; }
		}

		private ShoppingCart _cart;

		public ShoppingCart Cart
		{
			get
			{
				if (_cart != null)
				{
					return _cart;
				}

				var user = Contact;

				Adx_shoppingcart baseCart = null;

				if (user != null)
				{
					baseCart = (Adx_shoppingcart)XrmContext.GetCartsForContact(user, Website).FirstOrDefault();
				}
				else if (!string.IsNullOrEmpty(VisitorID))
				{
					baseCart = (Adx_shoppingcart)XrmContext.GetCartsForVisitor(VisitorID, Website).FirstOrDefault();
				}

				_cart = baseCart == null ? null : new ShoppingCart(baseCart, XrmContext);

				return _cart;
			}
		}

		public string PriceListName
		{
			get { return (Contact != null) ? ServiceContext.GetPriceListNameForParentAccount(Contact) : "Web"; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
			{
				return;
			}

			var context = ServiceContext;

			var webPage = Entity as Adx_webpage;

			if (webPage == null || webPage.adx_subjectid == null)
			{
				return;
			}

			var subjectId = webPage.adx_subjectid.Id;

			var products =
				from Product product in context.GetProductsBySubject(subjectId)
				select new
				{
					product.Name,
					product.ProductId,
					product.ProductNumber,
					Price = (context.GetProductPriceByPriceListName(product, Website, PriceListName) ?? new Money(0)).Value.ToString("C2")
				};

			Products.DataSource = products;
			Products.DataBind();
		}

		protected void ProductItemCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "AddToCart")
			{
				return;
			}

			if (Cart == null)
			{
				const string nameFormat = "Cart for {0}";

				var cart = Contact == null
					? new Adx_shoppingcart { Adx_name = string.Format(nameFormat, VisitorID),
						Adx_VisitorID = VisitorID, adx_websiteid = Website.ToEntityReference() }
					: new Adx_shoppingcart { Adx_name = string.Format(nameFormat, Contact.FullName), 
						adx_contactid = Contact.ToEntityReference(), adx_websiteid = Website.ToEntityReference() };

				XrmContext.AddObject(cart);
				XrmContext.SaveChanges();
			}

			var productId = new Guid(e.CommandArgument.ToString());

			if (Cart == null)
			{
				throw new Exception("Error Processing Cart.");
			}

			Cart.AddProductToCart(productId, PriceListName);

			Response.Redirect(Html.SiteMarkerUrl("Shopping Cart") ?? Request.Url.PathAndQuery);
		}
	}
}