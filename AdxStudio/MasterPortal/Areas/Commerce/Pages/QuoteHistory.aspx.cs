﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Web.UI;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;
using Xrm;

namespace Site.Areas.Commerce.Pages
{
	public partial class QuoteHistory : PortalPage
	{
		private DataTable _quotes;

		protected string SortDirection
		{
			get { return ViewState["SortDirection"] as string ?? "ASC"; }
			set { ViewState["SortDirection"] = value; }
		}
		protected string SortExpression
		{
			get { return ViewState["SortExpression"] as string ?? "Accepted"; }
			set { ViewState["SortExpression"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			var quotes = ServiceContext.GetQuotesForContact(Contact)
				.Cast<Quote>()
				.Where(q => q.StateCode != (int)Enums.QuoteState.Draft);

			var columnsGenerator = new SavedQueryColumnsGenerator(ServiceContext, "quote", "Quote History Web View");

			CustomerQuotesList.DataKeyNames = new[] { "quoteid" };

			CustomerQuotesList.DataSource = _quotes = columnsGenerator.ToDataTable(quotes);
			CustomerQuotesList.ColumnsGenerator = columnsGenerator;
			CustomerQuotesList.DataBind();
		}

		protected void CustomerQuotesList_Sorting(object sender, GridViewSortEventArgs e)
		{
			SortDirection = e.SortExpression == SortExpression
				? (SortDirection == "ASC" ? "DESC" : "ASC")
				: "ASC";

			SortExpression = e.SortExpression;

			_quotes.DefaultView.Sort = string.Format("{0} {1}", SortExpression, SortDirection);

			CustomerQuotesList.DataSource = _quotes;
			CustomerQuotesList.DataBind();
		}

		protected void CustomerQuotesList_OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var dataKey = CustomerQuotesList.DataKeys[e.Row.RowIndex].Value;

			e.Row.Cells[0].Text = string.Format(@"<a href=""{0}"">{1}</a>",
					ViewQuoteUrl(dataKey),
					e.Row.Cells[0].Text);

			e.Row.Cells[0].Attributes.Add("style", "white-space: nowrap;");
		}

		protected string ViewQuoteUrl(object id)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "View Quote") ?? ServiceContext.GetPageBySiteMarkerName(Website, "Quote Status");

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("QuoteID", id.ToString());

			return url;
		}
	}
}