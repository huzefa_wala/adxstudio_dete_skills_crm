﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Xrm.Portal.Web;
using Site.Pages;

namespace Site.Areas.Commerce.Pages
{
	public partial class ProductListing : PortalPage
	{
		private readonly Lazy<Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>> _childNodes = new Lazy<Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>>(GetChildNodes, LazyThreadSafetyMode.None);

		protected IEnumerable<SiteMapNode> Children
		{
			get { return _childNodes.Value.Item1; }
		}

		protected IEnumerable<SiteMapNode> Shortcuts
		{
			get { return _childNodes.Value.Item2; }
		}

		protected void Page_Load(object sender, EventArgs args)
		{
			ChildView.DataSource = Children;
			ChildView.DataBind();
		}

		private static Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>> GetChildNodes()
		{
			var currentNode = System.Web.SiteMap.CurrentNode;

			if (currentNode == null)
			{
				return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(new SiteMapNode[] { }, new SiteMapNode[] { });
			}

			var shortcutNodes = new List<SiteMapNode>();
			var otherNodes = new List<SiteMapNode>();

			foreach (SiteMapNode childNode in currentNode.ChildNodes)
			{
				var entityNode = childNode as CrmSiteMapNode;

				if (entityNode != null && entityNode.HasCrmEntityName("adx_shortcut"))
				{
					shortcutNodes.Add(childNode);
				}
				else
				{
					otherNodes.Add(childNode);
				}
			}

			return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(otherNodes, shortcutNodes);
		}

		protected string GetSummaryPropertyName(string entityLogicalName)
		{
			if (string.IsNullOrEmpty(entityLogicalName)) return null;

			switch (entityLogicalName)
			{
				case "adx_shortcut":
					return "Adx_description";
				case "adx_webfile":
					return "Adx_Summary";
				case "adx_webpage":
					return "Adx_Summary";
				default:
					return null;
			}
		}
	}
}