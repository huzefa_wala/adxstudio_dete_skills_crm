﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/WebForms.master" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="Site.Areas.Commerce.Pages.Checkout" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="Xrm" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Commerce/css/commerce.css") %>" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script src="<%: Url.Content("~/Areas/Commerce/js/commerce.js") %>"></script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" ViewStateMode="Enabled" runat="server">
	<div class="commerce-details">
		<asp:Panel ID="ErrorMessage" Visible="False" CssClass="alert alert-block alert-error" runat="server">
			<crm:Snippet SnippetName="Ecommerce/ProcessingError" DefaultText="There has been an error processing your order. Please contact support." EditType="html" runat="server" />
		</asp:Panel>
		
		<asp:Panel ID="CartNotFoundErrorMessage" Visible="False" CssClass="alert alert-block alert-error" runat="server">
			<crm:Snippet SnippetName="Ecommerce/CartNotFoundError" DefaultText="There was a problem retrieving your shopping cart. Please contact support." EditType="html" runat="server" />
		</asp:Panel>
	
		<asp:Panel ID="PaymentProviderMessage" CssClass="alert alert-block alert-info" runat="server">
			<crm:Snippet SnippetName="Ecommerce/PaymentProviderMessage" DefaultText="This website uses PayPal for order processing." EditType="html" runat="server" />
		</asp:Panel>
		
		<asp:Panel ID="CancelMessage" CssClass="alert alert-block alert-error" Visible="False" runat="server">
			<crm:Snippet SnippetName="Ecommerce/CancelLabel" DefaultText="There was an error processing your payment. Please contact support." EditType="html" runat="server" />
		</asp:Panel>

		<asp:Panel ID="ConfirmationPanel" Visible="False" runat="server">
			<asp:Panel ID="SuccessMessage" Visible="False" CssClass="alert alert-block alert-success" runat="server">
				<crm:Snippet SnippetName="Ecommerce/SuccessLabel" DefaultText="Thank you for your payment, we will contact you shortly. Please print this page for your records." EditType="html" runat="server" />
			</asp:Panel>
		
			<asp:Panel ID="ShoppingCartSummary" CssClass="well shopping-cart-summary" runat="server">
				<h4>
					<crm:Snippet SnippetName="Ecommerce/OrderNumberLabel" DefaultText="Order Number:" runat="server" EditType="text" />
					<asp:Label ID="OrderNumber" runat="server" />
				</h4>
				<ul class="unstyled">
					<asp:Repeater ID="CartRepeater" runat="server">
						<ItemTemplate>
							<li class="shopping-cart-item clearfix">
								<div class="pull-right">
									<asp:Label ID="Quantity" runat="server" Text='<%# ((Adx_shoppingcartitem)Container.DataItem).Adx_Quantity.GetValueOrDefault().ToString("N0") %>' />
									&times;
									<%# ((Adx_shoppingcartitem)Container.DataItem).Adx_QuotedPrice.GetValueOrDefault().ToString("C2") %>
								</div>
								<div class="title"><%# GetCartItemTitle((Adx_shoppingcartitem)Container.DataItem) %></div>
								<asp:TextBox ReadOnly="True" ID="CartItemID" runat="server" Visible="false" Text='<%# ((Adx_shoppingcartitem)Container.DataItem).Adx_shoppingcartitemId %>' />
							</li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
				<div class="total">
					<crm:Snippet SnippetName="Total" DefaultText="Total:" runat="server" EditType="text" />
					<asp:Label ID="Total" runat="server" />
				</div>
			</asp:Panel>
		</asp:Panel>
		
		<asp:Panel ID="AddressPanel" CssClass="form-horizontal" runat="server">
			<div class="row">
				<div class="span6">
					<fieldset>
						<legend>
							<crm:Snippet SnippetName="PrimaryBillingAddressLabel" DefaultText="Billing Address" runat="server" EditType="text" />
						</legend>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="firstname" Text="First Name" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="firstname" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="firstname" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="lastname" Text="Last Name" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="lastname" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="lastname" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="address1_phone" Text="Phone Number" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="address1_phone" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="address1_phone" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="address1_email" Text="Email" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="address1_email" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="address1_email" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="address1_line1" Text="Address" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="address1_line1" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="address1_line1" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="address1_city" Text="City" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="address1_city" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="address1_city" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="address1_country" Text="Country" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="address1_country" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="address1_country" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="address1_province" Text="State/Province" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="address1_province" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="address1_province" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="address1_postalcode" Text="ZIP/Postal Code" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="address1_postalcode" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="address1_postalcode" />
							</div>
						</div>
					</fieldset>
				</div>
				<div class="span6">
					<fieldset>
						<legend>
							<crm:Snippet SnippetName="ShippingAddressLabel" DefaultText="Shipping Address" runat="server" EditType="text" />
						</legend>
						<div class="control-group">
							<div class="controls">
								<div class="input-checkbox-inline">
									<asp:CheckBox ID="SameAsBilling" Text="Same as Billing Address" runat="server"/>
								</div>
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="address1" Text="Address" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="address1" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="address1" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="city" Text="City" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="city" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="city" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="country" Text="Country" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="country" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="country" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="state" Text="State/Province" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="state" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="state" />
							</div>
						</div>
						<div class="control-group">
							<asp:Label CssClass="control-label" AssociatedControlID="zip" Text="ZIP/Postal Code" runat="server"/>
							<div class="controls">
								<asp:TextBox ID="zip" runat="server"/>
								<asp:RequiredFieldValidator runat="server" CssClass="label label-important" ErrorMessage="Required" Text="Required" ControlToValidate="zip" />
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</asp:Panel>
		
		<asp:Panel ID="FormActions" CssClass="form-actions" runat="server">
			<div class="pull-right">
				<asp:Button ID="PayPalButton" CssClass="btn btn-primary" Text="<%$ Snippet: paypal_checkout_button, Process Order %>" OnClick="PayPalButton_OnClick" runat="server"/>
			</div>
		</asp:Panel>
	</div>
</asp:Content>