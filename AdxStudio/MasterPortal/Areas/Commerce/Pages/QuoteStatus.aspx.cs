﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Core;
using Adxstudio.Xrm.Text;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.Xrm.Client.Diagnostics;
using Adxstudio.Xrm.Cms;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Site.Pages;
using Xrm;

namespace Site.Areas.Commerce.Pages
{
	public partial class QuoteStatus : PortalPage
	{
		private CommerceQuote _quote;

		protected CommerceQuote QuoteToEdit
		{
			get
			{
				if (_quote != null)
				{
					return _quote;
				}

				Guid quoteId;

				if (!Guid.TryParse(Request["QuoteID"], out quoteId))
				{
					return null;
				}

				var myQuote = XrmContext.QuoteSet.FirstOrDefault(c => c.Id == quoteId);

				_quote = (myQuote != null) ? new CommerceQuote(myQuote, XrmContext) : null;

				return _quote;
			}
		}

		protected string QuoteStatusLabel
		{
			get { return XrmContext.GetOptionSetValueLabel("quote", "statuscode", QuoteToEdit.Entity.GetAttributeValue<OptionSetValue>("statuscode")); }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			RedirectToLoginIfAnonymous();

			var parameter = new Parameter("Filter", DbType.String, WebAnnotationPrefix);

			CrmNoteSource.WhereParameters.Add(parameter);

			var quoteToEditEntity = QuoteToEdit != null ? QuoteToEdit.Entity as Quote : null;

			if (quoteToEditEntity == null || quoteToEditEntity.CustomerId.Id != Contact.ContactId)
			{
				GenericError.Visible = true;
				QuoteHeader.Visible = false;
				QuoteDetails.Visible = false;

				return;
			}

			var quoteState = quoteToEditEntity.GetAttributeValue<OptionSetValue>("statecode");

			ConvertToOrder.Visible = quoteState != null
				&& (quoteState.Value == (int)Enums.QuoteState.Active || quoteState.Value == (int)Enums.QuoteState.Won);

			var formViewDataSource = new CrmDataSource { ID = "WebFormDataSource" };

			var fetchXml = string.Format("<fetch mapping='logical'><entity name='{0}'><all-attributes /><filter type='and'><condition attribute = '{1}' operator='eq' value='{{{2}}}'/></filter></entity></fetch>", Quote.EntityLogicalName, "quoteid", QuoteToEdit.Id);

			formViewDataSource.FetchXml = fetchXml;

			QuoteForm.Controls.Add(formViewDataSource);

			FormView.DataSourceID = "WebFormDataSource";

			var baseCartReference = quoteToEditEntity.adx_ShoppingCartId;

			var baseCart = (baseCartReference != null) ? ServiceContext.Adx_shoppingcartSet.FirstOrDefault(sc => sc.Adx_shoppingcartId == baseCartReference.Id)
				: null;

			var cartRecord = baseCart == null ? null : new ShoppingCart(baseCart, XrmContext);

			if (cartRecord == null)
			{
				ShoppingCartSummary.Visible = false;

				return;
			}

			var cartItems = cartRecord.GetCartItems().Select(sci => sci.Entity).Cast<Adx_shoppingcartitem>();

			if (!cartItems.Any())
			{
				ShoppingCartSummary.Visible = false;

				return;
			}

			CartRepeater.DataSource = cartItems;
			CartRepeater.DataBind();

			Total.Text = cartRecord.GetCartTotal().ToString("C2");
		}

		protected string GetCartItemTitle(Adx_shoppingcartitem item)
		{
			var product = item.adx_product_shoppingcartitem;

			return product.Name;
		}

		protected void OnItemUpdating(object sender, CrmEntityFormViewUpdatingEventArgs e)
		{
		}

		protected void OnItemUpdated(object sender, CrmEntityFormViewUpdatedEventArgs e)
		{
			UpdateSuccessMessage.Visible = true;
		}

		protected static IHtmlString FormatNote(object text)
		{
			return text == null ? null : new SimpleHtmlFormatter().Format(text.ToString().Replace("*WEB* ", string.Empty));
		}

		protected void AddNote_Click(object sender, EventArgs e)
		{
			if (QuoteToEdit == null || QuoteToEdit.Entity.GetAttributeValue<EntityReference>("customerid").Id != Contact.ContactId)
			{
				throw new InvalidOperationException("Unable to retrieve quote.");
			}

			var noteSubject = string.Format("Note created on {0} by {1}", DateTime.UtcNow, Contact.FullName);

			if (!string.IsNullOrEmpty(NewNoteText.Text) || (NewNoteAttachment.PostedFile != null && NewNoteAttachment.PostedFile.ContentLength > 0))
			{
				XrmContext.AddNoteAndSave(QuoteToEdit.Entity.ToEntityReference(), noteSubject, "*WEB* " + NewNoteText.Text, NewNoteAttachment.PostedFile);
			}

			Response.Redirect(Request.Url.PathAndQuery);
		}

		protected FileSize GetFileSize(object data)
		{
			try
			{
				return new FileSize(Convert.ToUInt64((data as int?).GetValueOrDefault(0)));
			}
			catch
			{
				return new FileSize(0);
			}
		}

		protected void ConvertToOrder_Click(object sender, EventArgs args)
		{
			var quoteToEditEntity = QuoteToEdit != null ? QuoteToEdit.Entity as Quote : null;

			if (quoteToEditEntity == null || quoteToEditEntity.CustomerId.Id != Contact.ContactId)
			{
				throw new InvalidOperationException("Unable to retrieve quote.");
			}

			SalesOrder order;
			
			try
			{
				order = QuoteToEdit.CreateOrder() as SalesOrder;

				if (order == null)
				{
					ConvertToOrderError.Visible = true;

					return;
				}
			}
			catch (Exception e)
			{
				Tracing.FrameworkError(GetType().FullName, MethodBase.GetCurrentMethod().Name, "{0}", e);

				ConvertToOrderError.Visible = true;

				return;
			}

			var orderid = order.SalesOrderId;

			if (!ServiceContext.IsAttached(Website))
			{
				ServiceContext.Attach(Website);
			}

			var page = ServiceContext.GetPageBySiteMarkerName(Website, "View Order") ?? ServiceContext.GetPageBySiteMarkerName(Website, "Order Status");

			if (page == null)
			{
				return;
			}

			var url = new UrlBuilder(ServiceContext.GetUrl(page));

			url.QueryString.Set("OrderID", orderid.ToString());

			Response.Redirect(url.PathWithQueryString);
		}

		protected string GetLabelClassForQuote(CommerceQuote quote)
		{
			var statusCodeOption = quote.Entity.GetAttributeValue<OptionSetValue>("statuscode");

			if (statusCodeOption == null)
			{
				return null;
			}

			switch (statusCodeOption.Value)
			{
				case (int)Enums.QuoteStatusCode.Canceled:
				case (int)Enums.QuoteStatusCode.Lost:
					return "label-important";
				case (int)Enums.QuoteStatusCode.Draft:
				case (int)Enums.QuoteStatusCode.New:
				case (int)Enums.QuoteStatusCode.Open:
					return "label-info";
				case (int)Enums.QuoteStatusCode.Won:
					return "label-success";
				case (int)Enums.QuoteStatusCode.Revised:
					return "label-warning";
				default:
					return null;
			}
		}
	}
}