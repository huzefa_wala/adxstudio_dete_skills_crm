﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Profile.master" CodeBehind="QuoteHistory.aspx.cs" Inherits="Site.Areas.Commerce.Pages.QuoteHistory" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="<%: Url.Content("~/Areas/Commerce/css/commerce.css") %>" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Scripts" runat="server">
	<script src="<%: Url.Content("~/Areas/Commerce/js/commerce.js") %>"></script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="quote-history">
		<asp:GridView ID="CustomerQuotesList" runat="server" CssClass="table table-striped" GridLines="None" AlternatingRowStyle-CssClass="alternate-row" AllowSorting="true" OnSorting="CustomerQuotesList_Sorting" OnRowDataBound="CustomerQuotesList_OnRowDataBound" >
			<EmptyDataRowStyle CssClass="empty" />
			<EmptyDataTemplate>
				<crm:Snippet  runat="server" SnippetName="Ecommerce/quote-history/list/empty" DefaultText="There are no items to display." Editable="true" EditType="html" />
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
	<script type="text/javascript">
		$(function () {
			$(".tabular-data tr").not(":has(th)").click(function () {
				window.location.href = $(this).find("a").attr("href");
			});

			$("form").submit(function () {
				blockUI();
			});

			$(".tabular-data th a").click(function () {
				blockUI();
			});
		});

		function blockUI() {
			$.blockUI({ message: null, overlayCSS: { opacity: .3} });
		}
	</script>
</asp:Content>
