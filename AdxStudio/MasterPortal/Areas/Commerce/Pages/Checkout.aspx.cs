﻿using System;
using System.Collections.Generic;
using System.Linq;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Web.Mvc.Html;
using Site.Pages;
using Xrm;

namespace Site.Areas.Commerce.Pages
{
	public partial class Checkout : PortalPage
	{
		protected IShoppingCart Cart { get; private set; }

		protected bool IsPaymentPaypal { get; set; }

		protected bool SendPaypalAddress { get; set; }

		protected void Page_Init(object sender, EventArgs e)
		{
			var shoppingCartDataAdapter = new ShoppingCartDataAdapter(
				new Adxstudio.Xrm.Commerce.PortalConfigurationDataAdapterDependencies(PortalName, Request.RequestContext),
				Context.Profile.UserName);

			Cart = shoppingCartDataAdapter.SelectCart();

			IsPaymentPaypal = string.Equals(Html.Setting("Ecommerce/PaymentProvider", "Demo"), "PayPal", StringComparison.InvariantCultureIgnoreCase);
			SendPaypalAddress = Html.BooleanSetting("Ecommerce/AddressPrompt").GetValueOrDefault(false);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPaymentPaypal && !SendPaypalAddress)
			{
				AddressPanel.Enabled = false;
				AddressPanel.Visible = false;
			}
			else if (Contact != null)
			{
				firstname.Text = Contact.FirstName;
				lastname.Text = Contact.LastName;
				address1_phone.Text = Contact.Telephone1;
				address1_email.Text = Contact.EMailAddress1;
				address1_line1.Text = Contact.Address1_Line1;
				address1_city.Text = Contact.Address1_City;
				address1_country.Text = Contact.Address1_Country;
				address1_province.Text = Contact.Address1_StateOrProvince;
				address1_postalcode.Text = Contact.Address1_PostalCode;
			}

			if (IsPostBack)
			{
				return;
			}

			if (Request.QueryString["PayPal"] != null)
			{
				HandlePayPalReturn();

				return;
			}

			if (Cart == null)
			{
				AddressPanel.Visible = false;
				FormActions.Visible = false;
				PaymentProviderMessage.Visible = false;
				CartNotFoundErrorMessage.Visible = true;

				return;
			}

			var cartItems = Cart.GetCartItems().Select(sci => sci.Entity).Cast<Adx_shoppingcartitem>();

			if (!cartItems.Any())
			{
				AddressPanel.Visible = false;
				FormActions.Visible = false;
				PaymentProviderMessage.Visible = false;
				CartNotFoundErrorMessage.Visible = true;
			}
		}

		protected void PayPalButton_OnClick(object sender, EventArgs e)
		{
			if (Cart == null)
			{
				AddressPanel.Visible = false;
				FormActions.Visible = false;
				PaymentProviderMessage.Visible = false;
				ErrorMessage.Visible = true;

				return;
			}

			// Validation that the order is going to be correct.
			var total = Cart.GetCartTotal();

			if (total < 0)
			{
				AddressPanel.Visible = false;
				FormActions.Visible = false;
				PaymentProviderMessage.Visible = false;
				ErrorMessage.Visible = true;

				return;
			}

			//if some other payment method is used, put a redirect here instead of using paypal!

			//Note that the demo "fake" paypal processing is still considered the paypal method and will still use the HandlePayPalRedirection() 
			//method.  Only a completley custom payment method should have this site setting set to false; 
			//in which case completely custom code must be written
			//var isPaymentPaypal = bool.Parse(ServiceContext.GetSiteSettingValueByName(Website, "Paypal/isPaypalUsed") ?? "true");

			if (IsPaymentPaypal)
			{
				HandlePaypalPayment(total);
			}
		}

		private void HandlePaypalPayment(decimal total)
		{
			var payPal = new PayPalHelper(Portal);

			var settingDataAdapter = new SettingDataAdapter(new Adxstudio.Xrm.Cms.PortalConfigurationDataAdapterDependencies(PortalName, Request.RequestContext));

			var currencyCode = settingDataAdapter.GetValue("Ecommerce/Paypal/CurrencyCode");
			var aggregateData = settingDataAdapter.GetBooleanValue("Ecommerce/Paypal/aggregateData").GetValueOrDefault(false);
			var itemizedData = settingDataAdapter.GetBooleanValue("Ecommerce/Paypal/itemizedData").GetValueOrDefault(true);
			var addressOverride = settingDataAdapter.GetBooleanValue("Ecommerce/Paypal/AddressOverride").GetValueOrDefault(true);

			HandlePayPalRedirection(payPal, total, payPal.PayPalAccountEmail, currencyCode, itemizedData, aggregateData, addressOverride);
		}

		/// <summary>
		/// Redirects the current request to the PayPal site by passing a querystring.
		/// PayPal then should return to this page with ?PayPal=Cancel or ?PayPal=Success
		/// This routine stores all the form vars so they can be restored later
		/// </summary>
		private void HandlePayPalRedirection(PayPalHelper payPal, decimal total, string accountEmail, string currencyCode, bool itemizedData, bool aggregateData, bool addressOverride)
		{
			// Set a flag so we know we redirected.
			Session["PayPal_Redirected"] = "True";

			var args = GetPaypalArgs(total, accountEmail, itemizedData, currencyCode, aggregateData, SendPaypalAddress, addressOverride);

			Response.Redirect(payPal.GetSubmitUrl(args));
		}

		/// <summary>
		/// Creates the dictionary of arguments for constructing the query string to send to paypal
		/// </summary>
		private Dictionary<string, string> GetPaypalArgs(decimal total, string accountEmail, bool itemizedData,
			string currencyCode, bool aggregateData, bool sendPaypalAddress, bool addressOverride)
		{
			var args = new Dictionary<string, string>();

			//use cmd = "_cart" for the cart upload option
			args.Add("cmd", "_cart");
			args.Add("upload", "1");
			args.Add("business", accountEmail);

			//Paypal STANDARD DATA -> does this have to be done after the items, or does it not matter?
			args.Add("no_note", "0");

			if (!string.IsNullOrEmpty(currencyCode)) { args.Add("currency_code", currencyCode); }

			//TODO: Invoice or Order ID

			args.Add("email", accountEmail);

			if (aggregateData)
			{
				args.Add("item_name", "Aggregated Items");
				args.Add("amount", total.ToString("#.00"));
			}
				//Paypal Item Data for itemized data
			else if (itemizedData)
			{
				var cartItems = Cart.GetCartItems().Select(sci => sci.Entity).Cast<Adx_shoppingcartitem>();

				int counter = 0;
				foreach (var item in cartItems)
				{
					counter++;

					args.Add(string.Format("item_name_{0}", counter), item.Adx_name);
					args.Add(string.Format("amount_{0}", counter),
					         (item.Adx_QuotedPrice != null) ? ((decimal) item.Adx_QuotedPrice).ToString("#.00") : "0.00");
					args.Add(string.Format("quantity_{0}", counter),
					         (item.Adx_Quantity != null) ? ((int) item.Adx_Quantity).ToString() : "0");
					//add arguments for shipping/handling cost?
					args.Add(string.Format("item_number_{0}", counter), item.Adx_shoppingcartitemId.ToString());
				}
				//if we are calculating the tax; this is done and added as an arg.
			}
			else
			{
				/*some other method is being used*/
			}

			/*******Add address and contact info if account if we are sending paypal our address info******/
			if (sendPaypalAddress)
			{
				args.Add("first_name", (!string.IsNullOrEmpty(firstname.Text)) ? firstname.Text : (Contact != null) ? Contact.FirstName : null);
				args.Add("last_name", (!string.IsNullOrEmpty(lastname.Text)) ? lastname.Text : (Contact != null) ? Contact.LastName : null);
				args.Add("address1", (!string.IsNullOrEmpty(address1.Text)) ? address1.Text : (Contact != null) ? Contact.Address1_Line1 : null);
				args.Add("city", (!string.IsNullOrEmpty(city.Text)) ? city.Text : (Contact != null) ? Contact.Address1_City : null);
				args.Add("country", (!string.IsNullOrEmpty(country.Text)) ? country.Text : (Contact != null) ? Contact.Address1_Country : null);
				args.Add("state", (!string.IsNullOrEmpty(state.Text)) ? state.Text : (Contact != null) ? Contact.Address1_StateOrProvince : null);
				args.Add("zip", (!string.IsNullOrEmpty(zip.Text)) ? zip.Text : (Contact != null) ? Contact.Address1_PostalCode : null);
				
				//we must also set it to override the account's existing shipping information, lest their be inconsistency
				if (addressOverride)
				{
					args.Add("address_override", "1");
				}
			}

			args.Add("invoice", Cart.Id.ToString());

			var successUrl = Request.Url + "?PayPal=Success&ConfirmationNumber=" + Cart.Id;

			// *** Have paypal return back to this URL
			var cancelUrl = Request.Url + "?PayPal=Cancel";

			args.Add("return", successUrl);
			args.Add("cancel_return", cancelUrl);

			return args;
		}

		/// <summary>
		/// Handles the return processing from a PayPal Request.
		/// Looks at the PayPal Querystring variable
		/// </summary>
		private void HandlePayPalReturn()
		{
			var result = Request.QueryString["PayPal"];
			var redirected = (string) Session["PayPal_Redirected"];

			if (redirected != "True")
			{
				return;
			}

			Session.Remove("PayPal_Redirected");

			if (result == "Cancel")
			{
				CancelMessage.Visible = true;
				AddressPanel.Visible = false;
				FormActions.Visible = false;

				return;
			}

			if (result != "Success")
			{
				return;
			}

			// TODO: Create CRM order
			AddressPanel.Visible = false;
			FormActions.Visible = false;
			PaymentProviderMessage.Visible = false;

			Guid confirmationNumber;

			if (!Guid.TryParse(Request.QueryString["ConfirmationNumber"], out confirmationNumber))
			{
				ErrorMessage.Visible = true;
			}

			var baseCart = XrmContext.Adx_shoppingcartSet.FirstOrDefault(sc => sc.Adx_shoppingcartId == confirmationNumber);
			var cartRecord = baseCart == null ? null : new ShoppingCart(baseCart, XrmContext);

			if (cartRecord == null)
			{
				ErrorMessage.Visible = true;

				return;
			}

			var cartItems = cartRecord.GetCartItems().Select(sci => sci.Entity).Cast<Adx_shoppingcartitem>();

			if (!cartItems.Any())
			{
				ErrorMessage.Visible = true;
			}
			else
			{
				CartRepeater.DataSource = cartItems;
				CartRepeater.DataBind();

				Total.Text = cartRecord.GetCartTotal().ToString("C2");

				cartRecord.DeactivateCart();
			}

			var order = XrmContext.SalesOrderSet.FirstOrDefault(so => so.adx_ShoppingCartId.Id == cartRecord.Entity.Id);

			if (order != null)
			{
				OrderNumber.Text = order.OrderNumber;
			}

			SuccessMessage.Visible = !ErrorMessage.Visible;
			ConfirmationPanel.Visible = !ErrorMessage.Visible;
		}

		protected string GetCartItemTitle(Adx_shoppingcartitem item)
		{
			var product = item.adx_product_shoppingcartitem;

			return product.Name;
		}
	}
}

