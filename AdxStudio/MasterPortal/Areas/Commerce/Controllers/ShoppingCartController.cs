﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Web.Mvc;

namespace Site.Areas.Commerce.Controllers
{
	public class ShoppingCartController : Controller
	{
		[NoCache]
		public ActionResult Status()
		{
			var dataAdapter = new ShoppingCartDataAdapter(
				new PortalConfigurationDataAdapterDependencies(requestContext: Request.RequestContext),
				HttpContext.Profile.UserName);

			var cart = dataAdapter.SelectCart();

			if (cart == null)
			{
				return new HttpStatusCodeResult((int)HttpStatusCode.NoContent);
			}

			var cartItems = cart.GetCartItems().ToArray();

			return Json(new ShoppingCartStatus
			{
				Count = cartItems.Sum(item => item.Quantity)
			}, JsonRequestBehavior.AllowGet);
		}

		public class ShoppingCartStatus
		{
			public decimal Count { get; set; }
		}
	}
}
