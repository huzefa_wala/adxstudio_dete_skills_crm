﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Adxstudio.Xrm.Commerce;
using Microsoft.Xrm.Portal;
using Xrm;

namespace Site.Areas.Commerce
{
	public static class QuoteFunctions
	{
		public static CommerceQuote CreateQuote(PayPalHelper payPal, bool aggregateData, bool itemizedData, Adx_website website, IShoppingCart cart, IPortalContext portal)
		{
			var args = new Dictionary<string, string>();

			if (aggregateData)
			{
				args.Add("item_name", "Aggregated Items");
				args.Add("amount", cart.GetCartTotal().ToString("#.00"));
			}
			// Paypal Item Data for itemized data.
			else if (itemizedData)
			{
				var cartItems = cart.GetCartItems().Select(sci => sci.Entity).Cast<Adx_shoppingcartitem>();

				var counter = 0;

				foreach (var item in cartItems)
				{
					counter++;

					args.Add(string.Format("item_name_{0}", counter), item.Adx_name);
					args.Add(string.Format("amount_{0}", counter),
						(item.Adx_QuotedPrice != null) ? ((decimal)item.Adx_QuotedPrice).ToString("#.00") : "0.00");
					args.Add(string.Format("quantity_{0}", counter),
						(item.Adx_Quantity != null) ? ((int)item.Adx_Quantity).ToString(CultureInfo.InvariantCulture) : "0");
					// Add arguments for shipping/handling cost?
					args.Add(string.Format("item_number_{0}", counter), item.Adx_shoppingcartitemId.ToString());
				}

				// If we are calculating the tax, this is done and added as an arg.
			}		

			// If a quote was created, pass in the quote ID.
			args.Add("invoice", cart.Id.ToString());

			return new CommerceQuote(args, portal, "PayPal");
		}
	}
}