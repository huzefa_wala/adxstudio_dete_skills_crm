﻿using System;
using System.Linq;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Web.Mvc;
using Adxstudio.Xrm.Web.UI.WebControls;
using Microsoft.Xrm.Portal;
using Microsoft.Xrm.Portal.Configuration;
using Microsoft.Xrm.Sdk;
using Site.Controls;
using Xrm;

namespace Site.Areas.Commerce.Controls
{
	public class WebFormCommercePortalUserControl : WebFormPortalUserControl
	{
		private readonly Lazy<XrmServiceContext> _xrmContext;

		public WebFormCommercePortalUserControl()
		{
			_xrmContext = new Lazy<XrmServiceContext>(CreateXrmServiceContext);
		}

		private XrmServiceContext CreateXrmServiceContext()
		{
			return PortalCrmConfigurationManager.CreateServiceContext(PortalName) as XrmServiceContext;
		}

		protected IPurchaseDataAdapter CreatePurchaseDataAdapter(EntityReference target, string targetPrimaryKeyLogicalName)
		{
			var dependencies = new PortalConfigurationDataAdapterDependencies(PortalName, Request.RequestContext);

			return new WebFormPurchaseDataAdapter(
				target,
				targetPrimaryKeyLogicalName,
				new EntityReference("adx_webform", WebForm.CurrentSessionHistory.WebFormId),
				WebFormMetadata,
				WebForm.CurrentSessionHistory,
				dependencies);
		}

	}
}
