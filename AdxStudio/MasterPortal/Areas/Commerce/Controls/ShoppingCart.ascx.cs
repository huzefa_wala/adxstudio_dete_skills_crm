﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Adxstudio.Xrm.Cms;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Web.Mvc.Html;
using Microsoft.Xrm.Portal.Web;
using Microsoft.Xrm.Sdk;
using Site.Controls;
using Xrm;
using IDataAdapterDependencies = Adxstudio.Xrm.Commerce.IDataAdapterDependencies;
using Adxstudio.Xrm.Data;
using PortalConfigurationDataAdapterDependencies = Adxstudio.Xrm.Commerce.PortalConfigurationDataAdapterDependencies;

namespace Site.Areas.Commerce.Controls
{
	public partial class ShoppingCart : PortalUserControl
	{
		protected const string ShoppingCartIdQueryStringParameterName = "cartid";

		public IShoppingCart Cart { get; set; }

		public string PriceListName
		{
			get
			{
				return ServiceContext.GetDefaultPriceListName(Website.Id);
			}
		}

		public bool SaveToQuoteEnabled
		{
			get { return Html.BooleanSetting("Ecommerce/SaveToQuoteEnabled").GetValueOrDefault(false); }
		}

		public bool UserAccountRequired
		{
			get { return Html.BooleanSetting("Ecommerce/RequiresAuthentication").GetValueOrDefault(true); }
		}
		
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
			{
			    return;
			}
			
			if (Cart == null)
			{
				ShoppingCartPanel.Visible = false;
				ShoppingCartEmptyPanel.Visible = true;

				return;
			}

			var cartItems = Cart.GetCartItems().Select(sci => sci.Entity).Cast<Adx_shoppingcartitem>().ToArray();

			if (!cartItems.Any())
			{
				ShoppingCartPanel.Visible = false;
				ShoppingCartEmptyPanel.Visible = true;

				return;
			}

			ShoppingCartEmptyPanel.Visible = false;

			CartItems.DataSource = cartItems.Select(item =>
			{
				var product = item.adx_product_shoppingcartitem;

				return new
				{
					item.Id,
					Description = product == null ? item.Adx_name : product.Name,
					Price = item.Adx_QuotedPrice ?? 0,
					Quantity = item.Adx_Quantity ?? 1,
					Total = item.Adx_Quantity.GetValueOrDefault(1) * item.Adx_QuotedPrice.GetValueOrDefault(0),
					Url = GetProductUrl(product)
				};
			});

			CartItems.DataBind();

			Total.Text = Cart.GetCartTotal().ToString("C2");

			SaveToQuote.Visible = SaveToQuoteEnabled;
		}

		protected void OnUpdateCart(object sender, EventArgs e)
		{
			UpdateCartItems();

			Response.Redirect(Request.Url.PathAndQuery);
		}

		protected void OnCheckOut(object sender, EventArgs e)
		{
			UpdateCartItems();

			if (Contact == null && UserAccountRequired)
			{
				// Must Authenticate
				var page = ServiceContext.GetPageBySiteMarkerName(Website, "Login");
				var targetPageUrl = ServiceContext.GetUrl(ServiceContext.GetPageBySiteMarkerName(Website, "Shopping Cart"));
				var url = new UrlBuilder(ServiceContext.GetUrl(page));
				var id = Cart.Id;

				url.QueryString.Set("ShoppingCartID", id.ToString());
				url.QueryString.Set("ReturnURL", targetPageUrl);

				Response.Redirect(url);
			}
			
			var checkoutUrl = GetCheckoutUrl(Cart.Id);

			Response.Redirect(checkoutUrl);
		}

		protected void OnSaveToQuote(object sender, EventArgs e)
		{
			UpdateCartItems();

			var dependencies = new PortalConfigurationDataAdapterDependencies(requestContext: Request.RequestContext);

			var quoteId = GetPurchaseableQuote(Cart, dependencies, Request.RequestContext.HttpContext.Profile.UserName).Quote.Id;

			if (!XrmContext.IsAttached(Website))
			{
				XrmContext.Attach(Website);
			}

			var myPage = XrmContext.GetPageBySiteMarkerName(Website, "Quote");

			var myUrl = new UrlBuilder(XrmContext.GetUrl(myPage));

			myUrl.QueryString.Set("QuoteID", quoteId.ToString());

			Cart.DeactivateCart();

			Response.Redirect(myUrl);
		}
		
		protected void UpdateCartItems()
		{
			var context = XrmContext;

			foreach (var item in CartItems.Items)
			{
				var cartItemIdTextBox = item.FindControl("CartItemID") as TextBox;

				if (cartItemIdTextBox == null)
				{
				    continue;
				}

				var quantityTextBox = item.FindControl("Quantity") as TextBox;

				if (quantityTextBox == null)
				{
				    continue;
				}

				int quantity;

				if (!int.TryParse(quantityTextBox.Text, out quantity))
				{
					throw new InvalidOperationException("Could not parse quantity");
				}

				try
				{
					var cartItemId = new Guid(cartItemIdTextBox.Text);
					var cartItem = Cart.GetCartItemByID(cartItemId) as ShoppingCartItem;

					if (cartItem == null)
					{
						throw new InvalidOperationException("Unable to find cart item corresponding to CartItemID");
					}

					if (cartItem.Quantity != quantity)
					{
						cartItem.Quantity = (decimal)quantity;
					}

					cartItem.UpdateItemPrice(PriceListName);

				}
				catch (FormatException)
				{
					throw new InvalidOperationException("Unable to parse Guid from CartItemID value");
				}
			}

			context.SaveChanges();
		}

		protected void CartItems_ItemCommand(object source, CommandEventArgs e)
		{
			if (e.CommandName == "Remove" && e.CommandArgument != null)
			{
				Cart.RemoveItemFromCart(new Guid(e.CommandArgument.ToString()));
			}

			Response.Redirect(Request.Url.PathAndQuery);
		}

		private string GetProductUrl(Product product)
		{
			if (product == null || product.SubjectId == null)
			{
				return null;
			}

			var productPage = XrmContext.Adx_webpageSet
				.FirstOrDefault(e => e.adx_subjectid.Id == product.SubjectId.Id
					&& e.adx_websiteid.Id == Website.Id);

			return productPage == null ? null : XrmContext.GetUrl(productPage);
		}

		protected string GetCheckoutUrl(Guid shoppingCartId)
		{
			var page = ServiceContext.GetPageBySiteMarkerName(Website, "Checkout");

			if (page == null)
			{
				throw new ApplicationException("Page could not be found for Site Marker named 'Checkout'");
			}

			var url = ServiceContext.GetUrl(page);

			if (string.IsNullOrWhiteSpace(url))
			{
				throw new ApplicationException("Url could not be determined for Site Marker named 'Checkout'");
			}

			var urlBuilder = new UrlBuilder(url);

			urlBuilder.QueryString.Add(ShoppingCartIdQueryStringParameterName, shoppingCartId.ToString());

			return urlBuilder.PathWithQueryString;
		}

		protected IPurchasable GetPurchaseableQuote(IShoppingCart Cart, IDataAdapterDependencies dependencies, string visitorId)
		{
			var entities = Cart.GetCartItems().Select(i => i.Entity).ToArray();

			if (!entities.Any()) { return null; }

			var productIds = entities
				.Select(e => e.GetAttributeValue<EntityReference>("adx_productid"))
				.Where(product => product != null)
				.Select(product => product.Id)
				.ToArray();

			if (!productIds.Any()) { return null; }

			var products = ServiceContext.CreateQuery("product")
				.WhereIn(e => e.GetAttributeValue<Guid>("productid"), productIds)
				.ToDictionary(e => e.Id, e => e);

			var lineItems = entities
				.Select(e => LineItem.GetLineItemFromLineItemEntity(e, "adx_productid", null, "adx_specialinstructions", null, null, "adx_quantity", "adx_uomid", products))
				.Where(lineItem => lineItem != null);

			var quote = Adxstudio.Xrm.Commerce.QuoteFunctions.CreateQuote(lineItems, Entity.ToEntityReference(), ServiceContext, null, dependencies.GetPortalUser(), dependencies.GetPriceList(), visitorId);

			var dataAdapter = new QuotePurchaseDataAdapter(quote, dependencies, false);

			var purchasable = quote == null
				? null
				: dataAdapter.Select();

			return purchasable;
		}
	}
}
