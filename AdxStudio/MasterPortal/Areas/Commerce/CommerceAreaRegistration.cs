﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Profile;
using System.Web.Routing;
using System.Web.Security;
using Adxstudio.Xrm.Commerce;
using Adxstudio.Xrm.Configuration;
using Adxstudio.Xrm.Web.Mvc;
using Microsoft.Xrm.Client.Diagnostics;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Portal.Configuration;
using Xrm;

namespace Site.Areas.Commerce
{
	public class CommerceAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get { return "Commerce"; }
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute("ShoppingCartStatus", "_services/commerce/{__portalScopeId__}/shopping-cart/status", new
			{
				controller = "ShoppingCart",
				action = "Status"
			});

			context.Routes.Add("PaymentHandler", new Route("_services/commerce/payment", new PaymentRouteHandler()));

			var portalAreaRegistrationState = context.State as IPortalAreaRegistrationState;

			if (portalAreaRegistrationState != null)
			{
				portalAreaRegistrationState.Profile_MigrateAnonymous += Profile_MigrateAnonymous;
			}
		}

		protected static string PriceListName
		{
			get
			{
				var portal = PortalCrmConfigurationManager.CreatePortalContext();
				var context = portal.ServiceContext;
				return (portal.User != null) ? context.GetPriceListNameForParentAccount(portal.User) : "Web";
			}
		}

		protected static void Profile_MigrateAnonymous(object sender, ProfileMigrateEventArgs e)
		{
			// transfer the anonyous shopping cart items to the authenticated shopping cart

			var visitorId = e.AnonymousID;
			var portal = PortalCrmConfigurationManager.CreatePortalContext();

			using (var context = PortalCrmConfigurationManager.CreateServiceContext() as XrmServiceContext)
			{
				if (!AdxstudioCrmConfigurationManager.TryAssertSolutionName(Adxstudio.Xrm.Cms.Solutions.CommerceSolutionName))
				{
					Tracing.FrameworkInformation("CommerceAreaRegistration", "Profile_MigrateAnonymous", "Execution aborted. {0} has not been imported.", Adxstudio.Xrm.Cms.Solutions.CommerceSolutionName);
					return;
				}
				
				var website = context.Adx_websiteSet.First(ws => ws.Adx_websiteId == portal.Website.Id);
				//var visitorBaseCart = context.GetCartsForVisitor(visitorId, website).FirstOrDefault() as Adx_shoppingcart;
				var visitorCart = context.GetCartsForVisitor(visitorId, website).FirstOrDefault() as Adx_shoppingcart;

				if (visitorCart != null)
				{
					var contactCartBase = context.GetCartsForContact(portal.User, website).FirstOrDefault() as Adx_shoppingcart;

					var contactCart = contactCartBase != null ? new ShoppingCart((contactCartBase), context) : null;

					if (contactCart != null)
					{
						// merge the anonymous cart with the authenticated cart

						foreach (var item in visitorCart.adx_shoppingcart_shoppingcartitem)
						{
							if (item.adx_productid == null)
							{
								continue;
							}

							contactCart.AddProductToCart(item.adx_productid.Id, PriceListName);
						}

						if (!context.IsAttached(visitorCart))
						{
							context.Attach(visitorCart);
						}

						context.DeleteObject(visitorCart);
					}
					else
					{
						// transfer the cart directly

						const string nameFormat = "Cart for {0}";

						var contact = portal.User as Contact;

						if (contact != null)
						{
							visitorCart.Adx_name = string.Format(nameFormat, contact.FullName);
						}

						visitorCart.Adx_VisitorID = string.Empty;
						visitorCart.adx_contactid = portal.User.ToEntityReference();

						if (!context.IsAttached(visitorCart))
						{
							context.Attach(visitorCart);
						}

						context.UpdateObject(visitorCart);
					}

					if (!context.IsAttached(visitorCart))
					{
						context.Attach(visitorCart);
					}

					context.SaveChanges();
				}
			}

			AnonymousIdentificationModule.ClearAnonymousIdentifier();
		}
	}
}