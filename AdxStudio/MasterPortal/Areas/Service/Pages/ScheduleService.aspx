<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebForms.master" CodeBehind="ScheduleService.aspx.cs" Inherits="Site.Areas.Service.Pages.ScheduleService" EnableEventValidation="false" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/Areas/Service/css/service.css" />
</asp:Content>

<asp:Content runat="server" ViewStateMode="Enabled" ContentPlaceHolderID="MainContent">
	<div class="page-heading">
		<% Html.RenderPartial("Breadcrumbs"); %>
		<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
		<div class="page-header">
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
		</div>
	</div>
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />

	<div class="service-schedule">
		<asp:Panel runat="server" ID="SearchPanel" CssClass="form-horizontal">
			<div class="control-group">
				<asp:Label CssClass="control-label" AssociatedControlID="ServiceType" Text='<%$ Snippet: Services/ScheduleService/ServiceType, Requested Service Type %>' runat="server"></asp:Label>
				<div class="controls">
					<asp:DropDownList runat="server" ID="ServiceType" />
				</div>
			</div>
			<div class="control-group">
				<asp:Label CssClass="control-label" AssociatedControlID="StartDate" Text='<%$ Snippet: Services/ScheduleService/DateRange, Service date range %>' runat="server"></asp:Label>
				<div class="controls">
					<div class="calendar">
						<asp:Calendar runat="server" ID="StartDate" />
					</div>
					<div class="calendar">
						<asp:Calendar runat="server" ID="EndDate" />
					</div>
				</div>
			</div>
			<div class="control-group">
				<asp:Label CssClass="control-label" AssociatedControlID="TimeZoneSelection" Text='<%$ Snippet: Services/ScheduleService/TimeZone, Select your time zone %>' runat="server"></asp:Label>
				<div class="controls">
					<asp:DropDownList EnableViewState="True" runat="server" ID="TimeZoneSelection" />
				</div>
			</div>
			<div class="control-group">
				<asp:Label CssClass="control-label" AssociatedControlID="StartTime" Text='<%$ Snippet: Services/ScheduleService/TimeOfDay, Time of day %>' runat="server"></asp:Label>
				<div class="controls">
					<asp:DropDownList runat="server" ID="StartTime" />
					<asp:DropDownList runat="server" ID="EndTime" />
				</div>
			</div>
			<asp:Label runat="server" ID="ErrorLabel" CssClass="alert alert-error alert-block" Visible="False" />
			<div class="form-actions">
				<asp:Button runat="server" OnClick="FindTimes_Click" CssClass="btn btn-primary" Text="<%$ Snippet: Services/ScheduleService/FindAvailableTimes, Find Available Times %>" />
			</div>
		</asp:Panel>

		<asp:Panel ID="NoServicesMessage" CssClass="alert alert-block alert-info" runat="server" Visible="False">
			<crm:Snippet runat="server" SnippetName="Services/ScheduleService/NoServicesAvailable" DefaultText="There are no services available to be scheduled." EditType="html" />
		</asp:Panel>

		<asp:Panel ID="ResultsDisplay" runat="server" Visible="false">
			<div class="form-horizontal">
				<div class="control-group">
					<div class="controls">
						<crm:Snippet runat="server" SnippetName="Services/ScheduleService/AppointmentTimeLabel" DefaultText="Please select from the following list of available appointment times." EditType="html" />
					</div>
				</div>
				<div class="control-group">		
					<div class="controls">
						<asp:GridView runat="server" ID="AvailableTimes" CssClass="table table-striped table-hover table-bordered"
							AutoGenerateColumns="false"
							DataKeyNames="AvailableResource, ScheduledStartUniversalTime, ScheduledEndUniversalTime"
							GridLines="None"
							OnRowDataBound="AvailableTimes_RowDataBound"
							OnSelectedIndexChanged="AvailableTimes_SelectedIndexChanged">
							<SelectedRowStyle CssClass="success"></SelectedRowStyle>
							<Columns>
								<asp:TemplateField HeaderText="Scheduled Start">
									<ItemTemplate>
										<%# Eval("ScheduledStart") %>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="Scheduled End">
									<ItemTemplate>
										<%# Eval("ScheduledEnd") %>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
					</div>
				</div>
				<asp:Label runat="server" ID="BookingError" CssClass="alert alert-block alert-error" Visible="False" />
				<div class="form-actions">
					<asp:Button runat="server" ID="ScheduleServiceButton" CssClass="btn btn-primary" Text="<%$ Snippet: Services/ScheduleService/ScheduleService %>" OnClick="ScheduleService_Click" />
				</div>
			</div>
		</asp:Panel>
	</div>
</asp:Content>