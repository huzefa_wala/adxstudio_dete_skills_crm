<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/WebForms.master" CodeBehind="ServiceDetails.aspx.cs" Inherits="Site.Areas.Service.Pages.ServiceDetails" %>
<%@ OutputCache CacheProfile="User" %>
<%@ Import Namespace="System.Web.Mvc.Html" %>

<asp:Content ContentPlaceHolderID="Head" runat="server">
	<link rel="stylesheet" href="~/Areas/Service/css/service.css" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
	<div class="page-heading">
		<% Html.RenderPartial("Breadcrumbs"); %>
		<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
		<div class="page-header">
			<h1>
				<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Title,Adx_name" EditType="text" runat="server" />
			</h1>
		</div>
	</div>
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" EditType="html" CssClass="page-copy" runat="server" />

	<div class="service-schedule">
		<dl>
			<dt>
				<crm:Snippet runat="server" SnippetName="Services/ServiceDetails/Service" DefaultText="Service" EditType="text" />
			</dt>
			<dd>
				<asp:Label runat="server" ID="serviceType" />
			</dd>
			<dt>
				<crm:Snippet runat="server" SnippetName="Services/ServiceDetails/StartTime" DefaultText="Start Time" EditType="text" />
			</dt>
			<dd>
				<asp:Label runat="server" ID="startTime" />
			</dd>
			<dt>
				<crm:Snippet runat="server" SnippetName="Services/ServiceDetails/EndTime" DefaultText="End Time" EditType="text" />
			</dt>
			<dd>
				<asp:Label runat="server" ID="endTime" />
			</dd>
		</dl>
		<div>
			<crm:CrmHyperLink runat="server" SiteMarkerName="View Scheduled Services" CssClass="btn btn-primary">
				<crm:Snippet runat="server" SnippetName="Services/ServiceDetails/ServicesLink" DefaultText="View All Scheduled Services" />
			</crm:CrmHyperLink>
		</div>
	</div>
</asp:Content>
