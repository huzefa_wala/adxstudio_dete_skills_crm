﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Comments.ascx.cs" Inherits="Site.Controls.Comments" %>
<%@ Import Namespace="Adxstudio.Xrm.Cms" %>
<%@ Import Namespace="Site.Helpers" %>
<%@ Register src="~/Controls/CommentCreator.ascx" tagname="CommentCreator" tagprefix="crm" %>
<%@ Register src="~/Controls/MultiRatingControl.ascx" tagname="MultiRatingControl" tagprefix="adx" %>

<asp:ObjectDataSource ID="CommentDataSource" TypeName="Adxstudio.Xrm.Cms.ICommentDataAdapter" OnObjectCreating="CreateCommentDataAdapter" SelectMethod="SelectComments" SelectCountMethod="SelectCommentCount" runat="server" />
<asp:ListView ID="CommentsView" DataSourceID="CommentDataSource" runat="server">
	<LayoutTemplate>
		<div class="post-comments">
			<legend>
				<asp:Literal Text='<%$ Snippet: Comments Heading, Comments %>' runat="server"></asp:Literal>
			</legend>
			<ol class="comments">
				<li id="itemPlaceholder" runat="server" />
			</ol>
		</div>
	</LayoutTemplate>
	<ItemTemplate>
		<li class='<%# ((bool)Eval("IsApproved")) ? "approved" : "unapproved" %>' runat="server">
			<asp:Panel  CssClass="alert alert-block alert-info" Visible='<%# !(bool)Eval("IsApproved") %>' runat="server" >
				<asp:Label  Text='<%$ Snippet: Unapproved Comment Message, This comment is not yet approved. It is only visible to you. %>' runat="server"></asp:Label>
			</asp:Panel>
			<asp:Panel  Visible='<%# Eval("Editable") ?? false %>' CssClass='<%# Eval("Entity.LogicalName", "post-comment-controls xrm-entity xrm-editable-{0}")%>' runat="server">
				<div class="btn-group">
					<a class="btn xrm-edit"><i class="icon-cog"></i></a>
					<a class="btn dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="#" class="xrm-edit"><i class="icon-edit"></i> Edit</a>
						</li>
						<li>
							<a href="#" class="xrm-delete"><i class="icon-remove"></i> Delete</a>
						</li>
					</ul>
				</div>
				<asp:HyperLink NavigateUrl='<%# Eval("EditPath.AbsolutePath") %>' CssClass="xrm-entity-ref" style="display:none;" runat="server"/>
				<asp:HyperLink NavigateUrl='<%# Eval("DeletePath.AbsolutePath") %>' CssClass="xrm-entity-delete-ref" style="display:none;" runat="server"/>
			</asp:Panel>
			<div class="pull-right">
				<adx:MultiRatingControl ID="Rating"
					EnableRatings='<%# Eval("RatingEnabled") %>'
					SourceID='<%# Eval("Entity.Id") %>' 
					LogicalName='<%# Eval("Entity.LogicalName") %>' 
					RatingInfo='<%# Bind("RatingInfo") %>' 
					RatingType="rating" 
					InitEventName="OnDataBinding"
					runat="server" />
			</div>
			<h4>
				<i class="icon-comment"></i>
				<asp:HyperLink rel="nofollow" NavigateUrl="<%# Url.AuthorUrl(Container.DataItem as IComment) %>" Text='<%# HttpUtility.HtmlEncode(Eval("Author.DisplayName") ?? "") %>' runat="server"></asp:HyperLink>
				<small>
					&ndash;
					<abbr class="timeago"><%# Eval("Date", "{0:r}") %></abbr>
				</small>
			</h4>
			<div><%# Eval("Content") %></div>
		</li>
	</ItemTemplate>
</asp:ListView>		
<asp:Panel ID="NewCommentPanel" runat="server">
	<crm:CommentCreator ID="NewCommentCreator" runat="server" />
</asp:Panel>

