﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChildNavigation.ascx.cs" Inherits="Site.Controls.ChildNavigation" %>

<div class="page-children">
	<% if (ShowChildren && Children.Any()) {%>
		<h4>
			<i class="icon-folder-open"></i>
			<crm:Snippet SnippetName="Page Children Heading" DefaultText="In This Section" EditType="text" runat="server"/>
		</h4>
		<ul class="nav nav-tabs nav-stacked">
			<% foreach (var node in Children) { %>
				<li>
					<a href="<%= node.Url %>">
						<i class="icon-file"></i>
						<%= node.Title %>
					</a>
				</li>
			<% } %>
		</ul>
	<% } %>
	<% if (ShowShortcuts && Shortcuts.Any()) {%>
		<h4>
			<i class="icon-share"></i>
			<crm:Snippet SnippetName="Page Related Heading" DefaultText="Related Topics" EditType="text" runat="server"/>
		</h4>
		<ul class="nav nav-tabs nav-stacked">
			<% foreach (var node in Shortcuts) { %>
				<li>
					<a href="<%= node.Url %>">
						<i class="icon-share-alt"></i>
						<%= node.Title %>
					</a>
				</li>
			<% } %>
		</ul>
	<% } %>
</div>