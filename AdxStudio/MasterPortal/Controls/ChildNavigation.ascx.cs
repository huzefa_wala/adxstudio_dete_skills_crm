﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using Microsoft.Xrm.Portal.Web;

namespace Site.Controls
{
	public partial class ChildNavigation : UserControl
	{
		private readonly Lazy<Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>> _childNodes;

		public ChildNavigation()
		{
			_childNodes = new Lazy<Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>>(GetChildNodes, LazyThreadSafetyMode.None);
			ShowChildren = true;
			ShowShortcuts = true;
		}

		public string Exclude { get; set; }

		public bool ShowChildren { get; set; }

		public bool ShowShortcuts { get; set; }

		protected IEnumerable<SiteMapNode> Children
		{
			get { return _childNodes.Value.Item1; }
		}

		protected IEnumerable<SiteMapNode> Shortcuts
		{
			get { return _childNodes.Value.Item2; }
		}

		private Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>> GetChildNodes()
		{
			var currentNode = SiteMap.CurrentNode;

			if (currentNode == null)
			{
				return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(new SiteMapNode[] {}, new SiteMapNode[] {});
			}

			var excludeLogicalNames = string.IsNullOrEmpty(Exclude)
				? new HashSet<string>(StringComparer.InvariantCultureIgnoreCase)
				: new HashSet<string>(Exclude.Split(',').Select(name => name.Trim()), StringComparer.InvariantCultureIgnoreCase);

			var shortcutNodes = new List<SiteMapNode>();
			var otherNodes = new List<SiteMapNode>();

			foreach (SiteMapNode childNode in currentNode.ChildNodes)
			{
				var entityNode = childNode as CrmSiteMapNode;

				if (entityNode != null && excludeLogicalNames.Any(entityNode.HasCrmEntityName))
				{
					continue;
				}

				if (entityNode != null && entityNode.HasCrmEntityName("adx_shortcut"))
				{
					shortcutNodes.Add(childNode);

					continue;
				}
				
				otherNodes.Add(childNode);
			}

			return new Tuple<IEnumerable<SiteMapNode>, IEnumerable<SiteMapNode>>(otherNodes, shortcutNodes);
		}
	}
}