﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiRatingControl.ascx.cs" Inherits="Site.Controls.MultiRatingControl" %>
<%@ Register TagPrefix="adx" TagName="Rating" Src="~/Controls/Rating.ascx" %>
<%@ Register TagPrefix="adx" TagName="Vote" Src="~/Controls/Vote.ascx" %>

<adx:Rating ID="RatingControl" runat="server" Visible="false" OnRatingChanged="Rating_Changed" EnableViewState="true" />
<adx:Vote ID="VoteControl" runat="server" Visible="false" OnVoteChanged="Vote_Changed" EnableViewState="true" />
