﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Xrm;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace DETE_Skills_CRM_Portal.Web_Pages
{
    public partial class TrainingContractSearch : System.Web.UI.Page
    {
        XrmServiceContext context;

        public TrainingContractSearch()
        {
            context = new XrmServiceContext("Dete Site");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            btn_search.Click += new EventHandler(search_onClick);

            if (!Page.IsPostBack)
            {
                // Load Contract Status Drop down list
                list_contractStatus.DataSource = context.TP_type_contract_statusSet.Select(status => status.TP_name).Distinct();
                list_contractStatus.DataBind();

                // Load Contract Sub Status Drop down list
                list_contractSubStatus.DataSource = context.TP_type_contract_status_subtypeSet.Select(subStatus => subStatus.TP_name).Distinct();
                list_contractSubStatus.DataBind();

                // Load Contract Type Drop down list
                list_contractType.DataSource = context.TP_type_contractSet.Select(subStatus => subStatus.TP_name).Distinct();
                list_contractType.DataBind();
            }
        }

        protected void search_onClick(object sender, EventArgs e)
        {
            // Create the fetch xml for the search
            string fetchxml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                "<entity name='tp_contract'>" +
                "<attribute name='tp_contractid' />" +
                "<attribute name='tp_termination_date' />" +
                "<attribute name='tp_delta_contractid' />" +
                "<attribute name='tp_commencement_date' />" +
                "<attribute name='tp_contracttype' />" +
                "<attribute name='tp_apprentice' />" +
                "<attribute name='tp_employerworkplacetext' />" +
                "<attribute name='tp_contractstatus' />" +
                "<attribute name='tp_district' />" +
                "<attribute name='tp_contractid' />" +
                "<order attribute='tp_apprentice' descending='false' />" +
                "<filter type='and'>" +
                    "<condition attribute='tp_delta_contractid' operator='like' value='%" + txt_contractNumber.Text + "%' />" +
                    ((txt_commencementDate.Text == null || txt_commencementDate.Text.Trim().Length == 0) ?
                    "" :
                    "<condition attribute='tp_commencement_date' operator='on-or-after' value='" + txt_commencementDate.Text + "'/>") +
                "</filter>" +
                "<link-entity name='tp_type_contract_status' from='tp_type_contract_statusid' to='tp_type_contract_statusid' alias='status'>" +
                    "<filter type='and'>" +
                        "<condition attribute='tp_name' operator='eq' value='" + list_contractStatus.SelectedValue + "' />" +
                    "</filter>" +
                "</link-entity>" +
                "<link-entity name='tp_type_contract_status_subtype' from='tp_type_contract_status_subtypeid' to='tp_type_contract_status_subtypeid' alias='subStatus'>" +
                    "<filter type='and'>" +
                        "<condition attribute='tp_name' operator='eq' value='" + list_contractStatus.SelectedValue + "' />" +
                    "</filter>" +
                "</link-entity>" +
                "<link-entity name='tp_type_contract' from='tp_type_contractid' to='tp_type_contractid' alias='type'>" +
                    "<filter type='and'>" +
                        "<condition attribute='tp_name' operator='eq' value='" + list_contractType.SelectedValue + "' />" +
                    "</filter>" +
                "</link-entity>" +
                "<link-entity name='account' from='accountid' to='tp_aac_accountid' alias='aac'>" +
                    "<attribute name='name'/>" +
                    "<filter type='and'>" +
                    "<condition attribute='name' operator='like' value='%" + (txt_aac.Text.Trim().Equals("all") ? "" : txt_aac.Text) + "%' />" +
                    "</filter>" +
                "</link-entity>" +
                "<link-entity name='account' from='accountid' to='tp_accountid' alias='employer'>" +
                    "<filter type='and'>" +
                        "<condition attribute='name' operator='like' value='%" + txt_employer.Text + "%' />" +
                    "</filter>" +
                "</link-entity>" +
                "<link-entity name='contact' from='contactid' to='tp_apprentice_contactid' alias='apprentice'>" +
                    "<attribute name='fullname'/>" +
                    "<filter type='and'>" +
                        "<condition attribute='fullname' operator='like' value='%" + txt_apprenticeFullName.Text + "%' />" +
                    "</filter>" +
                "</link-entity>" +                
                addUserCheckConditionInFetchXml() +
            "</entity>" +
        "</fetch>";

            // Retrieve the entities
            EntityCollection trainingContracts = context.RetrieveMultiple(new FetchExpression(fetchxml));

            // Create Grid Table
            DataTable gridTable = new DataTable();
            gridTable.Columns.Add("guid");
            gridTable.Columns.Add("contractNumber");
            gridTable.Columns.Add("contractType");
            gridTable.Columns.Add("employer");
            gridTable.Columns.Add("contractStatus");
            gridTable.Columns.Add("district");
            gridTable.Columns.Add("aac");
            gridTable.Columns.Add("apprentice");
            gridTable.Columns.Add("commencementDate");

            // Add rows to table
            DataRow gridRow;
            foreach (var contract in trainingContracts.Entities)
            {
                gridRow = gridTable.NewRow();
                gridRow["guid"] = contract.GetAttributeValue<Guid>("tp_contractid");
                gridRow["contractNumber"] = contract.GetAttributeValue<string>("tp_delta_contractid");
                gridRow["contractType"] = contract.GetAttributeValue<string>("tp_contracttype");
                gridRow["employer"] = contract.GetAttributeValue<string>("tp_employerworkplacetext");
                gridRow["contractStatus"] = contract.GetAttributeValue<string>("tp_contractstatus");
                gridRow["district"] = contract.GetAttributeValue<string>("tp_district");
                gridRow["aac"] = contract.GetAttributeValue<AliasedValue>("aac.name").Value;
                gridRow["apprentice"] = contract.GetAttributeValue<AliasedValue>("apprentice.fullname").Value;
                gridRow["commencementDate"] = contract.GetAttributeValue<DateTime>("tp_commencement_date").ToShortDateString();
                gridTable.Rows.Add(gridRow);
            }
            // Assign grid table to ASP Grid
            dataGrid.DataSource = gridTable;
            dataGrid.DataBind();
        }

        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dataGrid.PageIndex = e.NewPageIndex;
            search_onClick(sender, e);
        }

        private string addUserCheckConditionInFetchXml()
        {
            string strUserCondition = "";

            var portalContext = Microsoft.Xrm.Portal.Configuration.PortalCrmConfigurationManager.CreatePortalContext();
            Contact contactUser = (Contact)portalContext.User;

            if (contactUser == null)
            {
                // If no login user is found, default it to eddy.barker (only for POC purpose)
                contactUser = context.ContactSet.FirstOrDefault(contact => contact.FirstName == "eddy" && contact.LastName == "barker");
            }

            if (contactUser != null && !txt_aac.Text.Trim().Equals("all")){
                strUserCondition = "<link-entity name='account' from='accountid' to='tp_aac_accountid' alias='aacUser'>" +
                    "<filter type='and'>" +
                        "<condition attribute='name' operator='like' value='%" + contactUser.contact_customer_accounts.ToEntity<Account>().Name + "%' />" +
                    "</filter>" +
                "</link-entity>";
            }

            return strUserCondition;
        }

    }
}