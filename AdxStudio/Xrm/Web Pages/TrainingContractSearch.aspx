﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrainingContractSearch.aspx.cs" Inherits="DETE_Skills_CRM_Portal.Web_Pages.TrainingContractSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Scripts/moment.js"></script>
    <link href="../main.css" rel="stylesheet" type="text/css" />
    <link href="../css/pikaday.css" rel="stylesheet" />
    <link href="../css/site.css" rel="stylesheet" />
    <link href="../css/theme.css" rel="stylesheet" />
    <script src="../pikaday.js"></script>
    <link href="../Content/bootstrap-theme.css" rel="stylesheet" />
    <link href="../Content/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <adx:WebForm runat="server" ID="WebFormControl" />
    <form id="form1" runat="server">
        <%--<div>--%>
        <div class="row">
            <div class="col-lg-3">
                <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
                    <asp:TableRow ID="TableRow1" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Training Contract Number</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_contractNumber" runat="server"></asp:TextBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow2" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Training Contract Status</asp:Label>
                                <asp:DropDownList class="form-control" ID="list_contractStatus" runat="server" />
                            </div>                                 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow3" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Training Contract Sub Status</asp:Label>
                                <asp:DropDownList class="form-control" ID="list_contractSubStatus" runat="server" />
                            </div>                               
                        </asp:TableCell>
                    </asp:TableRow>                    
                </asp:Table>
            </div>
            <div class="col-lg-3">
                <asp:Table ID="Table3" runat="server" HorizontalAlign="Center">
                     <asp:TableRow ID="TableRow7" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Commencement Date</asp:Label>
                                 <asp:TextBox class="form-control" ID="txt_commencementDate" runat="server"></asp:TextBox>
                            <script type="text/javascript">
                                var picker = new Pikaday(
                                    {
                                        field: document.getElementById('txt_commencementDate'),
                                        firstday: 1,
                                        minDate: new Date('2000-01-01'),
                                        maxDate: new Date('2025-12-31'),
                                        yearRange: [2000, 2025],
                                        numberOfMonths: 1,
                                        theme: 'dark-theme',
                                        format: 'YYYY/MM/DD'
                                    });
                            </script>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow15" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Employer - Legal & Trading</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_employer" runat="server"></asp:TextBox>
                            </div>                              
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow5" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">AAC</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_aac" runat="server"></asp:TextBox>
                            </div>                             
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>

            <div class="col-lg-3">
                <asp:Table ID="Table2" runat="server" HorizontalAlign="Center">
                    <asp:TableRow ID="TableRow4" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Training Contract Type</asp:Label>
                                <asp:DropDownList class="form-control" ID="list_contractType" runat="server" />
                            </div>                             
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow13" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Apprentice name</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_apprenticeFullName" runat="server"></asp:TextBox>
                            </div>                               
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow10" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:Button CssClass="btn btn-primary" ID="btn_search" runat="server" Text="Search" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">

                <asp:Label ID="Message"
                    ForeColor="Red"
                    runat="server" />

                <asp:GridView ID="dataGrid" class="dataTable" runat="server" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="7" OnPageIndexChanging="grdData_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField='guid' HeaderText="ID" Visible="false" />
                        <asp:HyperLinkField DataTextField='contractNumber' NavigateUrl="~/TrainingContractForm.aspx"
                            DataNavigateUrlFormatString="TrainingContractForm.aspx?ID={0}" DataNavigateUrlFields="guid"
                            HeaderText="Contract Number" ItemStyle-CssClass="ContractNumberContent" />
                        <asp:BoundField DataField='contractType' HeaderText="Contract Type" SortExpression="name" />
                        <asp:BoundField DataField='contractStatus' HeaderText="Contract Status" />
                        <asp:BoundField DataField='employer' HeaderText="Employer Workplace" />
                        <asp:BoundField DataField='district' HeaderText="District" />
                        <asp:BoundField DataField='aac' HeaderText="AAC" />
                        <asp:BoundField DataField='apprentice' HeaderText="Apprentice" />
                        <asp:BoundField DataField='commencementDate' HeaderText="Commencement Date" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <%--</div>--%>
    </form>
</body>
</html>
