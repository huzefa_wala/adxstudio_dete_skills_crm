﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrainingContractForm.aspx.cs" Inherits="DETE_Skills_CRM_Portal.Web_Pages.TrainingContractForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script src="../Scripts/moment.js"></script>
    <script src="../Scripts/jquery-1.9.0.js"></script>
    <script src="../Scripts/validator.js"></script>
    
    <link href="../main.css" rel="stylesheet" type="text/css" />
    <link href="../css/pikaday.css" rel="stylesheet" />
    <link href="../css/site.css" rel="stylesheet" />
    <link href="../css/theme.css" rel="stylesheet" />
    <script src="../pikaday.js"></script>
    <link href="../Content/bootstrap-theme.css" rel="stylesheet" />
    <link href="../Content/bootstrap.css" rel="stylesheet" />

    <script>
        function onCommencementDateChange(ctrlDate) {
            var dateCommencement = new Date(ctrlDate.value);

            // Nomination date
            var strNomDate = document.getElementById('<%=txt_Nominal_Completion.ClientID%>');
            if (strNomDate != null) {
                var dateNomination = new Date(strNomDate.value);
                if (dateCommencement > dateNomination) {
                    alert("Commencement date should be less than nomination date");
                    ctrlDate.value = strNomDate.value;
                }
            }
        }

        $('#formTrainingContract').validator({
            custom: {
                myOwnValidator: function (elem) {
                    // dummy example: value should be current timestamp
                    alert(1);
                    return $(elem).val() == new Date().getTime()
                }
            }
        })
    </script>
</head>
<body>
    <form id="formTrainingContract"  data-toggle="validator" runat="server">
        <asp:TextBox class="form-control" ID="txt_contractId" runat="server" Visible="false"></asp:TextBox>

        <div class="row">
            <div class="col-lg-4">
                <!-- Table 1 -->
                <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">
                    <asp:TableRow ID="TableRow1" runat="server" ForeColor="Black" Enabled="false">                        
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Data Source</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Data_Source" runat="server" required="true"/>
                            </div>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow2" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Intray/Contract No</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_intray" runat="server"></asp:TextBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow3" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Apprentice</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Apprentice" runat="server"></asp:TextBox>
                            </div>                                    
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow4" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">School</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_School" data-custom="myOwnValidator" runat="server"></asp:TextBox>
                             </div>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow5" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Commencement</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_commencement" required="true" data-custom="myOwnValidator" onchange="javascript: onCommencementDateChange(this);"  runat="server"/>
                                <div class="help-block with-errors"></div>
                                    <script type="text/javascript">
                                        var picker = new Pikaday(
                                            {
                                                field: document.getElementById('txt_commencement'),
                                                firstday: 1,
                                                minDate: new Date('2000-01-01'),
                                                maxDate: new Date('2025-12-31'),
                                                yearRange: [2000, 2025],
                                                numberOfMonths: 1,
                                                theme: 'dark-theme',
                                                format: 'YYYY/MM/DD'
                                            });
                                    </script>
                            </div>                       
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow6" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Recommencement?</asp:Label>
                                <asp:CheckBox class="form-control" ID="check_Recommencement" runat="server"></asp:CheckBox>
                            </div>                                   
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow7" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Nominal Completion</asp:Label>
                            <asp:TextBox class="form-control" ID="txt_Nominal_Completion" required="true" runat="server"></asp:TextBox>
                            <div class="help-block with-errors"></div>
                            <script type="text/javascript">
                                var picker = new Pikaday(
                                    {
                                        field: document.getElementById('txt_Nominal_Completion'),
                                        firstday: 1,
                                        minDate: new Date('2000-01-01'),
                                        maxDate: new Date('2025-12-31'),
                                        yearRange: [2000, 2025],
                                        numberOfMonths: 1,
                                        theme: 'dark-theme',
                                        format: 'YYYY/MM/DD'
                                    });
                            </script>
                                </div>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow16" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Contract Substatus</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Contract_Substatus" runat="server" />
                            </div>                              
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow8" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">District</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_District" runat="server"></asp:TextBox>
                            </div>                                
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow17" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Termination</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Termination" required="true" runat="server"></asp:TextBox>
                            <div class="help-block with-errors"></div>
                            <script type="text/javascript">
                                var picker = new Pikaday(
                                    {
                                        field: document.getElementById('txt_Termination'),
                                        firstday: 1,
                                        minDate: new Date('2000-01-01'),
                                        maxDate: new Date('2025-12-31'),
                                        yearRange: [2000, 2025],
                                        numberOfMonths: 1,
                                        theme: 'dark-theme',
                                        format: 'YYYY/MM/DD'
                                    });
                            </script>
                            </div>                             
                        </asp:TableCell>
                    </asp:TableRow>


                    <asp:TableRow ID="TableRow18" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Probation Completion</asp:Label>
                                 <asp:TextBox class="form-control" ID="txt_Probation_Completion" runat="server"></asp:TextBox>
                                     <script type="text/javascript">
                                var picker = new Pikaday(
                                    {
                                        field: document.getElementById('txt_Probation_Completion'),
                                        firstday: 1,
                                        minDate: new Date('2000-01-01'),
                                        maxDate: new Date('2025-12-31'),
                                        yearRange: [2000, 2025],
                                        numberOfMonths: 1,
                                        theme: 'dark-theme',
                                        format: 'YYYY/MM/DD'
                                    });
                            </script>
                            </div>    
                           
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow21" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Probation Period</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Probation_Period" runat="server"></asp:TextBox>
                            </div>                                   
                        </asp:TableCell>
                    </asp:TableRow>                   

                </asp:Table>
            </div>
            <%-- ******END OF TABLE 1***** --%>


            <!-- Start of table 2 -->
            <div class="col-lg-3">
            <asp:Table ID="Table2" runat="server" HorizontalAlign="Center">                 
                    <asp:TableRow ID="TableRow22" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Previous Employee</asp:Label>
                                <asp:CheckBox class="form-control" ID="check_Previous_Employee" runat="server"></asp:CheckBox>
                            </div>      
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow50" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Credit (Months)</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Credit_Months" runat="server"></asp:TextBox>
                            </div>                               
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow51" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Full Time Employment Start Date</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Full_Time_Employment_Start_Date" runat="server"></asp:TextBox>
                            </div>                              
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow52" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Full Time Employment Months</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Full_Time_Employment_Months" runat="server"></asp:TextBox>
                            </div>                             
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow53" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Part Time Employment Start Date</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Part_Time_Employment_Start_Date" runat="server"></asp:TextBox>
                            </div>                                                         
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow54" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Part Time Employment Months</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Part_Time_Employment_Months" runat="server"></asp:TextBox>
                            </div>                                 
                        </asp:TableCell>
                    </asp:TableRow>


                    <asp:TableRow ID="TableRow55" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Casual Time Employment Start Date</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Casual_Time_Employment_Start_Date" runat="server"></asp:TextBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow56" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Casual Time Employment Months</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Casual_Time_Employment_Months" runat="server"></asp:TextBox>
                            </div>                             
                        </asp:TableCell>
                    </asp:TableRow>

                 <asp:TableRow ID="TableRow9" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Qualification</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Qualification" runat="server"></asp:TextBox>
                            </div>                             
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow10" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                             <div class="form-group">
                                <asp:Label runat="server" class="control-label">Occupational Model</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Occupational_Model" runat="server"></asp:TextBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow11" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Current SRTO</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Current_SRTO" runat="server"></asp:TextBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow12" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Contract Mode</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Current_Mode" runat="server"></asp:TextBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>

                </asp:Table>
            </div>
        
        <!-- End of table 2 -->

            <div class="col-lg-3">
                <asp:Table ID="Table3" runat="server" HorizontalAlign="Center">                  

                    <asp:TableRow ID="TableRow13" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Contract Type</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_contractType" runat="server"></asp:TextBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>


                    <asp:TableRow ID="TableRow14" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Receipt No</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Receipt_No" runat="server"></asp:TextBox>
                            </div>                                  
                        </asp:TableCell>
                    </asp:TableRow>


                    <asp:TableRow ID="TableRow15" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Contract Status</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_contractStatus" runat="server"></asp:TextBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow19" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">AAC</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_AAC" runat="server"></asp:TextBox>
                            </div>                                 
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow20" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">State Funding?</asp:Label>
                                <asp:CheckBox class="form-control" ID="check_State_Funding" runat="server"></asp:CheckBox>
                            </div>                              
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow23" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Existing Worker</asp:Label>
                                <asp:CheckBox class="form-control" ID="check_Existing_Worker" runat="server"></asp:CheckBox>
                            </div>                            
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow24" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Full Time Employment End Date</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Full_Time_Employment_End_Date" runat="server"></asp:TextBox>
                            </div>                                 
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow25" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Part Time Employment End Date</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Part_Time_Employment_End_Date" runat="server"></asp:TextBox>
                            </div>                              
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow26" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Part Time Employment Hours</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Part_Time_Employment_Hours" runat="server"></asp:TextBox>
                            </div>                              
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow27" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Casual Time Employment End Date</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Casual_Time_Employment_End_Date" runat="server"></asp:TextBox>
                            </div>                              
                        </asp:TableCell>
                    </asp:TableRow>


                    <asp:TableRow ID="TableRow28" runat="server" ForeColor="Black" Enabled="false">
                        <asp:TableCell>
                            <div class="form-group">
                                <asp:Label runat="server" class="control-label">Casual Time Employment Hours</asp:Label>
                                <asp:TextBox class="form-control" ID="txt_Casual_Time_Employment_Hours" runat="server"></asp:TextBox>
                            </div>                                   
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow36" runat="server" ForeColor="Black">
                        <asp:TableCell>
                            <asp:Button CssClass="btn btn-primary" ID="btn_update" runat="server" Text="Update" />
                        </asp:TableCell>

                    </asp:TableRow>

                </asp:Table>
            </div>
            <%--************END OF TABLE 2************  --%>
            <%--</div>--%>
        </div>
    </form>
</body>
</html>
