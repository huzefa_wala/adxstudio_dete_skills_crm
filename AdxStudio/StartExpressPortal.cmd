@echo off

echo ----
echo Usage: %0 [site name]
echo Example: %0 "ADX - Community Portal"
echo  - pass empty parameters to launch the default "Adxstudio Portals" website
echo  - use the IIS Express system tray tool to browse the website
echo  - if IIS Express shows errors at start, you may need to close an existing IIS Express from the system tray or configure the site to run on another port
echo ----

set exec-iisexpress=%PROGRAMFILES%\IIS Express\iisexpress.exe
set appHostConfigPath=%~dp0applicationhost.config
set defaultSiteName="Adxstudio Portals"
if [%1]==[] (set siteName=%defaultSiteName%) else (set siteName=%1)

@echo on

REM variables referenced within the applicationhost.config
set SCRIPT_PATH=%~dp0

start "" "%exec-iisexpress%" /config:"%appHostConfigPath%" /site:%siteName%
