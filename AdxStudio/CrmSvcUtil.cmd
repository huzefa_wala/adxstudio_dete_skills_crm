REM CrmSvcUtil.exe command line usage examples.  
REM Uncomment out the appropriate line for your CRM environment.
REM Replace the url and credential parameters with your values.
REM For complete documenation visit http://community.adxstudio.com/Default.aspx?DN=36c4a636-55d8-4c1d-a7e9-a591ab29fa23

REM Integrated Authentication Usage Example:
REM $0\..\..\Framework\CrmSvcUtil.exe /codeCustomization:"Microsoft.Xrm.Client.CodeGeneration.CodeCustomization, Microsoft.Xrm.Client.CodeGeneration" /url:http://mycrmserver/myorgname/XRMServices/2011/Organization.svc /out:"Xrm\Xrm.cs" /namespace:Xrm /servicecontextname:XrmServiceContext /servicecontextprefix:Xrm
$0\..\..\Framework\CrmSvcUtil.exe /codeCustomization:"Microsoft.Xrm.Client.CodeGeneration.CodeCustomization, Microsoft.Xrm.Client.CodeGeneration" /url:http://crm2011/AdxstudioPortals/XRMServices/2011/Organization.svc /out:"Xrm\Xrm.cs" /namespace:Xrm /servicecontextname:XrmServiceContext /servicecontextprefix:Xrm

REM Active Directory Authentication Usage Example:
REM $0\..\..\Framework\CrmSvcUtil.exe /codeCustomization:"Microsoft.Xrm.Client.CodeGeneration.CodeCustomization, Microsoft.Xrm.Client.CodeGeneration" /url:http://mycrmserver/myorgname/XRMServices/2011/Organization.svc /domain:contoso /username:administrator /password:pass@word1 /out:"Xrm\Xrm.cs" /namespace:Xrm /servicecontextname:XrmServiceContext /servicecontextprefix:Xrm


REM Live ID (CRM Online) Authentication Usage Example:
REM $0\..\..\Framework\CrmSvcUtil.exe /codeCustomization:"Microsoft.Xrm.Client.CodeGeneration.CodeCustomization, Microsoft.Xrm.Client.CodeGeneration" /url:http://myorg.api.crm.dynamics.com/XRMServices/2011/Organization.svc /username:user@hotmail.com /password:pass@word1 /deviceid:mydeviceid /devicepassword:mydevicepassword /out:"Xrm\Xrm.cs" /namespace:Xrm /servicecontextname:XrmServiceContext /servicecontextprefix:Xrm


REM Claims Authentication (IFD, Active Directory) Usage Example:
REM $0\..\..\Framework\CrmSvcUtil.exe /codeCustomization:"Microsoft.Xrm.Client.CodeGeneration.CodeCustomization, Microsoft.Xrm.Client.CodeGeneration" /url:https://mycrmserver:555/myorgname/XRMServices/2011/Organization.svc /username:administrator /password:pass@word1 /out:"Xrm\Xrm.cs" /namespace:Xrm /servicecontextname:XrmServiceContext /servicecontextprefix:Xrm

pause