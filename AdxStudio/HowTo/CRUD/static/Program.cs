﻿using System;
using System.Linq;
using Microsoft.Xrm.Client;
using Xrm;

namespace CRUDstatic
{
	class Program
	{
		/// <summary>
		/// The following sample demonstrates how to do basic entity CRUD operations such as create, 
		/// retrieve, update, and delete with statically typed entities.
		/// </summary>
		static void Main()
		{
			Console.WriteLine("Sample demonstrating basic CRUD operations with statically typed entities:");

			// Provide the connection string to your Microsoft Dynamics CRM server.
			const string connectionString = "ServiceUri=http://crm/Contoso;";

			// The following details specify the format of the connection string:
			// ServiceUri - Required parameter, specifies the url to the CRM server.  It can be http or https and the port is optional 
			//    if it is http over port 80 or https over port 443.  The server url is typically in the format of 
			//    http://crm-server:port/organization-name or for CRM Online https://organization-name.crm.dynamics.com. The organization-name is required.
			// Integrated On Premise Authentication does not require any additional parameters:
			//   *** Example *** "ServiceUri=http://crm.contoso.com/xrmContoso;"
			// Active Directory Authentication requires the following additional parameters:
			//   Domain - Specifies the domain that will verify user credentials.
			//   Username - The user's identification name associated with the credentials.
			//   Password - The pass code for the user associated with the credentials.
			//   *** Example *** "ServiceUri=http://crm.contoso.com/xrmContoso; Domain=CONTOSO; Username=jsmith; Password=pass@word1;"
			// CRM Online Windows Live ID Authentication requires the following parameters;
			//   Username - The user's identification name associated with the credentials.
			//   Password - The pass code for the user associated with the credentials.
			//   DeviceID - Specifies the user-defined Windows Live Services Device ID.
			//   DevicePassword - Specifies the user-defined Windows Live Services Device Password.
			//   *** Example *** "ServiceUri=https://xrmcontoso.crm.dynamics.com; Username=jsmith@live.com; Password=pass@word1; DeviceID=contoso-ba9f6b7b2e6d; DevicePassword=pass@word2;"
			// For further connection string details, please visit http://community.adxstudio.com/Default.aspx?DN=8c4ce1a6-25f4-4ae6-b917-63cdb49f4fd0
			if (string.IsNullOrEmpty(connectionString))
			{
				Console.WriteLine("Please assign a value to the connectionString variable.");

				Console.ReadLine();

				return;
			}

			// Connect to CRM
			var connection = CrmConnection.Parse(connectionString);

			// Create a service context
			var context = new XrmServiceContext(connection);

			// Create entity
			var account1 = new Account {Name = "Fabrikam"};

			context.AddObject(account1);
			context.SaveChanges();

			Console.WriteLine(string.Format("\n{0} {1} created.", account1.LogicalName, account1.Name));

			var accountId = account1.Id;

			// Retrieve entity
			var fabrikam = context.AccountSet.FirstOrDefault(a => a.AccountId == accountId);

			Console.WriteLine(string.Format("\n{0} - City: {1} - Phone#: {2}", fabrikam.Name, fabrikam.Address1_City ?? "unknown", fabrikam.Telephone1 ?? "unknown"));

			// Update entity
			fabrikam.Telephone1 = "(306)596-6500";
			fabrikam.Address1_City = "Regina";

			context.UpdateObject(fabrikam);
			context.SaveChanges();

			Console.WriteLine(string.Format("\n{0} {1} updated.", fabrikam.LogicalName, fabrikam.Name));
			Console.WriteLine(string.Format("\n{0} - City: {1} - Phone#: {2}", fabrikam.Name, fabrikam.Address1_City ?? "unknown", fabrikam.Telephone1 ?? "unknown"));
			Console.WriteLine("\nSample CRUD demo complete.");
			Console.WriteLine("\nPress any key to delete the entity records created in this sample.");

			Console.ReadLine();

			// Delete entity
			context.DeleteObject(account1);
			context.SaveChanges();

			Console.WriteLine("Entity records have been deleted.");
			Console.WriteLine("\nPress any key to quit");

			Console.ReadLine();
		}
	}
}
