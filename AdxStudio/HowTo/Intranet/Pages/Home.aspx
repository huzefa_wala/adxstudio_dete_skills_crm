<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="True" CodeBehind="Home.aspx.cs" Inherits="Site.Pages.Home" %>
<%@ OutputCache VaryByParam="*" VaryByCustom="user" Duration="60" %>

<asp:Content ContentPlaceHolderID="Breadcrumbs" runat="server" />

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
	<crm:CrmEntityDataSource ID="CurrentEntity" DataItem="<%$ CrmSiteMap: Current %>" runat="server" />
	<crm:Property DataSourceID="CurrentEntity" PropertyName="Adx_Copy" CssClass="copy" EditType="html" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div id="Tabular-Data">
		<asp:Label ID="ViewLabel" Font-Bold="true" runat="server">View: </asp:Label>
		<asp:DropDownList ID="ActivityFilter" AutoPostBack="true" runat="server" CssClass="case-filter">
		</asp:DropDownList>
		<asp:GridView ID="ActivityList" runat="server" CssClass="tabular-data" AlternatingRowStyle-CssClass="alternate-row">
			<EmptyDataTemplate>
				<crm:Snippet runat="server" SnippetName="activities/view/empty" DefaultText="No Activity records are available in this view."/>
			</EmptyDataTemplate>
		</asp:GridView>
	</div>
</asp:Content>
