<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.master" AutoEventWireup="True" CodeBehind="SiteMap.aspx.cs" Inherits="Site.Pages.SiteMap" %>
<%@ OutputCache VaryByParam="*" VaryByCustom="user" Duration="60" %>

<asp:Content ContentPlaceHolderID="ContentBottom" runat="server">
	<div class="site-map frame">
		<asp:SiteMapDataSource ID="SiteMapDataSource" runat="server" />
		<asp:TreeView DataSourceID="SiteMapDataSource" runat="server" />
	</div>
</asp:Content>
