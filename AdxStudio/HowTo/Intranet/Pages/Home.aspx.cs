﻿using System;
using System.Linq;
using Microsoft.Xrm.Portal.Data;
using Microsoft.Xrm.Portal.Web.UI;
using Microsoft.Xrm.Sdk.Query;

namespace Site.Pages
{
	public partial class Home : PortalPage
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			using (var context = CreateXrmServiceContext())
			{
				var views = context.CreateQuery("savedquery")
					.Where(sq => sq.GetAttributeValue<string>("returnedtypecode") == "activitypointer"
						&& sq.GetAttributeValue<int>("querytype") == 0)
					.OrderBy(sq => sq.GetAttributeValue<string>("name"))
					.Select(sq => new { Name = sq.GetAttributeValue<string>("name"), IsDefault = sq.GetAttributeValue<bool>("isdefault") })
					.ToArray();

				var defaultView = views.FirstOrDefault(v => v.IsDefault);

				ActivityFilter.DataSource = views.Select(v => v.Name);
				ActivityFilter.DataBind();
				ActivityFilter.Text = defaultView != null ? defaultView.Name : null;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			using (var context = CreateXrmServiceContext())
			{
				var viewName = ActivityFilter.Text;

				var savedQueries = context.CreateQuery("savedquery")
					.Where(sq => sq.GetAttributeValue<string>("returnedtypecode") == "activitypointer"
						&& sq.GetAttributeValue<string>("name") == viewName
						&& sq.GetAttributeValue<int>("querytype") == 0);

				var savedQuery = savedQueries.First();
				var activities = context.RetrieveMultiple(new FetchExpression(savedQuery.GetAttributeValue<string>("fetchxml")));

				ActivityList.DataSource = activities.Entities.ToDataTable(context, savedQuery, true);
				ActivityList.ColumnsGenerator = new CrmSavedQueryColumnsGenerator(viewName);
				ActivityList.DataBind();
			}
		}
	}
}
