﻿<%@ Page Language="C#" ContentType="text/css" %>
<%@ OutputCache Duration="600" VaryByContentEncoding="gzip;deflate" VaryByParam="None" %>

html, body {
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/body_background, #e0e0e0 %>" />;
}

a {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/links, #36566b %>" />;
}

a:visited {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/links_visited %>" />;
}

a:hover{
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/links_hover %>" />;
}

a:active {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/links_active %>" />;
}

#layout {
	width: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/layout_width, 984px %>" />;
}

#hd {
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/header_background, #034488 %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/header_txt_color, #ffffff %>" />;
}

#hd a {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/header_links, #ffffff %>" />;
}

#hd a:visited {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/header_links_visited, #ffffff %>" />;
}

#hd a:hover {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/header_links_hover, #ffffff %>" />;
}

#hd a:active {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/header_links_active, #ffffff %>" />;
}

#hd .navigation li a {
	background-color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/navigation_background, #1c2d38 %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/navigation_font, #ffffff %>" />;
}

#hd .navigation li a:hover {
	background-color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/navigation_background_hover, #36566b %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/navigation_font_hover, #ffffff %>" />;
}

#hd .navigation li a.active{
	background-color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/navigation_background_active, #36566b %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/navigation_font_active, #ffffff %>" />;
}

#main {
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/left_column_background, #ffffff %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/left_column_txt_color, #000000 %>" />;
}

#bd {
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/left_column_background, #ffffff %>" />;
}

#bd .breadcrumbs {
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/breadcrumbs_background, #eff5fa %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/breadcrumbs_txt_color, #000000 %>" />;
}

#bd .breadcrumbs a {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/breadcrumbs_links %>" />;
}

#bd .breadcrumbs a:visited {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/breadcrumbs_links_visted %>" />;
}

#bd .breadcrumbs a:hover {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/breadcrumbs_links_hover %>" />;
}

#bd .breadcrumbs a:active {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/breadcrumbs_links_active %>" />;
}

.pageTitle {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/pageTitle_font_color, #1c2d38 %>" />;
}

#content {
	width: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/left_column_width, 690px %>" />;
}

#sidebar {
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/right_column_background, #f7feff %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/right_column_txt_color, #000000 %>" />;
	width: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/right_column_width, 190px %>" />;
}

#sidebar a {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/sidebar_links %>" />;
}

#sidebar a:visited {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/sidebar_links_visited %>" />;
}

#sidebar a:hover {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/sidebar_links_hover %>" />;
}

#sidebar a:active {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/sidebar_links_active %>" />;
}

#ft {
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/footer_background, #034488 %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/footer_txt_color, #ffffff %>" />;
}

#ft a {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/footer_links, #ffffff %>" />;
}

#ft a:visited {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/footer_links_visited, #ffffff %>" />;
}

#ft a:hover {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/footer_links_hover, #ffffff %>" />;
}

#ft a:active {
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/footer_links_active, #ffffff %>" />;
}

<%--Tabular Data / Gridviews--%>

#Tabular-Data .tabular-data th {
	background-color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/grid_table_header, #1c2d38 %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/grid_table_header_font_color, #ffffff %>" />;
}

#Tabular-Data .tabular-data .alternate-row {
	background-color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/grid_table_alternate_row, #cccccc %>" />;
}

<%--Form--%>

#content .frame {
	background-color:<asp:Literal runat="server" Text="<%$ SiteSetting: /css/form_background, #eff5fa %>" />;
}

input.button {
	border: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/button_border %>" />;
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/button_background, #014083 %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/button_text_color, #ffffff %>" />;
}

input.button:hover {
	border: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/button_border_hover %>" />;
	background: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/button_background_hover, #36566b %>" />;
	color: <asp:Literal runat="server" Text="<%$ SiteSetting: /css/button_text_color_hover, #ffffff %>" />;
}