﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Microsoft.IdentityModel.Web;
using Microsoft.Xrm.Portal.IdentityModel;

namespace Site
{
	public class Global : HttpApplication
	{
		void Application_Start(object sender, EventArgs e)
		{
			FederatedAuthentication.ServiceConfigurationCreated += Extensions.OnServiceConfigurationCreated;
		}

		public override string GetVaryByCustomString(HttpContext context, string arg)
		{
			switch (arg)
			{
				case "user":

					var identity = context.User != null ? context.User.Identity.Name : string.Empty;

					return string.Format("IsAuthenticated={0},Identity={1}", context.Request.IsAuthenticated, identity);
			}

			return string.Empty;
		}

		// when the IPrincipal/IIdentity is not serialization friendly (applies to WindowsPrincipal/WindowsIdentity),
		// the System.Runtime.Caching API throws an exception and results in an inoperable MemoryCache

		public void WindowsAuthentication_OnAuthenticate(object sender, WindowsAuthenticationEventArgs args)
		{
			if (!args.Identity.IsAnonymous)
			{
				// convert the WindowsIdentity into a GenericIdentity to avoid a System.Runtime.Caching error

				args.User = new RolePrincipal(new GenericIdentity(args.Identity.Name));
			}
		}
	}
}