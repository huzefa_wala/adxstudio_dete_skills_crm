﻿using System;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;

namespace WhoAmI
{
	class Program
	{
		static void Main()
		{
			Console.WriteLine("Who Am I Example: \nRetrieve the system user entity instance for the currently logged on user or the user under whose context the executed code is running.");
			
			var connectionString = GetConnectionString();

			if (string.IsNullOrEmpty(connectionString))
			{
				Console.Clear();

				Main();
			}
			
			var crm = new CrmOrganizationServiceContext(CrmConnection.Parse(connectionString));

			Console.WriteLine("\nExecuting WhoAmIRequest...");

			try
			{
				var user = crm.Execute(new WhoAmIRequest()) as WhoAmIResponse;

				Console.WriteLine("\nWhoAmIResponse:");

				Console.WriteLine(user == null ? "\n: UserId could not be determined." : string.Format("\nUserId = {0}", user.UserId));
			}
			catch (Exception e)
			{
				Console.WriteLine(string.Format("\nError: {0}", e.InnerException.Message));
			}

			Console.WriteLine("\nPress any key to quit.");

			Console.ReadLine();
		}

		/// <summary>
		/// Prompts the user for CRM server organization URL and credentials for authentication.
		/// </summary>
		/// <returns>Returns a connection string to a CRM server organization</returns>
		protected static string GetConnectionString()
		{
			string serverUrl;
			var domain = string.Empty;
			string username;
			string password;
			var deviceId = string.Empty;
			var devicePassword = string.Empty;
			String[] domainAndUserName;

			do
			{
				Console.Write("\nEnter a URL to a CRM server [https://name.crm.dynamics.com or http://crmserver/organization]: ");

				serverUrl = Console.ReadLine();

				if (string.IsNullOrWhiteSpace(serverUrl))
				{
					Console.WriteLine("\nInvalid server url.");
				}
			} while (string.IsNullOrWhiteSpace(serverUrl));

			if (!serverUrl.EndsWith("crm.dynamics.com"))
			{
				do
				{
					Console.Write("\nEnter Domain\\Username: ");

					domainAndUserName = Console.ReadLine().Split('\\');

					if (domainAndUserName.Length != 2 || String.IsNullOrWhiteSpace(domainAndUserName[0]) || String.IsNullOrWhiteSpace(domainAndUserName[1]))
					{
						Console.WriteLine("\nInvalid Domain\\Username.");
					}

				} while (domainAndUserName.Length != 2 || String.IsNullOrWhiteSpace(domainAndUserName[0]) || String.IsNullOrWhiteSpace(domainAndUserName[1]));

				domain = domainAndUserName[0];

				username = domainAndUserName[1];

			} else
			{
				do
				{
					Console.Write("\nEnter Windows Live ID: ");

					username = Console.ReadLine();

					if (string.IsNullOrWhiteSpace(username))
					{
						Console.WriteLine("\nInvalid Windows Live ID.");
					}
				} while (string.IsNullOrWhiteSpace(username));
			}

			do
			{
				Console.Write("\nEnter Password: ");

				password = ReadPassword();

				if (string.IsNullOrWhiteSpace(password))
				{
					Console.WriteLine("\nInvalid Password.");
				}
			} while (string.IsNullOrWhiteSpace(password));

			if (serverUrl.EndsWith("crm.dynamics.com"))
			{
				do
				{
					Console.Write("\nEnter Device Id: ");

					deviceId = Console.ReadLine();

					if (string.IsNullOrWhiteSpace(deviceId))
					{
						Console.WriteLine("\nInvalid Device Id.");
					}
				} while (string.IsNullOrWhiteSpace(deviceId));
				
				do
				{
					Console.Write("\nEnter Device Password: ");

					devicePassword = ReadPassword();

					if (string.IsNullOrWhiteSpace(devicePassword))
					{
						Console.WriteLine("\nInvalid Device Password.");
					}
				} while (string.IsNullOrWhiteSpace(devicePassword));
			}

			var connectionString = string.Format("Url={0}", serverUrl);

			connectionString += string.Format(";");

			connectionString += string.Format("; Domain={0}; UserName={1}; Password={2};", domain, username, password);

			if (serverUrl.EndsWith("crm.dynamics.com"))
			{
				connectionString += string.Format("DeviceID={0}; DevicePassword={1};", deviceId, devicePassword);
			}

			return connectionString;
		}

		/// <summary>
		/// Retrieves the characters as they are typed and outputs an asterisk to conceal the password
		/// from the console window and returns the password upon the Enter key being pressed.
		/// </summary>
		/// <returns>Password as a string</returns>
		protected static string ReadPassword()
		{
			var sbPassword = new StringBuilder();
			var info = Console.ReadKey(true);

			while (info.Key != ConsoleKey.Enter)
			{
				if (info.Key == ConsoleKey.Backspace)
				{
					if (sbPassword.Length != 0)
					{
						sbPassword.Remove(sbPassword.Length - 1, 1);

						Console.Write("\b \b"); // erase last char
					}
				}
				else if (info.KeyChar >= ' ') // no control chars
				{
					sbPassword.Append(info.KeyChar);

					Console.Write("*");
				}

				info = Console.ReadKey(true);
			}

			Console.WriteLine();

			return sbPassword.ToString();
		}
	}
}
