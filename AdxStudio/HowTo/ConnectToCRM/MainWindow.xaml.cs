﻿using System.Windows;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Windows.Controls.ConnectionDialog;

namespace ConnectToCRM
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void btnConnect_Click(object sender, RoutedEventArgs e)
		{
			var connectionDialog = new ConnectionDialog();
			
			var result = connectionDialog.ShowDialog();
			
			if (result == true)
			{
				txtConnectionString.Text = connectionDialog.ConnectionString;
			}
		}

		private void btnRefresh_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(txtConnectionString.Text))
			{
				return;
			}
			
			var context = new CrmOrganizationServiceContext(CrmConnection.Parse(txtConnectionString.Text));
			
			var contacts = context.CreateQuery("contact");

			txtResults.Text = string.Empty;

			foreach (var contact in contacts)
			{
				txtResults.Text += contact["fullname"] + "\n";
			}
		}
	}
}
