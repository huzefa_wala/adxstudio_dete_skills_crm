@echo off

echo Usage: %0 [app^|intranet^|removeall]
echo  - pass empty parameters to create the sample portals in IIS
echo  - site: create the portals in IIS as separate websites
echo  - app: create the portals in IIS as applications under a single website
echo  - intranet: create the intranet portal in IIS
echo  - removeall: remove all of the sample portals from IIS

set action=%~1
set appPoolName=XRM
set masterPortalPath=%~dp0MasterPortal
set welcomePortalPath=%~dp0Welcome

call:init

if "%action%" equ "removeall" (
	call:removeSites
	goto :eof
)

if "%action%" equ "intranet" (
	call:addIntranet
	goto :eof
)

if "%action%" equ "app" (
	call:addApps
	goto :eof
)

if "%action%" equ "site" (
	call:addSites
	goto :eof
)

if "%action%" equ "" (
	call:addSites
	call:addApps
	goto :eof
)

goto :eof

:addSites
setlocal
	call:addAppPool "%appPoolName%"
	if errorlevel 1 goto :eof
	call:addWebsite "ADX - Customer Portal" 7500 "%appPoolName%" "%masterPortalPath%"
	call:addWebsite "ADX - Company Portal" 7501 "%appPoolName%" "%masterPortalPath%"
	call:addWebsite "ADX - Conference Portal" 7502 "%appPoolName%" "%masterPortalPath%"
	call:addWebsite "ADX - Government Portal" 7503 "%appPoolName%" "%masterPortalPath%"
	call:addWebsite "ADX - Community Portal" 7504 "%appPoolName%" "%masterPortalPath%"
	call:addWebsite "ADX - Partner Portal" 7506 "%appPoolName%" "%masterPortalPath%"
	call:addWebsite "ADX - Retail Portal" 7507 "%appPoolName%" "%masterPortalPath%"
endlocal
goto :eof

:addIntranet
setlocal
	call:addAppPool "%appPoolName%"
	if errorlevel 1 goto :eof
	call:addIntranetWebsite "ADX - Intranet" 7509 "%appPoolName%" "%~dp0HowTo\Intranet"
endlocal
goto :eof

:removeSites
setlocal
	call:removeWebsite "ADX - Customer Portal"
	call:removeWebsite "ADX - Company Portal"
	call:removeWebsite "ADX - Conference Portal"
	call:removeWebsite "ADX - Government Portal"
	call:removeWebsite "ADX - Community Portal"
	call:removeWebsite "ADX - Partner Portal"
	call:removeWebsite "ADX - Retail Portal"
	call:removeWebsite "ADX - Intranet"
	call:removeWebsite "Adxstudio Portals"
	call:removeAppPool "%appPoolName%"
endlocal
goto :eof

:addApps
setlocal
	call:addAppPool "%appPoolName%"
	if errorlevel 1 goto :eof
	set siteName=Adxstudio Portals
	call:addWebsite "%siteName%" 7600 "%appPoolName%" "%welcomePortalPath%"
	if errorlevel 1 goto :eof
	call:addApplication "%siteName%" "customer-portal" "%masterPortalPath%"
	call:addApplication "%siteName%" "company-portal" "%masterPortalPath%"
	call:addApplication "%siteName%" "conference-portal" "%masterPortalPath%"
	call:addApplication "%siteName%" "government-portal" "%masterPortalPath%"
	call:addApplication "%siteName%" "community-portal" "%masterPortalPath%"
	call:addApplication "%siteName%" "partner-portal" "%masterPortalPath%"
	call:addApplication "%siteName%" "retail-portal" "%masterPortalPath%"
endlocal
goto :eof

:init
set exec-appcmd=%windir%\system32\inetsrv\appcmd.exe
goto :eof

:addAppPool
:: %~1: application pool name
setlocal
	set name=%~1

	echo Setting up "%name%" application pool
	%exec-appcmd% list apppool /Name:"%name%"
	if not errorlevel 1 goto addAppPoolEnd
	%exec-appcmd% add apppool /Name:"%name%"
	%exec-appcmd% set apppool /apppool.name:"%name%" /managedRuntimeVersion:v4.0
	%exec-appcmd% set apppool /apppool.name:"%name%" /managedPipelineMode:Integrated
	%exec-appcmd% set apppool /apppool.name:"%name%" /processModel.idleTimeout:00:00:00
	%exec-appcmd% set apppool /apppool.name:"%name%" /recycling.periodicRestart.time:00:00:00
	echo Pausing briefly before starting application pool
	timeout 5
	%exec-appcmd% start apppool /apppool.name:"%name%"
	:addAppPoolEnd
endlocal
goto :eof

:removeAppPool
:: %~1: application pool name
setlocal
	set name=%~1

	%exec-appcmd% list apppool /Name:"%name%"
	if errorlevel 1 goto removeAppPoolEnd
	echo Removing "%name%"
	%exec-appcmd% delete apppool "%name%"
	:removeAppPoolEnd
endlocal
goto :eof

:addWebsite
:: %~1: portal name
:: %~2: port
:: %~3: application pool name
:: %~4: physical path
setlocal
	set siteName=%~1
	set port=%~2
	set appPoolName=%~3
	set physicalPath=%~4

	echo Setting up "%siteName%" on port %port% at %physicalPath%
	%exec-appcmd% list site /name:"%siteName%"
	if not errorlevel 1 goto addWebsiteEnd
	%exec-appcmd% add site /name:"%siteName%" /physicalPath:"%physicalPath%" /bindings:http://*:%port% 
	%exec-appcmd% set app /app.name:"%siteName%/" /applicationPool:"%appPoolName%"
	:addWebsiteEnd
endlocal
goto :eof

:addIntranetWebsite
:: %~1: portal name
:: %~2: port
:: %~3: application pool name
:: %~4: physical path
setlocal
	set siteName=%~1
	set port=%~2
	set appPoolName=%~3
	set physicalPath=%~4

	echo Setting up "%siteName%" on port %port% at %physicalPath%
	%exec-appcmd% list site /name:"%siteName%"
	if not errorlevel 1 goto addWebsiteEnd
	%exec-appcmd% add site /name:"%siteName%" /physicalPath:"%physicalPath%" /bindings:http://*:%port% 
	%exec-appcmd% set app /app.name:"%siteName%/" /applicationPool:"%appPoolName%"
	%exec-appcmd% unlock config /section:windowsAuthentication
	%exec-appcmd% set config "%siteName%" /section:windowsAuthentication /enabled:true
	:addIntranetWebsiteEnd
endlocal
goto :eof

:removeWebsite
:: %~1: portal name
setlocal
	set siteName=%~1
	
	%exec-appcmd% list site /name:"%siteName%"
	if errorlevel 1 goto removeWebsiteEnd
	echo Removing "%siteName%"
	%exec-appcmd% delete site "%siteName%"
	:removeWebsiteEnd
endlocal
goto :eof

:addApplication
:: %~1: portal name
:: %~2: application name
:: %~3: physical path
setlocal
	set siteName=%~1
	set appName=%~2
	set physicalPath=%~3

	echo Adding app "%appName%" to site "%siteName%" at %physicalPath%
	%exec-appcmd% list app /site.name:"%siteName%" /path:"/%appName%"
	if not errorlevel 1 goto addApplicationEnd
	%exec-appcmd% add app /site.name:"%siteName%" /path:"/%appName%" /applicationPool:"%appPoolName%" /physicalPath:"%physicalPath%"
	:addApplicationEnd
endlocal
goto :eof
